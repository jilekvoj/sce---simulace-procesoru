`timescale 1ns/10ps
`include "uvm_macros.svh"

import uvm_pkg::*;

class my_env extends uvm_env;

	`uvm_component_utils(my_env)
  
	my_pkg::ral_env m_ral_env;
  
	function new (string name = "my_env", uvm_component parent = null);
		super.new(name, parent);
	endfunction
  
	function void build_phase (uvm_phase phase);
		super.build_phase(phase);
		m_ral_env = my_pkg::ral_env::type_id::create("m_reg_env", this);
		uvm_reg::include_coverage("*", UVM_CVR_ALL);
	endfunction
  
endclass
