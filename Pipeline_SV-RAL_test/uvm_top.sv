`timescale 1ns/10ps
import uvm_pkg::*;

`include "my_pkg.sv"
import my_pkg::*;


module uvm_top;
	bit T_CLK, T_RST;

	always
		#5 T_CLK = ~T_CLK;

	initial begin
		T_RST <= 1'b0;
		T_CLK <= 1'b1;
		#1ns;
		T_RST <= 1'b1;
		#2ns;
		T_RST <= 1'b0;
	end

	MIPSpipeline  _dut(CLK, RESET);

	initial begin
		run_test("my_test");
	end
endmodule

