# SCE - Simulace procesoru

Cílem semináře je praktické seznámení se s pokročilými konstrukcemi jazyka SystemVerilog a knihovny UVM (zejména testování CPU pomocí UVM knihovny a registrového modelu​) a implementace testovacích prostředí pro CPU použité v předmětu BI-APS. Cíle lze rozšířit o testování pokročilejšího typu CPU (s pipeline),
případně o rešerši způsobů testování CPU v praxi.