`timescale 1ns/10ps

import uvm_pkg::*;

`include "my_pkg.sv"
import my_pkg::*;

`include "RegAccessInterface.sv"
`include "MonitorInterface.sv"
`include "TopDUT.sv"

module uvm_top;

	bit T_CLK, T_RST;

	always
		#5 T_CLK = ~T_CLK;

	initial begin
		T_RST <= 1'b0;
		T_CLK <= 1'b1;
		#1 T_RST <= 1'b1;
		#21 T_RST <= 1'b0;
	end


	if_mon inst_if_mon (.clk(T_CLK), .reset(T_RST));
	if_reg_acc inst_if_reg_acc (.clk(T_CLK), .reset(T_RST));
	TopDUT _dut (.CLK(T_CLK), .RESET(T_RST), .if_mon_inst(inst_if_mon.dut), .if_reg_acc_inst(inst_if_reg_acc.dut));

	assign inst_if_mon.tb_op = inst_if_reg_acc.reg_rd_en | inst_if_reg_acc.reg_wr_en | inst_if_reg_acc.PC_rd_en | inst_if_reg_acc.PC_wr_en;

	initial begin
		// save references to DUT interfaces to uvm databese
		uvm_config_db#(virtual if_mon)::set(uvm_root::get(), "*", "inst_if_mon", inst_if_mon);
		uvm_config_db#(virtual if_reg_acc)::set(uvm_root::get(), "*", "inst_if_reg_acc", inst_if_reg_acc);

		run_test("my_test");
	end
endmodule