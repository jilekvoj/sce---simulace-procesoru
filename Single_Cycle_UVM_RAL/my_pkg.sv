package my_pkg;
	parameter MEM_SIZE = 1024;

	`include "my_item.sv"
	`include "my_instruction.sv"
	`include "ral_item.sv"
	`include "ral_regs.sv"
	`include "ral_adapter.sv"
	`include "ral_driver.sv"
	`include "ral_agent.sv"
	`include "ral_env.sv"
	`include "my_scoreboard.sv"
	`include "my_monitor.sv"
	`include "my_env.sv"
	`include "my_test.sv"
endpackage