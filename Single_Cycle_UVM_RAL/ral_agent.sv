`include "uvm_macros.svh"
import uvm_pkg::*;

class ral_agent extends uvm_agent;
	`uvm_component_utils (ral_agent)

	function new (string name = "my_agent", uvm_component parent = null);
		super.new(name, parent);
	endfunction

	my_pkg::ral_driver m_driver;
	uvm_sequencer #(my_pkg::ral_item) m_seq;

	virtual function void build_phase (uvm_phase phase);
		super.build_phase(phase);
		m_driver = my_pkg::ral_driver::type_id::create("m_driver", this);
		m_seq = uvm_sequencer #(my_pkg::ral_item)::type_id::create("m_seq", this);
	endfunction	

	virtual function void connect_phase (uvm_phase phase);
		super.connect_phase(phase);
		m_driver.seq_item_port.connect(m_seq.seq_item_export);
	endfunction
endclass