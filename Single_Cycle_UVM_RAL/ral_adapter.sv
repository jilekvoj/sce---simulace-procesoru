`include "uvm_macros.svh"
import uvm_pkg::*;

class ral_adapter extends uvm_reg_adapter;
	`uvm_object_utils (ral_adapter)
 
	function new (string name = "ral_adapter");
		super.new (name);
	endfunction

	// convert RAL transaction to bus transaction 
   	function uvm_sequence_item reg2bus(const ref uvm_reg_bus_op rw);	
		my_pkg::ral_item tx = my_pkg::ral_item::type_id::create("tx");   
     
		tx.write = (rw.kind == UVM_WRITE);
		tx.addr  = rw.addr;
		if (tx.write)  tx.wdata = rw.data;
		if (!tx.write) tx.rdata = rw.data;
 
		return tx;
	endfunction
 
	// convert bus transaction to RAL transaction 
	function void bus2reg(uvm_sequence_item bus_item, ref uvm_reg_bus_op rw);
		my_pkg::ral_item tx;
     
		assert( $cast(tx, bus_item) )
			else `uvm_fatal("", "A bad thing has just happened in my_adapter")
 
		rw.kind = tx.write ? UVM_WRITE : UVM_READ;
		rw.addr = tx.addr;
		rw.data = tx.rdata;
 
		rw.status = UVM_IS_OK;
	endfunction
endclass