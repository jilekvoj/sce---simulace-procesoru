`timescale 1ns/10ps
`include "uvm_macros.svh"

import uvm_pkg::*;

class my_env extends uvm_env;
	`uvm_component_utils(my_env)
  
	my_pkg::my_monitor inst_monitor;
	my_pkg::my_scoreboard inst_scoreboard;
	my_pkg::ral_env m_ral_env;
	my_pkg::ral_agent m_agent;
  
	function new (string name = "my_env", uvm_component parent = null);
		super.new(name, parent);
	endfunction
  
	function void build_phase (uvm_phase phase);
		super.build_phase(phase);
		inst_monitor = my_pkg::my_monitor::type_id::create("inst_monitor", this);
		inst_scoreboard = my_pkg::my_scoreboard::type_id::create("inst_scoreboard", this);
		m_ral_env = my_pkg::ral_env::type_id::create("m_reg_env", this);
		uvm_reg::include_coverage("*", UVM_CVR_ALL);
		m_agent = my_pkg::ral_agent::type_id::create("m_agent", this); 
	endfunction
  
	virtual function void connect_phase (uvm_phase phase);
		inst_monitor.inst_collected_item_port.connect(inst_scoreboard.inst_out_fifo.analysis_export);
		m_ral_env.m_reg_model.default_map.set_sequencer(m_agent.m_seq, m_ral_env.m_adapter);
	endfunction
endclass