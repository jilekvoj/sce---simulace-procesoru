`timescale 1ns/10ps

`include "uvm_macros.svh"

import uvm_pkg::*;

class my_scoreboard extends uvm_scoreboard;
	`uvm_component_utils(my_scoreboard)
	
	// port for communication with monitor
	uvm_tlm_analysis_fifo #(my_pkg::my_item) inst_out_fifo;
	
	my_pkg::my_reg_model m_reg_model;
	uvm_status_e status;

	function new (string name = "my_scoreboard", uvm_component parent = null);
		super.new(name, parent);
	endfunction

	function void build_phase (uvm_phase phase);
		super.build_phase(phase);
		inst_out_fifo = new("inst_out_fifo", this);
	endfunction

	task run_phase (uvm_phase phase);
		super.run_phase(phase);
		phase.raise_objection(this);

		// get reference to the reg model from uvm database
		if(!uvm_config_db#(my_pkg::my_reg_model)::get(null, "uvm_top", "m_reg_model", m_reg_model))
			`uvm_fatal("NOVIF", {"Missing my_reg_model: ", get_full_name(), ".m_reg_model"})

		scoreboard_match(phase);
	endtask

	task scoreboard_match(uvm_phase phase);
		my_pkg::my_item item;
		uvm_report_server server = get_report_server();
		int instr_cnt = 0;
		forever begin
			inst_out_fifo.get(item);

			// decode instruction
			case(item.instruction[31:26])
				6'b000000: EvalALUOP(item);
				6'b001000: EvalAddi(item);
				6'b100011: EvalLw(item);
				6'b101011: EvalSw(item);
				6'b000100: EvalBeq(item);
				6'b000011: EvalJal(item);
				6'b000111: EvalJr(item);
				6'b011111: EvalAddu(item);
				6'b000010: EvalJ(item);
				default: `uvm_error(get_full_name(), "Bad instruction!")
			endcase
			
			// stop on first error
			/*
			if (server.get_severity_count(UVM_ERROR) != 0)
				$stop(); 
			*/
			
			// coverage sampling
			item.item_cover_instr.sample();
			instr_cnt++;
      
      			if ($get_coverage() > 95) begin
				`uvm_info(get_full_name(), $sformatf("\n\nScoreboard: coverage done -> finishing test.\n\nNumber of instructions: %0d\nNumber of errors: %0d\n", instr_cnt, server.get_severity_count(UVM_ERROR)), UVM_LOW)  
				phase.drop_objection(this);
			end
		end
	endtask

	task EvalALUOP (my_pkg::my_item item);
		logic [31:0] imm, res, PC;
		logic [31:0] reg1, reg2;
		string op;
		string instr;
		// compute correct results
		reg1 = m_reg_model.m_reg_model_gr.my_reg_inst[item.instruction[25:21]].get();
		reg2 = m_reg_model.m_reg_model_gr.my_reg_inst[item.instruction[20:16]].get();
		PC = m_reg_model.m_reg_model_gr.my_PC.get();
		casez(item.instruction[10:0])
			11'b00000100000: begin res = reg1 + reg2; op = "+"; end
			11'b00000100010: begin res = reg1 - reg2; op = "-"; end
			11'b00000100100: begin res = reg1 & reg2; op = "&"; end
			11'b00000100101: begin res = reg1 | reg2; op = "|"; end
			11'b00000101010: begin res = ($signed(reg1) < $signed(reg2)) ? 1 : 0; op = "slt"; end
			11'b?????000100: begin res = reg1 << reg2; op = "<<"; end
			11'b00000000110: begin res = reg1 >> reg2; op = ">>"; end
			11'b00000000111: begin res = $signed(reg1) >> reg2; op = "s>>"; end
			default: begin res = 0; op = "X"; end
		endcase
		PC = PC + 4;
		if (item.instruction[15:11] == 0)
			res = 0;
		// check values in DUT
		instr = $sformatf("ALU OP (%b -> s%0d = s%0d %s s%0d)", item.instruction, item.instruction[15:11], item.instruction[25:11], op, item.instruction[20:16]);
		wait (!m_reg_model.m_reg_model_gr.my_reg_inst[item.instruction[15:11]].is_busy);
		m_reg_model.m_reg_model_gr.my_reg_inst[item.instruction[15:11]].predict(res, -1, UVM_PREDICT_DIRECT, UVM_BACKDOOR);	
		wait (!m_reg_model.m_reg_model_gr.my_PC.is_busy);
		m_reg_model.m_reg_model_gr.my_PC.predict(PC, -1, UVM_PREDICT_DIRECT, UVM_BACKDOOR);
		m_reg_model.m_reg_model_gr.my_reg_inst[item.instruction[15:11]].SetInstr(instr);
		m_reg_model.m_reg_model_gr.my_PC.SetInstr(instr);
		m_reg_model.m_reg_model_gr.my_reg_inst[item.instruction[15:11]].mirror(status, UVM_CHECK, UVM_BACKDOOR);
		m_reg_model.m_reg_model_gr.my_PC.mirror(status, UVM_CHECK, UVM_BACKDOOR);
		m_reg_model.m_reg_model_gr.my_reg_inst[item.instruction[15:11]].poke(status, res);
		m_reg_model.m_reg_model_gr.my_PC.poke(status, PC);
	endtask

	task EvalAddi (my_pkg::my_item item);
		logic [31:0] imm, res, PC;
		logic [31:0] reg1;
		string instr;
		// compute correct results
		reg1 = m_reg_model.m_reg_model_gr.my_reg_inst[item.instruction[25:21]].get();
		PC = m_reg_model.m_reg_model_gr.my_PC.get();
		imm = {{16{item.instruction[15]}}, item.instruction[15:0]};
		res = imm + reg1;
		PC = PC + 4;
		if (item.instruction[20:16] == 0)
			res = 0;
		// check values in DUT
		instr = $sformatf("Addi (%b -> s%0d = s%0d + %0d)", item.instruction, item.instruction[20:16], item.instruction[25:21], $signed(item.instruction[15:0]));
		wait (!m_reg_model.m_reg_model_gr.my_reg_inst[item.instruction[20:16]].is_busy);
		m_reg_model.m_reg_model_gr.my_reg_inst[item.instruction[20:16]].predict(res, -1, UVM_PREDICT_DIRECT, UVM_BACKDOOR);
		wait (!m_reg_model.m_reg_model_gr.my_PC.is_busy);
		m_reg_model.m_reg_model_gr.my_PC.predict(PC, -1, UVM_PREDICT_DIRECT, UVM_BACKDOOR);
		m_reg_model.m_reg_model_gr.my_reg_inst[item.instruction[20:16]].SetInstr(instr);
		m_reg_model.m_reg_model_gr.my_PC.SetInstr(instr);
		m_reg_model.m_reg_model_gr.my_reg_inst[item.instruction[20:16]].mirror(status, UVM_CHECK, UVM_BACKDOOR);
		m_reg_model.m_reg_model_gr.my_PC.mirror(status, UVM_CHECK, UVM_BACKDOOR);
		m_reg_model.m_reg_model_gr.my_reg_inst[item.instruction[20:16]].poke(status, res);
		m_reg_model.m_reg_model_gr.my_PC.poke(status, PC);
	endtask

	task EvalLw (my_pkg::my_item item);
		logic [31:0] imm, res, PC;
		logic [31:0] reg1, reg2, mem1;
		string instr;
		// compute correct results
		reg1 = m_reg_model.m_reg_model_gr.my_reg_inst[item.instruction[25:21]].get();
		reg2 = m_reg_model.m_reg_model_gr.my_reg_inst[item.instruction[20:16]].get();
		PC = m_reg_model.m_reg_model_gr.my_PC.get();
		imm = {{16{item.instruction[15]}}, item.instruction[15:0]};
		res = imm + reg1;
		PC = PC + 4;
		res = res % my_pkg::MEM_SIZE;
		m_reg_model.m_data_mem_gr.my_mem_cell[res].peek(status, mem1);
		if (item.instruction[20:16] == 0)
			mem1 = 0;
		// check values in DUT
		instr = $sformatf("Lw (%b -> s%0d = Mem[s%0d + %0d])", item.instruction, item.instruction[20:16], item.instruction[25:21], $signed(item.instruction[15:0]));
		wait (!m_reg_model.m_reg_model_gr.my_reg_inst[item.instruction[20:16]].is_busy);
		m_reg_model.m_reg_model_gr.my_reg_inst[item.instruction[20:16]].predict(mem1, -1, UVM_PREDICT_DIRECT, UVM_BACKDOOR);	
		wait (!m_reg_model.m_reg_model_gr.my_PC.is_busy);
		m_reg_model.m_reg_model_gr.my_PC.predict(PC, -1, UVM_PREDICT_DIRECT, UVM_BACKDOOR);
		m_reg_model.m_reg_model_gr.my_reg_inst[item.instruction[20:16]].SetInstr(instr);
		m_reg_model.m_reg_model_gr.my_PC.SetInstr(instr);
		m_reg_model.m_reg_model_gr.my_reg_inst[item.instruction[20:16]].mirror(status, UVM_CHECK, UVM_BACKDOOR);
		m_reg_model.m_reg_model_gr.my_PC.mirror(status, UVM_CHECK, UVM_BACKDOOR);
		m_reg_model.m_reg_model_gr.my_reg_inst[item.instruction[20:16]].poke(status, mem1);
		m_reg_model.m_reg_model_gr.my_PC.poke(status, PC);
	endtask

	task EvalSw (my_pkg::my_item item);
		logic [31:0] imm, res, PC;
		logic [31:0] reg1, reg2;
		string instr;
		// compute correct results
		reg1 = m_reg_model.m_reg_model_gr.my_reg_inst[item.instruction[25:21]].get();
		reg2 = m_reg_model.m_reg_model_gr.my_reg_inst[item.instruction[20:16]].get();
		PC = m_reg_model.m_reg_model_gr.my_PC.get();
		imm = {{16{item.instruction[15]}}, item.instruction[15:0]};
		res = imm + reg1;
		PC = PC + 4;
		res = res % my_pkg::MEM_SIZE;
		// check values in DUT
		instr = $sformatf("Sw (%b -> Mem[s%0d + %0d] = s%0d)", item.instruction, item.instruction[25:21], $signed(item.instruction[15:0]), item.instruction[20:16]);
		m_reg_model.m_data_mem_gr.my_mem_cell[res].predict(reg2, -1, UVM_PREDICT_DIRECT, UVM_BACKDOOR);
		wait (!m_reg_model.m_reg_model_gr.my_PC.is_busy);
		m_reg_model.m_reg_model_gr.my_PC.predict(PC, -1, UVM_PREDICT_DIRECT, UVM_BACKDOOR);
		m_reg_model.m_data_mem_gr.my_mem_cell[res].SetInstr(instr);
		m_reg_model.m_reg_model_gr.my_PC.SetInstr(instr);
		m_reg_model.m_data_mem_gr.my_mem_cell[res].mirror(status, UVM_CHECK, UVM_BACKDOOR);
		m_reg_model.m_reg_model_gr.my_PC.mirror(status, UVM_CHECK, UVM_BACKDOOR);
		m_reg_model.m_data_mem_gr.my_mem_cell[res].poke(status, reg1);
		m_reg_model.m_reg_model_gr.my_PC.poke(status, PC);
	endtask

	task EvalBeq (my_pkg::my_item item);
		logic [31:0] imm, res, PC;
		logic [31:0] reg1, reg2;
		string instr;
		// compute correct results
		reg1 = m_reg_model.m_reg_model_gr.my_reg_inst[item.instruction[25:21]].get();
		reg2 = m_reg_model.m_reg_model_gr.my_reg_inst[item.instruction[20:16]].get();
		PC = m_reg_model.m_reg_model_gr.my_PC.get();
		imm = {{16{item.instruction[15]}}, item.instruction[15:0]};
		res = imm << 2;
		PC = PC + 4;
		if (reg1 == reg2)
			PC = PC + res;
		// check values in DUT
		instr = $sformatf("Beq (%b -> if s%0d == s%0d then PC = PC + 4 + %d, %0d)", item.instruction,item.instruction[25:21], item.instruction[20:16], $signed(item.instruction[15:0]), (reg1 == reg2));
		wait (!m_reg_model.m_reg_model_gr.my_PC.is_busy);
		m_reg_model.m_reg_model_gr.my_PC.predict(PC, -1, UVM_PREDICT_DIRECT, UVM_BACKDOOR);
		m_reg_model.m_reg_model_gr.my_PC.SetInstr(instr);
		m_reg_model.m_reg_model_gr.my_PC.mirror(status, UVM_CHECK, UVM_BACKDOOR);
		m_reg_model.m_reg_model_gr.my_PC.poke(status, PC);
	endtask

	task EvalJal (my_pkg::my_item item);
		logic [31:0] imm, res, PC;
		string instr;
		// compute correct results
		PC = m_reg_model.m_reg_model_gr.my_PC.get();
		res = PC + 4;
		PC = {PC[31:28], item.instruction[25:0], 2'b00};
		// check values in DUT
		instr = $sformatf("Jal (%b -> PC = (PC & 0xf0000000) | %b00)", item.instruction, item.instruction[25:0]);
		wait (!m_reg_model.m_reg_model_gr.my_PC.is_busy);
		m_reg_model.m_reg_model_gr.my_PC.predict(PC, -1, UVM_PREDICT_DIRECT, UVM_BACKDOOR);
		m_reg_model.m_reg_model_gr.my_reg_inst[31].predict(res, -1, UVM_PREDICT_DIRECT, UVM_BACKDOOR);
		m_reg_model.m_reg_model_gr.my_reg_inst[31].SetInstr(instr);
		m_reg_model.m_reg_model_gr.my_PC.SetInstr(instr);
		m_reg_model.m_reg_model_gr.my_reg_inst[31].mirror(status, UVM_CHECK, UVM_BACKDOOR);
		m_reg_model.m_reg_model_gr.my_PC.mirror(status, UVM_CHECK, UVM_BACKDOOR);		
		m_reg_model.m_reg_model_gr.my_reg_inst[31].poke(status, res);
		m_reg_model.m_reg_model_gr.my_PC.poke(status, PC);
	endtask

	task EvalJr (my_pkg::my_item item);
		logic [31:0] PC = m_reg_model.m_reg_model_gr.my_reg_inst[item.instruction[25:21]].get();
		string instr = $sformatf("Jr (%b -> PC = s%0d)", item.instruction, item.instruction[25:21]);
		// check values in DUT
		wait (!m_reg_model.m_reg_model_gr.my_PC.is_busy);
		m_reg_model.m_reg_model_gr.my_PC.predict(PC, -1, UVM_PREDICT_DIRECT, UVM_BACKDOOR);
		m_reg_model.m_reg_model_gr.my_PC.SetInstr(instr);
		m_reg_model.m_reg_model_gr.my_PC.mirror(status, UVM_CHECK, UVM_BACKDOOR);	
		m_reg_model.m_reg_model_gr.my_PC.poke(status, PC);
	endtask

	task EvalAddu (my_pkg::my_item item);
		logic [31:0] imm, res, PC;
		logic [8:0] subres [3:0];
		logic [31:0] reg1, reg2;
		string op;
		string instr;
		// compute correct results
		reg1 = m_reg_model.m_reg_model_gr.my_reg_inst[item.instruction[25:21]].get();
		reg2 = m_reg_model.m_reg_model_gr.my_reg_inst[item.instruction[20:16]].get();
		PC = m_reg_model.m_reg_model_gr.my_PC.get();
		PC = PC + 4;
		subres[0] = {1'b0, reg1[7:0]} + {1'b0, reg2[7:0]};
		subres[1] = {1'b0, reg1[15:8]} + {1'b0, reg2[15:8]};
		subres[2] = {1'b0, reg1[23:16]} + {1'b0, reg2[23:16]};
		subres[3] = {1'b0, reg1[31:24]} + {1'b0, reg2[31:24]};
		if (item.instruction[8]) begin
			op = "s";
			if (subres[0][8]) subres[0] = 9'b111111111;
			if (subres[1][8]) subres[1] = 9'b111111111;
			if (subres[2][8]) subres[2] = 9'b111111111;
			if (subres[3][8]) subres[3] = 9'b111111111;
		end
		res = {subres[3][7:0], subres[2][7:0], subres[1][7:0], subres[0][7:0]};
		if (item.instruction[15:11] == 0)
			res = 0;
		// check values in DUT
		instr = $sformatf("Addu%s (%b -> s%0d = s%0d + s%0d)", op, item.instruction, item.instruction[15:11], item.instruction[25:11], item.instruction[20:16]);
		wait (!m_reg_model.m_reg_model_gr.my_reg_inst[item.instruction[15:11]].is_busy);
		m_reg_model.m_reg_model_gr.my_reg_inst[item.instruction[15:11]].predict(res, -1, UVM_PREDICT_DIRECT, UVM_BACKDOOR);
		wait (!m_reg_model.m_reg_model_gr.my_PC.is_busy);
		m_reg_model.m_reg_model_gr.my_PC.predict(PC, -1, UVM_PREDICT_DIRECT, UVM_BACKDOOR);
		m_reg_model.m_reg_model_gr.my_reg_inst[item.instruction[15:11]].SetInstr(instr);
		m_reg_model.m_reg_model_gr.my_PC.SetInstr(instr);
		m_reg_model.m_reg_model_gr.my_reg_inst[item.instruction[15:11]].mirror(status, UVM_CHECK, UVM_BACKDOOR);
		m_reg_model.m_reg_model_gr.my_PC.mirror(status, UVM_CHECK, UVM_BACKDOOR);	
		m_reg_model.m_reg_model_gr.my_reg_inst[item.instruction[15:11]].poke(status, res);
		m_reg_model.m_reg_model_gr.my_PC.poke(status, PC);
	endtask

	task EvalJ (my_pkg::my_item item);
		logic [31:0] imm, res, PC;
		string instr;
		// compute correct results
		PC = m_reg_model.m_reg_model_gr.my_PC.get();
		PC = {PC[31:28], item.instruction[25:0], 2'b00};
		// check values in DUT
		instr = $sformatf("J (%b -> PC = (PC & 0xf0000000) | %b00)", item.instruction, item.instruction[25:0]);
		wait (!m_reg_model.m_reg_model_gr.my_PC.is_busy);
		m_reg_model.m_reg_model_gr.my_PC.predict(PC, -1, UVM_PREDICT_DIRECT, UVM_BACKDOOR);
		m_reg_model.m_reg_model_gr.my_PC.SetInstr(instr);
		m_reg_model.m_reg_model_gr.my_PC.mirror(status, UVM_CHECK, UVM_BACKDOOR);	
		m_reg_model.m_reg_model_gr.my_PC.poke(status, PC);
	endtask
endclass