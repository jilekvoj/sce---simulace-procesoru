`include "uvm_macros.svh"
import uvm_pkg::*;

class ral_driver extends uvm_driver #(my_pkg::ral_item);
	`uvm_component_utils (ral_driver)
	
	my_pkg::ral_item m_item;
	virtual if_reg_acc m_if;

	function new (string name = "m_driver", uvm_component parent = null);
		super.new(name, parent);
	endfunction

	virtual function void build_phase (uvm_phase phase);
		super.build_phase (phase);
		// get reference to a DUT register access interface
		if(!uvm_config_db#(virtual if_reg_acc)::get(null, "", "inst_if_reg_acc", m_if))
			`uvm_fatal("NOVIF", {"Missing if_reg_acc: ", get_full_name(), ".m_if"})
	endfunction

	virtual task run_phase (uvm_phase phase);
		m_if.reg_addr = 0;
		m_if.reg_data_wr = 0;
		m_if.PC_wr = 0;
		m_if.PC_wr_en = 1'b0;
		m_if.PC_rd_en = 1'b0;
		m_if.reg_wr_en = 1'b0;
		m_if.reg_rd_en = 1'b0;	

		forever begin
			seq_item_port.get_next_item(m_item);
			if (m_item.write) begin
				if (m_item.addr == 128) begin
					m_if.PC_wr_en = 1'b1;
					m_if.PC_wr = m_item.wdata;
					@(posedge m_if.clk);
					m_if.PC_wr_en = 1'b0;
				end
				else begin
					m_if.reg_wr_en = 1'b1;
					m_if.reg_data_wr = m_item.wdata;
					m_if.reg_addr = m_item.addr >> 2;
					@(posedge m_if.clk);
					m_if.reg_wr_en = 1'b0;
				end
			end
			else begin
				if (m_item.addr == 128) begin
					m_if.PC_rd_en = 1'b1;
					m_item.rdata = m_if.PC_rd;
					m_if.PC_rd_en = 1'b0;
				end
				else begin
					m_if.reg_rd_en = 1'b1;
					m_if.reg_addr = m_item.addr >> 2;
					#1ns;
					m_item.rdata = m_if.reg_data_rd;
					m_if.reg_rd_en = 1'b0;
				end
			end
			seq_item_port.item_done();
		end
	endtask
endclass