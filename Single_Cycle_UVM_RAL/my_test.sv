`timescale 1ns/10ps

`include "uvm_macros.svh"

// import knihovny UVM
import uvm_pkg::*;

class my_test extends uvm_test;
	`uvm_component_utils(my_test)
  
	my_pkg::my_env env;
	virtual if_mon inst_if_mon;
	my_pkg::my_reg_model m_reg_model;
	uvm_status_e status;
	int ErrrosGenerated = 0;
  
	function new (string name = "my_test", uvm_component parent = null);
		super.new(name, parent);
	endfunction
  
	virtual function void build_phase (uvm_phase phase);
		super.build_phase(phase);
		env = my_pkg::my_env::type_id::create("env", this);

		// get reference to a DUT input interface
		if(!uvm_config_db#(virtual if_mon)::get(this, "", "inst_if_mon", inst_if_mon))
			`uvm_fatal("NOVIF", {"Missing inst_if_mon: ", get_full_name(), ".inst_if_mon"})
	endfunction
  
	task run_phase (uvm_phase phase);
		uvm_objection objection;
		int rdata;
		super.run_phase(phase);

		phase.phase_done.set_drain_time(this, 50ns);

		if(!uvm_config_db#(my_pkg::my_reg_model)::get(null, "uvm_top", "m_reg_model", m_reg_model))
			`uvm_fatal("NOVIF", {"Missing m_reg_model: ", get_full_name(), ".m_reg_model"})

		// separate test for registers
		TestRegs();

		// inject errors to DUT
		/*
		fork
			DoSomethingBad();
		join_none
		*/

		forever begin
				InitMem();
				m_reg_model.m_reg_model_gr.my_PC.write(status, 0);
				`uvm_info(get_type_name(), "Mem initialized - starting test\n\n", UVM_LOW)
				for (int i = 0; i < my_pkg::MEM_SIZE << 1; i++)
					@(posedge inst_if_mon.clk);
				`uvm_info(get_type_name(), "Test step done\n\n", UVM_LOW)
				// "harmless" delay - allowing scoreboard to finish everything before next cycle
				m_reg_model.m_reg_model_gr.my_reg_inst[0].write(status, 0);
				m_reg_model.m_reg_model_gr.my_reg_inst[0].write(status, 0);
				m_reg_model.m_reg_model_gr.my_reg_inst[1].peek(status, rdata);
				m_reg_model.m_reg_model_gr.my_reg_inst[1].write(status, rdata);
		end
	endtask

	virtual function void report_phase (uvm_phase phase);
		`uvm_info(get_type_name(), $sformatf("Errors generated: %d", ErrrosGenerated), UVM_LOW)
	endfunction

	task DoSomethingBad;
		logic [31:0] PC, Instr, RegVal, PCval, MemVal, RegAddr, error;
		string msg = "";
		int rnderror;
		forever begin
			// random delay
			int rndtime = ($random() % 50) * 10;
			for (int i = 0; i < rndtime; i++)
				@(posedge inst_if_mon.clk);
			msg = "Inserting error after ";
			@(posedge inst_if_mon.clk);
			if (inst_if_mon.tb_op) continue;
			// insert error after posedge/negedge CLK
			if ($random() % 2) begin
				msg = {msg, "posedge clk"};
			end
			else begin
				@(negedge inst_if_mon.clk);
				msg = {msg, "negedge clk"};
			end
			# 1ns
			rnderror = $random() % 3;
			// insert error to instruction memory
			if (rnderror == 0) begin
				string strerr;
				m_reg_model.m_reg_model_gr.my_PC.peek(status, PC);
				m_reg_model.m_instr_mem_gr.my_mem_cell[PC % my_pkg::MEM_SIZE].peek(status, MemVal);
				error = 1 << ($random() % 32);
				MemVal = MemVal ^ error;
				m_reg_model.m_instr_mem_gr.my_mem_cell[PC % my_pkg::MEM_SIZE].poke(status, MemVal);
				strerr.bintoa(error);
				msg = {msg, " into instruction memory - error vector: ", strerr};
			end
			// insert error to PC
			else if (rnderror == 1) begin
				string strerr;
				m_reg_model.m_reg_model_gr.my_PC.peek(status, PCval);
				error = 1 << ($random() % 32);
				PCval = PCval ^ error;
				m_reg_model.m_reg_model_gr.my_PC.poke(status, PCval);
				strerr.bintoa(error);
				msg = {msg, " into PC - error vector: ", strerr};
			end
			// insert error to GPR reg
			else begin
				string strerr;
				m_reg_model.m_reg_model_gr.my_PC.peek(status, PC);
				`uvm_info(get_type_name(), $sformatf("%d", PC % my_pkg::MEM_SIZE), UVM_LOW)
				m_reg_model.m_instr_mem_gr.my_mem_cell[PC % my_pkg::MEM_SIZE].peek(status, Instr);
				if (Instr[31:26] == 0) begin
					RegAddr = Instr[15:11];
				end
				else if ((Instr[31:26] == 8) || (Instr[31:26] == 35)) begin
					RegAddr = Instr[20:16];
				end
				else begin
					continue;
				end
				m_reg_model.m_reg_model_gr.my_reg_inst[RegAddr].peek(status, RegVal);
				error = 1 << ($random() % 32);
				RegVal = RegVal ^ error;
				m_reg_model.m_reg_model_gr.my_reg_inst[RegAddr].poke(status, RegVal);
				strerr.bintoa(error);
				msg = {msg, " into target register - error vector: ", strerr};
			end
			`uvm_info(get_type_name(), $sformatf("%s", msg), UVM_LOW)
			ErrrosGenerated = ErrrosGenerated + 1;
		end
	endtask

	task TestRegs;
		int rdata;
		// testing random values
		for (int i = 0; i < 1000; i++) begin
			int addr = $urandom() % 32;
			int data = $urandom();
		
			m_reg_model.m_reg_model_gr.my_reg_inst[addr].write(status, data);
			if (addr == 0)
				data = 0;
			m_reg_model.m_reg_model_gr.my_reg_inst[addr].read(status, rdata);
			if (rdata != data)
				`uvm_error(get_full_name(), $sformatf("Reg test failed (frontdoor) - reg s%d: expected: %d, got: %d", addr, data, rdata))
			m_reg_model.m_reg_model_gr.my_reg_inst[addr].read(status, rdata, UVM_BACKDOOR);
			if (rdata != data)
				`uvm_error(get_full_name(), $sformatf("Reg test failed (backdoor) - reg s%d: expected: %d, got: %d", addr, data, rdata))

		end
		// init all registres
		for (int i = 0; i < 32; i++) begin
			m_reg_model.m_reg_model_gr.my_reg_inst[i].write(status, i);
		end
		
		`uvm_info(get_type_name(), "Reg test done", UVM_LOW)
	endtask

	task InitMem;
		bit res;
		my_pkg::my_instruction instr = my_pkg::my_instruction::type_id::create();
		for (int i = 0; i < my_pkg::MEM_SIZE; i++) begin
			m_reg_model.m_data_mem_gr.my_mem_cell[i].write(status, $urandom(), UVM_BACKDOOR);
			res = instr.randomize();
			instr.item_cover_instr.sample();
			m_reg_model.m_instr_mem_gr.my_mem_cell[i].write(status, instr.instruction, UVM_BACKDOOR);
		end
	endtask

	task ReadMem;
		int data;
		for (int i = 0; i < 10; i++) begin
			m_reg_model.m_data_mem_gr.my_mem_cell[i].read(status, data, UVM_BACKDOOR);
			`uvm_info(get_type_name(), $sformatf("DMem[%d] = %b", i, data), UVM_LOW)

			m_reg_model.m_instr_mem_gr.my_mem_cell[i].read(status, data, UVM_BACKDOOR);
			`uvm_info(get_type_name(), $sformatf("IMem[%d] = %b", i, data), UVM_LOW)
		end
	endtask
endclass