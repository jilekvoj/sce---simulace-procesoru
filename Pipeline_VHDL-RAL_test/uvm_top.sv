`timescale 1ns/10ps

// import knihovny UVM
import uvm_pkg::*;

`include "my_pkg.sv"
import my_pkg::*;


module uvm_top;

	// deklarace clk a reset
	bit T_CLK, T_RST, T_TRAP_DT; // lze pouzit logic, ale zbytecne

	// generovani hodin
	always
		#5 T_CLK = ~T_CLK;

	// generovani resetu (muze byt soucasti sekvenci)
	initial begin
		T_RST <= 1'b0;
		T_CLK <= 1'b1;
		#1ns;
		T_RST <= 1'b1;
		#2ns;
		T_RST <= 1'b0;
	end

	// instance DUT
	Celk_Pohled _dut (.Clk(T_CLK), .Reset(T_RST), .Trap_Detected(T_TRAP_DT));

	// preposlani referenci na rozhrani do testovaciho prostredi
	initial begin
		run_test("my_test");
	end
endmodule

