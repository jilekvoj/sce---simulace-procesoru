`include "uvm_macros.svh"
import uvm_pkg::*;

class my_reg extends uvm_reg;
	`uvm_object_utils(my_reg)
    
	rand uvm_reg_field data;

	function new (string name = "intr");
		super.new(name, 32, UVM_NO_COVERAGE);
	endfunction
 
	function void build;
		data = uvm_reg_field::type_id::create("data");  
		data.configure(this, 32, 0, "RW", 0, 0, 0, 1, 0);
    endfunction
endclass

class my_GPR_gr extends uvm_reg_block;
	`uvm_object_utils(my_GPR_gr)

	rand my_reg  my_GPR[31:0]; 

	function new (string name = "");
		super.new(name, build_coverage(UVM_NO_COVERAGE));
	endfunction
 
	function void build;
		default_map = create_map("my_map", 0, 128, UVM_LITTLE_ENDIAN);
		for (int i = 0; i < 32; i++) begin
			my_GPR[i] = my_reg::type_id::create($sformatf("my_my_GPR%d", i));
			my_GPR[i].build();
			my_GPR[i].configure(this);
			my_GPR[i].add_hdl_path_slice($sformatf("Dlx_Inst.IntDlx_Inst.Registers_Inst.reg_file(%0d)", i), 0, my_GPR[i].get_n_bits());
			default_map.add_reg(my_GPR[i], 4 * i, "RW");
		end
		add_hdl_path("_dut");
	endfunction
endclass

class my_reg_model extends uvm_reg_block;
	`uvm_object_utils(my_reg_model)

	rand my_GPR_gr m_GPR_gr;
	
	function new (string name = "");
		super.new(name);
	endfunction

	function void build;
		default_map = create_map("", 0, 128, UVM_LITTLE_ENDIAN);		
	
		m_GPR_gr = my_GPR_gr::type_id::create("m_GPR_gr",,get_full_name());
		m_GPR_gr.configure(this, "_dut");	
		m_GPR_gr.build();

		add_hdl_path("uvm_top");
		default_map.add_submap(m_GPR_gr.default_map, 0);
	endfunction
endclass
