`timescale 1ns/10ps

`include "uvm_macros.svh"

import uvm_pkg::*;

class my_scoreboard extends uvm_scoreboard;

	`uvm_component_utils(my_scoreboard)
	
	my_pkg::my_reg_model m_reg_model;
	uvm_status_e status;

	function new (string name = "my_scoreboard", uvm_component parent = null);
		super.new(name, parent);
	endfunction

	function void build_phase (uvm_phase phase);
		super.build_phase(phase);
	endfunction

	// faze run - vlakno, ktere bude vybirat data z jednotlivych monitoru
	task run_phase (uvm_phase phase);
		super.run_phase(phase);
    
		// zamezeni ukonceni testu, dokud neni splneno pokryti
		phase.raise_objection(this);

		if(!uvm_config_db#(my_pkg::my_reg_model)::get(null, "uvm_top", "m_reg_model", m_reg_model))
			`uvm_fatal("NOVIF", {"Missing my_reg_model: ", get_full_name(), ".m_reg_model"})

		scoreboard_match(phase);
	endtask

	task scoreboard_match(uvm_phase phase);

		uvm_report_server server = get_report_server();
		forever begin
			#1ns;
			/*
			if (server.get_severity_count(UVM_ERROR) != 0)
				$stop();
			*/
      
      			// dosazeni pozadovaneho pokryti
      			/*if ($get_coverage() > 95) begin
				`uvm_info(get_full_name(), $sformatf("\n\nScoreboard: coverage done -> finishing test.\n\n"), UVM_LOW)  
				phase.drop_objection(this);
			end*/
		end
	endtask

	// funkce, do ktere lze vlozit vypis pokryti (i jinou zpravu) po skonceni testu
	function void report_phase(uvm_phase phase);
		`uvm_info(get_full_name(), $sformatf("Dopadlo to vyborne, jsem jen trochu v soku..."), UVM_LOW)
	endfunction
endclass
