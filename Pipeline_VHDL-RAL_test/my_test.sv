`timescale 1ns/10ps

`include "uvm_macros.svh"

import uvm_pkg::*;

class my_test extends uvm_test;

	`uvm_component_utils(my_test)
  
	my_pkg::my_env env;

	my_pkg::my_reg_model m_reg_model;
	uvm_status_e status;
  
	// konstruktor
	function new (string name = "my_test", uvm_component parent = null);
		super.new(name, parent);
	endfunction
  
	// faze build
	virtual function void build_phase (uvm_phase phase);
		super.build_phase(phase);
		// vytvoreni prostredi a sekvence
		env = my_pkg::my_env::type_id::create("env", this); // konstruktory komponent potrebuji referenci nadrazene komponenty
	endfunction
  
	// faze run
	task run_phase (uvm_phase phase);
		uvm_objection objection;
		int rdata;
		super.run_phase(phase);

		// moznost nastaveni casu, ktery dobehne jeste potom, co vsechny komponenty stahly sve namitky (tj. umoznily konec testu)
		phase.phase_done.set_drain_time(this, 50ns);

		if(!uvm_config_db#(my_pkg::my_reg_model)::get(null, "uvm_top", "m_reg_model", m_reg_model))
			`uvm_fatal("NOVIF", {"Missing m_reg_model: ", get_full_name(), ".m_reg_model"})


		#20ns;

		m_reg_model.m_GPR_gr.my_GPR[5].write(status, 5, UVM_BACKDOOR);
		#1ns;
		m_reg_model.m_GPR_gr.my_GPR[5].read(status, rdata, UVM_BACKDOOR);

		if (rdata == 5) begin
			`uvm_info(get_type_name(), $sformatf("%d -> ok", rdata), UVM_LOW);
		end
		else
			`uvm_error(get_type_name(), $sformatf("%d -> error", rdata));

		m_reg_model.m_GPR_gr.my_GPR[5].write(status, 7, UVM_BACKDOOR);
		#1ns;
		m_reg_model.m_GPR_gr.my_GPR[5].read(status, rdata, UVM_BACKDOOR);

		if (rdata == 7) begin
			`uvm_info(get_type_name(), $sformatf("%d -> ok", rdata), UVM_LOW);
		end
		else
			`uvm_error(get_type_name(), $sformatf("%d -> error", rdata));

		m_reg_model.m_GPR_gr.my_GPR[15].write(status, 10, UVM_BACKDOOR);
		#1ns;
		m_reg_model.m_GPR_gr.my_GPR[15].read(status, rdata, UVM_BACKDOOR);

		if (rdata == 10) begin
			`uvm_info(get_type_name(), $sformatf("%d -> ok", rdata), UVM_LOW);
		end
		else
			`uvm_error(get_type_name(), $sformatf("%d -> error", rdata));

		$stop();

	endtask
endclass
