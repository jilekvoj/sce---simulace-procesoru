`timescale 1ns/10ps

`include "uvm_macros.svh"

// import knihovny UVM
import uvm_pkg::*;

class my_test extends uvm_test;
	`uvm_component_utils(my_test)
  
	my_pkg::my_env env;
	my_pkg::my_sequence seq;
  
	function new (string name = "my_test", uvm_component parent = null);
		super.new(name, parent);
	endfunction
  
	virtual function void build_phase (uvm_phase phase);
		super.build_phase(phase);
		env = my_pkg::my_env::type_id::create("env", this);
	endfunction
  
	// faze run
	task run_phase (uvm_phase phase);
		uvm_objection objection;
		super.run_phase(phase);

		// endless loop - generating new sequences
		// simulation is stopped by scoreboard on reaching desired coverage 
		forever begin
			seq = my_pkg::my_sequence::type_id::create("seq", this);
			seq.start(env.inst_sequencer);
		end
	endtask
endclass