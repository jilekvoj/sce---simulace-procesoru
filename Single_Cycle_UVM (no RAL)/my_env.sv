`timescale 1ns/10ps

`include "uvm_macros.svh"

import uvm_pkg::*;

class my_env extends uvm_env;
	`uvm_component_utils(my_env)
  
	my_pkg::my_driver inst_driver;
	my_pkg::my_in_monitor inst_in_monitor;
	my_pkg::my_monitor inst_monitor;
	my_pkg::my_scoreboard inst_scoreboard;
	my_pkg::my_sequencer inst_sequencer;
  
	function new (string name = "my_env", uvm_component parent = null);
		super.new(name, parent);
	endfunction
  
	function void build_phase (uvm_phase phase);
		super.build_phase(phase);
		inst_driver = my_pkg::my_driver::type_id::create("inst_driver", this);
		inst_monitor = my_pkg::my_monitor::type_id::create("inst_monitor", this);
		inst_in_monitor = my_pkg::my_in_monitor::type_id::create("inst_in_monitor", this);
		inst_scoreboard = my_pkg::my_scoreboard::type_id::create("inst_scoreboard", this);
		inst_sequencer = my_pkg::my_sequencer::type_id::create("inst_sequencer", this);
	endfunction
  
	virtual function void connect_phase (uvm_phase phase);
		inst_monitor.inst_collected_item_port.connect(inst_scoreboard.inst_out_fifo.analysis_export);
		inst_in_monitor.inst_collected_item_port.connect(inst_scoreboard.inst_in_fifo.analysis_export);
		inst_driver.seq_item_port.connect(inst_sequencer.seq_item_export);
	endfunction
endclass