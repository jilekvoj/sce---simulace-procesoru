`timescale 1ns/10ps

`include "uvm_macros.svh"

import uvm_pkg::*;

class my_driver extends uvm_driver #(my_pkg::my_item);
	`uvm_component_utils(my_driver)
  
	virtual if_in inst_if_in;
  
	function new (string name = "my_driver", uvm_component parent = null);
		super.new(name, parent);
	endfunction
  
	virtual function void build_phase (uvm_phase phase);
		super.build_phase(phase);
		// get reference for a DUT input interface
		if (!uvm_config_db#(virtual if_in)::get(this, "", "inst_if_in", inst_if_in))
			`uvm_fatal("NOVIF", {"Missing inst_if_in: ", get_full_name(), ".inst_if_in"})
	endfunction
  
	task run_phase (uvm_phase phase);
		super.run_phase(phase);
	
		inst_if_in.instruction <= {32{1'bz}};
		inst_if_in.mem_data_out <= {32{1'bz}};

		// wait for reset
		@(posedge inst_if_in.reset);

		forever begin
			while(inst_if_in.reset !== 1'b0) begin
				// hold default values during reset
				inst_if_in.instruction <= {32{1'bz}};
				inst_if_in.mem_data_out <= {32{1'bz}};
				@(posedge inst_if_in.clk);
			end
			seq_item_port.get_next_item(req);
			inst_if_in.instruction <= req.instruction;
			inst_if_in.mem_data_out <= req.data_from_mem;
			seq_item_port.item_done();
			@(posedge inst_if_in.clk);
		end
	endtask
endclass