`timescale 1ns/10ps

`include "uvm_macros.svh"

// import knihovny UVM
import uvm_pkg::*;

class my_scoreboard extends uvm_scoreboard;
	`uvm_component_utils(my_scoreboard)

	// ports for communication with monitors
	uvm_tlm_analysis_fifo #(my_pkg::my_item) inst_in_fifo;
	uvm_tlm_analysis_fifo #(my_pkg::my_item) inst_out_fifo;
	// simulation copy of GPR registers
	logic [31:0] Registers [31:0];
	// simulation copy of PC
	logic [31:0] PC;
	
	function new (string name = "my_scoreboard", uvm_component parent = null);
		super.new(name, parent);
	endfunction

	function void build_phase (uvm_phase phase);
		super.build_phase(phase);
		inst_in_fifo = new("inst_in_fifo", this);
		inst_out_fifo = new("inst_out_fifo", this);
	endfunction

	task run_phase (uvm_phase phase);
		super.run_phase(phase);
		phase.raise_objection(this);
   		// init registers
		PC = 0;
		Registers[0] = 0;
		scoreboard_match(phase);
	endtask

	task scoreboard_match(uvm_phase phase);
		my_pkg::my_item in_item;
		my_pkg::my_item out_item;
		uvm_report_server server = get_report_server();
		int instr_cnt = 0;

		forever begin
			inst_out_fifo.get(out_item);
			inst_in_fifo.get(in_item);

			// decode instruction
			case(in_item.instruction[31:26])
				6'b000000: EvalALUOP(in_item, out_item);
				6'b001000: EvalAddi(in_item, out_item);
				6'b100011: EvalLw(in_item, out_item);
				6'b101011: EvalSw(in_item, out_item);
				6'b000100: EvalBeq(in_item, out_item);
				6'b000011: EvalJal(in_item, out_item);
				6'b000111: EvalJr(in_item, out_item);
				6'b011111: EvalAddu(in_item, out_item);
				6'b000010: EvalJ(in_item, out_item);
				default: `uvm_error(get_full_name(), "Bad instruction!")
			endcase
			
			Registers[0] = 0;

			// coverage sampling
			in_item.item_cover_instr.sample();
			instr_cnt++;
      
      			if ($get_coverage() > 95) begin
				`uvm_info(get_full_name(), $sformatf("\n\nScoreboard: coverage done -> finishing test.\n\nNumber of instructions: %0d\nNumber of errors: %0d\n", instr_cnt, server.get_severity_count(UVM_ERROR)), UVM_LOW)  
				phase.drop_objection(this);
			end
		end
	endtask

	// instructions executed in ALU
	task EvalALUOP (my_pkg::my_item in_item, my_pkg::my_item out_item);
		logic [31:0] imm, res, newPC;
		string op;
		//decode ALU operation and compute result
		casez(in_item.instruction[10:0])
			11'b00000100000: begin res = Registers[in_item.instruction[25:21]] + Registers[in_item.instruction[20:16]]; op = "+"; end
			11'b00000100010: begin res = Registers[in_item.instruction[25:21]] - Registers[in_item.instruction[20:16]]; op = "-"; end
			11'b00000100100: begin res = Registers[in_item.instruction[25:21]] & Registers[in_item.instruction[20:16]]; op = "&"; end
			11'b00000100101: begin res = Registers[in_item.instruction[25:21]] | Registers[in_item.instruction[20:16]]; op = "|"; end
			11'b00000101010: begin res = ($signed(Registers[in_item.instruction[25:21]]) < $signed(Registers[in_item.instruction[20:16]])) ? 1 : 0; op = "slt"; end
			11'b?????000100: begin res = Registers[in_item.instruction[25:21]] << Registers[in_item.instruction[20:16]]; op = "<<"; end
			11'b00000000110: begin res = Registers[in_item.instruction[25:21]] >> Registers[in_item.instruction[20:16]]; op = ">>"; end
			11'b00000000111: begin res = $signed(Registers[in_item.instruction[25:21]]) >> Registers[in_item.instruction[20:16]]; op = "s>>"; end
			default: begin res = 0; op = "X"; end
		endcase
		Registers[in_item.instruction[15:11]] = res;
		newPC = PC + 4;
		// check results with values from DUT
		if (newPC != out_item.PC)
			`uvm_error(get_full_name(), $sformatf("ALU OP (%b -> s%0d = s%0d %s s%0d) error - wrong PC (expected %b, got %b)", in_item.instruction, in_item.instruction[25:21], op, in_item.instruction[20:16], in_item.instruction[15:11], newPC, out_item.PC))
		else if (res != out_item.mem_addr)
			`uvm_error(get_full_name(), $sformatf("ALU OP (%b -> s%0d = s%0d %s s%0d) error - wrong result (expected %b, got %b)", in_item.instruction, in_item.instruction[25:21], op, in_item.instruction[20:16], in_item.instruction[15:11], res, out_item.mem_addr))
		PC = newPC;
	endtask

	task EvalAddi (my_pkg::my_item in_item, my_pkg::my_item out_item);
		logic [31:0] imm, res, newPC;
		// compute correct result
		imm = {{16{in_item.instruction[15]}}, in_item.instruction[15:0]};
		res = imm + Registers[in_item.instruction[25:21]];
		newPC = PC + 4;
		Registers[in_item.instruction[20:16]] = res;
		// check results with values from DUT
		if (newPC != out_item.PC)
			`uvm_error(get_full_name(), $sformatf("Addi (%b -> s%0d = s%0d + %d) error - wrong PC (expected %b, got %b)", in_item.instruction, in_item.instruction[20:16], in_item.instruction[25:21], $signed(in_item.instruction[15:0]), newPC, out_item.PC))
		else if (res != out_item.mem_addr)
			`uvm_error(get_full_name(), $sformatf("Addi (%b -> s%0d = s%0d + %0d) error - wrong result (expected %b, got %b)", in_item.instruction, in_item.instruction[20:16], in_item.instruction[25:21], $signed(in_item.instruction[15:0]), res, out_item.mem_addr))
		PC = newPC;
	endtask

	task EvalLw (my_pkg::my_item in_item, my_pkg::my_item out_item);
		logic [31:0] imm, res, newPC;
		// compute correct result
		imm = {{16{in_item.instruction[15]}}, in_item.instruction[15:0]};
		res = imm + Registers[in_item.instruction[25:21]];
		newPC = PC + 4;
		Registers[in_item.instruction[20:16]] = in_item.data_from_mem;
		// check results with values from DUT
		if (newPC != out_item.PC)
			`uvm_error(get_full_name(), $sformatf("Lw (%b -> s%0d = Mem[s%0d + %0d]) error - wrong PC (expected %b, got %b)", in_item.instruction, in_item.instruction[20:16], in_item.instruction[25:21], $signed(in_item.instruction[15:0]), newPC, out_item.PC))
		else if (res != out_item.mem_addr)
			`uvm_error(get_full_name(), $sformatf("Lw (%b -> s%0d = Mem[s%0d + %0d]) error - wrong addr (expected %b, got %b)", in_item.instruction, in_item.instruction[20:16], in_item.instruction[25:21], $signed(in_item.instruction[15:0]), res, out_item.mem_addr))
		PC = out_item.PC;
	endtask

	task EvalSw (my_pkg::my_item in_item, my_pkg::my_item out_item);
		logic [31:0] imm, res, newPC;
		// compute correct result
		imm = {{16{in_item.instruction[15]}}, in_item.instruction[15:0]};
		res = imm + Registers[in_item.instruction[25:21]];
		newPC = PC + 4;
		// check results with values from DUT
		if (newPC != out_item.PC)
			`uvm_error(get_full_name(), $sformatf("Sw (%b -> Mem[s%0d + %0d] = s%0d) error - wrong PC (expected %b, got %b)", in_item.instruction, in_item.instruction[25:21], $signed(in_item.instruction[15:0]), in_item.instruction[20:16], newPC, out_item.PC))
		else if (res != out_item.mem_addr)
			`uvm_error(get_full_name(), $sformatf("Sw (%b -> Mem[s%0d + %0d] = s%0d) error - wrong addr (expected %b, got %b)", in_item.instruction, in_item.instruction[25:21], $signed(in_item.instruction[15:0]), in_item.instruction[20:16], res, out_item.mem_addr))
		else if (Registers[in_item.instruction[20:16]] != out_item.data_to_mem)
			`uvm_error(get_full_name(), $sformatf("Sw (%b -> Mem[s%0d + %0d] = s%0d) error - wrong data to mem (expected %b, got %b)", in_item.instruction, in_item.instruction[25:21], $signed(in_item.instruction[15:0]), in_item.instruction[20:16], Registers[in_item.instruction[20:16]], out_item.data_to_mem))
		PC = newPC;
	endtask

	task EvalBeq (my_pkg::my_item in_item, my_pkg::my_item out_item);
		logic [31:0] imm, res, newPC;
		// compute correct result
		imm = {{16{in_item.instruction[15]}}, in_item.instruction[15:0]};
		res = imm << 2;
		newPC = PC + 4;
		// check results with values from DUT
		if (Registers[in_item.instruction[25:21]] == Registers[in_item.instruction[20:16]])
			newPC = newPC + res;
		if (newPC != out_item.PC)
			`uvm_error(get_full_name(), $sformatf("Beq (%b -> if s%0d == s%0d then PC = PC + 4 + %d, %0d) error - wrong PC (expected %b, got %b)", in_item.instruction, in_item.instruction[25:21], in_item.instruction[20:16], $signed(in_item.instruction[15:0]), (Registers[in_item.instruction[25:21]] == Registers[in_item.instruction[20:16]]), newPC, out_item.PC))
		PC = newPC;
	endtask

	task EvalJal (my_pkg::my_item in_item, my_pkg::my_item out_item);
		logic [31:0] imm, res, newPC;
		// compute correct result
		PC = PC + 4;
		Registers[31] = PC;
		newPC = {PC[31:28], in_item.instruction[25:0], 2'b00};
		// check results with values from DUT
		if (newPC != out_item.PC)
			`uvm_error(get_full_name(), $sformatf("Jal (%b -> PC = (PC & 0xf0000000) | %b00) error - wrong PC (expected %b, got %b)", in_item.instruction, in_item.instruction[25:0], newPC, out_item.PC))
		PC = newPC;
	endtask

	task EvalJr (my_pkg::my_item in_item, my_pkg::my_item out_item);
		logic [31:0] imm, res, newPC;
		// compute correct result
		newPC = Registers[in_item.instruction[25:21]];
		// check results with values from DUT
		if (newPC != out_item.PC)
			`uvm_error(get_full_name(), $sformatf("Jr (%b -> PC = s%0d) error - wrong PC (expected %b, got %b)", in_item.instruction, in_item.instruction[25:21], newPC, out_item.PC))
		PC = newPC;
	endtask

	task EvalAddu (my_pkg::my_item in_item, my_pkg::my_item out_item);
		logic [31:0] imm, res, newPC;
		logic [8:0] subres [3:0];
		string op = "";
		// compute correct result
		newPC = PC + 4;
		subres[0] = {1'b0, Registers[in_item.instruction[25:21]][7:0]} + {1'b0, Registers[in_item.instruction[20:16]][7:0]};
		subres[1] = {1'b0, Registers[in_item.instruction[25:21]][15:8]} + {1'b0, Registers[in_item.instruction[20:16]][15:8]};
		subres[2] = {1'b0, Registers[in_item.instruction[25:21]][23:16]} + {1'b0, Registers[in_item.instruction[20:16]][23:16]};
		subres[3] = {1'b0, Registers[in_item.instruction[25:21]][31:24]} + {1'b0, Registers[in_item.instruction[20:16]][31:24]};
		if (in_item.instruction[8]) begin
			op = "s";
			if (subres[0][8]) subres[0] = 9'b111111111;
			if (subres[1][8]) subres[1] = 9'b111111111;
			if (subres[2][8]) subres[2] = 9'b111111111;
			if (subres[3][8]) subres[3] = 9'b111111111;
		end
		res = {subres[3][7:0], subres[2][7:0], subres[1][7:0], subres[0][7:0]};
		Registers[in_item.instruction[15:11]] = res;
		// check results with values from DUT
		if (newPC != out_item.PC)
			`uvm_error(get_full_name(), $sformatf("Addu%s (%b -> s%0d = s%0d + s%0d) error - wrong PC (expected %b, got %b)", op, in_item.instruction, in_item.instruction[25:21], in_item.instruction[20:16], in_item.instruction[15:11], newPC, out_item.PC))
		else if (res != out_item.mem_addr)
			`uvm_error(get_full_name(), $sformatf("Addu%s (%b -> s%0d = s%0d + s%0d) error - wrong result (expected %b, got %b)", op, in_item.instruction, in_item.instruction[25:21], in_item.instruction[20:16], in_item.instruction[15:11], res, out_item.mem_addr))
		PC = newPC;
	endtask

	task EvalJ (my_pkg::my_item in_item, my_pkg::my_item out_item);
		logic [31:0] imm, res, newPC;
		// compute correct result
		PC = PC + 4;
		newPC = {PC[31:28], in_item.instruction[25:0], 2'b00};
		// check results with values from DUT
		if (newPC != out_item.PC)
			`uvm_error(get_full_name(), $sformatf("J (%b -> PC = (PC & 0xf0000000) | %b00) error - wrong PC (expected %b, got %b)", in_item.instruction, in_item.instruction[25:0], newPC, out_item.PC))
		PC = newPC;
	endtask
endclass