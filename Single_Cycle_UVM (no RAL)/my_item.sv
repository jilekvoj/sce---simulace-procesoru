`timescale 1ns/10ps

`include "uvm_macros.svh"

// import knihovny UVM
import uvm_pkg::*;

class my_item extends uvm_sequence_item;

	// deklarace jednotlivych polozek
	rand bit [31:0] instruction;
	rand bit [31:0] data_from_mem;
	bit [31:0] PC;
	bit [31:0] data_to_mem;
	bit [31:0] mem_addr;
	bit mem_we;
  
	constraint valid_instruction
	{
		instruction[31:26] dist {6'b000000 := 8, 6'b001000 := 1, 6'b100011 := 1, 6'b101011 := 1, 6'b000100 := 1, 6'b000011 := 1, 6'b000111 := 1, 6'b011111 := 2, 6'b000010 := 1};
	}

	constraint valid_ALU_OP
	{
		(instruction[31:26] == 6'b000000) -> (instruction[10:0] inside {11'b00000100000, 11'b00000100010, 11'b00000100100, 11'b00000100101, 11'b00000101010, 11'b00000000100, 11'b00000000110, 11'b00000000111});
		(instruction[31:26] == 6'b011111) -> (instruction[10:0] inside {11'b00100010000, 11'b00000010000});
	}

	constraint Beq_Balance
	{
		((instruction[31:26] == 6'b000100) && (data_from_mem[0])) -> (instruction[25:21] == instruction[20:16]);
	}

	function void post_randomize ();
		time t = $time();
		int pos;
		// init registers at the beginning of the simulation
		if (t < 34 * 10ns) begin
			instruction[31:21] = 11'b10001100000;
			pos = t / 10ns;
			pos = pos - 2;
			instruction[20:16] = pos;
		end
		// JRE instruction fix
		if (instruction[31:26] == 6'b000111)
			instruction[20:0] = 21'b000000000000000001000;
	endfunction

	`uvm_object_utils_begin(my_item)
		`uvm_field_int(instruction, UVM_DEFAULT)
		`uvm_field_int(data_from_mem, UVM_DEFAULT)
		`uvm_field_int(PC, UVM_DEFAULT)
		`uvm_field_int(data_to_mem, UVM_DEFAULT)
		`uvm_field_int(mem_addr, UVM_DEFAULT)
		`uvm_field_int(mem_we, UVM_DEFAULT)
	`uvm_object_utils_end
 
	// instruction coverage
	covergroup item_cover_instr;
		option.auto_bin_max = 8;
		OP_ALU: coverpoint instruction[31:26]
		{
			bins x[] = {6'b000000}; 
		}
		OP_ADDI: coverpoint instruction[31:26]
		{
			bins x[] = {6'b001000}; 
		}
		OP_MEM: coverpoint instruction[31:26]
		{
			bins x[] = {6'b100011, 6'b101011}; 
		}
		OP_BEQ: coverpoint instruction[31:26]
		{
			bins x[] = {6'b000100}; 
		}
		OP_JAL: coverpoint instruction[31:26]
		{
			bins x[] = {6'b000011}; 
		}
		OP_JR: coverpoint instruction[31:26]
		{
			bins x[] = {6'b000111}; 
		}
		OP_J: coverpoint instruction[31:26]
		{
			bins x[] = {6'b000010}; 
		}
		ALU_FUNCT: coverpoint instruction[10:0]
		{
			bins x[] = {11'b00000100000, 11'b00000100010, 11'b00000100100, 11'b00000100101, 11'b00000101010, 11'b00000010000, 11'b00100010000, 11'b00000000100, 11'b00000000110, 11'b00000000111};
		}
		REGS: coverpoint instruction[25:21];
		REGT: coverpoint instruction[20:16];
		REGD: coverpoint instruction[15:11];
		IMM16:  coverpoint instruction[15:0];
		IMM26:  coverpoint instruction[25:0];

		ALU: cross OP_ALU, ALU_FUNCT, REGS, REGT, REGD;
		ADDI: cross OP_ADDI, REGS, REGT, IMM16;
		LWSW: cross OP_MEM, REGS, REGT, IMM16;
		BEQ: cross OP_BEQ, REGS, REGT, IMM16;
		JAL: cross OP_JAL, IMM26; 
		JR: cross OP_JR, REGS; 
		J: cross OP_J, IMM26; 
	endgroup
 
	function new (string name = "my_item");
		super.new(name);
		item_cover_instr = new();
	endfunction

endclass