`timescale 1ns/10ps

`include "uvm_macros.svh"

import uvm_pkg::*;

class my_sequence extends uvm_sequence #(my_pkg::my_item);
	`uvm_object_utils(my_sequence)

	my_pkg::my_item inst_item;

	function new(string name = "my_sequence");
		super.new(name);
	endfunction

	virtual task body();
		`uvm_do(inst_item)
	endtask
endclass