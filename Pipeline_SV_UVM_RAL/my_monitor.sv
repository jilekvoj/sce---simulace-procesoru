`timescale 1ns/10ps
`include "uvm_macros.svh"

import uvm_pkg::*;

class my_monitor extends uvm_monitor;
	`uvm_component_utils(my_monitor)

	uvm_analysis_port #(int) inst_collected_item_port;
	virtual my_if m_if;
  
	function new (string name = "my_monitor", uvm_component parent = null);
		super.new(name, parent);
	endfunction
  
	virtual function void build_phase(uvm_phase phase);
		super.build_phase(phase);
		inst_collected_item_port = new("inst_collected_item_port", this);
	endfunction
  
	virtual task run_phase(uvm_phase phase);
		int instruction;
		// get reference to the DUT ineterface
		if(!uvm_config_db#(virtual my_if)::get(null, "uvm_top", "m_if", m_if))
			`uvm_fatal("NOVIF", {"Missing interface: ", get_full_name(), ".m_if"})
		// backdoor read of current instruction from DUT
		forever begin
			@(negedge m_if.clk);
			uvm_hdl_read("uvm_top._dut.Instruction", instruction); 
			inst_collected_item_port.write(instruction);
		end
	endtask
endclass
