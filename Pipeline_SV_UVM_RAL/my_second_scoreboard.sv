`timescale 1ns/10ps

`include "uvm_macros.svh"

import uvm_pkg::*;

class my_second_scoreboard extends uvm_scoreboard;
	`uvm_component_utils(my_second_scoreboard)

	// port for communication with monitor
	uvm_tlm_analysis_fifo #(int) instr_fifo;

	my_pkg::my_reg_model m_reg_model;
	uvm_status_e status;
	virtual my_if m_if;
	bit enable_test;
	int stall;

	function new (string name = "my_second_scoreboard", uvm_component parent = null);
		super.new(name, parent);
		enable_test = 1'b0;
	endfunction

	function void build_phase (uvm_phase phase);
		super.build_phase(phase);
		instr_fifo = new("instr_fifo", this);
	endfunction

	task run_phase (uvm_phase phase);
		super.run_phase(phase);
		phase.raise_objection(this);

		// get reference to the reg model from uvm databese
		if(!uvm_config_db#(my_pkg::my_reg_model)::get(null, "uvm_top", "m_reg_model", m_reg_model))
			`uvm_fatal("NOVIF", {"Missing my_reg_model: ", get_full_name(), ".m_reg_model"})

		// get reference to the DUT interface from uvm databese
		if(!uvm_config_db#(virtual my_if)::get(null, "uvm_top", "m_if", m_if))
			`uvm_fatal("NOVIF", {"Missing interface: ", get_full_name(), ".m_if"})

		scoreboard_match(phase);
	endtask

	task scoreboard_match(uvm_phase phase);
		my_pkg::my_item item = my_pkg::my_item::type_id::create();
		int instr_cnt = 0;
		string instr [5:0];
		bit IF_FLUSH = 1'b0, STALL = 1'b0, IFID_WE = 1'b1;
 		bit EX_BneControl, EX_JRControl, EX_MemRead, JumpControl, PC_WE;
		int EX_RT;
		bit WB_WE, MEM_WE;
		int WB_Addr, MEM_Addr, WB_Data, MEM_Data;
		int PC, IDTarget, EXTarget;
		bit EXJumpEnable;
		bit prev_enable = 1'b0;
		uvm_report_server server = get_report_server();
		for (int i = 0; i < 6; i++) begin
			instr[i] = "";
		end
		instr_fifo.get(item.instruction);
		instr[0] = InstrToString(item.instruction);
		forever begin
			instr_fifo.get(item.instruction);
			//`uvm_info("", $sformatf("New instruction: %b ignore: %0d time: %0d", item.instruction, ignore, $time()), UVM_LOW)

			// ignore instructions during init phase
			if (!enable_test) begin
				prev_enable = 1'b0;
				continue;
			end

			// get correct desired values at the start of test phase
			if (enable_test && !prev_enable) begin
				prev_enable = 1'b1;
				UpdatePipeline();
				continue;
			end

			for (int i = 5; i > 0; i--) begin
				instr[i] = instr[i - 1];
			end
			instr[0] = InstrToString(item.instruction);

			EvalWBstage(instr[5], WB_WE, WB_Addr, WB_Data);
			EvalMEMstage(instr[4], MEM_WE, MEM_Addr, MEM_Data);
			EvalEXstage(instr[3], WB_WE, MEM_WE, WB_Addr, MEM_Addr, WB_Data, MEM_Data, EX_BneControl, EX_JRControl, EX_MemRead, EX_RT, EXJumpEnable, EXTarget);
			EvalIDstage(instr[2], EX_BneControl, EX_JRControl, EX_MemRead, EX_RT, IF_FLUSH, JumpControl, PC_WE, IFID_WE, IDTarget);
			EvalIFstage(instr[1], IF_FLUSH, IFID_WE);

			EvalPC(instr[2], instr[3], JumpControl, EXJumpEnable, PC_WE, IDTarget, EXTarget);

			item.item_cover_instr.sample();
			instr_cnt++;
			
			// stop simulation on first error
			/*
			if (server.get_severity_count(UVM_ERROR) != 0)
				$stop();
			*/
			
      			if ($get_coverage() > 95) begin
				`uvm_info(get_full_name(), $sformatf("\n\nScoreboard: coverage done -> finishing test.\n\nNumber of instructions: %0d\nNumber of errors: %0d\n", instr_cnt, server.get_severity_count(UVM_ERROR)), UVM_LOW)  
				phase.drop_objection(this);
			end
		end
	endtask

	function string InstrToString (int Instr);
		case(Instr[31:26])
			6'b000000: 	begin
						if (Instr[10:0] == 11'b00000001000)
							return $sformatf("Jr %0d  (%b)", Instr[25:21], Instr);
						if (Instr[10:0] == 11'b00000100000)
							return $sformatf("ADD %0d = %0d + %0d  (%b)", Instr[15:11], Instr[20:16], Instr[25:21], Instr);
						if (Instr[10:0] == 11'b00000100010)
							return $sformatf("SUB %0d = %0d - %0d  (%b)", Instr[15:11], Instr[25:21], Instr[20:16], Instr);
						if (Instr[10:0] == 11'b00000101010)
							return $sformatf("SLT %0d = %0d < %0d  (%b)", Instr[15:11], Instr[25:21], Instr[20:16], Instr);
						return "Unknown instruction";				
					end
			6'b000010: return $sformatf("J  (%b)", Instr);
			6'b000101: return $sformatf("Bne %0d != %0d  (%b)", Instr[20:16], Instr[25:21], Instr);
			6'b001110: return $sformatf("Xori %0d = %0d ^ imm  (%b)", Instr[20:16], Instr[25:21], Instr);
			6'b101011: return $sformatf("Sw M[%0d + imm] = %0d (%b)", Instr[25:21], Instr[20:16], Instr);
			6'b100011: return $sformatf("Lw %0d = M[%0d + imm] (%b)", Instr[20:16], Instr[25:21], Instr);
			default: return "Unknown instruction";
		endcase
	endfunction

	task EvalPC (string IDInstr, string EXInstr, bit IDJump, bit EXJump, bit PC_WE, int IDTarget, int EXTarget);
		int PC;
		if (!PC_WE)
			PC = m_reg_model.m_GPR_gr.my_PC.get();
		else if (EXJump)
			PC = EXTarget;
		else if (IDJump)
			PC = IDTarget;
		else 
			PC = m_reg_model.m_GPR_gr.my_PC.get() + 4;

		m_reg_model.m_GPR_gr.my_PC.predict(PC, -1, UVM_PREDICT_DIRECT, UVM_BACKDOOR);
		m_reg_model.m_GPR_gr.my_PC.SetInstr($sformatf("ID: %s\nEX:%s", IDInstr, EXInstr));
		m_reg_model.m_GPR_gr.my_PC.mirror(status, UVM_CHECK, UVM_BACKDOOR);
		m_reg_model.m_GPR_gr.my_PC.poke(status, PC);	
	endtask

	task EvalIFstage (string Instr, bit IF_FLUSH, bit IFID_WE);
		int IFID_PC;
		int IFID_instruction;
		int MemAddr;
		if (IFID_WE) begin
			IFID_PC = m_reg_model.m_GPR_gr.my_PC.get();
			MemAddr = IFID_PC % my_pkg::MEM_SIZE;
			if (MemAddr < 0)
				MemAddr = MemAddr + my_pkg::MEM_SIZE;
			IFID_instruction = m_reg_model.m_instr_mem_gr.my_mem_cell[MemAddr].get();
		end
		else begin
			IFID_PC = m_reg_model.m_pipeline_gr.IFID_PC4.get() - 4;
			IFID_instruction = m_reg_model.m_pipeline_gr.IFID_Instruction.get();
			IF_FLUSH = m_reg_model.m_pipeline_gr.IF_flush_bit.get();
		end
		
		// check values in DUT
		m_reg_model.m_pipeline_gr.IFID_PC4.predict(IFID_PC + 4, -1, UVM_PREDICT_DIRECT, UVM_BACKDOOR);
		m_reg_model.m_pipeline_gr.IFID_PC4.SetInstr(Instr);
		m_reg_model.m_pipeline_gr.IFID_PC4.mirror(status, UVM_CHECK, UVM_BACKDOOR);
		m_reg_model.m_pipeline_gr.IFID_PC4.poke(status, IFID_PC + 4);

		m_reg_model.m_pipeline_gr.IFID_Instruction.predict(IFID_instruction, -1, UVM_PREDICT_DIRECT, UVM_BACKDOOR);
		m_reg_model.m_pipeline_gr.IFID_Instruction.SetInstr(Instr);
		m_reg_model.m_pipeline_gr.IFID_Instruction.mirror(status, UVM_CHECK, UVM_BACKDOOR);
		m_reg_model.m_pipeline_gr.IFID_Instruction.poke(status, IFID_instruction);

		m_reg_model.m_pipeline_gr.IF_flush_bit.predict(IF_FLUSH, -1, UVM_PREDICT_DIRECT, UVM_BACKDOOR);
		m_reg_model.m_pipeline_gr.IF_flush_bit.SetInstr(Instr);
		m_reg_model.m_pipeline_gr.IF_flush_bit.mirror(status, UVM_CHECK, UVM_BACKDOOR);
		m_reg_model.m_pipeline_gr.IF_flush_bit.poke(status, IF_FLUSH);
	endtask

	task EvalIDstage (string Instr, bit EX_BneControl, bit EX_JRControl, bit EX_MemRead, int EX_RT, output bit IF_FLUSH, output bit JumpControl, output bit PC_WE, output bit IFID_WE, output int IDTarget);
		int ID_PC4, ID_instruction;
		bit ID_FLUSH;
		int IDEX_PC4, IDEX_ReadData1, IDEX_ReadData2, IDEX_Im16_Ext, IDEX_rs_rt_rd;
		bit IDEX_RegDst, IDEX_ALUSrc, IDEX_MemtoReg, IDEX_RegWrite, IDEX_MemRead, IDEX_MemWrite, IDEX_Branch, IDEX_JRControl, IDEX_ALUOp1, IDEX_ALUOp0;
		int ID_Opcode, ID_RS, ID_RT, ID_RD, ID_Imm;
		bit ID_SignZero, ID_Jump, ID_FLUSH_imm, STALL_FLUSH;

		// get values from IFID registers
		ID_PC4 = m_reg_model.m_pipeline_gr.IFID_PC4.get();
		ID_instruction = m_reg_model.m_pipeline_gr.IFID_Instruction.get();
		ID_FLUSH = m_reg_model.m_pipeline_gr.IF_flush_bit.get();

		// split instruction	
		ID_Opcode = ID_instruction[31:26];
		ID_RS = ID_instruction[25:21];
		ID_RT = ID_instruction[20:16];
		ID_RD = ID_instruction[15:11];
		ID_Imm = ID_instruction[15:0];

		// decode instruction
		Driver(ID_Opcode, IDEX_RegDst, IDEX_ALUSrc, IDEX_MemtoReg, IDEX_RegWrite, IDEX_MemRead, IDEX_MemWrite, IDEX_Branch, IDEX_ALUOp1, IDEX_ALUOp0, ID_Jump, ID_SignZero);
		
		// data from regs
		IDEX_ReadData1 = m_reg_model.m_GPR_gr.my_GPR[ID_RS].get();
		IDEX_ReadData2 = m_reg_model.m_GPR_gr.my_GPR[ID_RT].get();

		// extend imm
		if (ID_SignZero)
			IDEX_Im16_Ext = {16'b0000000000000000, ID_Imm};
		else
			IDEX_Im16_Ext = {{17{ID_Imm[15]}}, ID_Imm[14:0]};
		
		// JRcontrol
		if ({IDEX_ALUOp1, IDEX_ALUOp0, ID_instruction[5:0]} == 8'b10001000)
			IDEX_JRControl = 1'b1;
		else
			IDEX_JRControl = 1'b0;

		// Jump controll
		JumpControl = ~ID_FLUSH & ID_Jump & ~EX_BneControl;

		// JumpTarget
		IDTarget = {ID_PC4[31:28], ID_instruction[25:0], 2'b00};

		// flush control
		if (EX_BneControl | EX_JRControl)
			ID_FLUSH_imm = 1'b1;
		else
			ID_FLUSH_imm = 1'b0;
		if (EX_BneControl | EX_JRControl | JumpControl)
			IF_FLUSH = 1'b1;
		else
			IF_FLUSH = 1'b0;

		// stall control
		if (EX_MemRead && (((EX_RT == ID_RS) && (ID_Opcode != 6'b000010)) || ((EX_RT == ID_RT) && (ID_Opcode != 6'b001110) && (ID_Opcode != 6'b100011) && (ID_Opcode != 6'b000010) && ((ID_Opcode != 6'b000000) || (ID_Imm != 16'b0000000000001000))))) begin
			PC_WE = 1'b0;
			IFID_WE = 1'b0;
			STALL_FLUSH = 1'b1;
		end
		else begin
			PC_WE = 1'b1;
			IFID_WE = 1'b1;
			STALL_FLUSH = 1'b0;
		end

		// flush instruction
		if (ID_FLUSH_imm | ID_FLUSH | STALL_FLUSH) begin
			//IDEX_ReadData1 = 0;
			//IDEX_ReadData2 = 0;
			//IDEX_Im16_Ext = 0;
			IDEX_RegDst = 1'b0;
			IDEX_ALUSrc = 1'b0;
			IDEX_MemtoReg = 1'b0;
			IDEX_RegWrite = 1'b0;
			IDEX_MemRead = 1'b0;
			IDEX_MemWrite = 1'b0;
			IDEX_Branch = 1'b0;
			IDEX_JRControl = 1'b0;
			IDEX_ALUOp1 = 1'b0;
			IDEX_ALUOp0 = 1'b0;
		end

		// check values in DUT
		m_reg_model.m_pipeline_gr.IDEX_PC4.predict(ID_PC4, -1, UVM_PREDICT_DIRECT, UVM_BACKDOOR);
		m_reg_model.m_pipeline_gr.IDEX_PC4.SetInstr(Instr);
		m_reg_model.m_pipeline_gr.IDEX_PC4.mirror(status, UVM_CHECK, UVM_BACKDOOR);
		m_reg_model.m_pipeline_gr.IDEX_PC4.poke(status, ID_PC4);

		m_reg_model.m_pipeline_gr.IDEX_ReadData1.predict(IDEX_ReadData1, -1, UVM_PREDICT_DIRECT, UVM_BACKDOOR);
		m_reg_model.m_pipeline_gr.IDEX_ReadData1.SetInstr(Instr);
		m_reg_model.m_pipeline_gr.IDEX_ReadData1.mirror(status, UVM_CHECK, UVM_BACKDOOR);
		m_reg_model.m_pipeline_gr.IDEX_ReadData1.poke(status, IDEX_ReadData1);

		m_reg_model.m_pipeline_gr.IDEX_ReadData2.predict(IDEX_ReadData2, -1, UVM_PREDICT_DIRECT, UVM_BACKDOOR);
		m_reg_model.m_pipeline_gr.IDEX_ReadData2.SetInstr(Instr);
		m_reg_model.m_pipeline_gr.IDEX_ReadData2.mirror(status, UVM_CHECK, UVM_BACKDOOR);
		m_reg_model.m_pipeline_gr.IDEX_ReadData2.poke(status, IDEX_ReadData2);

		m_reg_model.m_pipeline_gr.IDEX_Im16_Ext.predict(IDEX_Im16_Ext, -1, UVM_PREDICT_DIRECT, UVM_BACKDOOR);
		m_reg_model.m_pipeline_gr.IDEX_Im16_Ext.SetInstr(Instr);
		m_reg_model.m_pipeline_gr.IDEX_Im16_Ext.mirror(status, UVM_CHECK, UVM_BACKDOOR);
		m_reg_model.m_pipeline_gr.IDEX_Im16_Ext.poke(status, IDEX_Im16_Ext);

		m_reg_model.m_pipeline_gr.IDEX_rs_rt_rd.predict(ID_instruction, -1, UVM_PREDICT_DIRECT, UVM_BACKDOOR);
		m_reg_model.m_pipeline_gr.IDEX_rs_rt_rd.SetInstr(Instr);
		m_reg_model.m_pipeline_gr.IDEX_rs_rt_rd.mirror(status, UVM_CHECK, UVM_BACKDOOR);
		m_reg_model.m_pipeline_gr.IDEX_rs_rt_rd.poke(status, ID_instruction);

		m_reg_model.m_pipeline_gr.IDEX_RegDst.predict(IDEX_RegDst, -1, UVM_PREDICT_DIRECT, UVM_BACKDOOR);
		m_reg_model.m_pipeline_gr.IDEX_RegDst.SetInstr(Instr);
		m_reg_model.m_pipeline_gr.IDEX_RegDst.mirror(status, UVM_CHECK, UVM_BACKDOOR);
		m_reg_model.m_pipeline_gr.IDEX_RegDst.poke(status, IDEX_RegDst);

		m_reg_model.m_pipeline_gr.IDEX_ALUSrc.predict(IDEX_ALUSrc, -1, UVM_PREDICT_DIRECT, UVM_BACKDOOR);
		m_reg_model.m_pipeline_gr.IDEX_ALUSrc.SetInstr(Instr);
		m_reg_model.m_pipeline_gr.IDEX_ALUSrc.mirror(status, UVM_CHECK, UVM_BACKDOOR);
		m_reg_model.m_pipeline_gr.IDEX_ALUSrc.poke(status, IDEX_ALUSrc);

		m_reg_model.m_pipeline_gr.IDEX_MemtoReg.predict(IDEX_MemtoReg, -1, UVM_PREDICT_DIRECT, UVM_BACKDOOR);
		m_reg_model.m_pipeline_gr.IDEX_MemtoReg.SetInstr(Instr);
		m_reg_model.m_pipeline_gr.IDEX_MemtoReg.mirror(status, UVM_CHECK, UVM_BACKDOOR);
		m_reg_model.m_pipeline_gr.IDEX_MemtoReg.poke(status, IDEX_MemtoReg);

		m_reg_model.m_pipeline_gr.IDEX_RegWrite.predict(IDEX_RegWrite, -1, UVM_PREDICT_DIRECT, UVM_BACKDOOR);
		m_reg_model.m_pipeline_gr.IDEX_RegWrite.SetInstr(Instr);
		m_reg_model.m_pipeline_gr.IDEX_RegWrite.mirror(status, UVM_CHECK, UVM_BACKDOOR);
		m_reg_model.m_pipeline_gr.IDEX_RegWrite.poke(status, IDEX_RegWrite);

		m_reg_model.m_pipeline_gr.IDEX_MemRead.predict(IDEX_MemRead, -1, UVM_PREDICT_DIRECT, UVM_BACKDOOR);
		m_reg_model.m_pipeline_gr.IDEX_MemRead.SetInstr(Instr);
		m_reg_model.m_pipeline_gr.IDEX_MemRead.mirror(status, UVM_CHECK, UVM_BACKDOOR);
		m_reg_model.m_pipeline_gr.IDEX_MemRead.poke(status, IDEX_MemRead);

		m_reg_model.m_pipeline_gr.IDEX_MemWrite.predict(IDEX_MemWrite, -1, UVM_PREDICT_DIRECT, UVM_BACKDOOR);
		m_reg_model.m_pipeline_gr.IDEX_MemWrite.SetInstr(Instr);
		m_reg_model.m_pipeline_gr.IDEX_MemWrite.mirror(status, UVM_CHECK, UVM_BACKDOOR);
		m_reg_model.m_pipeline_gr.IDEX_MemWrite.poke(status, IDEX_MemWrite);

		m_reg_model.m_pipeline_gr.IDEX_Branch.predict(IDEX_Branch, -1, UVM_PREDICT_DIRECT, UVM_BACKDOOR);
		m_reg_model.m_pipeline_gr.IDEX_Branch.SetInstr(Instr);
		m_reg_model.m_pipeline_gr.IDEX_Branch.mirror(status, UVM_CHECK, UVM_BACKDOOR);
		m_reg_model.m_pipeline_gr.IDEX_Branch.poke(status, IDEX_Branch);

		m_reg_model.m_pipeline_gr.IDEX_JRControl.predict(IDEX_JRControl, -1, UVM_PREDICT_DIRECT, UVM_BACKDOOR);
		m_reg_model.m_pipeline_gr.IDEX_JRControl.SetInstr(Instr);
		m_reg_model.m_pipeline_gr.IDEX_JRControl.mirror(status, UVM_CHECK, UVM_BACKDOOR);
		m_reg_model.m_pipeline_gr.IDEX_JRControl.poke(status, IDEX_JRControl);

		m_reg_model.m_pipeline_gr.IDEX_ALUOp1.predict(IDEX_ALUOp1, -1, UVM_PREDICT_DIRECT, UVM_BACKDOOR);
		m_reg_model.m_pipeline_gr.IDEX_ALUOp1.SetInstr(Instr);
		m_reg_model.m_pipeline_gr.IDEX_ALUOp1.mirror(status, UVM_CHECK, UVM_BACKDOOR);
		m_reg_model.m_pipeline_gr.IDEX_ALUOp1.poke(status, IDEX_ALUOp1);

		m_reg_model.m_pipeline_gr.IDEX_ALUOp0.predict(IDEX_ALUOp0, -1, UVM_PREDICT_DIRECT, UVM_BACKDOOR);
		m_reg_model.m_pipeline_gr.IDEX_ALUOp0.SetInstr(Instr);
		m_reg_model.m_pipeline_gr.IDEX_ALUOp0.mirror(status, UVM_CHECK, UVM_BACKDOOR);
		m_reg_model.m_pipeline_gr.IDEX_ALUOp0.poke(status, IDEX_ALUOp0);
	endtask

	task EvalEXstage (string Instr, bit WB_WE, bit MEM_WE, int WB_Addr, int MEM_Addr, int WB_Data, int MEM_Data, output bit EX_BneControl, output bit EX_JRControl, output bit EX_MemRead, output int EX_RT, output bit EXJumpEnable, output int EXPCTarget);
		int EX_PC4, EX_ReadData1, EX_ReadData2, EX_Im16_Ext, EX_rs_rt_rd;
		bit EX_RegDst, EX_ALUSrc, EX_MemtoReg, EX_RegWrite, EX_MemWrite, EX_Branch, EX_ALUOp1, EX_ALUOp0;
		int EX_RS, EX_RD;
		int EX_ALU_A, EX_ALU_B, EX_ALU_RES, EX_ALU_B_forward, EX_ALU_Control, EX_Target;	

		// get values form IDEX registers
		EX_PC4 = m_reg_model.m_pipeline_gr.IDEX_PC4.get();
		EX_ReadData1 = m_reg_model.m_pipeline_gr.IDEX_ReadData1.get();
		EX_ReadData2 = m_reg_model.m_pipeline_gr.IDEX_ReadData2.get();
		EX_Im16_Ext = m_reg_model.m_pipeline_gr.IDEX_Im16_Ext.get();
		EX_rs_rt_rd = m_reg_model.m_pipeline_gr.IDEX_rs_rt_rd.get();
		EX_RegDst = m_reg_model.m_pipeline_gr.IDEX_RegDst.get();
		EX_ALUSrc = m_reg_model.m_pipeline_gr.IDEX_ALUSrc.get();
		EX_MemtoReg = m_reg_model.m_pipeline_gr.IDEX_MemtoReg.get();
		EX_RegWrite = m_reg_model.m_pipeline_gr.IDEX_RegWrite.get();
		EX_MemRead = m_reg_model.m_pipeline_gr.IDEX_MemRead.get();
		EX_MemWrite = m_reg_model.m_pipeline_gr.IDEX_MemWrite.get();
		EX_Branch = m_reg_model.m_pipeline_gr.IDEX_Branch.get();
		EX_JRControl = m_reg_model.m_pipeline_gr.IDEX_JRControl.get();
		EX_ALUOp1 = m_reg_model.m_pipeline_gr.IDEX_ALUOp1.get();
		EX_ALUOp0 = m_reg_model.m_pipeline_gr.IDEX_ALUOp0.get();

		// get target registers
		EX_RS = EX_rs_rt_rd[25:21];
		EX_RT = EX_rs_rt_rd[20:16];
		EX_RD = EX_rs_rt_rd[15:11];

		EX_ALU_A = EX_ReadData1;
		EX_ALU_B_forward = EX_ReadData2;

		// forwarding
		if (WB_WE && (WB_Addr == EX_RS) && (EX_RS != 0))
			EX_ALU_A = WB_Data;
		if (WB_WE && (WB_Addr == EX_RT) && (EX_RT != 0))
			EX_ALU_B_forward = WB_Data;
		if (MEM_WE && (MEM_Addr == EX_RS) && (EX_RS != 0))
			EX_ALU_A = MEM_Data;
		if (MEM_WE && (MEM_Addr == EX_RT) && (EX_RT != 0))
			EX_ALU_B_forward = MEM_Data;

		// ALU source
		if (EX_ALUSrc)
			EX_ALU_B = EX_Im16_Ext;
		else
			EX_ALU_B = EX_ALU_B_forward;

		// ALU
		EX_ALU_Control = {EX_ALUOp1, EX_ALUOp0, EX_Im16_Ext[5:0]};
		casex(EX_ALU_Control)
 			8'b11xxxxxx: EX_ALU_RES = EX_ALU_A ^ EX_ALU_B;
 			8'b00xxxxxx: EX_ALU_RES = EX_ALU_A + EX_ALU_B;
 			8'b01xxxxxx: EX_ALU_RES = EX_ALU_A - EX_ALU_B;
 			8'b10100000: EX_ALU_RES = EX_ALU_A + EX_ALU_B;
 			8'b10100010: EX_ALU_RES = EX_ALU_A - EX_ALU_B;
 			8'b10101010: EX_ALU_RES = (EX_ALU_A < EX_ALU_B ? 1'b1 : 1'b0);
 			default: EX_ALU_RES = EX_ALU_A + EX_ALU_B;
		endcase

		// bne control
		EX_BneControl = (EX_ALU_RES != 0) && EX_Branch;
		
		// target register
		EX_Target = EX_RegDst ? EX_RD : EX_RT;

		// jump target
		if (EX_JRControl)
			EXPCTarget = EX_ALU_A;
		else
			EXPCTarget = EX_PC4 + (EX_Im16_Ext << 2);
		EXJumpEnable = EX_JRControl | EX_BneControl;


		// check values in DUT	
		m_reg_model.m_pipeline_gr.EXMEM_ALUResult.predict(EX_ALU_RES, -1, UVM_PREDICT_DIRECT, UVM_BACKDOOR);
		m_reg_model.m_pipeline_gr.EXMEM_ALUResult.SetInstr(Instr);
		m_reg_model.m_pipeline_gr.EXMEM_ALUResult.mirror(status, UVM_CHECK, UVM_BACKDOOR);
		m_reg_model.m_pipeline_gr.EXMEM_ALUResult.poke(status, EX_ALU_RES);	

		m_reg_model.m_pipeline_gr.EXMEM_WriteDataOfMem.predict(EX_ALU_B_forward, -1, UVM_PREDICT_DIRECT, UVM_BACKDOOR);
		m_reg_model.m_pipeline_gr.EXMEM_WriteDataOfMem.SetInstr(Instr);
		m_reg_model.m_pipeline_gr.EXMEM_WriteDataOfMem.mirror(status, UVM_CHECK, UVM_BACKDOOR);
		m_reg_model.m_pipeline_gr.EXMEM_WriteDataOfMem.poke(status, EX_ALU_B_forward);	

		m_reg_model.m_pipeline_gr.EXMEM_MemtoReg.predict(EX_MemtoReg, -1, UVM_PREDICT_DIRECT, UVM_BACKDOOR);
		m_reg_model.m_pipeline_gr.EXMEM_MemtoReg.SetInstr(Instr);
		m_reg_model.m_pipeline_gr.EXMEM_MemtoReg.mirror(status, UVM_CHECK, UVM_BACKDOOR);
		m_reg_model.m_pipeline_gr.EXMEM_MemtoReg.poke(status, EX_MemtoReg);	

		m_reg_model.m_pipeline_gr.EXMEM_RegWrite.predict(EX_RegWrite, -1, UVM_PREDICT_DIRECT, UVM_BACKDOOR);
		m_reg_model.m_pipeline_gr.EXMEM_RegWrite.SetInstr(Instr);
		m_reg_model.m_pipeline_gr.EXMEM_RegWrite.mirror(status, UVM_CHECK, UVM_BACKDOOR);
		m_reg_model.m_pipeline_gr.EXMEM_RegWrite.poke(status, EX_RegWrite);	

		m_reg_model.m_pipeline_gr.EXMEM_MemRead.predict(EX_MemRead, -1, UVM_PREDICT_DIRECT, UVM_BACKDOOR);
		m_reg_model.m_pipeline_gr.EXMEM_MemRead.SetInstr(Instr);
		m_reg_model.m_pipeline_gr.EXMEM_MemRead.mirror(status, UVM_CHECK, UVM_BACKDOOR);
		m_reg_model.m_pipeline_gr.EXMEM_MemRead.poke(status, EX_MemRead);	

		m_reg_model.m_pipeline_gr.EXMEM_MemWrite.predict(EX_MemWrite, -1, UVM_PREDICT_DIRECT, UVM_BACKDOOR);
		m_reg_model.m_pipeline_gr.EXMEM_MemWrite.SetInstr(Instr);
		m_reg_model.m_pipeline_gr.EXMEM_MemWrite.mirror(status, UVM_CHECK, UVM_BACKDOOR);
		m_reg_model.m_pipeline_gr.EXMEM_MemWrite.poke(status, EX_MemWrite);	

		m_reg_model.m_pipeline_gr.EXMEM_WriteRegister4.predict(EX_Target[4], -1, UVM_PREDICT_DIRECT, UVM_BACKDOOR);
		m_reg_model.m_pipeline_gr.EXMEM_WriteRegister4.SetInstr(Instr);
		m_reg_model.m_pipeline_gr.EXMEM_WriteRegister4.mirror(status, UVM_CHECK, UVM_BACKDOOR);
		m_reg_model.m_pipeline_gr.EXMEM_WriteRegister4.poke(status, EX_Target[4]);	

		m_reg_model.m_pipeline_gr.EXMEM_WriteRegister3.predict(EX_Target[3], -1, UVM_PREDICT_DIRECT, UVM_BACKDOOR);
		m_reg_model.m_pipeline_gr.EXMEM_WriteRegister3.SetInstr(Instr);
		m_reg_model.m_pipeline_gr.EXMEM_WriteRegister3.mirror(status, UVM_CHECK, UVM_BACKDOOR);
		m_reg_model.m_pipeline_gr.EXMEM_WriteRegister3.poke(status, EX_Target[3]);	

		m_reg_model.m_pipeline_gr.EXMEM_WriteRegister2.predict(EX_Target[2], -1, UVM_PREDICT_DIRECT, UVM_BACKDOOR);
		m_reg_model.m_pipeline_gr.EXMEM_WriteRegister2.SetInstr(Instr);
		m_reg_model.m_pipeline_gr.EXMEM_WriteRegister2.mirror(status, UVM_CHECK, UVM_BACKDOOR);
		m_reg_model.m_pipeline_gr.EXMEM_WriteRegister2.poke(status, EX_Target[2]);	

		m_reg_model.m_pipeline_gr.EXMEM_WriteRegister1.predict(EX_Target[1], -1, UVM_PREDICT_DIRECT, UVM_BACKDOOR);
		m_reg_model.m_pipeline_gr.EXMEM_WriteRegister1.SetInstr(Instr);
		m_reg_model.m_pipeline_gr.EXMEM_WriteRegister1.mirror(status, UVM_CHECK, UVM_BACKDOOR);
		m_reg_model.m_pipeline_gr.EXMEM_WriteRegister1.poke(status, EX_Target[1]);	

		m_reg_model.m_pipeline_gr.EXMEM_WriteRegister0.predict(EX_Target[0], -1, UVM_PREDICT_DIRECT, UVM_BACKDOOR);
		m_reg_model.m_pipeline_gr.EXMEM_WriteRegister0.SetInstr(Instr);
		m_reg_model.m_pipeline_gr.EXMEM_WriteRegister0.mirror(status, UVM_CHECK, UVM_BACKDOOR);
		m_reg_model.m_pipeline_gr.EXMEM_WriteRegister0.poke(status, EX_Target[0]);		
	endtask

	task EvalMEMstage (string Instr, output bit MEM_WE, output int MEM_Addr, output int MEM_Data);
		int MEM_ALUResult, MEM_WriteDataOfMem;
		bit MEM_MemtoReg, MEM_RegWrite, MEM_MemWrite, MEM_MemRead, MEM_WriteRegister4, MEM_WriteRegister3, MEM_WriteRegister2, MEM_WriteRegister1, MEM_WriteRegister0;
		int MEM_ReadData = 0, MEM_Addr1;

		// get values form EXMEM registers
		MEM_ALUResult = m_reg_model.m_pipeline_gr.EXMEM_ALUResult.get();
		MEM_WriteDataOfMem = m_reg_model.m_pipeline_gr.EXMEM_WriteDataOfMem.get();
		MEM_MemtoReg = m_reg_model.m_pipeline_gr.EXMEM_MemtoReg.get();
		MEM_RegWrite = m_reg_model.m_pipeline_gr.EXMEM_RegWrite.get();
		MEM_MemRead = m_reg_model.m_pipeline_gr.EXMEM_MemRead.get();
		MEM_MemWrite = m_reg_model.m_pipeline_gr.EXMEM_MemWrite.get();
		MEM_WriteRegister4 = m_reg_model.m_pipeline_gr.EXMEM_WriteRegister4.get();
		MEM_WriteRegister3 = m_reg_model.m_pipeline_gr.EXMEM_WriteRegister3.get();
		MEM_WriteRegister2 = m_reg_model.m_pipeline_gr.EXMEM_WriteRegister2.get();
		MEM_WriteRegister1 = m_reg_model.m_pipeline_gr.EXMEM_WriteRegister1.get();
		MEM_WriteRegister0 = m_reg_model.m_pipeline_gr.EXMEM_WriteRegister0.get();

		MEM_Data = MEM_ALUResult;
		MEM_Addr = {MEM_WriteRegister4, MEM_WriteRegister3, MEM_WriteRegister2, MEM_WriteRegister1, MEM_WriteRegister0};
		MEM_WE = MEM_RegWrite;
		MEM_Addr1 = MEM_ALUResult % my_pkg::MEM_SIZE;
		if (MEM_Addr1 < 0)
			MEM_Addr1 = MEM_Addr1 + my_pkg::MEM_SIZE;

		// Memory
		MEM_ReadData = m_reg_model.m_data_mem_gr.my_mem_cell[MEM_Addr1].get();

		// check data written to mem
		if (MEM_MemWrite) begin
			m_reg_model.m_data_mem_gr.my_mem_cell[MEM_Addr1].predict(MEM_WriteDataOfMem, -1, UVM_PREDICT_DIRECT, UVM_BACKDOOR);
			m_reg_model.m_data_mem_gr.my_mem_cell[MEM_Addr1].SetInstr(Instr);
			m_reg_model.m_data_mem_gr.my_mem_cell[MEM_Addr1].mirror(status, UVM_CHECK, UVM_BACKDOOR);
			m_reg_model.m_data_mem_gr.my_mem_cell[MEM_Addr1].poke(status, MEM_WriteDataOfMem);	
		end

		// check values in DUT
		m_reg_model.m_pipeline_gr.MEMWB_ReadDataOfMem.predict(MEM_ReadData, -1, UVM_PREDICT_DIRECT, UVM_BACKDOOR);
		m_reg_model.m_pipeline_gr.MEMWB_ReadDataOfMem.SetInstr(Instr);
		m_reg_model.m_pipeline_gr.MEMWB_ReadDataOfMem.mirror(status, UVM_CHECK, UVM_BACKDOOR);
		m_reg_model.m_pipeline_gr.MEMWB_ReadDataOfMem.poke(status, MEM_ReadData);

		m_reg_model.m_pipeline_gr.MEMWB_ALUResult.predict(MEM_ALUResult, -1, UVM_PREDICT_DIRECT, UVM_BACKDOOR);
		m_reg_model.m_pipeline_gr.MEMWB_ALUResult.SetInstr(Instr);
		m_reg_model.m_pipeline_gr.MEMWB_ALUResult.mirror(status, UVM_CHECK, UVM_BACKDOOR);
		m_reg_model.m_pipeline_gr.MEMWB_ALUResult.poke(status, MEM_ALUResult);

		m_reg_model.m_pipeline_gr.MEMWB_WriteRegister4.predict(MEM_WriteRegister4, -1, UVM_PREDICT_DIRECT, UVM_BACKDOOR);
		m_reg_model.m_pipeline_gr.MEMWB_WriteRegister4.SetInstr(Instr);
		m_reg_model.m_pipeline_gr.MEMWB_WriteRegister4.mirror(status, UVM_CHECK, UVM_BACKDOOR);
		m_reg_model.m_pipeline_gr.MEMWB_WriteRegister4.poke(status, MEM_WriteRegister4);

		m_reg_model.m_pipeline_gr.MEMWB_WriteRegister3.predict(MEM_WriteRegister3, -1, UVM_PREDICT_DIRECT, UVM_BACKDOOR);
		m_reg_model.m_pipeline_gr.MEMWB_WriteRegister3.SetInstr(Instr);
		m_reg_model.m_pipeline_gr.MEMWB_WriteRegister3.mirror(status, UVM_CHECK, UVM_BACKDOOR);
		m_reg_model.m_pipeline_gr.MEMWB_WriteRegister3.poke(status, MEM_WriteRegister3);

		m_reg_model.m_pipeline_gr.MEMWB_WriteRegister2.predict(MEM_WriteRegister2, -1, UVM_PREDICT_DIRECT, UVM_BACKDOOR);
		m_reg_model.m_pipeline_gr.MEMWB_WriteRegister2.SetInstr(Instr);
		m_reg_model.m_pipeline_gr.MEMWB_WriteRegister2.mirror(status, UVM_CHECK, UVM_BACKDOOR);
		m_reg_model.m_pipeline_gr.MEMWB_WriteRegister2.poke(status, MEM_WriteRegister2);

		m_reg_model.m_pipeline_gr.MEMWB_WriteRegister1.predict(MEM_WriteRegister1, -1, UVM_PREDICT_DIRECT, UVM_BACKDOOR);
		m_reg_model.m_pipeline_gr.MEMWB_WriteRegister1.SetInstr(Instr);
		m_reg_model.m_pipeline_gr.MEMWB_WriteRegister1.mirror(status, UVM_CHECK, UVM_BACKDOOR);
		m_reg_model.m_pipeline_gr.MEMWB_WriteRegister1.poke(status, MEM_WriteRegister1);

		m_reg_model.m_pipeline_gr.MEMWB_WriteRegister0.predict(MEM_WriteRegister0, -1, UVM_PREDICT_DIRECT, UVM_BACKDOOR);
		m_reg_model.m_pipeline_gr.MEMWB_WriteRegister0.SetInstr(Instr);
		m_reg_model.m_pipeline_gr.MEMWB_WriteRegister0.mirror(status, UVM_CHECK, UVM_BACKDOOR);
		m_reg_model.m_pipeline_gr.MEMWB_WriteRegister0.poke(status, MEM_WriteRegister0);

		m_reg_model.m_pipeline_gr.MEMWB_MemtoReg.predict(MEM_MemtoReg, -1, UVM_PREDICT_DIRECT, UVM_BACKDOOR);
		m_reg_model.m_pipeline_gr.MEMWB_MemtoReg.SetInstr(Instr);
		m_reg_model.m_pipeline_gr.MEMWB_MemtoReg.mirror(status, UVM_CHECK, UVM_BACKDOOR);
		m_reg_model.m_pipeline_gr.MEMWB_MemtoReg.poke(status, MEM_MemtoReg);

		m_reg_model.m_pipeline_gr.MEMWB_RegWrite.predict(MEM_RegWrite, -1, UVM_PREDICT_DIRECT, UVM_BACKDOOR);
		m_reg_model.m_pipeline_gr.MEMWB_RegWrite.SetInstr(Instr);
		m_reg_model.m_pipeline_gr.MEMWB_RegWrite.mirror(status, UVM_CHECK, UVM_BACKDOOR);
		m_reg_model.m_pipeline_gr.MEMWB_RegWrite.poke(status, MEM_RegWrite);
	endtask

	task EvalWBstage (string Instr, output bit WB_WE, output int WB_Addr, output int WB_Data);
		int WB_ReadDataOfMem, WB_ALUResult;
		bit WB_WriteRegister4, WB_WriteRegister3, WB_WriteRegister2, WB_WriteRegister1, WB_WriteRegister0, WB_MemtoReg, WB_RegWrite;
		
		// get values form MEMWB registers
		WB_ReadDataOfMem = m_reg_model.m_pipeline_gr.MEMWB_ReadDataOfMem.get();
		WB_ALUResult = m_reg_model.m_pipeline_gr.MEMWB_ALUResult.get();
		WB_WriteRegister4 = m_reg_model.m_pipeline_gr.MEMWB_WriteRegister4.get();
		WB_WriteRegister3 = m_reg_model.m_pipeline_gr.MEMWB_WriteRegister3.get();
		WB_WriteRegister2 = m_reg_model.m_pipeline_gr.MEMWB_WriteRegister2.get();
		WB_WriteRegister1 = m_reg_model.m_pipeline_gr.MEMWB_WriteRegister1.get();
		WB_WriteRegister0 = m_reg_model.m_pipeline_gr.MEMWB_WriteRegister0.get();
		WB_MemtoReg = m_reg_model.m_pipeline_gr.MEMWB_MemtoReg.get();
		WB_RegWrite = m_reg_model.m_pipeline_gr.MEMWB_RegWrite.get();

		WB_WE = WB_RegWrite;
		WB_Addr = {WB_WriteRegister4, WB_WriteRegister3, WB_WriteRegister2, WB_WriteRegister1, WB_WriteRegister0};
		if (WB_MemtoReg)
			WB_Data = WB_ReadDataOfMem;
		else
			WB_Data = WB_ALUResult;

		// reg 0
		if (WB_Addr == 0)
			WB_Data = 0;

		// check values in DUT
		if (WB_WE) begin
			m_reg_model.m_GPR_gr.my_GPR[WB_Addr].predict(WB_Data, -1, UVM_PREDICT_DIRECT, UVM_BACKDOOR);
			m_reg_model.m_GPR_gr.my_GPR[WB_Addr].SetInstr(Instr);
			m_reg_model.m_GPR_gr.my_GPR[WB_Addr].mirror(status, UVM_CHECK, UVM_BACKDOOR);
			m_reg_model.m_GPR_gr.my_GPR[WB_Addr].poke(status, WB_Data);
		end
	endtask

	task Driver (int Opcode, output bit RegDst, output bit ALUSrc, output bit MemtoReg, output bit RegWrite, output bit MemRead, output bit MemWrite, output bit Branch, output bit ALUOp1, output bit ALUOp0, output bit Jump, output bit SignZero);
		case(Opcode)
			6'b000000 : {RegDst, ALUSrc, MemtoReg, RegWrite, MemRead, MemWrite, Branch, ALUOp1, ALUOp0, Jump, SignZero} = 11'b10010001000;
			6'b100011 : {RegDst, ALUSrc, MemtoReg, RegWrite, MemRead, MemWrite, Branch, ALUOp1, ALUOp0, Jump, SignZero} = 11'b01111000000;
			6'b101011 : {RegDst, ALUSrc, MemtoReg, RegWrite, MemRead, MemWrite, Branch, ALUOp1, ALUOp0, Jump, SignZero} = 11'bx1x00100000;
			6'b000101 : {RegDst, ALUSrc, MemtoReg, RegWrite, MemRead, MemWrite, Branch, ALUOp1, ALUOp0, Jump, SignZero} = 11'b00000010100;
			6'b001110 : {RegDst, ALUSrc, MemtoReg, RegWrite, MemRead, MemWrite, Branch, ALUOp1, ALUOp0, Jump, SignZero} = 11'b01010001101;
			6'b000010 : {RegDst, ALUSrc, MemtoReg, RegWrite, MemRead, MemWrite, Branch, ALUOp1, ALUOp0, Jump, SignZero} = 11'b00000000010;
			default : {RegDst, ALUSrc, MemtoReg, RegWrite, MemRead, MemWrite, Branch, ALUOp1, ALUOp0, Jump, SignZero} =   11'b00000001000;
		endcase
	endtask

	task UpdatePipeline ();
		int val;
		m_reg_model.m_pipeline_gr.IFID_PC4.peek(status, val);
		m_reg_model.m_pipeline_gr.IFID_Instruction.peek(status, val);
		m_reg_model.m_pipeline_gr.IF_flush_bit.peek(status, val);
	
		m_reg_model.m_pipeline_gr.IDEX_PC4.peek(status, val);
		m_reg_model.m_pipeline_gr.IDEX_ReadData1.peek(status, val);
		m_reg_model.m_pipeline_gr.IDEX_ReadData2.peek(status, val);
		m_reg_model.m_pipeline_gr.IDEX_Im16_Ext.peek(status, val);
		m_reg_model.m_pipeline_gr.IDEX_rs_rt_rd.peek(status, val);
		m_reg_model.m_pipeline_gr.IDEX_RegDst.peek(status, val);
		m_reg_model.m_pipeline_gr.IDEX_ALUSrc.peek(status, val);
		m_reg_model.m_pipeline_gr.IDEX_MemtoReg.peek(status, val);
		m_reg_model.m_pipeline_gr.IDEX_RegWrite.peek(status, val);
		m_reg_model.m_pipeline_gr.IDEX_MemRead.peek(status, val);
		m_reg_model.m_pipeline_gr.IDEX_MemWrite.peek(status, val);
		m_reg_model.m_pipeline_gr.IDEX_Branch.peek(status, val);
		m_reg_model.m_pipeline_gr.IDEX_JRControl.peek(status, val);
		m_reg_model.m_pipeline_gr.IDEX_ALUOp1.peek(status, val);
		m_reg_model.m_pipeline_gr.IDEX_ALUOp0.peek(status, val);

		m_reg_model.m_pipeline_gr.EXMEM_ALUResult.peek(status, val);
		m_reg_model.m_pipeline_gr.EXMEM_WriteDataOfMem.peek(status, val);
		m_reg_model.m_pipeline_gr.EXMEM_MemtoReg.peek(status, val);
		m_reg_model.m_pipeline_gr.EXMEM_RegWrite.peek(status, val);
		m_reg_model.m_pipeline_gr.EXMEM_MemRead.peek(status, val);
		m_reg_model.m_pipeline_gr.EXMEM_MemWrite.peek(status, val);
		m_reg_model.m_pipeline_gr.EXMEM_WriteRegister4.peek(status, val);
		m_reg_model.m_pipeline_gr.EXMEM_WriteRegister3.peek(status, val);
		m_reg_model.m_pipeline_gr.EXMEM_WriteRegister2.peek(status, val);
		m_reg_model.m_pipeline_gr.EXMEM_WriteRegister1.peek(status, val);
		m_reg_model.m_pipeline_gr.EXMEM_WriteRegister0.peek(status, val);

		m_reg_model.m_pipeline_gr.MEMWB_ReadDataOfMem.peek(status, val);
		m_reg_model.m_pipeline_gr.MEMWB_ALUResult.peek(status, val);
		m_reg_model.m_pipeline_gr.MEMWB_WriteRegister4.peek(status, val);
		m_reg_model.m_pipeline_gr.MEMWB_WriteRegister3.peek(status, val);
		m_reg_model.m_pipeline_gr.MEMWB_WriteRegister2.peek(status, val);
		m_reg_model.m_pipeline_gr.MEMWB_WriteRegister1.peek(status, val);
		m_reg_model.m_pipeline_gr.MEMWB_WriteRegister0.peek(status, val);
		m_reg_model.m_pipeline_gr.MEMWB_MemtoReg.peek(status, val);
		m_reg_model.m_pipeline_gr.MEMWB_RegWrite.peek(status, val);

		m_reg_model.m_GPR_gr.my_PC.peek(status, val);
	
		for (int i = 0; i < 32; i++)
			m_reg_model.m_GPR_gr.my_GPR[i].peek(status, val);

		for (int i = 0; i < my_pkg::MEM_SIZE; i++)
			m_reg_model.m_data_mem_gr.my_mem_cell[i].peek(status, val);
	endtask
endclass