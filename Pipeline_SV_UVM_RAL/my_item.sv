`timescale 1ns/10ps
`include "uvm_macros.svh"

import uvm_pkg::*;

class my_item extends uvm_sequence_item;
	bit [31:0] instruction;
  
	`uvm_object_utils_begin(my_item)
		`uvm_field_int(instruction, UVM_DEFAULT)
	`uvm_object_utils_end
 
	covergroup item_cover_instr;
		option.auto_bin_max = 8;
		OP_ALU: coverpoint instruction[31:26]
		{
			bins x[] = {6'b000000}; 
		}
		OP_J: coverpoint instruction[31:26]
		{
			bins x[] = {6'b000010}; 
		}
		OP_BNE: coverpoint instruction[31:26]
		{
			bins x[] = {6'b000101}; 
		}
		OP_XORI: coverpoint instruction[31:26]
		{
			bins x[] = {6'b001110}; 
		}
		OP_MEM: coverpoint instruction[31:26]
		{
			bins x[] = {6'b100011, 6'b101011}; 
		}
		ALU_FUNCT: coverpoint instruction[10:0]
		{
			bins x[] = {11'b00000100000, 11'b00000100010, 11'b00000101010};
		}
		JR_FUNCT: coverpoint instruction[10:0]
		{
			bins x[] = {11'b00000001000};
		}
		REGS: coverpoint instruction[25:21];
		REGT: coverpoint instruction[20:16];
		REGD: coverpoint instruction[15:11];
		IMM16:  coverpoint instruction[15:0];
		IMM26:  coverpoint instruction[25:0];

		ALU: cross OP_ALU, ALU_FUNCT, REGS, REGT, REGD;
		JR: cross OP_ALU, JR_FUNCT, REGS;
		J: cross OP_J, IMM26;
		BNE: cross OP_BNE, REGS, REGT, IMM16;
		XORI: cross OP_XORI, REGS, REGT, IMM16; 
		LWSW: cross OP_MEM, REGS, REGT, IMM16; 
	endgroup

	function new (string name = "my_item");
		super.new(name);
		item_cover_instr = new();
	endfunction

endclass
