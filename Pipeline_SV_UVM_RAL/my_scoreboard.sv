`timescale 1ns/10ps

`include "uvm_macros.svh"

import uvm_pkg::*;

class my_scoreboard extends uvm_scoreboard;
	`uvm_component_utils(my_scoreboard)
	
	// port for communication with monitor
	uvm_tlm_analysis_fifo #(int) instr_fifo;

	my_pkg::my_reg_model m_reg_model;
	uvm_status_e status;
	virtual my_if m_if;
	bit enable_test;
	int stall;

	function new (string name = "my_scoreboard", uvm_component parent = null);
		super.new(name, parent);
		enable_test = 1'b0;
	endfunction

	function void build_phase (uvm_phase phase);
		super.build_phase(phase);
		instr_fifo = new("instr_fifo", this);
	endfunction

	task run_phase (uvm_phase phase);
		super.run_phase(phase);
		phase.raise_objection(this);

		// get reference to the reg model from uvm databese
		if(!uvm_config_db#(my_pkg::my_reg_model)::get(null, "uvm_top", "m_reg_model", m_reg_model))
			`uvm_fatal("NOVIF", {"Missing my_reg_model: ", get_full_name(), ".m_reg_model"})

		// get reference to the DUT interface from uvm databese
		if(!uvm_config_db#(virtual my_if)::get(null, "uvm_top", "m_if", m_if))
			`uvm_fatal("NOVIF", {"Missing interface: ", get_full_name(), ".m_if"})

		scoreboard_match(phase);
	endtask

	task scoreboard_match(uvm_phase phase);
		my_pkg::my_item item = my_pkg::my_item::type_id::create();
		int ignore = 0;
		int target, prev_target = 32;
		int instr_cnt = 0;
		uvm_report_server server = get_report_server();
		forever begin
			instr_fifo.get(item.instruction);
			//`uvm_info("", $sformatf("New instruction: %b ignore: %0d time: %0d", item.instruction, ignore, $time()), UVM_LOW)

			// ignore instructions after flush
      			if (ignore > 0) begin
				ignore--;
				continue;
			end

			// ignore instructions during init phase
			if (!enable_test)
				continue;

			// decode instructions
			case(item.instruction[31:26])
				6'b000000: EvalALU_OP(item, prev_target, target, ignore);
				6'b000010: EvalJ(item, prev_target, target, ignore);
				6'b000101: EvalBne(item, prev_target, target, ignore);
				6'b001110: EvalXori(item, prev_target, target, ignore);
				6'b101011: EvalSw(item, prev_target, target, ignore);
				6'b100011: EvalLw(item, prev_target, target, ignore);
				default: continue;
			endcase
			prev_target = target;
			item.item_cover_instr.sample();
			instr_cnt++;

			if (stall)
				`uvm_info("", $sformatf("\n\nSTALL\n\n"), UVM_LOW)

			/*
			// stop simulation in first error
			if (server.get_severity_count(UVM_ERROR) != 0)
				$stop();
			*/

      			if ($get_coverage() > 95) begin
				`uvm_info(get_full_name(), $sformatf("\n\nScoreboard: coverage done -> finishing test.\n\nNumber of instructions: %0d\nNumber of errors: %0d\n", instr_cnt, server.get_severity_count(UVM_ERROR)), UVM_LOW)  
				phase.drop_objection(this);
			end
		end
	endtask

	/*
		ignore - num of instruction to ignore (after jump - flush)
		target - current target register
		prev_target - target reg of prev instruction -> to detect stall (if prev target = current operand -> stall 1 clk)
	*/

	// tasks for evaluating instructions
	task EvalALU_OP (my_pkg::my_item item, int prev_target, output int target, output int ignore);
		string instr;
		int reg1, reg2, PC, res;
		stall = 0;
		if (item.instruction[10:0] == 11'b00000001000) begin
			EvalJr(item, prev_target, target, ignore);
			return;
		end
		target = 32;
		ignore = 0;
		
		if ((item.instruction[20:16] == prev_target) || (item.instruction[25:21] == prev_target))
			stall = 1;
			
		reg2 = m_reg_model.m_GPR_gr.my_GPR[item.instruction[20:16]].GetVal();
		reg1 = m_reg_model.m_GPR_gr.my_GPR[item.instruction[25:21]].GetVal();		
		PC = m_reg_model.m_GPR_gr.my_PC.GetVal();
		PC = PC + 4;

		case(item.instruction[10:0])
			11'b00000100000: begin res = reg1 + reg2; instr = $sformatf("ADD %0d = %0d + %0d  (%b)", item.instruction[15:11], item.instruction[20:16], item.instruction[25:21], item.instruction); end
			11'b00000100010: begin res = reg1 - reg2; instr = $sformatf("SUB %0d = %0d - %0d  (%b)", item.instruction[15:11], item.instruction[20:16], item.instruction[25:21], item.instruction); end
			11'b00000101010: begin res = (reg1 < reg2 ? 1 : 0); instr = $sformatf("SLT %0d = %0d < %0d  (%b)", item.instruction[15:11], item.instruction[20:16], item.instruction[25:21], item.instruction); end
			default: begin `uvm_info(get_full_name(), "Unknown ALU OP!", UVM_LOW); return; end
		endcase	
			
 		if (item.instruction[15:11] == 0)
			res = 0;

		fork
			CheckReg(item.instruction[15:11], res, 5 + stall, instr);
		join_none
		fork
			CheckPC(PC, 1 + stall, instr);
		join_none

		m_reg_model.m_GPR_gr.my_GPR[item.instruction[15:11]].SetVal(res);
		m_reg_model.m_GPR_gr.my_PC.SetVal(PC); 
		ignore = ignore + stall;
	endtask

	task EvalJr (my_pkg::my_item item, int prev_target, output int target, output int ignore);
		string instr = $sformatf("Jr %0d  (%b)", item.instruction[25:21], item.instruction);
		int PC;
		stall = 0;
		target = 32;
		ignore = 2;

		if (item.instruction[25:21] == prev_target)
			stall = 1;
		
		PC = m_reg_model.m_GPR_gr.my_GPR[item.instruction[25:21]].GetVal();
		fork
			CheckPC(PC, 3 + stall, instr);
		join_none
		m_reg_model.m_GPR_gr.my_PC.SetVal(PC);
		ignore = ignore + stall;
	endtask

	task EvalJ (my_pkg::my_item item, int prev_target, output int target, output int ignore);
		string instr = $sformatf("J  (%b)", item.instruction);
		int PC;
		stall = 0;
		target = 32;
		ignore = 1;
		
		PC = m_reg_model.m_GPR_gr.my_PC.GetVal();
		PC = PC + 4;
		PC = {PC[31:28], item.instruction[25:0], 2'b00}; 
		fork
			CheckPC(PC, 2 + stall, instr);
		join_none
		m_reg_model.m_GPR_gr.my_PC.SetVal(PC);
		ignore = ignore + stall;
	endtask

	task EvalBne (my_pkg::my_item item, int prev_target, output int target, output int ignore);
		string instr = $sformatf("Bne %0d != %0d  (%b)", item.instruction[20:16], item.instruction[25:21], item.instruction);
		int reg1, reg2, PC, imm;
		stall = 0;
		target = 32;
		ignore = 0;

		if ((item.instruction[20:16] == prev_target) || (item.instruction[25:21] == prev_target))
			stall = 1;
		
		reg1 = m_reg_model.m_GPR_gr.my_GPR[item.instruction[20:16]].GetVal();
		reg2 = m_reg_model.m_GPR_gr.my_GPR[item.instruction[25:21]].GetVal();			
		PC = m_reg_model.m_GPR_gr.my_PC.GetVal();
		PC = PC + 4;
		imm = {{16{item.instruction[15]}}, item.instruction[15:0]};
		imm = imm << 2;
		if (reg1 != reg2) begin
			PC = PC + imm;
			ignore = 2;
			fork	
				CheckPC(PC, 3 + stall, instr);
			join_none
		end
		else begin
			fork	
				CheckPC(PC, 1 + stall, instr);
			join_none
		end

		m_reg_model.m_GPR_gr.my_PC.SetVal(PC);
		ignore = ignore + stall;
	endtask

	task EvalXori (my_pkg::my_item item, int prev_target, output int target, output int ignore);
		string instr = $sformatf("Xori %0d = %0d ^ imm  (%b)", item.instruction[20:16], item.instruction[25:21], item.instruction);
		int reg1, PC, imm, res;
		stall = 0;
		target = 32;
		ignore = 0;

		if (item.instruction[25:21] == prev_target)
			stall = 1;
		
		reg1 = m_reg_model.m_GPR_gr.my_GPR[item.instruction[25:21]].GetVal();			
		PC = m_reg_model.m_GPR_gr.my_PC.GetVal();
		PC = PC + 4;
		imm = {{16{1'b0}}, item.instruction[15:0]};
		res = reg1 ^ imm;

		if (item.instruction[20:16] == 0)
			res = 0;

		fork
			CheckReg(item.instruction[20:16], res, 5 + stall, instr);
		join_none
		fork
			CheckPC(PC, 1 + stall, instr);
		join_none
					
		m_reg_model.m_GPR_gr.my_GPR[item.instruction[20:16]].SetVal(res);			
		m_reg_model.m_GPR_gr.my_PC.SetVal(PC);
		ignore = ignore + stall;
	endtask

	task EvalSw (my_pkg::my_item item, int prev_target, output int target, output int ignore);
		string instr = $sformatf("Sw M[%0d + imm] = %0d (%b)", item.instruction[25:21], item.instruction[20:16], item.instruction);
		int reg1, reg2, PC, imm, addr;
		stall = 0;
		target = 32;
		ignore = 0;

		if ((item.instruction[20:16] == prev_target) || (item.instruction[25:21] == prev_target))
			stall = 1;
		
		reg1 = m_reg_model.m_GPR_gr.my_GPR[item.instruction[25:21]].GetVal();
		reg2 = m_reg_model.m_GPR_gr.my_GPR[item.instruction[20:16]].GetVal();		
		PC = m_reg_model.m_GPR_gr.my_PC.GetVal();
		PC = PC + 4;
		imm = {{16{item.instruction[15]}}, item.instruction[15:0]};
		addr = reg1 + imm;
		addr = addr % my_pkg::MEM_SIZE;
		if (addr < 0) 
			addr = addr + my_pkg::MEM_SIZE;

		fork
			CheckPC(PC, 1 + stall, instr);
		join_none
		fork
			CheckMem(addr, reg2, 5 + stall, instr);
		join_none

		m_reg_model.m_data_mem_gr.my_mem_cell[addr].SetVal(reg2);
		m_reg_model.m_GPR_gr.my_PC.SetVal(PC);
		ignore = ignore + stall;
	endtask

	task EvalLw (my_pkg::my_item item, int prev_target, output int target, output int ignore);
		string instr = $sformatf("Lw %0d = M[%0d + imm] (%b)", item.instruction[20:16], item.instruction[25:21], item.instruction);
		int reg1, PC, imm, mem, addr;
		stall = 0;
		target = item.instruction[20:16];
		ignore = 0;

		if (item.instruction[25:21] == prev_target)
			stall = 1;
		
		reg1 = m_reg_model.m_GPR_gr.my_GPR[item.instruction[25:21]].GetVal();	
		PC = m_reg_model.m_GPR_gr.my_PC.GetVal();
		PC = PC + 4;	
		imm = {{16{item.instruction[15]}}, item.instruction[15:0]};
		addr = reg1 + imm;
		addr = addr % my_pkg::MEM_SIZE;
		if (addr < 0) 
			addr = addr + my_pkg::MEM_SIZE;
		mem = m_reg_model.m_data_mem_gr.my_mem_cell[addr].GetVal();

		if (target == 0)
			mem = 0;

		fork
			CheckPC(PC, 1 + stall, instr);
		join_none
		fork
			CheckReg(target, mem, 5 + stall, instr);
		join_none	

		m_reg_model.m_GPR_gr.my_GPR[target].SetVal(mem);
		m_reg_model.m_GPR_gr.my_PC.SetVal(PC);
		ignore = ignore + stall;	
	endtask

	/* tasks for checking results
		addr - address
		val - value to check
		delay - number of CLK cycles to wait until checking
		instr - instruction description
	*/
	task CheckReg (int addr, int val, int delay, string instr);
		for (int i = 0; i < delay; i++)
			@(posedge m_if.clk);
		@(negedge m_if.clk);
		m_reg_model.m_GPR_gr.my_GPR[addr].predict(val, -1, UVM_PREDICT_DIRECT, UVM_BACKDOOR);
		m_reg_model.m_GPR_gr.my_GPR[addr].SetInstr(instr);
		m_reg_model.m_GPR_gr.my_GPR[addr].mirror(status, UVM_CHECK, UVM_BACKDOOR);
		m_reg_model.m_GPR_gr.my_GPR[addr].poke(status, val);
	endtask

	task CheckPC (int val, int delay, string instr);
		for (int i = 0; i < delay; i++)
			@(posedge m_if.clk);
		@(negedge m_if.clk);
		m_reg_model.m_GPR_gr.my_PC.predict(val, -1, UVM_PREDICT_DIRECT, UVM_BACKDOOR);
		m_reg_model.m_GPR_gr.my_PC.SetInstr(instr);
		m_reg_model.m_GPR_gr.my_PC.mirror(status, UVM_CHECK, UVM_BACKDOOR);
		m_reg_model.m_GPR_gr.my_PC.poke(status, val);
	endtask

	task CheckMem (int addr, int val, int delay, string instr);
		for (int i = 0; i < delay; i++)
			@(posedge m_if.clk);
		@(negedge m_if.clk);
		m_reg_model.m_data_mem_gr.my_mem_cell[addr].predict(val, -1, UVM_PREDICT_DIRECT, UVM_BACKDOOR);
		m_reg_model.m_data_mem_gr.my_mem_cell[addr].SetInstr(instr);
		m_reg_model.m_data_mem_gr.my_mem_cell[addr].mirror(status, UVM_CHECK, UVM_BACKDOOR);
		m_reg_model.m_data_mem_gr.my_mem_cell[addr].poke(status, val);
	endtask
endclass