`timescale 1ns/10ps
`include "uvm_macros.svh"

import uvm_pkg::*;

class my_instruction extends uvm_sequence_item;

	rand bit [31:0] instruction;
	static bit [4:0] prev_target [2:0];
	
	function void post_randomize ();
		bit [4:0] target = 0;
		int rnd;
		int target_pos = 16;

		// jr
		if ((instruction[31:26] == 6'b000000) && (instruction[10:0] == 11'b00000001000))
			instruction[20:0] = 21'b000000000000000001000;

		// hazard generation control
		case(instruction[31:26])
			6'b000000: begin target_pos = 11; target = instruction[15:11]; end
			6'b000010: target = 0;
			6'b000101: target = 0;
			6'b001110: target = instruction[20:16];
			6'b101011: target = 0;
			6'b100011: target = instruction[20:16];
		endcase

		if (target != 0) begin
			rnd = $urandom() % 15;
			if (rnd < 3) begin
				if (target_pos == 16)
					instruction[20:16] = prev_target[rnd];
				else
					instruction[15:11] = prev_target[rnd];
			end
		end

		prev_target[2] = prev_target[1];
		prev_target[1] = prev_target[0];
		prev_target[0] = target;
	endfunction

	constraint valid_instruction
	{
		instruction[31:26] dist {6'b000000 := 4, 6'b000010 := 1, 6'b000101 := 1, 6'b001110 := 1, 6'b101011 := 1, 6'b100011 := 1};
	}

	constraint valid_ALU_OP
	{
		(instruction[31:26] == 6'b000000) -> (instruction[10:0] inside {11'b00000100000, 11'b00000100010, 11'b00000101010, 11'b00000001000});
	}

	constraint Bne_Balance
	{
		((instruction[31:26] == 6'b000101) && (instruction[5])) -> (instruction[25:21] == instruction[20:16]);
	}

	`uvm_object_utils_begin(my_instruction)
		`uvm_field_int(instruction, UVM_DEFAULT)
	`uvm_object_utils_end
 
	covergroup item_cover_instr;
		option.auto_bin_max = 8;
		OP_ALU: coverpoint instruction[31:26]
		{
			bins x[] = {6'b000000}; 
		}
		OP_J: coverpoint instruction[31:26]
		{
			bins x[] = {6'b000010}; 
		}
		OP_BNE: coverpoint instruction[31:26]
		{
			bins x[] = {6'b000101}; 
		}
		OP_XORI: coverpoint instruction[31:26]
		{
			bins x[] = {6'b001110}; 
		}
		OP_MEM: coverpoint instruction[31:26]
		{
			bins x[] = {6'b100011, 6'b101011}; 
		}
		ALU_FUNCT: coverpoint instruction[10:0]
		{
			bins x[] = {11'b00000100000, 11'b00000100010, 11'b00000101010};
		}
		JR_FUNCT: coverpoint instruction[10:0]
		{
			bins x[] = {11'b00000001000};
		}
		REGS: coverpoint instruction[25:21];
		REGT: coverpoint instruction[20:16];
		REGD: coverpoint instruction[15:11];
		IMM16:  coverpoint instruction[15:0];
		IMM26:  coverpoint instruction[25:0];

		ALU: cross OP_ALU, ALU_FUNCT, REGS, REGT, REGD;
		JR: cross OP_ALU, JR_FUNCT, REGS;
		J: cross OP_J, IMM26;
		BNE: cross OP_BNE, REGS, REGT, IMM16;
		XORI: cross OP_XORI, REGS, REGT, IMM16; 
		LWSW: cross OP_MEM, REGS, REGT, IMM16; 
	endgroup
 
	function new (string name = "my_instruction");
		super.new(name);
		item_cover_instr = new();
	endfunction
endclass
