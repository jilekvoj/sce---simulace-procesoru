`timescale 1ns/10ps

import uvm_pkg::*;

`include "my_pkg.sv"
import my_pkg::*;


module uvm_top;
	bit T_CLK;

	always
		#5 T_CLK = ~T_CLK;

	initial begin
		Interface.reset <= 1'b0;
		T_CLK <= 1'b1;
		#1ns;
		Interface.reset <= 1'b1;
		#2ns;
		Interface.reset <= 1'b0;
	end

	MIPSpipeline  _dut(T_CLK, Interface.reset);
	my_if Interface(T_CLK);

	initial begin
		// save reference to DUT interface to uvm database
		uvm_config_db#(virtual my_if)::set(uvm_root::get(), "*", "m_if", Interface);
		run_test("my_test");
	end
endmodule
