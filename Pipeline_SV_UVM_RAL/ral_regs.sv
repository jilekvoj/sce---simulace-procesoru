`include "uvm_macros.svh"
import uvm_pkg::*;

class my_reg extends uvm_reg;
	`uvm_object_utils(my_reg)
    
	rand uvm_reg_field data;
	string instr;

	// aditional value that holds "future" value of register (computed when instruction is in IF stage)
	// it helps with "single cycle" simulation of pipeline procesor 
	int my_val;

	function new (string name = "intr");
		super.new(name, 32, UVM_NO_COVERAGE);
	endfunction
 
	function void build;
		data = uvm_reg_field::type_id::create("data");  
		data.configure(this, 32, 0, "RW", 0, 0, 0, 1, 0);
	endfunction

	task SetInstr (string instruction);
		instr = instruction;
	endtask

	// override do_check of the base class - custom error message
	function bit do_check(input uvm_reg_data_t expected, input uvm_reg_data_t actual, uvm_reg_map map);
		uvm_reg_data_t  valid_bits_mask = 0;
		foreach(m_fields[i]) begin
			string acc = m_fields[i].get_access(map);
			acc = acc.substr(0, 1);
			if (!(m_fields[i].get_compare() == UVM_NO_CHECK ||acc == "WO")) begin
				valid_bits_mask |= ((1 << m_fields[i].get_n_bits())-1)<< m_fields[i].get_lsb_pos();
			end
		end

		if ((actual&valid_bits_mask) === (expected&valid_bits_mask)) return 1;

		`uvm_error("RegModel", $sformatf("Error - instruction %s\nWrong value in DUT - %s\nExpected value:%b (%0d), got:%b (%0d)", instr, get_full_name(), expected, expected, actual, actual))

		return 0;
	endfunction

	task SetVal (int val);
		my_val = val;
	endtask

	function int GetVal ();
		return my_val;
	endfunction

endclass

class my_1bit_reg extends uvm_reg;
	`uvm_object_utils(my_1bit_reg)
    
	rand uvm_reg_field data;
	string instr;

	function new (string name = "intr");
		super.new(name, 1, UVM_NO_COVERAGE);
	endfunction
 
	function void build;
		data = uvm_reg_field::type_id::create("data");  
		data.configure(this, 1, 0, "RW", 0, 0, 0, 1, 0);
	endfunction

	task SetInstr (string instruction);
		instr = instruction;
	endtask

	function bit do_check(input uvm_reg_data_t expected, input uvm_reg_data_t actual, uvm_reg_map map);
		uvm_reg_data_t  valid_bits_mask = 0;
		foreach(m_fields[i]) begin
			string acc = m_fields[i].get_access(map);
			acc = acc.substr(0, 1);
			if (!(m_fields[i].get_compare() == UVM_NO_CHECK ||acc == "WO")) begin
				valid_bits_mask |= ((1 << m_fields[i].get_n_bits())-1)<< m_fields[i].get_lsb_pos();
			end
		end

		if ((actual&valid_bits_mask) === (expected&valid_bits_mask)) return 1;

		`uvm_error("RegModel", $sformatf("Error - instruction %s\nWrong value in DUT - %s\nExpected value:%b, got:%b", instr, get_full_name(), expected, actual))

		return 0;
	endfunction

endclass

class my_GPR_gr extends uvm_reg_block;
	`uvm_object_utils(my_GPR_gr)

	rand my_reg my_GPR[31:0];
	rand my_reg my_PC;

	function new (string name = "");
		super.new(name, build_coverage(UVM_NO_COVERAGE));
	endfunction
 
	function void build;
		default_map = create_map("my_map", 0, 132, UVM_LITTLE_ENDIAN);
		for (int i = 0; i < 32; i++) begin
			my_GPR[i] = my_reg::type_id::create($sformatf("my_GPR%d", i));
			my_GPR[i].build();
			my_GPR[i].configure(this);
			my_GPR[i].add_hdl_path_slice($sformatf("Register_File.RegArray[%0d]", i), 0, my_GPR[i].get_n_bits());
			default_map.add_reg(my_GPR[i], 4 * i, "RW");
		end
		my_PC = my_reg::type_id::create("my_PC");
		my_PC.build();
		my_PC.configure(this);
		my_PC.add_hdl_path_slice("PC", 0, my_PC.get_n_bits());
		default_map.add_reg(my_PC, 128, "RW");
		add_hdl_path("_dut");
	endfunction
endclass

class my_pipeline_gr extends uvm_reg_block;
	`uvm_object_utils(my_pipeline_gr)

	rand my_reg IFID_PC4;
	rand my_reg IFID_Instruction;
	rand my_1bit_reg IF_flush_bit;
	
	rand my_reg IDEX_PC4;
	rand my_reg IDEX_ReadData1;
	rand my_reg IDEX_ReadData2;
	rand my_reg IDEX_Im16_Ext;
	rand my_reg IDEX_rs_rt_rd;
	rand my_1bit_reg IDEX_RegDst;
	rand my_1bit_reg IDEX_ALUSrc;
	rand my_1bit_reg IDEX_MemtoReg;
	rand my_1bit_reg IDEX_RegWrite;
	rand my_1bit_reg IDEX_MemRead;
	rand my_1bit_reg IDEX_MemWrite;
	rand my_1bit_reg IDEX_Branch;
	rand my_1bit_reg IDEX_JRControl;
	rand my_1bit_reg IDEX_ALUOp1;
	rand my_1bit_reg IDEX_ALUOp0;

	rand my_reg EXMEM_ALUResult;
	rand my_reg EXMEM_WriteDataOfMem;
	rand my_1bit_reg EXMEM_MemtoReg;
	rand my_1bit_reg EXMEM_RegWrite;
	rand my_1bit_reg EXMEM_MemRead;
	rand my_1bit_reg EXMEM_MemWrite;
	rand my_1bit_reg EXMEM_WriteRegister4;
	rand my_1bit_reg EXMEM_WriteRegister3;
	rand my_1bit_reg EXMEM_WriteRegister2;
	rand my_1bit_reg EXMEM_WriteRegister1;
	rand my_1bit_reg EXMEM_WriteRegister0;

	rand my_reg MEMWB_ReadDataOfMem;
	rand my_reg MEMWB_ALUResult;
	rand my_1bit_reg MEMWB_WriteRegister4;
	rand my_1bit_reg MEMWB_WriteRegister3;
	rand my_1bit_reg MEMWB_WriteRegister2;
	rand my_1bit_reg MEMWB_WriteRegister1;
	rand my_1bit_reg MEMWB_WriteRegister0;
	rand my_1bit_reg MEMWB_MemtoReg;
	rand my_1bit_reg MEMWB_RegWrite;

	function new (string name = "");
		super.new(name, build_coverage(UVM_NO_COVERAGE));
	endfunction

	function void build;
		default_map = create_map("my_map", 0, 71, UVM_LITTLE_ENDIAN);

		IFID_PC4 = my_reg::type_id::create("IFID_PC4");
		IFID_PC4.build();
		IFID_PC4.configure(this);
		IFID_PC4.add_hdl_path_slice("ID_PC4", 0, IFID_PC4.get_n_bits());
		default_map.add_reg(IFID_PC4, 0, "RW");

		IFID_Instruction = my_reg::type_id::create("IFID_Instruction");
		IFID_Instruction.build();
		IFID_Instruction.configure(this);
		IFID_Instruction.add_hdl_path_slice("ID_Instruction", 0, IFID_Instruction.get_n_bits());
		default_map.add_reg(IFID_Instruction, 4, "RW");

		IF_flush_bit = my_1bit_reg::type_id::create("IF_flush_bit");
		IF_flush_bit.build();
		IF_flush_bit.configure(this);
		IF_flush_bit.add_hdl_path_slice("IFID_flush", 0, IF_flush_bit.get_n_bits());
		default_map.add_reg(IF_flush_bit, 8, "RW");



		IDEX_PC4 = my_reg::type_id::create("IDEX_PC4");
		IDEX_PC4.build();
		IDEX_PC4.configure(this);
		IDEX_PC4.add_hdl_path_slice("EX_PC4", 0, IDEX_PC4.get_n_bits());
		default_map.add_reg(IDEX_PC4, 9, "RW");

		IDEX_ReadData1 = my_reg::type_id::create("IDEX_ReadData1");
		IDEX_ReadData1.build();
		IDEX_ReadData1.configure(this);
		IDEX_ReadData1.add_hdl_path_slice("EX_ReadData1", 0, IDEX_ReadData1.get_n_bits());
		default_map.add_reg(IDEX_ReadData1, 13, "RW");

		IDEX_ReadData2 = my_reg::type_id::create("IDEX_ReadData2");
		IDEX_ReadData2.build();
		IDEX_ReadData2.configure(this);
		IDEX_ReadData2.add_hdl_path_slice("EX_ReadData2", 0, IDEX_ReadData2.get_n_bits());
		default_map.add_reg(IDEX_ReadData2, 17, "RW");

		IDEX_Im16_Ext = my_reg::type_id::create("IDEX_Im16_Ext");
		IDEX_Im16_Ext.build();
		IDEX_Im16_Ext.configure(this);
		IDEX_Im16_Ext.add_hdl_path_slice("EX_Im16_Ext", 0, IDEX_Im16_Ext.get_n_bits());
		default_map.add_reg(IDEX_Im16_Ext, 21, "RW");

		IDEX_rs_rt_rd = my_reg::type_id::create("IDEX_rs_rt_rd");
		IDEX_rs_rt_rd.build();
		IDEX_rs_rt_rd.configure(this);
		IDEX_rs_rt_rd.add_hdl_path_slice("EX_Instruction", 0, IDEX_rs_rt_rd.get_n_bits());
		default_map.add_reg(IDEX_rs_rt_rd, 25, "RW");

		IDEX_RegDst = my_1bit_reg::type_id::create("IDEX_RegDst");
		IDEX_RegDst.build();
		IDEX_RegDst.configure(this);
		IDEX_RegDst.add_hdl_path_slice("EX_RegDst", 0, IDEX_RegDst.get_n_bits());
		default_map.add_reg(IDEX_RegDst, 29, "RW");

		IDEX_ALUSrc = my_1bit_reg::type_id::create("IDEX_ALUSrc");
		IDEX_ALUSrc.build();
		IDEX_ALUSrc.configure(this);
		IDEX_ALUSrc.add_hdl_path_slice("EX_ALUSrc", 0, IDEX_ALUSrc.get_n_bits());
		default_map.add_reg(IDEX_ALUSrc, 30, "RW");

		IDEX_MemtoReg = my_1bit_reg::type_id::create("IDEX_MemtoReg");
		IDEX_MemtoReg.build();
		IDEX_MemtoReg.configure(this);
		IDEX_MemtoReg.add_hdl_path_slice("EX_MemtoReg", 0, IDEX_MemtoReg.get_n_bits());
		default_map.add_reg(IDEX_MemtoReg, 31, "RW");

		IDEX_RegWrite = my_1bit_reg::type_id::create("IDEX_RegWrite");
		IDEX_RegWrite.build();
		IDEX_RegWrite.configure(this);
		IDEX_RegWrite.add_hdl_path_slice("EX_RegWrite", 0, IDEX_RegWrite.get_n_bits());
		default_map.add_reg(IDEX_RegWrite, 32, "RW");

		IDEX_MemRead = my_1bit_reg::type_id::create("IDEX_MemRead");
		IDEX_MemRead.build();
		IDEX_MemRead.configure(this);
		IDEX_MemRead.add_hdl_path_slice("EX_MemRead", 0, IDEX_MemRead.get_n_bits());
		default_map.add_reg(IDEX_MemRead, 33, "RW");

		IDEX_MemWrite = my_1bit_reg::type_id::create("IDEX_MemWrite");
		IDEX_MemWrite.build();
		IDEX_MemWrite.configure(this);
		IDEX_MemWrite.add_hdl_path_slice("EX_MemWrite", 0, IDEX_MemWrite.get_n_bits());
		default_map.add_reg(IDEX_MemWrite, 34, "RW");

		IDEX_Branch = my_1bit_reg::type_id::create("IDEX_Branch");
		IDEX_Branch.build();
		IDEX_Branch.configure(this);
		IDEX_Branch.add_hdl_path_slice("EX_Branch", 0, IDEX_Branch.get_n_bits());
		default_map.add_reg(IDEX_Branch, 35, "RW");

		IDEX_JRControl = my_1bit_reg::type_id::create("IDEX_JRControl");
		IDEX_JRControl.build();
		IDEX_JRControl.configure(this);
		IDEX_JRControl.add_hdl_path_slice("EX_JRControl", 0, IDEX_JRControl.get_n_bits());
		default_map.add_reg(IDEX_JRControl, 36, "RW");

		IDEX_ALUOp1 = my_1bit_reg::type_id::create("IDEX_ALUOp1");
		IDEX_ALUOp1.build();
		IDEX_ALUOp1.configure(this);
		IDEX_ALUOp1.add_hdl_path_slice("EX_ALUOp[1]", 0, IDEX_ALUOp1.get_n_bits());
		default_map.add_reg(IDEX_ALUOp1, 37, "RW");

		IDEX_ALUOp0 = my_1bit_reg::type_id::create("IDEX_ALUOp0");
		IDEX_ALUOp0.build();
		IDEX_ALUOp0.configure(this);
		IDEX_ALUOp0.add_hdl_path_slice("EX_ALUOp[0]", 0, IDEX_ALUOp0.get_n_bits());
		default_map.add_reg(IDEX_ALUOp0, 38, "RW");



		EXMEM_ALUResult = my_reg::type_id::create("EXMEM_ALUResult");
		EXMEM_ALUResult.build();
		EXMEM_ALUResult.configure(this);
		EXMEM_ALUResult.add_hdl_path_slice("MEM_ALUResult", 0, EXMEM_ALUResult.get_n_bits());
		default_map.add_reg(EXMEM_ALUResult, 39, "RW");

		EXMEM_WriteDataOfMem = my_reg::type_id::create("EXMEM_WriteDataOfMem");
		EXMEM_WriteDataOfMem.build();
		EXMEM_WriteDataOfMem.configure(this);
		EXMEM_WriteDataOfMem.add_hdl_path_slice("WriteDataOfMem", 0, EXMEM_WriteDataOfMem.get_n_bits());
		default_map.add_reg(EXMEM_WriteDataOfMem, 43, "RW");

		EXMEM_MemtoReg = my_1bit_reg::type_id::create("EXMEM_MemtoReg");
		EXMEM_MemtoReg.build();
		EXMEM_MemtoReg.configure(this);
		EXMEM_MemtoReg.add_hdl_path_slice("MEM_MemtoReg", 0, EXMEM_MemtoReg.get_n_bits());
		default_map.add_reg(EXMEM_MemtoReg, 47, "RW");

		EXMEM_RegWrite = my_1bit_reg::type_id::create("EXMEM_RegWrite");
		EXMEM_RegWrite.build();
		EXMEM_RegWrite.configure(this);
		EXMEM_RegWrite.add_hdl_path_slice("MEM_RegWrite", 0, EXMEM_RegWrite.get_n_bits());
		default_map.add_reg(EXMEM_RegWrite, 48, "RW");

		EXMEM_MemRead = my_1bit_reg::type_id::create("EXMEM_MemRead");
		EXMEM_MemRead.build();
		EXMEM_MemRead.configure(this);
		EXMEM_MemRead.add_hdl_path_slice("MEM_MemRead", 0, EXMEM_MemRead.get_n_bits());
		default_map.add_reg(EXMEM_MemRead, 49, "RW");

		EXMEM_MemWrite = my_1bit_reg::type_id::create("EXMEM_MemWrite");
		EXMEM_MemWrite.build();
		EXMEM_MemWrite.configure(this);
		EXMEM_MemWrite.add_hdl_path_slice("MEM_MemWrite", 0, EXMEM_MemWrite.get_n_bits());
		default_map.add_reg(EXMEM_MemWrite, 50, "RW");

		EXMEM_WriteRegister4 = my_1bit_reg::type_id::create("EXMEM_WriteRegister4");
		EXMEM_WriteRegister4.build();
		EXMEM_WriteRegister4.configure(this);
		EXMEM_WriteRegister4.add_hdl_path_slice("MEM_WriteRegister[4]", 0, EXMEM_WriteRegister4.get_n_bits());
		default_map.add_reg(EXMEM_WriteRegister4, 51, "RW");

		EXMEM_WriteRegister3 = my_1bit_reg::type_id::create("EXMEM_WriteRegister3");
		EXMEM_WriteRegister3.build();
		EXMEM_WriteRegister3.configure(this);
		EXMEM_WriteRegister3.add_hdl_path_slice("MEM_WriteRegister[3]", 0, EXMEM_WriteRegister3.get_n_bits());
		default_map.add_reg(EXMEM_WriteRegister3, 52, "RW");

		EXMEM_WriteRegister2 = my_1bit_reg::type_id::create("EXMEM_WriteRegister2");
		EXMEM_WriteRegister2.build();
		EXMEM_WriteRegister2.configure(this);
		EXMEM_WriteRegister2.add_hdl_path_slice("MEM_WriteRegister[2]", 0, EXMEM_WriteRegister2.get_n_bits());
		default_map.add_reg(EXMEM_WriteRegister2, 53, "RW");

		EXMEM_WriteRegister1 = my_1bit_reg::type_id::create("EXMEM_WriteRegister1");
		EXMEM_WriteRegister1.build();
		EXMEM_WriteRegister1.configure(this);
		EXMEM_WriteRegister1.add_hdl_path_slice("MEM_WriteRegister[1]", 0, EXMEM_WriteRegister1.get_n_bits());
		default_map.add_reg(EXMEM_WriteRegister1, 54, "RW");

		EXMEM_WriteRegister0 = my_1bit_reg::type_id::create("EXMEM_WriteRegister0");
		EXMEM_WriteRegister0.build();
		EXMEM_WriteRegister0.configure(this);
		EXMEM_WriteRegister0.add_hdl_path_slice("MEM_WriteRegister[0]", 0, EXMEM_WriteRegister0.get_n_bits());
		default_map.add_reg(EXMEM_WriteRegister0, 55, "RW");



		MEMWB_ReadDataOfMem = my_reg::type_id::create("MEMWB_ReadDataOfMem");
		MEMWB_ReadDataOfMem.build();
		MEMWB_ReadDataOfMem.configure(this);
		MEMWB_ReadDataOfMem.add_hdl_path_slice("WB_ReadDataOfMem", 0, MEMWB_ReadDataOfMem.get_n_bits());
		default_map.add_reg(MEMWB_ReadDataOfMem, 56, "RW");

		MEMWB_ALUResult = my_reg::type_id::create("MEMWB_ALUResult");
		MEMWB_ALUResult.build();
		MEMWB_ALUResult.configure(this);
		MEMWB_ALUResult.add_hdl_path_slice("WB_ALUResult", 0, MEMWB_ALUResult.get_n_bits());
		default_map.add_reg(MEMWB_ALUResult, 60, "RW");

		MEMWB_WriteRegister4 = my_1bit_reg::type_id::create("MEMWB_WriteRegister4");
		MEMWB_WriteRegister4.build();
		MEMWB_WriteRegister4.configure(this);
		MEMWB_WriteRegister4.add_hdl_path_slice("WB_WriteRegister[4]", 0, MEMWB_WriteRegister4.get_n_bits());
		default_map.add_reg(MEMWB_WriteRegister4, 64, "RW");

		MEMWB_WriteRegister3 = my_1bit_reg::type_id::create("MEMWB_WriteRegister3");
		MEMWB_WriteRegister3.build();
		MEMWB_WriteRegister3.configure(this);
		MEMWB_WriteRegister3.add_hdl_path_slice("WB_WriteRegister[3]", 0, MEMWB_WriteRegister3.get_n_bits());
		default_map.add_reg(MEMWB_WriteRegister3, 65, "RW");

		MEMWB_WriteRegister2 = my_1bit_reg::type_id::create("MEMWB_WriteRegister2");
		MEMWB_WriteRegister2.build();
		MEMWB_WriteRegister2.configure(this);
		MEMWB_WriteRegister2.add_hdl_path_slice("WB_WriteRegister[2]", 0, MEMWB_WriteRegister2.get_n_bits());
		default_map.add_reg(MEMWB_WriteRegister2, 66, "RW");

		MEMWB_WriteRegister1 = my_1bit_reg::type_id::create("MEMWB_WriteRegister1");
		MEMWB_WriteRegister1.build();
		MEMWB_WriteRegister1.configure(this);
		MEMWB_WriteRegister1.add_hdl_path_slice("WB_WriteRegister[1]", 0, MEMWB_WriteRegister1.get_n_bits());
		default_map.add_reg(MEMWB_WriteRegister1, 67, "RW");

		MEMWB_WriteRegister0 = my_1bit_reg::type_id::create("MEMWB_WriteRegister0");
		MEMWB_WriteRegister0.build();
		MEMWB_WriteRegister0.configure(this);
		MEMWB_WriteRegister0.add_hdl_path_slice("WB_WriteRegister[0]", 0, MEMWB_WriteRegister0.get_n_bits());
		default_map.add_reg(MEMWB_WriteRegister0, 68, "RW");

		MEMWB_MemtoReg = my_1bit_reg::type_id::create("MEMWB_MemtoReg");
		MEMWB_MemtoReg.build();
		MEMWB_MemtoReg.configure(this);
		MEMWB_MemtoReg.add_hdl_path_slice("WB_MemtoReg", 0, MEMWB_MemtoReg.get_n_bits());
		default_map.add_reg(MEMWB_MemtoReg, 69, "RW");

		MEMWB_RegWrite = my_1bit_reg::type_id::create("MEMWB_RegWrite");
		MEMWB_RegWrite.build();
		MEMWB_RegWrite.configure(this);
		MEMWB_RegWrite.add_hdl_path_slice("WB_RegWrite", 0, MEMWB_RegWrite.get_n_bits());
		default_map.add_reg(MEMWB_RegWrite, 70, "RW");

		add_hdl_path("_dut");
	endfunction

endclass

class my_data_mem_gr extends uvm_reg_block;
	`uvm_object_utils(my_data_mem_gr)

	rand my_reg  my_mem_cell[0:my_pkg::MEM_SIZE]; 

	function new (string name = "");
		super.new(name, build_coverage(UVM_NO_COVERAGE));
	endfunction

	function void build;
		default_map = create_map("my_map", 0, my_pkg::MEM_SIZE * 4, UVM_LITTLE_ENDIAN);
		for (int i = 0; i < my_pkg::MEM_SIZE; i++) begin
			my_mem_cell[i] = my_reg::type_id::create($sformatf("data_mem[%0d]", i));
			my_mem_cell[i].build();
			my_mem_cell[i].configure(this);
			my_mem_cell[i].add_hdl_path_slice($sformatf("dataMem1.Mem[%d]", i), 0, my_mem_cell[i].get_n_bits());
			default_map.add_reg(my_mem_cell[i], 4 * i, "RW");
		end
		add_hdl_path("_dut");
	endfunction

endclass 

class my_instr_mem_gr extends uvm_reg_block;
	`uvm_object_utils(my_instr_mem_gr)

	rand my_reg  my_mem_cell[0:my_pkg::MEM_SIZE]; 

	function new (string name = "");
		super.new(name, build_coverage(UVM_NO_COVERAGE));
	endfunction

	function void build;
		default_map = create_map("my_map", 0, my_pkg::MEM_SIZE * 4, UVM_LITTLE_ENDIAN);
		for (int i = 0; i < my_pkg::MEM_SIZE; i++) begin
			my_mem_cell[i] = my_reg::type_id::create($sformatf("inst_mem[%0d]", i));
			my_mem_cell[i].build();
			my_mem_cell[i].configure(this);
			my_mem_cell[i].add_hdl_path_slice($sformatf("InstructionMem1.Mem[%d]", i), 0, my_mem_cell[i].get_n_bits());
			default_map.add_reg(my_mem_cell[i], 4 * i, "RW");
		end
		add_hdl_path("_dut");
	endfunction

endclass 

class my_reg_model extends uvm_reg_block;
	`uvm_object_utils(my_reg_model)

	rand my_GPR_gr m_GPR_gr;
	rand my_data_mem_gr m_data_mem_gr;
	rand my_instr_mem_gr m_instr_mem_gr;
	rand my_pipeline_gr m_pipeline_gr;
	
	function new (string name = "");
		super.new(name);
	endfunction

	function void build;
		default_map = create_map("", 0, 132 + (8 * my_pkg::MEM_SIZE) + 71, UVM_LITTLE_ENDIAN);		
	
		m_GPR_gr = my_GPR_gr::type_id::create("m_GPR_gr",,get_full_name());
		m_GPR_gr.configure(this, "_dut");	
		m_GPR_gr.build();

		m_data_mem_gr = my_data_mem_gr::type_id::create("m_data_mem_gr",,get_full_name());
		m_data_mem_gr.configure(this, "_dut");	
		m_data_mem_gr.build();	

		m_instr_mem_gr = my_instr_mem_gr::type_id::create("m_instr_mem_gr",,get_full_name());
		m_instr_mem_gr.configure(this, "_dut");	
		m_instr_mem_gr.build();	

		m_pipeline_gr = my_pipeline_gr::type_id::create("m_pipeline_gr",,get_full_name());
		m_pipeline_gr.configure(this, "_dut");	
		m_pipeline_gr.build();	

		add_hdl_path("uvm_top");
		default_map.add_submap(m_GPR_gr.default_map, 0);
		default_map.add_submap(m_data_mem_gr.default_map, 132);
		default_map.add_submap(m_instr_mem_gr.default_map, 132 + (4 * my_pkg::MEM_SIZE));
		default_map.add_submap(m_pipeline_gr.default_map, 132 + (8 * my_pkg::MEM_SIZE));
	endfunction
endclass
