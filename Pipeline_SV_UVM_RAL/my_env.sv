`timescale 1ns/10ps
`include "uvm_macros.svh"

import uvm_pkg::*;

class my_env extends uvm_env;

	`uvm_component_utils(my_env)
  
	my_pkg::my_scoreboard m_scoreboard;
	//my_pkg::my_second_scoreboard m_scoreboard;
	my_pkg::ral_env m_ral_env;
	my_pkg::my_monitor m_monitor;
  
	function new (string name = "my_env", uvm_component parent = null);
		super.new(name, parent);
	endfunction
  
	function void build_phase (uvm_phase phase);
		super.build_phase(phase);

		m_monitor = my_pkg::my_monitor::type_id::create("m_monitor", this);
		m_scoreboard = my_pkg::my_scoreboard::type_id::create("m_scoreboard", this);
		//m_scoreboard = my_pkg::my_second_scoreboard::type_id::create("m_scoreboard", this);

		m_ral_env = my_pkg::ral_env::type_id::create("m_reg_env", this);
		uvm_reg::include_coverage("*", UVM_CVR_ALL);
	endfunction

	virtual function void connect_phase (uvm_phase phase);
		m_monitor.inst_collected_item_port.connect(m_scoreboard.instr_fifo.analysis_export);
	endfunction
  
endclass