`timescale 1ns/10ps
`include "uvm_macros.svh"

import uvm_pkg::*;

class my_test extends uvm_test;
	`uvm_component_utils(my_test)
  
	virtual my_if m_if;
	my_pkg::my_env env;
	int ErrrosGenerated = 0;
	my_pkg::my_reg_model m_reg_model;
	uvm_status_e status;

	function new (string name = "my_test", uvm_component parent = null);
		super.new(name, parent);
	endfunction
  
	virtual function void build_phase (uvm_phase phase);
		super.build_phase(phase);
		env = my_pkg::my_env::type_id::create("env", this); // konstruktory komponent potrebuji referenci nadrazene komponenty
	endfunction
  
	task run_phase (uvm_phase phase);
		super.run_phase(phase);
		phase.phase_done.set_drain_time(this, 50ns);

		// get reference to the register model from uvm database
		if(!uvm_config_db#(my_pkg::my_reg_model)::get(null, "uvm_top", "m_reg_model", m_reg_model))
			`uvm_fatal("NOVIF", {"Missing m_reg_model: ", get_full_name(), ".m_reg_model"})

		// get reference to the DUT interface form uvm database
		if(!uvm_config_db#(virtual my_if)::get(null, "uvm_top", "m_if", m_if))
			`uvm_fatal("NOVIF", {"Missing interface: ", get_full_name(), ".m_if"})

		/*
		// function for injecting errrors to DUT
		fork
			DoSomethingBad();
		join_none
		*/

		forever begin
				// initialize
				m_if.reset = 1'b1;
				#2ns;
				m_if.reset = 1'b0;
				InitMem();
				InitRegs();
				`uvm_info(get_type_name(), "Mem initialized - starting test\n\n", UVM_LOW)
				env.m_scoreboard.enable_test = 1'b1;
				// run test
				for (int i = 0; i < my_pkg::MEM_SIZE >> 1; i++) begin
					@(posedge m_if.clk);
					if (i == (my_pkg::MEM_SIZE >> 1) - 6)
						env.m_scoreboard.enable_test = 1'b0;
				end
				`uvm_info(get_type_name(), "Test step done\n\n", UVM_LOW)
		end
	endtask

	virtual function void report_phase (uvm_phase phase);
		`uvm_info(get_type_name(), $sformatf("Errors generated: %d", ErrrosGenerated), UVM_LOW)
	endfunction

	// function for injecting errros to DUT
	task DoSomethingBad;
		logic [31:0] PC, Instr, RegVal, PCval, MemVal, RegAddr, error;
		string msg = "";
		int rnderror;
		forever begin
			int rndtime = ($random() % 50) * 10;
			for (int i = 0; i < rndtime; i++)
				@(posedge m_if.clk);
			msg = "Inserting error after ";
			@(posedge m_if.clk);
			if ($random() % 2) begin
				// insert error after posedge CLK
				msg = {msg, "posedge clk"};
			end
			else begin
				// insert error after negedge CLK
				@(negedge m_if.clk);
				msg = {msg, "negedge clk"};
			end
			# 1ns
			rnderror = $random() % 3;
			if (rnderror == 0) begin
				// insert error to instruction memory
				string strerr;
				m_reg_model.m_GPR_gr.my_PC.peek(status, PC);
				m_reg_model.m_instr_mem_gr.my_mem_cell[PC % my_pkg::MEM_SIZE].peek(status, MemVal);
				error = 1 << ($random() % 32);
				MemVal = MemVal ^ error;
				m_reg_model.m_instr_mem_gr.my_mem_cell[PC % my_pkg::MEM_SIZE].poke(status, MemVal);
				strerr.bintoa(error);
				msg = {msg, " into instruction memory - error vector: ", strerr};
			end
			else if (rnderror == 1) begin
				// insert error to PC
				string strerr;
				m_reg_model.m_GPR_gr.my_PC.peek(status, PCval);
				error = 1 << ($random() % 32);
				PCval = PCval ^ error;
				m_reg_model.m_GPR_gr.my_PC.poke(status, PCval);
				strerr.bintoa(error);
				msg = {msg, " into PC - error vector: ", strerr};
			end
			else begin
				// insert error to target register
				string strerr;
				m_reg_model.m_GPR_gr.my_PC.peek(status, PC);
				`uvm_info(get_type_name(), $sformatf("%d", PC % my_pkg::MEM_SIZE), UVM_LOW)
				m_reg_model.m_instr_mem_gr.my_mem_cell[PC % my_pkg::MEM_SIZE].peek(status, Instr);
				if (Instr[31:26] == 0) begin
					RegAddr = Instr[15:11];
				end
				else if ((Instr[31:26] == 14) || (Instr[31:26] == 35)) begin
					RegAddr = Instr[20:16];
				end
				else begin
					continue;
				end
				m_reg_model.m_GPR_gr.my_GPR[RegAddr].peek(status, RegVal);
				error = 1 << ($random() % 32);
				RegVal = RegVal ^ error;
				m_reg_model.m_GPR_gr.my_GPR[RegAddr].poke(status, RegVal);
				strerr.bintoa(error);
				msg = {msg, " into target register - error vector: ", strerr};
			end
			`uvm_info(get_type_name(), $sformatf("%s", msg), UVM_LOW)
			ErrrosGenerated = ErrrosGenerated + 1;
		end
	endtask

	// initialize registers
	task InitRegs;
		int rnd;
		for (int i = 1; i < 32; i++) begin
			rnd = $urandom();
			m_reg_model.m_GPR_gr.my_GPR[i].poke(status, rnd);
			m_reg_model.m_GPR_gr.my_GPR[i].SetVal(rnd);
		end
		m_reg_model.m_GPR_gr.my_GPR[0].poke(status, 0);
		m_reg_model.m_GPR_gr.my_GPR[0].SetVal(0);
		m_reg_model.m_GPR_gr.my_PC.poke(status, 0);
		m_reg_model.m_GPR_gr.my_PC.SetVal(0);
	endtask

	// initialize instruction and data memories
	task InitMem;
		bit res;
		int rnd;
		my_pkg::my_instruction instr = my_pkg::my_instruction::type_id::create();
		for (int i = 0; i < my_pkg::MEM_SIZE; i++) begin
			rnd = $urandom();
			m_reg_model.m_data_mem_gr.my_mem_cell[i].write(status, rnd, UVM_BACKDOOR);
			m_reg_model.m_data_mem_gr.my_mem_cell[i].SetVal(rnd);
			res = instr.randomize();
			instr.item_cover_instr.sample();
			m_reg_model.m_instr_mem_gr.my_mem_cell[i].write(status, instr.instruction, UVM_BACKDOOR);
		end
	endtask

endclass