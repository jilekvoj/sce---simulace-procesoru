; for the benefit of gcc.
.text
Start:
	lhi	r29,((_stack + 4092)>>16)&0xffff
	addui	r29,r29,(_stack + 4092)&0xffff
	jal	_main        
	nop

;
; DLX traps and other support functions.
;
; Aaron Sawdey 1994,1995; released to the Public Domain.
;
.text
_trap_exit:		
	trap	#0
	jr	r31
	nop

_trap_open:		
	trap	#1
	jr	r31
	nop

_trap_close:		
	trap	#2
	jr	r31
	nop

_trap_read:		
	trap	#3
	jr	r31
	nop

_trap_write:		
	trap	#4
	jr	r31
	nop

_trap_printf:		
	trap	#5
	jr	r31
	nop

_trap_lseek:		
	trap	#6
	jr	r31
	nop

_trap_random:		
	trap	#7
	jr	r31
	nop

_barrier:		
	trap	#8
	jr	r31
	nop

_trap_tell:		
	trap	#9
	jr	r31
	nop

_trap_isatty:		
	trap	#10
	jr	r31
	nop

_trap_access:		
	trap	#11
	jr	r31
	nop

_fork:		
	trap	#13
	jr	r31
	nop

_join:		
	trap	#14
	jr	r31
	nop

_trap_srandom:		
	trap	#15
	jr	r31
	nop

;
; Math related traps.
;

_trap_cos:
        trap 	#20
	jr	r31
	nop

_trap_acos:
        trap 	#21
	jr	r31
	nop

_trap_sin:
        trap 	#22
	jr	r31
	nop

_trap_asin:
        trap 	#23
	jr	r31
	nop

_trap_tan:
        trap 	#24
	jr	r31
	nop

_trap_atan:
        trap 	#25
	jr	r31
	nop

_trap_log10:
        trap 	#26
	jr	r31
	nop

_trap_log:
        trap 	#27
	jr	r31
	nop

_trap_exp:
        trap 	#28
	jr	r31
	nop

_trap_sqrt:		
	trap	#29
	jr	r31
	nop

_trap_pow:
        trap 	#30
	jr	r31
	nop

; for the benefit of gcc.
___main:
        nop
	jr	r31
	nop

.data
.align 2
_stack:
.space 4096

;
; Stub functions for DLX traps.
;
; Aaron Sawdey 1996; released to the Public Domain.
;
.text
_exit:		
	trap	#0
	jr	r31
	nop

;
; Stub functions for DLX traps.
;
; Aaron Sawdey 1996; released to the Public Domain.
;
.text
_printf:		
	trap	#5
	jr	r31
	nop

;
; Stub functions for DLX traps.
;
; Aaron Sawdey 1996; released to the Public Domain.
;
.text
_srandom:		
	trap	#15
	jr	r31
	nop


;
; Stub functions for DLX traps.
;
; Aaron Sawdey 1996; released to the Public Domain.
;
.text
_cos:
        trap 	#20
	jr	r31
	nop

_acos:
        trap 	#21
	jr	r31
	nop

_sin:
        trap 	#22
	jr	r31
	nop

_asin:
        trap 	#23
	jr	r31
	nop

_tan:
        trap 	#24
	jr	r31
	nop

_atan:
        trap 	#25
	jr	r31
	nop

_log10:
        trap 	#26
	jr	r31
	nop

_log:
        trap 	#27
	jr	r31
	nop

_exp:
        trap 	#28
	jr	r31
	nop

_sqrt:		
	trap	#29
	jr	r31
	nop

_pow:
        trap 	#30
	jr	r31
	nop


;
; Stub functions for DLX traps.
;
; Aaron Sawdey 1996; released to the Public Domain.
;
.text
_random:		
	trap	#7
	jr	r31
	nop

;
; Stub functions for DLX traps.
;
; Aaron Sawdey 1996; released to the Public Domain.
;
.text
_write:		
	trap	#4
	jr	r31
	nop

.text
_lseek:		
	trap	#6
	jr	r31
	nop

.text
_tell:		
	trap	#9
	jr	r31
	nop


.text
_isatty:		
	trap	#10
	jr	r31
	nop


.text
_access:		
	trap	#11
	jr	r31
	nop

.data
	.align 2
_a:
	.word	1
	.word	2
	.word	3
	.word	4
	.word	5
	.word	6
	.word	7
	.word	8
	.word	9
	.word	10
	.word	11
	.word	12
	.word	13
	.word	14
	.word	15
	.word	16
	.word	17
	.word	18
	.word	19
	.word	20
	.word	21
	.word	22
	.word	23
	.word	24
	.word	25
	.word	1
	.word	2
	.word	3
	.word	4
	.word	5
	.word	6
	.word	7
	.word	8
	.word	9
	.word	10
	.word	11
	.word	12
	.word	13
	.word	14
	.word	15
	.word	16
	.word	17
	.word	18
	.word	19
	.word	20
	.word	21
	.word	22
	.word	23
	.word	24
	.word	25
	.word	1
	.word	2
	.word	3
	.word	4
	.word	5
	.word	6
	.word	7
	.word	8
	.word	9
	.word	10
	.word	11
	.word	12
	.word	13
	.word	14
	.word	15
	.word	16
	.word	17
	.word	18
	.word	19
	.word	20
	.word	21
	.word	22
	.word	23
	.word	24
	.word	25
	.word	1
	.word	2
	.word	3
	.word	4
	.word	5
	.word	6
	.word	7
	.word	8
	.word	9
	.word	10
	.word	11
	.word	12
	.word	13
	.word	14
	.word	15
	.word	16
	.word	17
	.word	18
	.word	19
	.word	20
	.word	21
	.word	22
	.word	23
	.word	24
	.word	25
	.align 2

_d:
	.word	1
	.word	2
	.word	3
	.word	4
	.word	5
	.word	6
	.word	7
	.word	8
	.word	9
	.word	10
	.word	11
	.word	12
	.word	13
	.word	14
	.word	15
	.word	16
	.word	17
	.word	18
	.word	19
	.word	20
	.word	21
	.word	22
	.word	23
	.word	24
	.word	25
	.word	1
	.word	2
	.word	3
	.word	4
	.word	5
	.word	6
	.word	7
	.word	8
	.word	9
	.word	10
	.word	11
	.word	12
	.word	13
	.word	14
	.word	15
	.word	16
	.word	17
	.word	18
	.word	19
	.word	20
	.word	21
	.word	22
	.word	23
	.word	24
	.word	25
	.word	1
	.word	2
	.word	3
	.word	4
	.word	5
	.word	6
	.word	7
	.word	8
	.word	9
	.word	10
	.word	11
	.word	12
	.word	13
	.word	14
	.word	15
	.word	16
	.word	17
	.word	18
	.word	19
	.word	20
	.word	21
	.word	22
	.word	23
	.word	24
	.word	25
	.word	1
	.word	2
	.word	3
	.word	4
	.word	5
	.word	6
	.word	7
	.word	8
	.word	9
	.word	10
	.word	11
	.word	12
	.word	13
	.word	14
	.word	15
	.word	16
	.word	17
	.word	18
	.word	19
	.word	20
	.word	21
	.word	22
	.word	23
	.word	24
	.word	25
	.align 2
_b:
	.word	21
	.word	1
	.word	20
	.word	11
	.word	10
	.word	22
	.word	2
	.word	19
	.word	12
	.word	9
	.word	23
	.word	3
	.word	18
	.word	13
	.word	8
	.word	24
	.word	4
	.word	17
	.word	14
	.word	7
	.word	25
	.word	5
	.word	16
	.word	15
	.word	6
	.word	21
	.word	1
	.word	20
	.word	11
	.word	10
	.word	22
	.word	2
	.word	19
	.word	12
	.word	9
	.word	23
	.word	3
	.word	18
	.word	13
	.word	8
	.word	24
	.word	4
	.word	17
	.word	14
	.word	7
	.word	25
	.word	5
	.word	16
	.word	15
	.word	6
	.word	21
	.word	1
	.word	20
	.word	11
	.word	10
	.word	22
	.word	2
	.word	19
	.word	12
	.word	9
	.word	23
	.word	3
	.word	18
	.word	13
	.word	8
	.word	24
	.word	4
	.word	17
	.word	14
	.word	7
	.word	25
	.word	5
	.word	16
	.word	15
	.word	6
	.word	21
	.word	1
	.word	20
	.word	11
	.word	10
	.word	22
	.word	2
	.word	19
	.word	12
	.word	9
	.word	23
	.word	3
	.word	18
	.word	13
	.word	8
	.word	24
	.word	4
	.word	17
	.word	14
	.word	7
	.word	25
	.word	5
	.word	16
	.word	15
	.word	6
	.align 2
_c:
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0

_n:

	.word   5


.text
	.align 2


; 6350 cyklu, 5823 instrukci, 203 RAW, 222 Ctrl (+3)   n=10




_main:
;  Function 'main'; 0 bytes of locals, 0 regs to save.
	sw	-4(r29),r30	; push fp
	add	r30,r0,r29	; fp = sp
	sw	-8(r29),r31	; push ret addr
	subui	r29,r29,#8	; alloc local storage
	jal	___main

;4-7
;16-23

	sw	-12(r29),r4		;n

	lhi	r4,((_n)>>16)&0xffff
	addui	r4,r4,(_n)&0xffff	;r4=adr(n);
	lw	r4,(r4)			;r4=n;

	sw	-44(r29),r20		;n div 10
	sw	-48(r29),r21		;n mod 10


	add	r20,r0,r4
	addi	r21,r0,#10

	div	r20,r20,r21		;r20=n div 10



;			r1		;adr(a)
;			r2		;adr(b) (Trans); adr(d) (Nas)
;			r3		;adr(d) (Trans); adr(c) (Nas)
;			r4		;n
	sw	-16(r29),r5		;i
	sw	-20(r29),r6		;j
	sw	-24(r29),r7		;4*n
;			r8		;
;			r9		;
;			r10		;Nacitani prvku (Trans)
;			r11		;Nacitani prvku (Trans)
;			r12		;
;			r13		;
;			r14		;vysl_mult1 (Nas)
;			r15		;pom (Nas)
	sw	-28(r29),r16		;
	sw	-32(r29),r17		;
	sw	-36(r29),r18		;
	sw	-40(r29),r19		;
;			r20		;n div 10
;			r21		;n mod 10; n mod 4
	sw	-52(r29),r22		;n div 4
	sw	-56(r29),r23		;k(1),k(10) (Nas)
;			r24		;b (a)
;			r25		;d


	lhi	r1,((_a)>>16)&0xffff
	addui	r1,r1,(_a)&0xffff	;r1=adr(a)
	lhi	r2,((_b)>>16)&0xffff
	addui	r2,r2,(_b)&0xffff	;r2=adr(b)

	lhi	r3,((_d)>>16)&0xffff
	addui	r3,r3,(_d)&0xffff	;r3=adr(d)



	add	r5,r0,r4		;i=n
	add	r6,r0,r4		;j=n


	slli	r7,r4,#2		;4*n
	mult	r8,r20,r21		;r8=Tmp (r20=n div 10)


	add	r24,r0,r2		;r24=b
	add	r25,r0,r3		;r25=d



	sub	r21,r4,r8		;r21=n mod 10

	srli	r22,r21,#2		;r22=n div 4
	andi	r21,r21,#3		;r21=n mod 4

LoopTr1:


LoopTr211:

	beqz	r21,LoopTr241

	add	r6,r0,r21		

LoopTr212:


	lw 	r10,(r24)
	addi	r24,r24,#4
	sw 	(r25),r10


	subi 	r6,r6,#1
	add	r25,r25,r7
	bnez 	r6,LoopTr212



LoopTr241:


	beqz	r22,LoopTr2a1

	add	r6,r0,r22

LoopTr242:





	lw 	r10,(r24)
	lw 	r11,4(r24)

	sw 	(r25),r10
	add	r25,r25,r7
	sw 	(r25),r11
	add	r25,r25,r7


	lw 	r10,8(r24)
	lw 	r11,12(r24)

	sw 	(r25),r10
	add	r25,r25,r7
	sw 	(r25),r11
	add	r25,r25,r7




	subi 	r6,r6,#1
	addi	r24,r24,#16


	bnez 	r6,LoopTr242



LoopTr2a1:


	beqz	r20,LoopTr2

	add	r6,r0,r20

LoopTr2a2:





	lw 	r10,(r24)
	lw 	r11,4(r24)

	sw 	(r25),r10
	add	r25,r25,r7
	sw 	(r25),r11
	add	r25,r25,r7


	lw 	r10,8(r24)
	lw 	r11,12(r24)

	sw 	(r25),r10
	add	r25,r25,r7
	sw 	(r25),r11
	add	r25,r25,r7




	lw 	r10,16(r24)
	lw 	r11,20(r24)

	sw 	(r25),r10
	add	r25,r25,r7
	sw 	(r25),r11
	add	r25,r25,r7


	lw 	r10,24(r24)
	lw 	r11,28(r24)

	sw 	(r25),r10
	add	r25,r25,r7
	sw 	(r25),r11
	add	r25,r25,r7

	lw 	r10,32(r24)
	lw 	r11,36(r24)

	sw 	(r25),r10
	add	r25,r25,r7
	sw 	(r25),r11
	add	r25,r25,r7


	subi 	r6,r6,#1
	addi	r24,r24,#40


	bnez 	r6,LoopTr2a2


LoopTr2:



	addi	r3,r3,#4

	subi 	r5,r5,#1
	add	r25,r0,r3
	bnez 	r5,LoopTr1







;	jal	_exit
;	nop



	lhi	r2,((_d)>>16)&0xffff
	addui	r2,r2,(_d)&0xffff	;r2=adr(d)
	lhi	r3,((_c)>>16)&0xffff
	addui	r3,r3,(_c)&0xffff	;r3=adr(c)


	add	r24,r0,r1		;a
	add	r25,r0,r2		;d

	add	r5,r0,r4		;i=n
	add	r6,r0,r4		;j=n




Loop1:


Loop2:


Loop311:

	beqz	r21,Loop341

	add	r23,r0,r21		;

Loop312:

	lw 	r8,(r24)
	lw 	r9,(r25)

	subi 	r23,r23,#1
	mult 	r10,r8,r9
	addi	r24,r24,#4
	addi	r25,r25,#4
	add	r17,r17,r10
	bnez 	r23,Loop312


Loop341:

	beqz	r22,Loop3a1

	add	r23,r0,r22

Loop342:
	

	lw 	r8,(r24)
	lw 	r9,(r25)
	lw 	r11,4(r24)

	mult 	r10,r8,r9

	lw 	r12,4(r25)
	lw 	r14,8(r24)

	mult 	r13,r11,r12

	lw 	r15,8(r25)
	lw 	r8,12(r24)

	mult 	r16,r14,r15

	lw 	r9,12(r25)
	add	r17,r17,r10

	mult 	r10,r8,r9




	add	r17,r17,r13


	add	r17,r17,r16
	add	r17,r17,r10


	subi 	r23,r23,#1

	addi	r24,r24,#16
	addi	r25,r25,#16



	bnez 	r23,Loop342




Loop3a1:

	beqz	r20,Loop3

	add	r23,r0,r20

Loop3a2:
	

	lw 	r8,(r24)
	lw 	r9,(r25)
	lw 	r11,4(r24)

	mult 	r10,r8,r9

	lw 	r12,4(r25)
	lw 	r14,8(r24)

	mult 	r13,r11,r12

	lw 	r15,8(r25)
	lw 	r8,12(r24)

	mult 	r16,r14,r15

	lw 	r9,12(r25)
	add	r17,r17,r10

	mult 	r10,r8,r9




	add	r17,r17,r13


	add	r17,r17,r16
	add	r17,r17,r10



	lw 	r11,16(r24)
	lw 	r12,16(r25)
	lw 	r14,20(r24)

	mult 	r13,r11,r12

	lw 	r15,20(r25)
	lw 	r8,24(r24)

	mult 	r16,r14,r15

	lw 	r9,24(r25)
	lw 	r11,28(r24)

	mult 	r10,r8,r9

	lw 	r12,28(r25)
	add	r17,r17,r13

	mult 	r13,r11,r12

	add	r17,r17,r16


	add	r17,r17,r10
	add	r17,r17,r13




	lw 	r14,32(r24)
	lw 	r15,32(r25)
	lw 	r8,36(r24)

	mult 	r16,r14,r15

	lw 	r9,36(r25)
	subi 	r23,r23,#1

	mult 	r10,r8,r9

	add	r17,r17,r16
	addi	r24,r24,#40
	addi	r25,r25,#40
	add	r17,r17,r10



	bnez 	r23,Loop3a2


Loop3:




	add	r24,r0,r1
	sw 	(r3),r17
	addi	r17,r0,#0
	subi 	r6,r6,#1
	addi	r3,r3,#4
	bnez 	r6,Loop2





	add	r1,r1,r7
	add	r24,r0,r1
	add	r25,r0,r2
	subi 	r5,r5,#1
	add	r6,r0,r4
	bnez 	r5,Loop1








	lw	r4,-12(r29)
	lw	r5,-16(r29)
	lw	r6,-20(r29)
	lw	r7,-24(r29)

	lw	r16,-28(r29)
	lw	r17,-32(r29)
	lw	r18,-36(r29)
	lw	r19,-40(r29)
	lw	r20,-44(r29)
	lw	r21,-48(r29)
	lw	r22,-52(r29)
	lw	r23,-56(r29)





	jal	_exit
	nop



