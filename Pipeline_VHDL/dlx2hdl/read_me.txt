					
------------------------------------------
dlx2hdl converts DLX programs for HDLDLX
------------------------------------------

Current limitations:
--------------------
1. Programs must be written in lower-case
2. .data , .text constructs are not supported


Usage:
------

java -jar dlx2hdl.jar infile [outfile]


Example:
--------

java -jar dlx2hdl.jar ukazka.s program_mem.txt

			