	;r4	n
	;r1	adresa zacatku a
	;r2	adresa zacatku b
	;r3	adresa zacatku d (transponovane b)

	;r5	pocitadlo i
	;r6	pocitadlo j

	;r7	4*n (pro posun o cely radek)
	;r8	cyklus 1x ano/ne
	;r9	cyklus 2x ano/ne
	;r10	cyklus 4x ano/ne
	;r11	pro kopirovani
	;r12	pro kopirovani
	;r13	pro kopirovani
	;r14	pro kopirovani
	;r15	pro kopirovani
	;r16	pro kopirovani
	;r17	pro kopirovani
	;r18	pro kopirovani

	;r24	adresa aktualniho prvku v b
	;r25	adresa aktualniho prvku v d



	addui	r4,r0,#15		;r4=n

	addui	r1,r0,#1024		;r1=adr(a)
	addui	r2,r0,#2024		;r2=adr(b)
	addui	r3,r0,#3024		;r3=adr(d)


	add	r5,r0,r4		;i=n
	add	r6,r0,r4		;j=n

	slli	r7,r4,#2		;4*n

	add	r24,r0,r2		;r24=b
	add	r25,r0,r3		;r25=d


	andi	r8, r4, #1		;n mod 2 - je cyklus 1x?
	andi	r9, r4, #2		;(n mod 4) div 2 - je cyklus 2x?
	andi	r10, r4, #3		;(n mod 8) mod 4 - je cyklus 4x?

;
;transpozice
;



LoopTrZac:


	beqz	r8, LoopTr2x		;n mod 2 = 0 - preskoc cyklus 1x


	lw 	r11,(r24)		;lw (b)
	sw 	(r25),r11		;sw (d)
	addi	r24,r24,#4		;dalsi b
	add	r25,r25,r7		;dalsi radek d

	subi 	r6,r6,#1		;j-1



LoopTr2x:

	beqz	r9, LoopTr4x		;(n mod 4) div 2 = 0 - preskoc cyklus 2x


	lw 	r11,(r24)		;lw (b)
	lw 	r12,4(r24)		;lw (dalsi b)
	sw 	(r25),r11		;sw (d)
	add	r25,r25,r7		;dalsi radek d
	sw 	(r25),r12		;sw (d)
	add	r25,r25,r7		;dalsi radek d

	addi	r24,r24,#8		;b o dve dal

	subi 	r6,r6,#2		;j-2


LoopTr4x:

	beqz	r10, LoopTr8x1		;(n mod 8) div 4 = 0 - preskoc cyklus 4x


	lw 	r11,(r24)		;lw (b)
	lw 	r12,4(r24)		;lw (dalsi b)
	lw 	r13,8(r24)		;lw (b o dve dal)
	lw 	r14,12(r24)		;lw (b o tri dal)
	sw 	(r25),r11		;sw (d)
	add	r25,r25,r7		;dalsi radek d
	sw 	(r25),r12		;sw (d)
	add	r25,r25,r7		;dalsi radek d
	sw 	(r25),r13		;sw (d)
	add	r25,r25,r7		;dalsi radek d
	sw 	(r25),r14		;sw (d)
	add	r25,r25,r7		;dalsi radek d

	addi	r24,r24,#16		;b o ctyri dal

	subi 	r6,r6,#4		;j-4


LoopTr8x1:

	beqz	r6, LoopTrVnitEnd	;j = 0 - skoc na konec


LoopTr8x2:


	lw 	r11,(r24)		;lw (b)
	lw 	r12,4(r24)		;lw (dalsi b)
	lw 	r13,8(r24)		;lw (b o dve dal)
	lw 	r14,12(r24)		;lw (b o tri dal)
	lw 	r15,16(r24)		;lw (b o ctyri dal)
	lw 	r16,20(r24)		;lw (b o pet dal)
	lw 	r17,24(r24)		;lw (b o sest dal)
	lw 	r18,28(r24)		;lw (b o sedm dal)
	sw 	(r25),r11		;sw (d)
	add	r25,r25,r7		;dalsi radek d
	sw 	(r25),r12		;sw (d)
	add	r25,r25,r7		;dalsi radek d
	sw 	(r25),r13		;sw (d)
	add	r25,r25,r7		;dalsi radek d
	sw 	(r25),r14		;sw (d)
	add	r25,r25,r7		;dalsi radek d
	sw 	(r25),r15		;sw (d)
	add	r25,r25,r7		;dalsi radek d
	sw 	(r25),r16		;sw (d)
	add	r25,r25,r7		;dalsi radek d
	sw 	(r25),r17		;sw (d)
	add	r25,r25,r7		;dalsi radek d
	sw 	(r25),r18		;sw (d)
	add	r25,r25,r7		;dalsi radek d

	addi	r24,r24,#32		;b o osm dal

	subi 	r6,r6,#8		;j-8
	bnez 	r6,LoopTr8x2		;until j=0


LoopTrVnitEnd: ;konec vnitrniho cyklu


	addi	r3,r3,#4		;dalsi sloupec d

	subi 	r5,r5,#1		;i--
	add	r25,r0,r3		;navrat na prvni radek d
	add	r6, r0, r4		;j=n


	bnez 	r5,LoopTrZac		;until i=0






;	trap	#0

	;r4	n
	;r1	adresa zacatku a
	;r2	adresa zacatku d (transponovane b)
	;r3	adresa zacatku c

	;r5	pocitadlo i
	;r6	pocitadlo j

	;r7	4*n (pro posun o cely radek)
	;r8	cyklus 1x ano/ne
	;r9	cyklus 2x ano/ne
	;r10	cyklus 4x ano/ne
	;r11	pro kopirovani
	;r12	pro kopirovani
	;r13	pro kopirovani
	;r14	pro kopirovani
	;r15	pro kopirovani
	;r16	pro kopirovani
	;r17	pro kopirovani
	;r18	pro kopirovani

	;r22	uchovani souctu
	;r23	pocitadlo k
	;r24	adresa aktualniho prvku v b
	;r25	adresa aktualniho prvku v d







	addui	r2,r0,#3024		;r2=adr(d)
	addui	r3,r0,#4024		;r3=adr(c)

	add	r24,r0,r1		;a
	add	r25,r0,r2		;d

	add	r5,r0,r4		;i=n
	add	r6,r0,r4		;j=n
	add	r23,r0,r4		;k=n

	addi	r22,r0,#0		;sum=0



LoopNasZac:


	beqz	r8, LoopNas2x		;n mod 2 = 0 - preskoc cyklus 1x



	lw 	r8,(r24)		;lw (a)
	lw 	r9,(r25)		;lw (b)

	subi 	r23,r23,#1		;k--
	mult 	r10,r8,r9		;a*b
	addi	r24,r24,#4		;next a
	addi	r25,r25,#4		;next b
	add	r22,r22,r10		;sum=sum+a*b
	bnez 	r23,Loop312		;until k=0

	subi 	r23,r23,#1		;k-1


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
LoopNas2x:

	beqz	r9, LoopNas4x		;(n mod 4) div 2 = 0 - preskoc cyklus 2x


	subi 	r23,r23,#2		;k-2


LoopNas4x:

	beqz	r10, LoopNas8x1		;(n mod 8) div 4 = 0 - preskoc cyklus 4x


	subi 	r23,r23,#4		;k-4


LoopNas8x1:

	beqz	r23, LoopNasVnitEnd	;k = 0 - skoc na konec


LoopNas8x2:





	subi 	r23,r23,#8		;k-8
	bnez 	r23,LoopNas8x2		;until k=0


LoopNasVnitEnd: ;konec vnitrniho cyklu



	add	r24,r0,r1		;navrat na zacatek radku v a
	sw 	(r3),r17		;sw (sum)
	addi	r17,r0,#0		;sum=0
	subi 	r6,r6,#1		;j--
	addi	r3,r3,#4		;dalsi c
	bnez 	r6,LoopNasZac		;until j=0





	add	r1,r1,r7		;
	add	r24,r0,r1		;dalsi radek v a
	add	r25,r0,r2		;navrat na zacatek b
	subi 	r5,r5,#1		;i--
	add	r6,r0,r4		;j=n
	bnez 	r5,LoopNasZac		;until i=0









Loop1:


Loop2:


Loop311:

	add	r23,r0,r4		;k=n

Loop312:

	lw 	r8,(r24)		;lw (a)
	lw 	r9,(r25)		;lw (b)

	subi 	r23,r23,#1		;k--
	mult 	r10,r8,r9		;a*b
	addi	r24,r24,#4		;next a
	addi	r25,r25,#4		;next b
	add	r17,r17,r10		;sum=sum+a*b
	bnez 	r23,Loop312		;until k=0



	add	r24,r0,r1		;return to first a in row
	sw 	(r3),r17		;sw (sum)
	addi	r17,r0,#0		;sum=0
	subi 	r6,r6,#1		;j--
	addi	r3,r3,#4		;next c
	bnez 	r6,Loop2		;until j=0





	add	r1,r1,r7		;next row a
	add	r24,r0,r1		;next row a
	add	r25,r0,r2		;return to first b
	subi 	r5,r5,#1		;i--
	add	r6,r0,r4		;j=n
	bnez 	r5,Loop1		;until i=0


	trap 	#0
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop


