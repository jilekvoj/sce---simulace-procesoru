addi r1, r0, #2
addi r2, r0, #5
addi r3, r0, #11

addi r10, r0, #100
addi r11, r0, #104
addi r12, r0, #108

sw (r10), r1
sw (r11), r2
sw (r12), r3


lw r18,0(r10) ;2
lw r19,0(r11) ;5
lw r20,0(r12) ;11
mult r22,r18,r20 ;2*11 = 22 (0x16)
mult r23,r19,r20 ;5*11 = 55 (0x37)

trap 0