onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -format Logic /intdlx/clk
add wave -noupdate -format Literal /intdlx/i4/cycle_number
add wave -noupdate -divider {Pipeline control registers}
add wave -noupdate -color Yellow -format Literal -itemcolor Yellow -label {IF PC Programm Counter} -radix hexadecimal /intdlx/if_pc
add wave -noupdate -color Orange -format Literal -itemcolor Orange -label {ID Instruction register} -radix hexadecimal /intdlx/id_ir
add wave -noupdate -color {Orange Red} -format Literal -itemcolor {Orange Red} -label {EX Instruction Register} -radix hexadecimal /intdlx/ex_ir
add wave -noupdate -color Green -format Literal -itemcolor Green -label {MEM Instruction Register} -radix hexadecimal /intdlx/mem_ir
add wave -noupdate -color Blue -format Literal -itemcolor Blue -label {WB Instruction Register} -radix hexadecimal /intdlx/wb_ir

quietly virtual signal -install /intdlx { /intdlx/instruction(31 downto 26)} IF_opcode
quietly virtual signal -install /intdlx { /intdlx/instruction(5 downto 0)} IF_funct
quietly virtual signal -install /intdlx { /intdlx/id_ir(31 downto 26)} ID_opcode
quietly virtual signal -install /intdlx { /intdlx/id_ir(5 downto 0)} ID_funct
quietly virtual signal -install /intdlx { /intdlx/ex_ir(31 downto 26)} EX_opcode
quietly virtual signal -install /intdlx { /intdlx/ex_ir(5 downto 0)} EX_funct
quietly virtual signal -install /intdlx { /intdlx/mem_ir(31 downto 26)} MEM_opcode
quietly virtual signal -install /intdlx { /intdlx/mem_ir(5 downto 0)} MEM_funct
quietly virtual signal -install /intdlx { /intdlx/wb_ir(31 downto 26)} WB_opcode
quietly virtual signal -install /intdlx { /intdlx/wb_ir(5 downto 0)} WB_funct




quietly virtual type {Rtype Float J JAL BEQZ BNEZ BFPT BFPF
              ADDI ADDUI SUBI SUBUI ANDI ORI XORI LHI
              RFE TRAP JR JALR SLLI undef SRLI SRAI
              SEQI SNEI SLTI SGTI SLEI SGEI undef undef
              LB LH undef LW LBU LHU LF LD
              SB SH undef SW undef undef SF SD
              undef undef undef undef undef undef undef undef
              undef undef undef undef undef undef undef undef} dlx_opcode_t

quietly virtual type {NOP undef undef undef SLL undef SLR SRA
             undef undef undef undef undef undef undef undef
             undef undef undef undef undef undef undef undef	
             undef undef undef undef undef undef undef undef
             ADD ADDU SUB SUBU AND OR XOR undef
             SEQ SNE SLT SGT SLE SGE undef undef
             MOVI2S MOVS2I MOVF MOVD MOVFP2I MOVI2FP undef undef
             undef undef undef undef undef undef undef undef} dlx_funct_t

quietly virtual function {(dlx_opcode_t)IF_opcode} IF_opc_decoded
quietly virtual function {(dlx_funct_t)IF_funct} IF_funct_decoded
quietly virtual function {(dlx_opcode_t)ID_opcode} ID_opc_decoded
quietly virtual function {(dlx_funct_t)ID_funct} ID_funct_decoded
quietly virtual function {(dlx_opcode_t)EX_opcode} EX_opc_decoded
quietly virtual function {(dlx_funct_t)EX_funct} EX_funct_decoded
quietly virtual function {(dlx_opcode_t)MEM_opcode} MEM_opc_decoded
quietly virtual function {(dlx_funct_t)MEM_funct} MEM_funct_decoded
quietly virtual function {(dlx_opcode_t)WB_opcode} WB_opc_decoded
quietly virtual function {(dlx_funct_t)WB_funct} WB_funct_decoded

quietly virtual function { &{/intdlx/IF_opc_decoded, /intdlx/IF_funct_decoded}} IF_instr_decoded
quietly virtual function { &{/intdlx/ID_opc_decoded, /intdlx/ID_funct_decoded}} ID_instr_decoded
quietly virtual function { &{/intdlx/EX_opc_decoded, /intdlx/EX_funct_decoded}} EX_instr_decoded
quietly virtual function { &{/intdlx/MEM_opc_decoded, /intdlx/MEM_funct_decoded}} MEM_instr_decoded
quietly virtual function { &{/intdlx/WB_opc_decoded, /intdlx/WB_funct_decoded}} WB_instr_decoded



add wave -noupdate -divider {Decoded Instructions}

add wave -color Yellow -itemcolor Yellow IF_instr_decoded
add wave -color Orange -itemcolor Orange ID_instr_decoded
add wave -color {Orange Red} -itemcolor {Orange Red} EX_instr_decoded
add wave -color Green -itemcolor Green MEM_instr_decoded
add wave -color Blue  -itemcolor Blue WB_instr_decoded

add wave -noupdate -divider {IF Stage}
add wave -noupdate -format Literal -radix hexadecimal /intdlx/if_pc
add wave -noupdate -format Literal -radix hexadecimal /intdlx/instruction
add wave -noupdate -format Literal -radix hexadecimal /intdlx/next_pc
add wave -noupdate -format Logic /intdlx/jump_now
add wave -noupdate -format Logic /intdlx/if_stall
add wave -noupdate -divider {ID Stage}
add wave -noupdate -format Literal -radix hexadecimal /intdlx/id_ir
add wave -noupdate -format Literal -radix unsigned /intdlx/ID_Ra
add wave -noupdate -format Literal -radix unsigned /intdlx/id_Rb
add wave -noupdate -format Literal -radix unsigned /intdlx/wb_rw
add wave -noupdate -format Logic /intdlx/i5/write_enable
add wave -noupdate -format Literal -radix hexadecimal /intdlx/busw
add wave -noupdate -format Literal -label {Register File} -radix hexadecimal /intdlx/i5/reg_file
add wave -noupdate -format Literal -radix hexadecimal /intdlx/id_busa
add wave -noupdate -format Literal -radix hexadecimal /intdlx/id_busb
add wave -noupdate -format Literal /intdlx/imm_type
add wave -noupdate -format Literal -radix hexadecimal /intdlx/id_imm32
add wave -noupdate -format Logic /intdlx/id_clear
add wave -noupdate -format Logic /intdlx/id_stall
add wave -noupdate -divider {EX Stage}
add wave -noupdate -format Literal -radix hexadecimal /intdlx/ex_ir
add wave -noupdate -format Literal /intdlx/alu_op
add wave -noupdate -format Literal -radix unsigned /intdlx/ex_Ra
add wave -noupdate -format Literal -radix unsigned /intdlx/ex_Rb
add wave -noupdate -format Literal -radix unsigned /intdlx/ex_rw
add wave -noupdate -format Logic /intdlx/ex_alusrc
add wave -noupdate -format Literal -radix hexadecimal /intdlx/ex_busa
add wave -noupdate -format Literal -radix hexadecimal /intdlx/ex_busb
add wave -noupdate -format Literal -radix hexadecimal /intdlx/alu_b
add wave -noupdate -format Literal -radix hexadecimal /intdlx/alu_c
add wave -noupdate -format Literal -radix hexadecimal /intdlx/ex_pc
add wave -noupdate -format Literal -radix hexadecimal /intdlx/ex_imm32
add wave -noupdate -format Literal -radix hexadecimal /intdlx/pc_rel_tar_addr
add wave -noupdate -format Literal -radix hexadecimal /intdlx/ex_target_addr
add wave -noupdate -format Literal -radix hexadecimal /intdlx/ex_s
add wave -noupdate -format Literal /intdlx/ex_branch
add wave -noupdate -format Logic /intdlx/ex_jmp_link
add wave -noupdate -format Logic /intdlx/ex_jmp_reg
add wave -noupdate -format Logic /intdlx/ex_store
add wave -noupdate -format Logic /intdlx/ex_load
add wave -noupdate -format Logic /intdlx/ex_stall
add wave -noupdate -format Logic /intdlx/ex_clear
add wave -noupdate -divider {MEM Stage}
add wave -noupdate -format Literal -radix hexadecimal /intdlx/mem_ir
add wave -noupdate -format Literal -radix hexadecimal /intdlx/mem_s
add wave -noupdate -format Literal -radix hexadecimal /intdlx/mem_target_addr
add wave -noupdate -format Literal -radix hexadecimal /intdlx/mem_busb
add wave -noupdate -format Literal /intdlx/mem_branch
add wave -noupdate -format Logic /intdlx/jump_now
add wave -noupdate -format Logic /intdlx/mem_load
add wave -noupdate -format Logic /intdlx/mem_store
add wave -noupdate -format Literal -radix hexadecimal /intdlx/mem_rw
add wave -noupdate -format Literal -radix hexadecimal /intdlx/mem_m
add wave -noupdate -format Logic /intdlx/mem_stall
add wave -noupdate -format Logic /intdlx/mem_clear
add wave -noupdate -divider {WB Stage}
add wave -noupdate -format Literal -radix hexadecimal /intdlx/wb_ir
add wave -noupdate -format Literal -radix unsigned /intdlx/wb_rw
add wave -noupdate -format Logic /intdlx/wb_store
add wave -noupdate -format Logic /intdlx/wb_load
add wave -noupdate -format Literal -radix hexadecimal /intdlx/wb_s
add wave -noupdate -format Literal -radix hexadecimal /intdlx/wb_m
add wave -noupdate -format Literal -radix hexadecimal /intdlx/busw
add wave -noupdate -format Logic /intdlx/wb_clear
add wave -noupdate -format Logic /intdlx/wb_stall
add wave -noupdate -divider {Asynchronnous Reset}
add wave -noupdate -format Logic /intdlx/reset
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {518 ns} 0}
WaveRestoreZoom {498 ns} {547 ns}
configure wave -namecolwidth 182
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
