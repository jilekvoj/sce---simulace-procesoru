--
-- VHDL Architecture integerdlx.Pipeline_reg.rtl
--
-- Created:
--          by - becvarm.UNKNOWN (BECVAR)
--          at - 16:17:23 21.02.2004
--
-- using Mentor Graphics HDL Designer(TM) 2003.3 (Build 60)
--
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all;

ENTITY Pipeline_reg IS
   GENERIC( 
      N : integer := 32
   );
   PORT( 
      Clear : IN     std_logic;
      Clk   : IN     std_logic;
      DIN   : IN     std_logic_vector (N-1 DOWNTO 0);
      Reset : IN     std_logic;
      Stall : IN     std_logic;
      DOUT  : OUT    std_logic_vector (N-1 DOWNTO 0)
   );

-- Declarations

END Pipeline_reg ;

--
ARCHITECTURE rtl OF Pipeline_reg IS

SIGNAL current_data: std_logic_vector(N-1 DOWNTO 0);

BEGIN

clkp: PROCESS(clk, reset)
BEGIN
  IF reset='1' THEN current_data<=(OTHERS=>'0'); -- async. initialization
  ELSIF clk'event AND clk='1' THEN
    IF Clear='1' THEN current_data<=(OTHERS=>'0'); -- forcing zeros (NOP)
    ELSIF Stall='1' THEN current_data<=current_data; -- stall 
    ELSE  current_data<=DIN; -- normal operation
    END IF;
  END IF;  
END PROCESS clkp;
  
  DOUT<=current_data;
  
END rtl;

