-- VHDL Entity integerdlx.WB_comb_logic.symbol
--
-- Created:
--          by - becvarm.UNKNOWN (BECVAR)
--          at - 19:18:37 22.02.2004
--
-- Generated by Mentor Graphics' HDL Designer(TM) 2003.3 (Build 60)
--
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_unsigned.all;

ENTITY WB_comb_logic IS
   PORT( 
      WB_IR    : IN     std_logic_vector (31 DOWNTO 0);
      WB_Load  : OUT    std_logic;
      WB_Store : OUT    std_logic
   );

-- Declarations

END WB_comb_logic ;

--
-- VHDL Architecture integerdlx.WB_comb_logic.tbl
--
-- Created:
--          by - becvarm.UNKNOWN (BECVAR)
--          at - 19:18:37 22.02.2004
--
-- Generated by Mentor Graphics' HDL Designer(TM) 2003.3 (Build 60)
--
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_unsigned.all;
ARCHITECTURE tbl OF WB_comb_logic IS
   
      -- Architecture declarations
    

BEGIN

   ---------------------------------------------------------------------------
   truth_process: PROCESS(WB_IR)
   ---------------------------------------------------------------------------
   BEGIN
      -- Block 1
      IF (WB_IR(31 downto 26) = "101011") THEN
         WB_Load <= '0';
         WB_Store <= '1';
         -- I-type SW
      ELSIF (WB_IR(31 downto 26) = "100011") THEN
         WB_Load <= '1';
         WB_Store <= '0';
         -- I-type LW
      ELSE
         WB_Load <= '0';
         WB_Store <= '0';
         -- other instr.
      END IF;

   END PROCESS truth_process;

-- Architecture concurrent statements
 

END tbl;
