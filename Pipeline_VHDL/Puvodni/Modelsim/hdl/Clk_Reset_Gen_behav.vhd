--
-- VHDL Architecture integerdlx.Clk_Reset_Gen.behav2
--
-- Created:
--          by - becvarm.UNKNOWN (BECVAR)
--          at - 15:44:30 21.02.2004
--
-- using Mentor Graphics HDL Designer(TM) 2003.3 (Build 60)
--
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all;

ENTITY Clk_Reset_Gen IS
   GENERIC( 
      Period : time := 10 ns
   );
   PORT( 
      Clk   : OUT    std_logic;
      Reset : OUT    std_logic
   );

-- Declarations

END Clk_Reset_Gen ;

--
ARCHITECTURE behav2 OF Clk_Reset_Gen IS

SIGNAL cycle_number : natural:=0;
SIGNAL clk_int, reset_int : std_logic;
BEGIN
  clkpr: PROCESS
  BEGIN
    clk_int<='0';
    wait for period /2;
    clk_int<='1';
    wait for period /2;
  END PROCESS clkpr;
  
  resetpr: PROCESS
  BEGIN
    reset_int<='1';
    wait for 2 ns; -- reset time is fixed to 2 ns
    reset_int<='0';
    wait;
  END PROCESS resetpr;    
  
  countp: PROCESS(clk_int, reset_int)
  BEGIN
     IF reset_int='1' THEN cycle_number<=0;
 ELSIF clk_int'event AND clk_int='1' THEN
     cycle_number<=cycle_number + 1;
     END IF;
  END PROCESS countp;
  
  reset<=reset_int;
  clk<=clk_int;
  
END behav2;

