			.data 1024
			
			.word 0xABCD1234, 0xF0F0EEEE, 0x10100101
			
			.text

			XORI  R7,R3,#4
		 	ADDUI R3,R0,1024
		 	ORI   R2,R0,10
		 	ADDUI R8,R0,2048
			LW    R1,(R3)    
			ANDI  R4,R1,0x00F
			ADD   R3,R3,R7
			LW    R10,(R3)
			SW    (R8),R10
			ANDI  R5,R10,0x0F0
			OR    R6,R4,R5
			SUB   R6,R5,R4
			SUB   R0,R4,R6
			ADD   R6,R6,R7
			ADDUI R9,R6,#1	
			XOR   R11,R10,R9
			SW    4(R8),R11
			ADD   R8,R0,R1
			ADD   R1,R8,R1