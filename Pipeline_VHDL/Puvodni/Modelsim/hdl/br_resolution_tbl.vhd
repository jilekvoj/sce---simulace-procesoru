-- VHDL Entity integerdlx.Br_resolution.symbol
--
-- Created:
--          by - becvarm.UNKNOWN (BECVAR)
--          at - 15:03:23 13.03.2005
--
-- Generated by Mentor Graphics' HDL Designer(TM) 2003.3 (Build 60)
--
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_unsigned.all;

ENTITY Br_resolution IS
   PORT( 
      MEM_Branch : IN     std_logic_vector (2 DOWNTO 0);
      Jump_now   : OUT    std_logic
   );

-- Declarations

END Br_resolution ;

--
-- VHDL Architecture integerdlx.Br_resolution.tbl
--
-- Created:
--          by - becvarm.UNKNOWN (BECVAR)
--          at - 15:03:23 13.03.2005
--
-- Generated by Mentor Graphics' HDL Designer(TM) 2003.3 (Build 60)
--
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_unsigned.all;
ARCHITECTURE tbl OF Br_resolution IS
   
      -- Architecture declarations
    

BEGIN

   ---------------------------------------------------------------------------
   truth_process: PROCESS(MEM_Branch)
   ---------------------------------------------------------------------------
   BEGIN
      -- Block 1
      IF (MEM_Branch = "101") THEN
         Jump_now <= '1';
         -- BEQZ
      ELSIF (MEM_Branch = "010") THEN
         Jump_now <= '1';
         -- BNEZ
      ELSIF (MEM_Branch = "011") THEN
         Jump_now <= '1';
         -- uncoditional
      ELSIF (MEM_Branch = "111") THEN
         Jump_now <= '1';
         -- uncoditional
      ELSE
         Jump_now <= '0';
         -- Other situations
      END IF;

   END PROCESS truth_process;

-- Architecture concurrent statements
 

END tbl;
