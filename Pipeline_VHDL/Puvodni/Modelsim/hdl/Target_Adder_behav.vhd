--
-- VHDL Architecture integerdlx.Target_Adder.behav
--
-- Created:
--          by - becvarm.UNKNOWN (BECVAR)
--          at - 14:23:38 13.03.2005
--
-- using Mentor Graphics HDL Designer(TM) 2003.3 (Build 60)
--
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_unsigned.all;
USE ieee.std_logic_arith.all;

ENTITY Target_Adder IS
   PORT( 
      EX_Imm32        : IN     std_logic_vector (31 DOWNTO 0);
      EX_PC           : IN     std_logic_vector (31 DOWNTO 0);
      PC_rel_tar_addr : OUT    std_logic_vector (31 DOWNTO 0)
   );

-- Declarations

END Target_Adder ;

--
ARCHITECTURE behav OF Target_Adder IS
BEGIN

PC_rel_tar_addr<=EX_PC + EX_Imm32;

END behav;

