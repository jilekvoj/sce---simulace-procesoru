virtual type { NOP undef undef undef SLL undef SLR SRA undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef ADD ADDU SUB SUBU AND OR XOR undef SEQ SNE SLT SGT SLE SGE undef undef MOVI2S MOVS2I MOVF MOVD MOVFP2I MOVI2FP undef undef undef undef undef undef undef undef undef undef} dlx_funct_t
virtual type { Rtype Float J JAL BEQZ BNEZ BFPT BFPF ADDI ADDUI SUBI SUBUI ANDI ORI XORI LHI RFE TRAP JR JALR SLLI undef SRLI SRAI SEQI SNEI SLTI SGTI SLEI SGEI undef undef LB LH undef LW LBU LHU LF LD SB SH undef SW undef undef SF SD undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef} dlx_opcode_t
virtual signal -install /intdlx { /intdlx/instruction(31 downto 26)} IF_opcode
virtual signal -install /intdlx { /intdlx/instruction(5 downto 0)} IF_funct
virtual signal -install /intdlx { /intdlx/id_ir(31 downto 26)} ID_opcode
virtual signal -install /intdlx { /intdlx/id_ir(5 downto 0)} ID_funct
virtual signal -install /intdlx { /intdlx/ex_ir(31 downto 26)} EX_opcode
virtual signal -install /intdlx { /intdlx/ex_ir(5 downto 0)} EX_funct
virtual signal -install /intdlx { /intdlx/mem_ir(31 downto 26)} MEM_opcode
virtual signal -install /intdlx { /intdlx/mem_ir(5 downto 0)} MEM_funct
virtual signal -install /intdlx { /intdlx/wb_ir(31 downto 26)} WB_opcode
virtual signal -install /intdlx { /intdlx/wb_ir(5 downto 0)} WB_funct
virtual function -install /intdlx -env /intdlx { (dlx_opcode_t)IF_opcode} IF_opc_decoded
virtual function -install /intdlx -env /intdlx { (dlx_funct_t)IF_funct} IF_funct_decoded
virtual function -install /intdlx -env /intdlx { (dlx_opcode_t)ID_opcode} ID_opc_decoded
virtual function -install /intdlx -env /intdlx { (dlx_funct_t)ID_funct} ID_funct_decoded
virtual function -install /intdlx -env /intdlx { (dlx_opcode_t)EX_opcode} EX_opc_decoded
virtual function -install /intdlx -env /intdlx { (dlx_funct_t)EX_funct} EX_funct_decoded
virtual function -install /intdlx -env /intdlx { (dlx_opcode_t)MEM_opcode} MEM_opc_decoded
virtual function -install /intdlx -env /intdlx { (dlx_funct_t)MEM_funct} MEM_funct_decoded
virtual function -install /intdlx -env /intdlx { (dlx_opcode_t)WB_opcode} WB_opc_decoded
virtual function -install /intdlx -env /intdlx { (dlx_funct_t)WB_funct} WB_funct_decoded
virtual function -install /intdlx -env /intdlx { &{/intdlx/IF_opc_decoded, /intdlx/IF_funct_decoded}} IF_instr_decoded
virtual function -install /intdlx -env /intdlx { &{/intdlx/ID_opc_decoded, /intdlx/ID_funct_decoded}} ID_instr_decoded
virtual function -install /intdlx -env /intdlx { &{/intdlx/EX_opc_decoded, /intdlx/EX_funct_decoded}} EX_instr_decoded
virtual function -install /intdlx -env /intdlx { &{/intdlx/MEM_opc_decoded, /intdlx/MEM_funct_decoded}} MEM_instr_decoded
virtual function -install /intdlx -env /intdlx { &{/intdlx/WB_opc_decoded, /intdlx/WB_funct_decoded}} WB_instr_decoded
virtual type { NOP undef undef undef SLL undef SLR SRA undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef ADD ADDU SUB SUBU AND OR XOR undef SEQ SNE SLT SGT SLE SGE undef undef MOVI2S MOVS2I MOVF MOVD MOVFP2I MOVI2FP undef undef undef undef undef undef undef undef undef undef} dlx_funct_t
virtual type { Rtype Float J JAL BEQZ BNEZ BFPT BFPF ADDI ADDUI SUBI SUBUI ANDI ORI XORI LHI RFE TRAP JR JALR SLLI undef SRLI SRAI SEQI SNEI SLTI SGTI SLEI SGEI undef undef LB LH undef LW LBU LHU LF LD SB SH undef SW undef undef SF SD undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef} dlx_opcode_t
virtual signal -install /intdlx { /intdlx/instruction(31 downto 26)} IF_opcode
virtual signal -install /intdlx { /intdlx/instruction(5 downto 0)} IF_funct
virtual signal -install /intdlx { /intdlx/id_ir(31 downto 26)} ID_opcode
virtual signal -install /intdlx { /intdlx/id_ir(5 downto 0)} ID_funct
virtual signal -install /intdlx { /intdlx/ex_ir(31 downto 26)} EX_opcode
virtual signal -install /intdlx { /intdlx/ex_ir(5 downto 0)} EX_funct
virtual signal -install /intdlx { /intdlx/mem_ir(31 downto 26)} MEM_opcode
virtual signal -install /intdlx { /intdlx/mem_ir(5 downto 0)} MEM_funct
virtual signal -install /intdlx { /intdlx/wb_ir(31 downto 26)} WB_opcode
virtual signal -install /intdlx { /intdlx/wb_ir(5 downto 0)} WB_funct
virtual function -install /intdlx -env /intdlx { (dlx_opcode_t)IF_opcode} IF_opc_decoded
virtual function -install /intdlx -env /intdlx { (dlx_funct_t)IF_funct} IF_funct_decoded
virtual function -install /intdlx -env /intdlx { (dlx_opcode_t)ID_opcode} ID_opc_decoded
virtual function -install /intdlx -env /intdlx { (dlx_funct_t)ID_funct} ID_funct_decoded
virtual function -install /intdlx -env /intdlx { (dlx_opcode_t)EX_opcode} EX_opc_decoded
virtual function -install /intdlx -env /intdlx { (dlx_funct_t)EX_funct} EX_funct_decoded
virtual function -install /intdlx -env /intdlx { (dlx_opcode_t)MEM_opcode} MEM_opc_decoded
virtual function -install /intdlx -env /intdlx { (dlx_funct_t)MEM_funct} MEM_funct_decoded
virtual function -install /intdlx -env /intdlx { (dlx_opcode_t)WB_opcode} WB_opc_decoded
virtual function -install /intdlx -env /intdlx { (dlx_funct_t)WB_funct} WB_funct_decoded
virtual function -install /intdlx -env /intdlx { &{/intdlx/IF_opc_decoded, /intdlx/IF_funct_decoded}} IF_instr_decoded
virtual function -install /intdlx -env /intdlx { &{/intdlx/ID_opc_decoded, /intdlx/ID_funct_decoded}} ID_instr_decoded
virtual function -install /intdlx -env /intdlx { &{/intdlx/EX_opc_decoded, /intdlx/EX_funct_decoded}} EX_instr_decoded
virtual function -install /intdlx -env /intdlx { &{/intdlx/MEM_opc_decoded, /intdlx/MEM_funct_decoded}} MEM_instr_decoded
virtual function -install /intdlx -env /intdlx { &{/intdlx/WB_opc_decoded, /intdlx/WB_funct_decoded}} WB_instr_decoded
virtual type { NOP undef undef undef SLL undef SLR SRA undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef ADD ADDU SUB SUBU AND OR XOR undef SEQ SNE SLT SGT SLE SGE undef undef MOVI2S MOVS2I MOVF MOVD MOVFP2I MOVI2FP undef undef undef undef undef undef undef undef undef undef} dlx_funct_t
virtual type { Rtype Float J JAL BEQZ BNEZ BFPT BFPF ADDI ADDUI SUBI SUBUI ANDI ORI XORI LHI RFE TRAP JR JALR SLLI undef SRLI SRAI SEQI SNEI SLTI SGTI SLEI SGEI undef undef LB LH undef LW LBU LHU LF LD SB SH undef SW undef undef SF SD undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef} dlx_opcode_t
virtual signal -install /intdlx { /intdlx/instruction(31 downto 26)} IF_opcode
virtual signal -install /intdlx { /intdlx/instruction(5 downto 0)} IF_funct
virtual signal -install /intdlx { /intdlx/id_ir(31 downto 26)} ID_opcode
virtual signal -install /intdlx { /intdlx/id_ir(5 downto 0)} ID_funct
virtual signal -install /intdlx { /intdlx/ex_ir(31 downto 26)} EX_opcode
virtual signal -install /intdlx { /intdlx/ex_ir(5 downto 0)} EX_funct
virtual signal -install /intdlx { /intdlx/mem_ir(31 downto 26)} MEM_opcode
virtual signal -install /intdlx { /intdlx/mem_ir(5 downto 0)} MEM_funct
virtual signal -install /intdlx { /intdlx/wb_ir(31 downto 26)} WB_opcode
virtual signal -install /intdlx { /intdlx/wb_ir(5 downto 0)} WB_funct
virtual function -install /intdlx -env /intdlx { (dlx_opcode_t)IF_opcode} IF_opc_decoded
virtual function -install /intdlx -env /intdlx { (dlx_funct_t)IF_funct} IF_funct_decoded
virtual function -install /intdlx -env /intdlx { (dlx_opcode_t)ID_opcode} ID_opc_decoded
virtual function -install /intdlx -env /intdlx { (dlx_funct_t)ID_funct} ID_funct_decoded
virtual function -install /intdlx -env /intdlx { (dlx_opcode_t)EX_opcode} EX_opc_decoded
virtual function -install /intdlx -env /intdlx { (dlx_funct_t)EX_funct} EX_funct_decoded
virtual function -install /intdlx -env /intdlx { (dlx_opcode_t)MEM_opcode} MEM_opc_decoded
virtual function -install /intdlx -env /intdlx { (dlx_funct_t)MEM_funct} MEM_funct_decoded
virtual function -install /intdlx -env /intdlx { (dlx_opcode_t)WB_opcode} WB_opc_decoded
virtual function -install /intdlx -env /intdlx { (dlx_funct_t)WB_funct} WB_funct_decoded
virtual function -install /intdlx -env /intdlx { &{/intdlx/IF_opc_decoded, /intdlx/IF_funct_decoded}} IF_instr_decoded
virtual function -install /intdlx -env /intdlx { &{/intdlx/ID_opc_decoded, /intdlx/ID_funct_decoded}} ID_instr_decoded
virtual function -install /intdlx -env /intdlx { &{/intdlx/EX_opc_decoded, /intdlx/EX_funct_decoded}} EX_instr_decoded
virtual function -install /intdlx -env /intdlx { &{/intdlx/MEM_opc_decoded, /intdlx/MEM_funct_decoded}} MEM_instr_decoded
virtual function -install /intdlx -env /intdlx { &{/intdlx/WB_opc_decoded, /intdlx/WB_funct_decoded}} WB_instr_decoded
virtual type { NOP undef undef undef SLL undef SLR SRA undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef ADD ADDU SUB SUBU AND OR XOR undef SEQ SNE SLT SGT SLE SGE undef undef MOVI2S MOVS2I MOVF MOVD MOVFP2I MOVI2FP undef undef undef undef undef undef undef undef undef undef} dlx_funct_t
virtual type { Rtype Float J JAL BEQZ BNEZ BFPT BFPF ADDI ADDUI SUBI SUBUI ANDI ORI XORI LHI RFE TRAP JR JALR SLLI undef SRLI SRAI SEQI SNEI SLTI SGTI SLEI SGEI undef undef LB LH undef LW LBU LHU LF LD SB SH undef SW undef undef SF SD undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef} dlx_opcode_t
virtual signal -install /intdlx { /intdlx/instruction(31 downto 26)} IF_opcode
virtual signal -install /intdlx { /intdlx/instruction(5 downto 0)} IF_funct
virtual signal -install /intdlx { /intdlx/id_ir(31 downto 26)} ID_opcode
virtual signal -install /intdlx { /intdlx/id_ir(5 downto 0)} ID_funct
virtual signal -install /intdlx { /intdlx/ex_ir(31 downto 26)} EX_opcode
virtual signal -install /intdlx { /intdlx/ex_ir(5 downto 0)} EX_funct
virtual signal -install /intdlx { /intdlx/mem_ir(31 downto 26)} MEM_opcode
virtual signal -install /intdlx { /intdlx/mem_ir(5 downto 0)} MEM_funct
virtual signal -install /intdlx { /intdlx/wb_ir(31 downto 26)} WB_opcode
virtual signal -install /intdlx { /intdlx/wb_ir(5 downto 0)} WB_funct
virtual function -install /intdlx -env /intdlx { (dlx_opcode_t)IF_opcode} IF_opc_decoded
virtual function -install /intdlx -env /intdlx { (dlx_funct_t)IF_funct} IF_funct_decoded
virtual function -install /intdlx -env /intdlx { (dlx_opcode_t)ID_opcode} ID_opc_decoded
virtual function -install /intdlx -env /intdlx { (dlx_funct_t)ID_funct} ID_funct_decoded
virtual function -install /intdlx -env /intdlx { (dlx_opcode_t)EX_opcode} EX_opc_decoded
virtual function -install /intdlx -env /intdlx { (dlx_funct_t)EX_funct} EX_funct_decoded
virtual function -install /intdlx -env /intdlx { (dlx_opcode_t)MEM_opcode} MEM_opc_decoded
virtual function -install /intdlx -env /intdlx { (dlx_funct_t)MEM_funct} MEM_funct_decoded
virtual function -install /intdlx -env /intdlx { (dlx_opcode_t)WB_opcode} WB_opc_decoded
virtual function -install /intdlx -env /intdlx { (dlx_funct_t)WB_funct} WB_funct_decoded
virtual function -install /intdlx -env /intdlx { &{/intdlx/IF_opc_decoded, /intdlx/IF_funct_decoded}} IF_instr_decoded
virtual function -install /intdlx -env /intdlx { &{/intdlx/ID_opc_decoded, /intdlx/ID_funct_decoded}} ID_instr_decoded
virtual function -install /intdlx -env /intdlx { &{/intdlx/EX_opc_decoded, /intdlx/EX_funct_decoded}} EX_instr_decoded
virtual function -install /intdlx -env /intdlx { &{/intdlx/MEM_opc_decoded, /intdlx/MEM_funct_decoded}} MEM_instr_decoded
virtual function -install /intdlx -env /intdlx { &{/intdlx/WB_opc_decoded, /intdlx/WB_funct_decoded}} WB_instr_decoded
