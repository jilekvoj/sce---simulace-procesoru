-- Changes report:
-- 2007/04/06   KK  Pridana nasobicka - vstupy: signaly EX_busA, EX_busB
--                                      vystup: signal EX_Mult_Output do i23combo_proc (4-vstupy mux ve fazi WB)
-- 2007/04/06   MK  Adresy obou pameti jsou ted 20-ti bitove - vyssi bity jsou stejne nevyuzity. Ted jde kompletne
--                  prelozit az do faze Place and Route. Trap necha dokoncit instrukce, ktere jsou pred nim.
--                  Pridan signal Trap_Detected (predevsim) pro potreby ukonceni simulace.
-- 2007/04/06   KK  Pridan FW u nasobicky, ale neni vyresena specialni datova zavislost - viz log.
-- 2007/04/08   KK  TRAP logika nyni upravena tak, ze i instrukce cekajici na forwarding ve fazi EX je spravne
--                  dokoncena (radek 351).
-- 2007/04/15   MK  Slouceni dekoderu ID_Comb_Logic, EX_Comb_Logic, MEM_Comb_Logic a WB_Comb_Logic do bloku ID_EX_MEM_WB_Inst_Decoder
--                  Signaly jdouci z noveho bloku jsou shodne se signaly, ktere sly z puvodnich bloku (Krome signalu WB_Load, ktery je prejmenovan na WB_Select, ALU_OP na EX_ALU_OP a Imm_Type na ID_Imm_Type)
-- 2007/04/15   KK  Odstraneni zavislosti u nasobicky pridanim pomocnych registru (WB2_Stage) a forwardingem. Odstranen timto nepotrebny "prupis" v registrovem poli pri shode indexu.
-- 2007/04/16   KK  Barrel shifter predelan na dvoutaktovy. Zaclenen do jadra podobne jako nasobicka.
-- 2007/05/01   MK  Signal ID_Do_Trap presunut do faze WB. 
--                  Opravena chyba, kdy TRAP zastavoval procesor, kdyz se pred nim nachazel skok, ktery byl vykonan a TRAP mel byt zahozen.
--                  Ted se ceka, az TRAP dojede do faze WB, tam se zastavi a vycisti ostatni stupne pipeline.
-- 2007/05/02   KK  ALU rozdelena na rychlou 1-taktovou a pomalou 2-taktovou cast. WB mux nyni 8-vstupy (vsechny 2-taktove instr.) Pro reseni skoku pridan blok EX_Branch_Executor.
-- 2007/05/06   MK  Fast ALU ted nezpracovava zadnou instrukci je pouze pro ucely vypoctu adresy (bez forwardingu by rychly vypocet stejne nemel smysl)
--                  Kontrola stallu a clearu pro pipeline registry presunuta do bloku Pipeline_Flow_Ctrl
--                  Nekolik zmen v nazvech instanci, nektere pipeline registry (cisla Rw registru) prehozeny do dekoderu
-- 2007/05/11   MK  Pridan signal MEM_Load
-- 2007/05/18   JF  Pridan signal ICache_hit
-- 2007/05/21   MJ  Rozsirena Pipeline_Flow_Ctrl o signaly pro dat. cache - DCache_hit, MEM_Load, MEM_Store
-- 2007/05/25   MK  Vstupy nasobicky rozsireny na 32 bitu

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

entity IntDLX is

  port(
      Clk            : in  std_logic;
      Reset          : in  std_logic;

      D_Addr         : out std_logic_vector(19 downto 0);
      D_to_Mem       : out std_logic_vector(31 downto 0);
      D_Wr_En        : out std_logic;
      D_Load         : out std_logic;
      D_From_Mem     : in  std_logic_vector(31 downto 0);
      D_Cache_Hit    : in  std_logic;

      I_Addr         : out std_logic_vector(19 downto 0);
      Instr_from_Mem : in  std_logic_vector(31 downto 0);
      ICache_hit     : in  std_logic;

      Trap_Detected  : out std_logic
      );

end IntDLX ;

ARCHITECTURE struct OF IntDLX IS

   -- Internal signal declarations
   SIGNAL ALU_B          : std_logic_vector(31 DOWNTO 0);
   SIGNAL EX_ALU_OP      : std_logic_vector(2 DOWNTO 0);
   SIGNAL EX_ALUSrc      : std_logic;
   SIGNAL EX_Branch      : std_logic_vector(1 DOWNTO 0);
   SIGNAL EX_Clear       : std_logic;
   SIGNAL EX_IR          : std_logic_vector(31 DOWNTO 0);
   SIGNAL EX_Imm32       : std_logic_vector(31 DOWNTO 0);
   SIGNAL EX_PC          : std_logic_vector(31 DOWNTO 0);
   SIGNAL EX_Ra          : std_logic_vector(4 DOWNTO 0);
   SIGNAL EX_Rb          : std_logic_vector(4 DOWNTO 0);
   SIGNAL EX_Rw          : std_logic_vector(4 DOWNTO 0);
   SIGNAL EX_Stall       : std_logic;
   SIGNAL EX_Target_Addr : std_logic_vector(31 DOWNTO 0);
   SIGNAL EX_busA        : std_logic_vector(31 DOWNTO 0);
   SIGNAL EX_busB        : std_logic_vector(31 DOWNTO 0);
   SIGNAL EX_jmp_reg     : std_logic;

----------------------------------------------------------------
   --- Custom Signals
----------------------------------------------------------------
   SIGNAL ALU_C_Fast         : std_logic_vector(31 DOWNTO 0);
  
   SIGNAL WB_Mem             : std_logic_vector(31 DOWNTO 0);
   SIGNAL WB_Mult            : std_logic_vector(31 DOWNTO 0);
   SIGNAL WB_Barrel          : std_logic_vector(31 DOWNTO 0);
   SIGNAL WB_ALU             : std_logic_vector(31 DOWNTO 0);
   SIGNAL WB_Cond_Res        : std_logic_vector(31 DOWNTO 0);

   SIGNAL EX_Unsigned_Comp   : std_logic;
   
   SIGNAL WB_Trap_Detected   : std_logic;
----------------------------------------------------------------

   SIGNAL ID_IR           : std_logic_vector(31 DOWNTO 0);
   SIGNAL ID_Imm32        : std_logic_vector(31 DOWNTO 0);
   SIGNAL ID_Ra           : std_logic_vector(4 DOWNTO 0);
   SIGNAL ID_Rb           : std_logic_vector(4 DOWNTO 0);
   SIGNAL ID_Rw           : std_logic_vector(4 DOWNTO 0);
   SIGNAL ID_busA         : std_logic_vector(31 DOWNTO 0);
   SIGNAL ID_busB         : std_logic_vector(31 DOWNTO 0);
   SIGNAL ID_clear        : std_logic;
   SIGNAL ID_stall        : std_logic;
   SIGNAL IF_PC           : std_logic_vector(31 DOWNTO 0);
   SIGNAL IF_stall        : std_logic;
   SIGNAL ID_Imm_type     : std_logic_vector(2 DOWNTO 0);
   SIGNAL Instruction     : std_logic_vector(31 DOWNTO 0);
   SIGNAL Jump_now        : std_logic;
   SIGNAL MEM_Clear       : std_logic;
   SIGNAL MEM_IR          : std_logic_vector(31 DOWNTO 0);
   SIGNAL MEM_M           : std_logic_vector(31 DOWNTO 0);
   SIGNAL MEM_dual_cycle  : std_logic;
   SIGNAL MEM_Rw          : std_logic_vector(4 DOWNTO 0);
   SIGNAL MEM_S           : std_logic_vector(31 DOWNTO 0);
   SIGNAL MEM_Stall       : std_logic;
   SIGNAL MEM_Load        : std_logic;
   SIGNAL MEM_Store       : std_logic;
   SIGNAL MEM_PC          : std_logic_vector(31 DOWNTO 0);
   SIGNAL MEM_busB        : std_logic_vector(31 DOWNTO 0);
   SIGNAL Next_PC         : std_logic_vector(31 DOWNTO 0);
   SIGNAL WB_IR           : std_logic_vector(31 DOWNTO 0);
   SIGNAL WB_Select       : std_logic_vector(2 DOWNTO 0);
   SIGNAL WB_Rw           : std_logic_vector(4 DOWNTO 0);
   SIGNAL WB_PC           : std_logic_vector(31 DOWNTO 0);
   SIGNAL WB_clear        : std_logic;
   SIGNAL WB_stall        : std_logic;
   SIGNAL busW            : std_logic_vector(31 DOWNTO 0);

--------------------------------------------------------------------------------------------------
   --- Custom Component Declarations
--------------------------------------------------------------------------------------------------

   COMPONENT Barrel_shifter
   PORT (
      left    : in  std_logic; -- '1' for left, '0' for right
      logical : in  std_logic; -- '1' logicky, '0' aritmeticky (pravy nezavisle na nastaveni "left"),
      shift   : in  std_logic_vector (4 downto 0);  -- shift count
      input   : in  std_logic_vector (31 downto 0);
      output  : out std_logic_vector (31 downto 0);
      clk     : in  std_logic
   );
   END COMPONENT;
   COMPONENT cond_resolver
   PORT(
      Clk : IN std_logic;
      Input1 : IN std_logic_vector(31 downto 0);
      Input2 : IN std_logic_vector(31 downto 0);
      Unsigned : IN std_logic;
      Test_Equal : IN std_logic;
      Test_Lesser : IN std_logic;
      Test_Greater : IN std_logic;          
      Output_Reg : OUT std_logic_vector(31 downto 0)
   );
   END COMPONENT;
   COMPONENT Multiplier
   PORT (
      A : in std_logic_vector(31 downto 0);
      B : in std_logic_vector(31 downto 0);
      Y : out std_logic_vector(31 downto 0);
      clk : in std_logic);
   END COMPONENT;
   COMPONENT ID_EX_MEM_WB_Inst_Decoder
   PORT(
      ID_IR : IN std_logic_vector(31 downto 0);
      Clk : IN std_logic;
      Reset : IN std_logic;
      EX_clear : IN std_logic;
      EX_stall : IN std_logic;
      MEM_clear : IN std_logic;
      MEM_stall : IN std_logic;
      WB_clear : IN std_logic;
      WB_stall : IN std_logic;          
      ID_Ra : OUT std_logic_vector(4 downto 0);
      ID_Rb : OUT std_logic_vector(4 downto 0);
      ID_Rw : OUT std_logic_vector(4 downto 0);
      ID_Imm_type : OUT std_logic_vector(2 downto 0);
      EX_Branch : OUT std_logic_vector(1 downto 0);
      EX_Ra : OUT std_logic_vector(4 downto 0);
      EX_Rb : OUT std_logic_vector(4 downto 0);
      EX_ALUSrc : OUT std_logic;
      EX_ALU_OP : OUT std_logic_vector(2 downto 0);
      EX_Rw : OUT std_logic_vector(4 downto 0);
      EX_Jmp_reg : OUT std_logic;
      EX_Unsigned : OUT std_logic;
      MEM_Load : OUT std_logic;
      MEM_Store : OUT std_logic;
      MEM_dual_cycle : OUT std_logic;
      MEM_Rw : OUT std_logic_vector(4 downto 0);
      WB_Select : OUT std_logic_vector(2 downto 0);
      WB_Do_Trap : OUT std_logic;
      WB_Rw : OUT std_logic_vector(4 downto 0)
      );
   END COMPONENT;
   COMPONENT EX_Branch_Executor
   PORT( 
      Clk           : IN     std_logic;
      EX_Branch     : IN     std_logic_vector (1 DOWNTO 0);
      EX_Jmp_Reg    : IN     std_logic;
      EX_Stall      : IN     std_logic;
      Register_In_A : IN     std_logic_vector (31 DOWNTO 0);
      Register_In_B : IN     std_logic_vector (31 DOWNTO 0);
      PC_In         : IN     std_logic_vector (31 DOWNTO 0);
      Imm_In        : IN     std_logic_vector (31 DOWNTO 0);
      Address       : OUT    std_logic_vector (31 DOWNTO 0);
      Jump_now      : OUT    std_logic
   );
   END COMPONENT;
  COMPONENT ALU_Fast
   PORT (
      F_A  : IN  std_logic_vector (31 DOWNTO 0);
      F_B  : IN  std_logic_vector (31 DOWNTO 0);
      F_C  : OUT std_logic_vector (31 DOWNTO 0)
   );
   END COMPONENT;
   COMPONENT ALU_Slow
   PORT (
      S_A  : IN  std_logic_vector (31 DOWNTO 0);
      S_B  : IN  std_logic_vector (31 DOWNTO 0);
      S_OP : IN  std_logic_vector (2 DOWNTO 0);
      S_C  : OUT std_logic_vector (31 DOWNTO 0);
    CLK  : IN  std_logic
   );
   END COMPONENT;
--------------------------------------------------------------------------------------------------
   COMPONENT Extender
   PORT (
      PCOffset26 : IN     std_logic_vector (25 DOWNTO 0);
      Imm_type   : IN     std_logic_vector (2 DOWNTO 0);
      Imm32      : OUT    std_logic_vector (31 DOWNTO 0)
   );
   END COMPONENT;
   COMPONENT Next_addr_logic
   PORT (
      IF_PC          : IN     std_logic_vector (31 DOWNTO 0);
      Jump           : IN     std_logic;
      Target_Address : IN     std_logic_vector (31 DOWNTO 0);
      Next_PC        : OUT    std_logic_vector (31 DOWNTO 0)
   );
   END COMPONENT;
   COMPONENT PC
   PORT (
      Clk        : IN     std_logic ;
      NextPC     : IN     std_logic_vector (31 DOWNTO 0);
      Reset      : IN     std_logic ;
      Stall      : IN     std_logic ;
      Instr_Addr : OUT    std_logic_vector (31 DOWNTO 0)
   );
   END COMPONENT;
   COMPONENT Pipeline_reg
   GENERIC (
      N : integer := 32
   );
   PORT (
      Clear : IN     std_logic ;
      Clk   : IN     std_logic ;
      DIN   : IN     std_logic_vector (N-1 DOWNTO 0);
      Reset : IN     std_logic ;
      Stall : IN     std_logic ;
      DOUT  : OUT    std_logic_vector (N-1 DOWNTO 0)
   );
   END COMPONENT;
   COMPONENT Registers
   PORT (
      Clk   : IN     std_logic ;
      Ra    : IN     std_logic_vector (4 DOWNTO 0);
      Rb    : IN     std_logic_vector (4 DOWNTO 0);
      Rw    : IN     std_logic_vector (4 DOWNTO 0);
      W     : IN     std_logic_vector (31 DOWNTO 0);
      A     : OUT    std_logic_vector (31 DOWNTO 0);
      B     : OUT    std_logic_vector (31 DOWNTO 0)
   );
   END COMPONENT;

   COMPONENT Pipeline_Flow_Ctrl
   Port (
      Clk               : in  STD_LOGIC;
      Reset             : in  STD_LOGIC;
      WB_Trap_Detected  : in  STD_LOGIC;
      Jump_Now          : in  STD_LOGIC;

      ICache_hit        : in  STD_LOGIC;

      DCache_hit        : in  STD_LOGIC;
      MEM_Load          : in  STD_LOGIC;
      MEM_Store         : in  STD_LOGIC;
      
      ID_Ra             : in  STD_LOGIC_VECTOR (4 downto 0);
      ID_Rb             : in  STD_LOGIC_VECTOR (4 downto 0);
      ID_Rw             : in  STD_LOGIC_VECTOR (4 downto 0);
      EX_Ra             : in  STD_LOGIC_VECTOR (4 downto 0);
      EX_Rb             : in  STD_LOGIC_VECTOR (4 downto 0);
      EX_Rw             : in  STD_LOGIC_VECTOR (4 downto 0);
      MEM_Rw            : in  STD_LOGIC_VECTOR (4 downto 0);
      WB_Rw             : in  STD_LOGIC_VECTOR (4 downto 0);
      IF_Stall          : out  STD_LOGIC;
      ID_Stall          : out  STD_LOGIC;
      EX_Stall          : out  STD_LOGIC;
      MEM_Stall         : out  STD_LOGIC;
      WB_Stall          : out  STD_LOGIC;

      ID_Clear          : out  STD_LOGIC;
      EX_Clear          : out  STD_LOGIC;
      MEM_Clear         : out  STD_LOGIC;
      WB_Clear          : out  STD_LOGIC
   );
   END COMPONENT;

--------------------------------------------------------------------------------------------

BEGIN

   Trap_Detected <= WB_Trap_Detected;

--------------------------------------------------------------------------------------------
   --- Custom Component Instantiation
--------------------------------------------------------------------------------------------

   Pipeline_Flow_Ctrl_Inst : Pipeline_Flow_Ctrl
   PORT MAP (
      Clk => Clk,
      Reset => Reset,
      WB_Trap_Detected => WB_Trap_Detected,
      Jump_Now => Jump_Now,
    
      ICache_hit => ICache_hit,

      DCache_hit => D_Cache_Hit,
      MEM_Load    => MEM_Load,
      MEM_Store  => MEM_Store,

      ID_Ra => ID_Ra,
      ID_Rb => ID_Rb,
      ID_Rw => ID_Rw,
      EX_Ra => EX_Ra,
      EX_Rb => EX_Rb,
      EX_Rw => EX_Rw,
      MEM_Rw => MEM_Rw,
      WB_Rw => WB_Rw,
      IF_Stall => IF_Stall,
      ID_Stall => ID_Stall,
      EX_Stall => EX_Stall,
      MEM_Stall => MEM_Stall,
      WB_Stall => WB_Stall,

      ID_Clear => ID_Clear,
      EX_Clear => EX_Clear,
      MEM_Clear => MEM_Clear,
      WB_Clear => WB_Clear
   );
   
   ALU_F : ALU_Fast
   PORT MAP (
      F_A => EX_busA,
      F_B => ALU_B,
      F_C => ALU_C_Fast
   );
   ALU_S : ALU_Slow
   PORT MAP (
      S_A  => EX_busA,
      S_B  => ALU_B,
      S_OP => EX_ALU_OP,
      S_C  => WB_ALU,
      Clk => Clk
   );
   Barrel : Barrel_shifter
   PORT MAP(
      left => EX_ALU_OP(1),
      logical => EX_ALU_OP(0),
      shift => ALU_B(4 downto 0),
      input => EX_busA,
      output => WB_Barrel,
      Clk => Clk
   );
   Mult : Multiplier
   PORT MAP (
      A => EX_busA,
      B => ALU_B,
      Y => WB_Mult,
      clk => Clk
   );
   Resolver : cond_resolver 
   PORT MAP(
      Clk => Clk,
      Input1 => EX_busA,
      Input2 => ALU_B,
      Unsigned => EX_Unsigned_Comp,
      Test_Equal => EX_ALU_OP(0),
      Test_Lesser => EX_ALU_OP(1),
      Test_Greater => EX_ALU_OP(2),
      Output_Reg => WB_Cond_Res
   );
   
   ID_EX_MEM_WB_Inst_Decoder_inst: ID_EX_MEM_WB_Inst_Decoder 
   PORT MAP(
      ID_IR => ID_IR,
      Clk => Clk,
      Reset => Reset,
      EX_clear => EX_clear,
      EX_stall => EX_stall,
      MEM_clear => MEM_clear,
      MEM_stall => MEM_stall,
      WB_clear => WB_clear,
      WB_stall => WB_stall,
      ID_Ra => ID_Ra,
      ID_Rb => ID_Rb,
      ID_Rw => ID_Rw,
      ID_Imm_type => ID_Imm_type,
      EX_Branch => EX_Branch,
      EX_Ra => EX_Ra,
      EX_Rb => EX_Rb,
      EX_ALUSrc => EX_ALUSrc,
      EX_ALU_OP => EX_ALU_OP,
      EX_Rw => EX_Rw,
      EX_Jmp_reg => EX_Jmp_reg,
      EX_Unsigned => EX_Unsigned_Comp,
      MEM_Load => MEM_Load,
      MEM_Store => MEM_Store,
      MEM_dual_cycle => MEM_dual_cycle,
      MEM_Rw => MEM_Rw,
      WB_Select => WB_Select,
      WB_Do_Trap => WB_Trap_Detected,
      WB_Rw => WB_Rw
   );
   
   -- ModuleWare code(v1.5) for instance 'I23' of 'mux'
   busW_Mux : PROCESS(WB_Mem,WB_Mult,WB_Barrel,WB_ALU,WB_Cond_Res,WB_PC,WB_Select)
   BEGIN
      CASE WB_Select IS
         WHEN "001" => busW <= WB_Mem;
         WHEN "010" => busW <= WB_Mult;
         WHEN "011" => busW <= WB_Barrel;
         WHEN "100" => busW <= WB_ALU;
         WHEN "101" => busW <= WB_Cond_Res;
         WHEN "110" => busW <= WB_PC;
         WHEN OTHERS => busW <= (OTHERS => '-');
      END CASE;
   END PROCESS busW_Mux;
   
   EX_Branch_Executor_Inst : EX_Branch_Executor
      PORT MAP (
         Clk             => Clk,
         EX_Branch       => EX_Branch,
         EX_Jmp_Reg      => EX_Jmp_Reg,
         EX_Stall        => EX_Stall,
         Register_In_A   => EX_busA,
         Register_In_B   => EX_busB,
         PC_In           => EX_PC,
         Imm_In          => EX_Imm32,
         Address         => EX_Target_Addr,
         Jump_now        => Jump_now
      );

--------------------------------------------------------------------------------------------

   ALU_B_Mux : PROCESS(EX_busB, EX_Imm32, EX_ALUSrc)
   BEGIN
      CASE EX_ALUSrc IS
         WHEN '0' => ALU_B <= EX_busB;
         WHEN '1' => ALU_B <= EX_Imm32;
         WHEN OTHERS => ALU_B <= (OTHERS => '-');
      END CASE;
   END PROCESS ALU_B_Mux;

--------------------------------------------------------------------------------

   D_Addr <= MEM_S(19 DOWNTO 0);
   D_to_Mem <= MEM_busB;
   D_Wr_En <= MEM_Store;
   D_Load  <= MEM_Load;
   MEM_M <= D_From_Mem;

   Extender_Inst : Extender
      PORT MAP (
         PCOffset26 => ID_IR(25 DOWNTO 0),
         Imm_type   => ID_Imm_type,
         Imm32      => ID_Imm32
      );
   Next_addr_logic_Inst : Next_addr_logic
      PORT MAP (
         IF_PC          => IF_PC,
         Jump           => Jump_now,
         Target_address => EX_Target_Addr,
         Next_PC        => Next_PC
      );
   Program_Counter_Inst : PC
      PORT MAP (
         Clk        => Clk,
         NextPC     => Next_PC,
         Reset      => Reset,
         Stall      => IF_stall,
         Instr_Addr => IF_PC
      );
      
   BusA_v_EX_Reg : Pipeline_reg
      GENERIC MAP (
         N => 32
      )
      PORT MAP (
         Clear => EX_Clear,
         Clk   => Clk,
         DIN   => ID_busA,
         Reset => Reset,
         Stall => EX_Stall,
         DOUT  => EX_busA
      );
   BusB_v_EX_Reg : Pipeline_reg
      GENERIC MAP (
         N => 32
      )
      PORT MAP (
         Clear => EX_Clear,
         Clk   => Clk,
         DIN   => ID_busB,
         Reset => Reset,
         Stall => EX_Stall,
         DOUT  => EX_busB
      );
   BusB_v_MEM_Reg : Pipeline_reg
      GENERIC MAP (
         N => 32
      )
      PORT MAP (
         Clear => MEM_Clear,
         Clk   => Clk,
         DIN   => EX_busB,
         Reset => Reset,
         Stall => MEM_Stall,
         DOUT  => MEM_busB
      );
   Memory_Path_v_WB_Reg : Pipeline_reg
      GENERIC MAP (
         N => 32
      )
      PORT MAP (
         Clear => WB_clear,
         Clk   => Clk,
         DIN   => MEM_M,
         Reset => Reset,
         Stall => WB_stall,
         DOUT  => WB_Mem
      );
   Fast_Path_v_MEM_Reg : Pipeline_reg
      GENERIC MAP (
         N => 32
      )
      PORT MAP (
         Clear => MEM_Clear,
         Clk   => Clk,
         DIN   => ALU_C_Fast,
         Reset => Reset,
         Stall => MEM_Stall,
         DOUT  => MEM_S
      );
   Imm32_v_EX_Reg : Pipeline_reg
      GENERIC MAP (
         N => 32
      )
      PORT MAP (
         Clear => EX_Clear,
         Clk   => Clk,
         DIN   => ID_Imm32,
         Reset => Reset,
         Stall => EX_Stall,
         DOUT  => EX_Imm32
      );
   PC_v_EX_Reg : Pipeline_reg
      GENERIC MAP (
         N => 32
      )
      PORT MAP (
         Clear => EX_Clear,
         Clk   => Clk,
         DIN   => IF_PC,
         Reset => Reset,
         Stall => EX_Stall,
         DOUT  => EX_PC
      );
   PC_v_MEM_Reg : Pipeline_reg
      GENERIC MAP (
         N => 32
      )
      PORT MAP (
         Clear => MEM_Clear,
         Clk   => Clk,
         DIN   => EX_PC,
         Reset => Reset,
         Stall => MEM_Stall,
         DOUT  => MEM_PC
      );
   PC_v_WB_Reg : Pipeline_reg
      GENERIC MAP (
         N => 32
      )
      PORT MAP (
         Clear => WB_Clear,
         Clk   => Clk,
         DIN   => MEM_PC,
         Reset => Reset,
         Stall => WB_Stall,
         DOUT  => WB_PC
      );
      
   IR_v_ID_Reg : Pipeline_reg
      GENERIC MAP (
         N => 32
      )
      PORT MAP (
         Clear => ID_clear,
         Clk   => Clk,
         DIN   => Instruction,
         Reset => Reset,
         Stall => ID_stall,
         DOUT  => ID_IR
      );
   IR_v_EX_Reg : Pipeline_reg
      GENERIC MAP (
         N => 32
      )
      PORT MAP (
         Clear => EX_Clear,
         Clk   => Clk,
         DIN   => ID_IR,
         Reset => Reset,
         Stall => EX_Stall,
         DOUT  => EX_IR
      );
      
   IR_v_MEM_Reg : Pipeline_reg
      GENERIC MAP (
         N => 32
      )
      PORT MAP (
         Clear => MEM_Clear,
         Clk   => Clk,
         DIN   => EX_IR,
         Reset => Reset,
         Stall => MEM_Stall,
         DOUT  => MEM_IR
      );
   IR_v_WB_Reg : Pipeline_reg
      GENERIC MAP (
         N => 32
      )
      PORT MAP (
         Clear => WB_clear,
         Clk   => Clk,
         DIN   => MEM_IR,
         Reset => Reset,
         Stall => WB_stall,
         DOUT  => WB_IR
      );


   I_Addr <= IF_PC(19 DOWNTO 0);
   Instruction <= Instr_from_Mem;

   Registers_Inst : Registers
      PORT MAP (
         Clk   => Clk,
         Ra    => ID_Ra,
         Rb    => ID_Rb,
         Rw    => WB_Rw,
         W     => busW,
         A     => ID_busA,
         B     => ID_busB
      );

END struct;
