-- Changes report:
-- 2006/4/6   MK  Adresy obou pameti jsou ted 20-ti bitove - vyssi bity jsou stejne nevyuzity

LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all;

LIBRARY std_developerskit;
USE std_developerskit.Std_Mempak.all;
--USE work.Std_Mempak.all;

--USE work.Std_IOpak.all;
--USE work.Std_timing.all;

ENTITY Data_memory IS
   PORT( 
      Addr  : IN     std_logic_vector (19 DOWNTO 0);
      Clk   : IN     std_logic;
      Din   : IN     std_logic_vector (31 DOWNTO 0);
      Reset : IN     std_logic;
      Wr_En : IN     std_logic;
      Dout  : OUT    std_logic_vector (31 DOWNTO 0)
   );

-- Declarations

END Data_memory ;

--
ARCHITECTURE behav OF Data_memory IS

CONSTANT preloadfile: string:="data_mem.txt";
CONSTANT dumpfile: string:="data_mem_out.txt";


BEGIN

   ---------------------------------------------------------------------------
   model : PROCESS
   ---------------------------------------------------------------------------

   -- Process declarations
   -- memory data structure pointer
   variable mem: mem_id_type;
   variable data_read : std_logic_vector(31 downto 0);
   variable address: std_logic_vector(15 downto 2);


   BEGIN
      -- initialize from file
      mem:=SRAM_INITIALIZE (name => "Data memory",
                          length => 65536,
                          width => 8,
      default_word => std_logic_vector'("")
      );
      
      MEM_LOAD ( mem_id => mem,
               file_name => preloadfile
        );
       
      l0:  LOOP
        
        wait until clk'event and clk='1'; -- wait on clock
        wait for 1 ns;               
        Address:=ADDR(15 DOWNTO 2);  
  
        MEM_READ( mem_id => mem,
                     address => Address & "11",
              data => data_read(7 downto 0) 
              );
        MEM_READ( mem_id => mem,
                      address => Address & "10",
                      data => data_read(15 downto 8) 
                      );
        MEM_READ( mem_id => mem,
                      address => Address & "01",
                      data => data_read(23 downto 16) 
                      );
        MEM_READ( mem_id => mem,
                      address => Address & "00",
                      data => data_read(31 downto 24) 
                      );
        -- note BIG Endian is used !!!
        
        Dout <= (OTHERS=>'X'),data_read after 4 ns;        
        
        IF Wr_EN='1' THEN
           MEM_WRITE (mem_id => mem,
                       address => Address & "11",
                       data => Din(7 downto 0),
                       write_per_bit => FALSE
                       );
           MEM_WRITE (mem_id => mem,
                       address => Address & "10",
                       data => Din(15 downto 8),
                       write_per_bit => FALSE
                       );
           MEM_WRITE (mem_id => mem,
                        address => Address & "01",
                        data => Din(23 downto 16),
                        write_per_bit => FALSE
                        );
           MEM_WRITE (mem_id => mem,
                        address => Address & "00",
                        data => Din(31 downto 24),
                        write_per_bit => FALSE
                        );             
        -- Dump memory on write                       
        MEM_DUMP ( mem_id => mem,
                     file_name => dumpfile,
         start_addr => 0,
         end_addr => 100000,
         header_flag => TRUE
         );

        END IF;               
         
           
       
      END LOOP l0;
      
   END PROCESS model;

END behav;
 
