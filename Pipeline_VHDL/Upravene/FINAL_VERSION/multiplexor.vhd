library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_unsigned.all;

entity MUX is
generic (
	dw : natural			-- data width (bits)
	);
port (
      i0: 	  in std_logic_vector(dw-1 downto 0);
		i1: 	  in std_logic_vector(dw-1 downto 0);
		vyber:  in std_logic;
		vystup: out std_logic_vector(dw-1 downto 0)
		);
end MUX;

architecture rtl of MUX is
begin

vystup <= i0 WHEN vyber='0'
	  ELSE i1;

end rtl;
