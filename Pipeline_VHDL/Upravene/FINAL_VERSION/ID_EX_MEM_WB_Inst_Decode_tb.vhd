
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_unsigned.all;
USE ieee.std_logic_arith.all;
USE ieee.numeric_std.ALL;

ENTITY ID_EX_MEM_WB_Inst_Decode_tb IS
END ID_EX_MEM_WB_Inst_Decode_tb;

ARCHITECTURE behavior OF ID_EX_MEM_WB_Inst_Decode_tb IS 

	-- Component Declaration for the Unit Under Test (UUT)
	COMPONENT ID_EX_MEM_WB_Inst_Decoder
	PORT(
		ID_IR : IN std_logic_vector(31 downto 0);
		Clk : IN std_logic;
		Reset : IN std_logic;
		EX_clear : IN std_logic;
		EX_stall : IN std_logic;
		MEM_clear : IN std_logic;
		MEM_stall : IN std_logic;
		WB_clear : IN std_logic;
		WB_stall : IN std_logic;          
		ID_Branch : OUT std_logic_vector(1 downto 0);
		ID_Ra : OUT std_logic_vector(4 downto 0);
		ID_Rb : OUT std_logic_vector(4 downto 0);
		ID_Imm_type : OUT std_logic_vector(1 downto 0);
		ID_Do_Trap : OUT std_logic;
		EX_Branch : OUT std_logic_vector(1 downto 0);
		EX_Ra : OUT std_logic_vector(4 downto 0);
		EX_Rb : OUT std_logic_vector(4 downto 0);
		EX_ALUSrc : OUT std_logic;
		EX_ALU_OP : OUT std_logic_vector(2 downto 0);
		EX_Rw : OUT std_logic_vector(4 downto 0);
		EX_Jmp_link : OUT std_logic;
		EX_Jmp_reg : OUT std_logic;
		EX_Load : OUT std_logic;
		EX_Store : OUT std_logic;
		EX_Unsigned : OUT std_logic;
		EX_Selector : OUT std_logic;
		MEM_Load : OUT std_logic;
		MEM_Store : OUT std_logic;
		MEM_Mult : OUT std_logic;
		WB_Store : OUT std_logic;
		WB_Select : OUT std_logic_vector(1 downto 0)
		);
	END COMPONENT;

	--Inputs
	SIGNAL Clk :  std_logic := '0';
	SIGNAL Reset :  std_logic := '0';
	SIGNAL EX_clear :  std_logic := '0';
	SIGNAL EX_stall :  std_logic := '0';
	SIGNAL MEM_clear :  std_logic := '0';
	SIGNAL MEM_stall :  std_logic := '0';
	SIGNAL WB_clear :  std_logic := '0';
	SIGNAL WB_stall :  std_logic := '0';
	SIGNAL ID_IR :  std_logic_vector(31 downto 0) := (others=>'0');

	--Outputs
	SIGNAL ID_Branch :  std_logic_vector(1 downto 0);
	SIGNAL ID_Ra :  std_logic_vector(4 downto 0);
	SIGNAL ID_Rb :  std_logic_vector(4 downto 0);
	SIGNAL ID_Imm_type :  std_logic_vector(1 downto 0);
	SIGNAL ID_Do_Trap :  std_logic;
	SIGNAL EX_Branch :  std_logic_vector(1 downto 0);
	SIGNAL EX_Ra :  std_logic_vector(4 downto 0);
	SIGNAL EX_Rb :  std_logic_vector(4 downto 0);
	SIGNAL EX_ALUSrc :  std_logic;
	SIGNAL EX_ALU_OP :  std_logic_vector(2 downto 0);
	SIGNAL EX_Rw :  std_logic_vector(4 downto 0);
	SIGNAL EX_Jmp_link :  std_logic;
	SIGNAL EX_Jmp_reg :  std_logic;
	SIGNAL EX_Load :  std_logic;
	SIGNAL EX_Store :  std_logic;
	SIGNAL EX_Unsigned :  std_logic;
	SIGNAL EX_Selector :  std_logic;
	SIGNAL MEM_Load :  std_logic;
	SIGNAL MEM_Store :  std_logic;
	SIGNAL MEM_Mult :  std_logic;
	SIGNAL WB_Store :  std_logic;
	SIGNAL WB_Select :  std_logic_vector(1 downto 0);

BEGIN

	-- Instantiate the Unit Under Test (UUT)
	uut: ID_EX_MEM_WB_Inst_Decoder PORT MAP(
		ID_IR => ID_IR,
		Clk => Clk,
		Reset => Reset,
		EX_clear => EX_clear,
		EX_stall => EX_stall,
		MEM_clear => MEM_clear,
		MEM_stall => MEM_stall,
		WB_clear => WB_clear,
		WB_stall => WB_stall,
		ID_Branch => ID_Branch,
		ID_Ra => ID_Ra,
		ID_Rb => ID_Rb,
		ID_Imm_type => ID_Imm_type,
		ID_Do_Trap => ID_Do_Trap,
		EX_Branch => EX_Branch,
		EX_Ra => EX_Ra,
		EX_Rb => EX_Rb,
		EX_ALUSrc => EX_ALUSrc,
		EX_ALU_OP => EX_ALU_OP,
		EX_Rw => EX_Rw,
		EX_Jmp_link => EX_Jmp_link,
		EX_Jmp_reg => EX_Jmp_reg,
		EX_Load => EX_Load,
		EX_Store => EX_Store,
		EX_Unsigned => EX_Unsigned,
		EX_Selector => EX_Selector,
		MEM_Load => MEM_Load,
		MEM_Store => MEM_Store,
		MEM_Mult => MEM_Mult,
		WB_Store => WB_Store,
		WB_Select => WB_Select
	);
  
  Clk_Gen: process
  begin
    wait for 10 ns;
    Clk <= '1';
    wait for 10 ns;
    Clk <= '0';
  end process;

  Reset_Gen: process
  begin
    Reset <= '1';
    wait for 55 ns;
    Reset <= '0';
    wait;
  end process;

  tb : PROCESS
  BEGIN

    wait until Reset = '0';
    wait until Clk = '1';
    
    ID_IR <= (others=>'0');
    
    for i in 0 to 2**6-1 loop
       wait until Clk = '1';
       wait until Clk = '1';
       wait until Clk = '1';
       wait until Clk = '1';
       wait until Clk = '1';
       wait until Clk = '1';
       wait for 1 ns;
       ID_IR <= "00000000000000000000000000" & CONV_STD_LOGIC_VECTOR (i, 6);
    end loop;

    wait until Clk = '1';
    wait until Clk = '1';
    wait until Clk = '1';
    wait until Clk = '1';
    wait until Clk = '1';
    wait until Clk = '1';
    wait until Clk = '1';
    wait until Clk = '1';
    wait until Clk = '1';
    wait until Clk = '1';
    wait until Clk = '1';
    wait until Clk = '1';
    
    for i in 0 to 2**6-1 loop
       wait until Clk = '1';
       wait until Clk = '1';
       wait until Clk = '1';
       wait until Clk = '1';
       wait until Clk = '1';
       wait until Clk = '1';
       wait for 1 ns;
       ID_IR <= CONV_STD_LOGIC_VECTOR (i, 6) & "00000000000000000000000000";
    end loop;
    
  

    assert false
      report ("Konec")
      severity failure;

  END PROCESS;

END;
