LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
--USE ieee.std_logic_unsigned.all;
USE ieee.numeric_std.ALL;
USE ieee.std_logic_arith.ALL;

ENTITY ALU_tb_vhd IS
END ALU_tb_vhd;

ARCHITECTURE behavior OF ALU_tb_vhd IS 

	-- Component Declaration for the Unit Under Test (UUT)
	COMPONENT alu
	PORT(
		ALU_A : IN std_logic_vector(31 downto 0);
		ALU_B : IN std_logic_vector(31 downto 0);
		ALU_OP : IN std_logic_vector(2 downto 0);          
		ALU_C : OUT std_logic_vector(31 downto 0);
		ZERO : OUT std_logic
  );
	END COMPONENT;

	--Inputs
	SIGNAL ALU_A :  std_logic_vector(31 downto 0) := (others=>'0');
	SIGNAL ALU_B :  std_logic_vector(31 downto 0) := (others=>'0');
	SIGNAL ALU_OP :  std_logic_vector(2 downto 0) := (others=>'0');

	--Outputs
	SIGNAL ALU_C     :  std_logic_vector(31 downto 0);
	SIGNAL ZERO      :  std_logic;


	SIGNAL A :  std_logic_vector(31 downto 0) := (others=>'0');
	SIGNAL B :  std_logic_vector(31 downto 0) := (others=>'0');

BEGIN

	-- Instantiate the Unit Under Test (UUT)
	uut: alu PORT MAP(
		ALU_A => ALU_A,
		ALU_B => ALU_B,
		ALU_OP => ALU_OP,
		ALU_C => ALU_C,
		ZERO => ZERO
	);

  ALU_A <= A; -- Pro snadne obraceni vstupu
  ALU_B <= B;


	tb : PROCESS
	BEGIN


    ALU_OP <= "010";

    wait for 10 ns;

    A <= CONV_STD_LOGIC_VECTOR(300, 32);
    B <= CONV_STD_LOGIC_VECTOR(600, 32);

    wait for 10 ns;

    A <= CONV_STD_LOGIC_VECTOR(300, 32);
    B <= CONV_STD_LOGIC_VECTOR(300, 32);

    wait for 10 ns;

    A <= CONV_STD_LOGIC_VECTOR(300, 32);
    B <= CONV_STD_LOGIC_VECTOR(100, 32);

    wait for 10 ns;

    A <= CONV_STD_LOGIC_VECTOR(300, 32);
    B <= CONV_STD_LOGIC_VECTOR(0, 32);

    wait for 10 ns;

    A <= CONV_STD_LOGIC_VECTOR(300, 32);
    B <= CONV_STD_LOGIC_VECTOR(-100, 32);

    wait for 10 ns;

    A <= CONV_STD_LOGIC_VECTOR(300, 32);
    B <= CONV_STD_LOGIC_VECTOR(-300, 32);

    wait for 10 ns;

    A <= CONV_STD_LOGIC_VECTOR(300, 32);
    B <= CONV_STD_LOGIC_VECTOR(-600, 32);

    wait for 10 ns;




    ALU_OP <= "011";




    wait for 10 ns;

    A <= CONV_STD_LOGIC_VECTOR(300, 32);
    B <= CONV_STD_LOGIC_VECTOR(600, 32);

    wait for 10 ns;

    A <= CONV_STD_LOGIC_VECTOR(300, 32);
    B <= CONV_STD_LOGIC_VECTOR(300, 32);

    wait for 10 ns;

    A <= CONV_STD_LOGIC_VECTOR(300, 32);
    B <= CONV_STD_LOGIC_VECTOR(100, 32);

    wait for 10 ns;

    A <= CONV_STD_LOGIC_VECTOR(300, 32);
    B <= CONV_STD_LOGIC_VECTOR(0, 32);

    wait for 10 ns;

    A <= CONV_STD_LOGIC_VECTOR(300, 32);
    B <= CONV_STD_LOGIC_VECTOR(-100, 32);

    wait for 10 ns;

    A <= CONV_STD_LOGIC_VECTOR(300, 32);
    B <= CONV_STD_LOGIC_VECTOR(-300, 32);

    wait for 10 ns;

    A <= CONV_STD_LOGIC_VECTOR(300, 32);
    B <= CONV_STD_LOGIC_VECTOR(-600, 32);

    wait for 10 ns;




    assert false
      report ("Konec")
      severity failure;
	END PROCESS;

END;
