LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_unsigned.all;

ENTITY ICache_core IS
  PORT( 
      -- interface with CPU core
      PC_Addr     : IN  std_logic_vector (31 DOWNTO 0);
      Instruction : OUT std_logic_vector (31 DOWNTO 0);
      ICache_hit  : OUT std_logic;
      -- interface to lower level memory 
      Miss_data   : INOUT  std_logic_vector (31 DOWNTO 0);
      Miss_addr   : OUT std_logic_vector (31 DOWNTO 0);
      start       : OUT std_logic;
      Rd          : OUT std_logic;
      Wr_En       : IN  std_logic;
      Clk       	: IN  std_logic;
      Reset       : IN  std_logic     
    );

-- Declarations

END ICache_core;

--
ARCHITECTURE rtl OF ICache_core IS

-- block ram must be in separated entity to make bram inferrence reliable

COMPONENT Bram_array 
   GENERIC (Aw: IN NATURAL:=8;
            Dw: IN NATURAL:=8);
   PORT( 
      addr_read   : IN  std_logic_vector(Aw-1 DOWNTO 0);
      addr_write  : IN  std_logic_vector(Aw-1 DOWNTO 0);
      Data_in     : IN  std_logic_vector(Dw-1 DOWNTO 0);
      Data_out    : OUT std_logic_vector(Dw-1 DOWNTO 0);
      Wr_En       : IN   std_logic;
      Clk_read    : IN   std_logic;
      Clk_write   : IN   std_logic
        );
END COMPONENT; -- Bram_array


CONSTANT off_width   : NATURAL :=4; -- block size=2**off_width B
CONSTANT index_width : NATURAL :=6; -- cache page size = 2**(index_width+off_width) B
CONSTANT tag_width   : NATURAL :=10;
CONSTANT num_tags    : NATURAL :=2**index_width;
CONSTANT num_words   : NATURAL :=2**(index_width + off_width-2);

SIGNAL miss_addr_reg : std_logic_vector(31 DOWNTO 0);
SIGNAL miss_int  : std_logic;
SIGNAL miss_reg  : std_logic;
SIGNAL miss_word_cnt : std_logic_vector(off_width-1 DOWNTO 2);

SIGNAL sig_release : std_logic;
SIGNAL PC_Tag     : std_logic_vector(tag_width-1 DOWNTO 0);
SIGNAL index_addr : std_logic_vector(index_width-1 DOWNTO 0);
SIGNAL word_addr  : std_logic_vector(off_width-1 DOWNTO 2);
SIGNAL tag_read, tag_write   : std_logic_vector(tag_width DOWNTO 0); 
SIGNAL word_read  : std_logic_vector(31 DOWNTO 0); 
SIGNAL data_addr : std_logic_vector(index_width+off_width-1 DOWNTO 2);
SIGNAL nclk : std_logic; 

BEGIN

index_addr<=PC_Addr(index_width+off_width-1 DOWNTO off_width) WHEN miss_reg='0' 
            ELSE miss_addr_reg(index_width+off_width-1 DOWNTO off_width);

word_addr<=PC_Addr(off_width-1 DOWNTO 2) WHEN miss_reg='0' 
           ELSE miss_word_cnt;

miss_regp: PROCESS(clk, reset)
BEGIN
  IF reset='1' THEN
    miss_addr_reg<=(OTHERS=>'0');
    miss_reg<='0';
   ELSIF clk'event AND clk='1' THEN
     IF miss_reg='0' AND miss_int='1' THEN
       miss_addr_reg<=PC_Addr;
       miss_reg<=miss_int;
     ELSE
       miss_reg<=miss_reg AND not(sig_release);
     END IF;
   END IF;
END PROCESS miss_regp; 

startpr: PROCESS(clk, reset)
BEGIN
  IF reset='1' THEN
    start<='0';
  ELSIF clk'event AND clk='1' THEN
    start<=miss_int AND not(miss_reg);
  END IF;
END PROCESS;
  
rd<=miss_int OR miss_reg;

ICache_hit<=not(miss_int OR miss_reg);
Miss_addr<=miss_addr_reg(31 DOWNTO off_width) & miss_word_cnt & "00";

miss_wcp: PROCESS(clk,reset)
BEGIN
  IF reset='1' THEN
    miss_word_cnt<=(OTHERS=>'0');
  ELSIF clk'event AND clk='1' THEN
    IF Wr_En='1' THEN
      miss_word_cnt<=miss_word_cnt+1;
    END IF;
  END IF;
END PROCESS;

sig_release<='1' WHEN (miss_word_cnt="11") AND (Wr_en='1') ELSE '0';

-- tag array read /write access

tag_write<=miss_addr_reg(tag_width+index_width+off_width-1 DOWNTO index_width+off_width) & '1';

nclk<=not(clk);

tag_array:bram_array
GENERIC MAP( AW => index_width,
             DW => tag_width + 1)
PORT MAP(
      addr_read   => index_addr,
      addr_write  => index_addr,
      Data_in     => tag_write,
      Data_out    => tag_read, 
      Wr_En       => Wr_en,
      Clk_read    => nclk, -- falling edge read
      Clk_write   => clk -- rising edge write
       );


PC_Tag<=PC_Addr(tag_width+index_width+off_width-1 DOWNTO index_width+off_width);

 miss_int<='1' WHEN (PC_Tag/=tag_read(tag_width DOWNTO 1)) OR (tag_read(0)='0') -- valid bit is LSB
              ELSE '0';

-- data array read/write access

data_addr<=index_addr & word_addr;

data_array:bram_array
GENERIC MAP( AW => index_width+off_width-2,
             DW => 32)
PORT MAP(
      addr_read   => data_addr,
      addr_write  => data_addr,
      Data_in     => miss_data,
      Data_out    => word_read, 
      Wr_En       => Wr_en,
      Clk_read    => nclk, -- falling edge read
      Clk_write   => clk -- rising edge write
       );

 
Instruction<=word_read; 

Miss_Data <= (others => 'Z');
 

END rtl;
 
