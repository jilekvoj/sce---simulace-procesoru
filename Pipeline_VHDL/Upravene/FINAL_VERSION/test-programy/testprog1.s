
; TestProg1 - zakladni test aritmetickologickych instrukci
; ver 1.0

; cilovy stav registru : r1 = 0xFF00, r2 = 0xFFFF, r3 = 4, r4 = 0xF

start:
  addi r1, r0, 0
  ori  r1, r1, 3 
  xor  r2, r2, r2
  addu r2, r2, r1  ; r2 = 3
  slli r1, r1, 2   ; r1 = 12
  addu r2, r2, r1  ; r2 = 15
  addui r3, r0, 4  ; r3 = 4
  addu r4, r0, r2  ; r4 = 15  
  mult r1, r1, r3  ; r1 = 48
  addu r1, r1, r4  ; r1 = 63
  mult r1, r1, r3
  andi r1, r1, 240
  addu r2, r2, r1  ; r2 = 255
  xor  r1, r1, r1  ; r1 = 0
  addu r1, r2, r0  ; r1 = 0xFF
  slli r1, r1, 8   ; r1 = 0xFF00
  or   r2, r2, r1  ; r2 = 0xFFFF
  
  trap 0
  
