
Init:       j Start
		nop                  ; for somebody with delayed branches
		

error:      sw 12(r20),r14     ; stores ffffffff
		trap 0		


Start: 		lhi		r1,0
		addui		r1,r1,0x1000; 
		lhi		r2,0
		addui		r2,r2,0x1004 ; 
		lhi		r20,0
		addui		r20,r20,0x1008 ; 
		lw 		r3,0(r1) ; read A
		lw 		r4,0(r2) ; read B
		jal         Mult_Uns ; 
		nop                  ; for somebody with delayed branches
		lw 		r3,0(r1) ; 
		mult        r17,r3,r4 ;
		lw          r18,0(r20) ; high part of result 0x0309EE4D
		sne         r10,r18,r6 ; checking high part of result
		lw          r18,4(r20) ; checking lower part of result
		sne         r11,r18,r7  ; checking lower part of result 0xEB9AC1C6
		sne         r12,r17,r7  ; checking lower part of result 0xEB9AC1C6
		or          r10,r10,r11 ; or of errors
		or          r10,r10,r12 ; or of errors
		snei        r15,r10,0   ; if error set r15
		bnez        r15, error ;
		nop                  ; for somebody with delayed branches
		sw          12(r20)  ,r6
		sw          16(r20) ,r7
		sw          20(r20) ,r17		
		xor         r3,r3,r14  ; inversion of r3 to get negative number
		mult        r21,r3,r4  ; multiplication to check correct handling of negative values
		lw          r22,8(r20) ;
		seq         r23,r22,r21 ; 0xE9AA5F5C
		beqz        r23, error
		nop                  ; for somebody with delayed branches
		sw          24(r20),r21 ;
		; checking correct handling of arithmetic shift
		addui       r25,r0,1 ;
		slli        r25,r25,31 ; logical shift left
		srai		r25,r25,31 ; arithmetic shift right (should result in 0xfffffff)
		addui       r25,r25,1  ; adding 1 to 0xffffffff
		bnez        r25, error
		nop
		trap        0

		; shift add multiplication unsigned 
		; r3, r4 inputs
		; r6 ... higher part of result 0x0309EE4D
		; r7 ... lower  part of result 0xEB9AC1C6
Mult_Uns:	add         r6,r0,r0 ; r6=0 (accumulator = high part of result)
		sub         r7,r0,r0 ; r7=0 (lower part of result)
		addui       r8,r7,1  ; just for checking
		xori        r10,r8,33 ; r10=32 
		subui       r14,r0,1  ; to get 0xffffffff
		slli        r16,r8,31 ; to get 0x80000000
Mul_loop:	andi        r5,r3,1 ; test bit of r5
            or          r12,r0,r0 ; clearing r12 (carry)
		seqi        r5,r5,1 ; redundant test of r5 to 1 (check of seqi)
		beqz        r5,lab  ;
		nop                  ; for somebody with delayed branches
		add         r11,r6,r4 ; accumulate
		; this is to emulate missing carry flag during accumulation
		and         r12,r6,r4 ; carry if r6(31)=r4(31)=1 
		xor         r13,r11,r14 ; inversion of MSB of r11 into r13 (missing NOT instr.)
		and         r15,r13,r6 ; carry if r6(31)=1 and r11(31)=0
		or          r12,r12,r15 ;
		and         r15,r13,r4  ;carry if r4(31)=1 and r11(31)=0
		or          r12,r12,r15 ;
		and		r12,r12,r16 ; now MSB of r12 contains carry
		or          r6, r0, r11 ; r6 is now correct lower bits of accumulator
lab:        and		r9,r6,r8 ;  masking LSB (r8=1)
		slli        r9,r9,31 ; shift LSB to MSB
            srl         r6,r6,r8 ; shift of accumulator right by r8=1
		or          r6,r6,r12 ; or of carry to shifted accumulator
		srli        r7,r7,1  ; shift r7 right by 1
		or          r7,r7,r9 ; copying LSB of r6 into r7
		srl         r3,r3,r8 ; shifting r3 by one
		subui       r10,r10,1;
		sgti        r11,r10,0; redundant check of zero 
		bnez        r10,Mul_loop ; 
		nop                  ; for somebody with delayed branches
		jr          r31 ; return
		nop                  ; for somebody with delayed branches
		j error  ; it should never reach this position 
		trap 0  ; stop simulation