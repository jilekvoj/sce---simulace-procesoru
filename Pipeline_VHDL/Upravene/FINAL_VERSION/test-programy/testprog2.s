
; TestProg2 - zakladni test cyklu, vhodne jak pro testovani skoku tak pro instrukcni cache
; ver 1.0

; cilovy stav registru : r1 = 0x8000, r2 = 0xFFFF, r3 = 0
; program posouva jednicku v r1 doleva a tento reg pricita k r2 
start:
  addi r1, r0, 1
  addu r2, r1, r0
  addi r3, r0, 15
loop:
  slli r1, r1, 1
  addu r2, r2, r1
  subui r3, r3, 1
  bnez r3, loop
  
  trap 0
