
; Test ALU operaci
; registry maji po probehnuti programu hodnoty podle komentaru

start:
  addui r1, r0, 0xf0
  addui r2, r0, 0x1010
  addi r3, r0, -100
  addi r4, r0, -16
  
  add r5, r2, r1  ; r5 = 0x1100 = 4352
  addu r6, r2, r4 ; r6 = 0x1000 = 4096
  
  sub r7, r1, r4    ; r7 = 0x100 = 256
  subu r8, r2, r1   ; r8 = 0xf20 = 3872
  subi r9, r4, -15  ; r9 = -1
  subui r10, r1, 14 ; r10 = 0xe2 = 226
  mult r11, r1, r3  ; r11 = -24000
  
  and r12, r2, r1   ; r12 = 0x10 = 16
  andi r13, r1, 0x3f ; r13 = 0x30 = 48
  or  r14, r2, r1  ; r14 = 0x10f0 = 4336
  ori r15, r1, 0x111  ; r15 = 0x1f1 = 497
  xor r16, r1, r2  ; r16 = 0x10e0 = 4320
  xori r17, r1, 0x33  ; r17 = 0xc3 = 195
  lhi r18, 0x1234     ; r18 = 0x12340000 = 305397760
  
  addui r19, r0, 2
  
  sll r20, r2, r19  ; r20 = 0x4040 = 16448
  srl r21, r4, r19  ; r21 = 0x3ffffffc = 1073741820
  sra r22, r4, r19  ; r22 = -4
  slli r23, r2, 2   ; r23 = 0x4040 = 16448
  srli r24, r4, 2   ; r24 = 0x3ffffffc = 1073741820
  srai r25, r4, 2   ; r25 = -4
  
  trap 0
  
