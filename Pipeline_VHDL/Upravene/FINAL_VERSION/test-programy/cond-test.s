
; Test znamenkovych podminek
; po probehnuti maji registry v rozsahu r5 .. r28 hodnoty takto:
; liche registry 0, sude registry 1

start:
  addui r1, r0, 10
  addui r2, r0, 11
  addi r3, r0, -10
  addi r4, r0, -11
  
  seq r5, r1, r3   ; 0
  seq r6, r1, r1  ; 1
  sne r7, r4, r4  ; 0
  sne r8, r3, r4  ; 1
  slt r9, r2, r2  ; 0
  slt r10, r4, r3 ; 1
  sgt r11, r4, r4 ; 0
  sgt r12, r2, r4 ; 1
  sle r13, r2, r1 ; 0
  sle r14, r3, r3 ; 1
  sge r15, r4, r3 ; 0
  sge r16, r1, r1 ; 1
  
  seqi r17, r1, -10   ; 0
  seqi r18, r1, 10  ; 1
  snei r19, r4, -11  ; 0
  snei r20, r3, -11  ; 1
  slti r21, r2, 11  ; 0
  slti r22, r4, -10 ; 1
  sgti r23, r4, -11 ; 0
  sgti r24, r2, -11 ; 1
  slei r25, r2, 10 ; 0
  slei r26, r3, -10 ; 1
  sgei r27, r4, -10 ; 0
  sgei r28, r1, 10 ; 1
  
  snei r29, r1, 10
  sgti r30, r2, 10
  trap 0
  
