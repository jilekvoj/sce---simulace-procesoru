
; Test skoku
; po probehnuti maji registry v rozsahu r5 .. r10 hodnotu 1
; r20 = 100
; r21 = 128 ... adresy ulozene jal a jalr do r31

start:
  addui r1, r0, 1
  addui r2, r0, 0

  addui r3, r0, 1
  beqz  r2, test_eq_taken
  addui r3, r0, 0
  nop
  nop
  nop
  nop
  
test_eq_taken:
  addui r4, r0, 0
  beqz  r1, test_eq_not_taken
  addui r4, r0, 1
  
test_eq_not_taken:
  addui r5, r0, 1
  bnez  r1, test_neq_taken
  addui r5, r0, 0  
  nop
  nop
  nop
  nop

test_neq_taken:
  addui r6, r0, 0
  bnez  r2, test_neq_not_taken
  addui r6, r0, 1

test_neq_not_taken:
  j test_j
  nop
  nop
  nop
  nop
  addui r7, r0, 0
  trap 0  
test_j: 
  addui r7, r0, 1
  
  lhi r29, test_jr>>16
  addui r29, r29, test_jr & 0xffff
  jr r29 
  nop
  nop
  nop
  nop
  addui r8, r0, 0
  trap 0
test_jr:
  addui r8, r0, 1  
  
  jal test_jal
  nop
  nop
  nop
  nop
  addui r9, r0, 0
  trap 0
test_jal:
  addui r9, r0, 1  
  addu r20, r0, r31

  lhi r28, test_jalr>>16
  addui r28, r28, test_jalr & 0xffff
  jalr r28 
  nop
  nop
  nop
  nop
  addui r10, r10, 0
  trap 0
test_jalr:
  addui r10, r0, 1  
  addu r21, r0, r31




  trap 0
  
