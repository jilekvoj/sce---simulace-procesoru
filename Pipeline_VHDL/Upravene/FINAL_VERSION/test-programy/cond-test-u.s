
; Test neznamenkovych podminek
; po probehnuti maji registry v rozsahu r5 .. r28 hodnoty takto:
; liche registry 0, sude registry 1

start:
  addui r1, r0, 10
  addui r2, r0, 11
  
  sequ r5, r1, r2   ; 0
  sequ r6, r1, r1  ; 1
  sneu r7, r2, r2  ; 0
  sneu r8, r1, r2  ; 1
  sltu r9, r2, r2  ; 0
  sltu r10, r1, r2 ; 1
  sgtu r11, r1, r1 ; 0
  sgtu r12, r2, r1 ; 1
  sleu r13, r2, r1 ; 0
  sleu r14, r1, r1 ; 1
  sgeu r15, r1, r2 ; 0
  sgeu r16, r2, r2 ; 1
  
  sequi r17, r1, 11   ; 0
  sequi r18, r1, 10  ; 1
  sneui r19, r2, 11  ; 0
  sneui r20, r1, 11  ; 1
  sltui r21, r2, 11  ; 0
  sltui r22, r1, 11 ; 1
  sgtui r23, r1, 10 ; 0
  sgtui r24, r2, 10 ; 1
  sleui r25, r2, 10 ; 0
  sleui r26, r1, 10 ; 1
  sgeui r27, r1, 11 ; 0
  sgeui r28, r2, 11 ; 1
  
  trap 0
  
