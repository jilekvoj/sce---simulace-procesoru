Start:
	addui	r29, r0, 0xfffc  ; init zasobniku

init:
   addui r26, r0, 1

main:
   
   addui r2, r0, 0x0 ; zde je v men N
   lw r1, 0(r2)

   addui r2, r0, 0x04
   addui r3, r0, 0x2a8
   addui r4, r0, 0x54c

   jal nasob                ; volani procedury nasobici matice

   subui r26, r26, 1
   bnez  r26, main     ; KONEC MERENI

   nop
   nop
   nop
   trap 0

;*************************************************************************************
nasob:
  ; procedura pro nasobeni matic
  ; R0 = 0;
  ; R1 = parametr, velikost matice N
  ; R2 = parametr, adresa prvni matice A; matice NxN ulozena po radcich
  ; R3 = parametr, adresa druhe matice B; matice NxN ulozena po radcich
  ; R4 = parametr, adresa vysledne matice C; misto pro ulozeni matice NxN po radcich
  ; R26,R27,R28 = rezervovano
  ; R29 = ukazatel zasoniku (SP) - nutne obnovit
  ; R30 = ukazatel ramce (FP) - nutne obnovit
  ; R31 = navratova adresa (LR) - ukoncit proceduru skokem sem
  ; hodnoty ve vsech registrech krome R1:R4 je nutne pred ukoncenim procedury obnovit
  ; tedy pred pouzitim v procedure ulozit na zasobnik

  ; POUZIVANI

  ; R0 = 0
  ; R1 = lichost velikosti matice
  ; R2 = parametr, adresa prvni matice A; matice NxN ulozena po radcich
  ; R3 = ukazatel na 1.sloupec z dvojice sloupcu matice B (levy horni roh bloku 2x2)
  ; R4 = ukazatel na prvek matice C v prvnim radku
  ; R5 = ukazatel na prvek matice C o radek niz nez R4
  ; R6 (const) = velikost radku matice v B (=4*N)  (N << 2)
  ; R7 = ridici promenna vnejsiho cyklu
  ; R8 = ridici promenna prvniho vnoreneho cyklu
  ; R9 = ridici promenna nejvnitrnejsiho cyklu
  ; R10 = pracovni ukazatel na spravny sloupec v 1.radku matice A
  ; R11 = pracovni ukazatel na spravny sloupec v 2.radku matice A
  ; R12 = pracovni ukazatel na spravny radek v 1.sloupci matice B (druhy sloupec se ziska jako offset)
  ; R13 = pracovni ukazatel na druhy radek v 1.sloupci matice B (druhy sloupec se ziska jako offset)
  ; soucet 1  |x.|        soucet 2  |.x|        soucet 3  |..|        soucet 4  |..|
  ;           |..|                  |..|                  |x.|                  |.x|
  ; R14 = prubezny soucet (1)
  ; R15 = prubezny soucet (2)
  ; R16 = prubezny soucet (3)
  ; R17 = prubezny soucet (4)
  ; R18 = operand 1 (A11-1.radek,1.sloupec), vysledek mult (2a)
  ; R19 = operand 2 (A21-2.radek,1.sloupec), vysledek mult (4a)
  ; R20 = operand 3 (B11-1.radek,1.sloupec)
  ; R21 = operand 4 (B12-1.radek,2.sloupec)
  ; R22 = vysledek mult (1a)
  ; R23 = vysledek mult (3a)
  ; R24 = operand 5 (A12)
  ; R25 = operand 6 (A22)
  ; R30 = operand 7 (B21)
  ; R31 = operand 8 (B22)

  ; LOKALNI PROMENNE
  ; 92(r29)     (param) parametr, adresa druhe matice B; matice NxN ulozena po radcich
  ; 96(r29)     (const) = N div 2 (N >> 1)
  ; 100(r29)    (param) N

  ; ulozeni pouzitych registru na zasobnik (R5,R6,R7,R8,R9,...)
  subui r29,r29,#104
  sw 0(r29),r5
  sw 4(r29),r6
  sw 8(r29),r7
  sw 12(r29),r8
  sw 16(r29),r9
  sw 20(r29),r10
  sw 24(r29),r11
  sw 28(r29),r12
  sw 32(r29),r13
  sw 36(r29),r14
  sw 40(r29),r15
  sw 44(r29),r16
  sw 48(r29),r17
  sw 52(r29),r18
  sw 56(r29),r19
  sw 60(r29),r20
  sw 64(r29),r21
  sw 68(r29),r22
  sw 72(r29),r23
  sw 76(r29),r24
  sw 80(r29),r25
  sw 84(r29),r30
  sw 88(r29),r31

  ; nastaveni pocatecnich hodnot
  sw 100(r29),r1              ; N
  srli r6,r1,1                ; N div 2
  sw 92(r29),r3   ; ulozeni lokalnich promennych
  sw 96(r29),r6
  slli r6,r1,2                ; velikost radku tabulky v bytech

  ; vypocet matice C, jednoduche
  ; for (int i1=0; i1<N; i1++)            ; loop1
  ;   for (int i2=0; i2<N; i2++)          ; loop2
  ;     for (int i3=0; i3<N; i3++)        ; loop3
  ;       C[i1,i2]+=A[i1,i3]*B[i3,i1]
  ; rozvijim, aby se nasobilo po blocich 2x2
  ;  a dale se dilci nasobeni deje po dvojicich
  ;  pro kazdy vypocitavany prvek matice C
  
  ; loop1
  lw r7,96(r29)                 ; r7(=i1) = N div 2
  andi r1,r1,#1                 ; urcuje lichost matice
  addu r5,r4,r6                 ; druhy radek matice C v bloku 2x2
  beqz r7, nasob_pre_cisti      ; vnejsi cyklus

nasob_loop1exec:
    ; loop2
    lw r3,92(r29)               ; prvni sloupec matice B
    lw r8,96(r29)               ; r8(=i2) = N div 2

nasob_loop2exec:
      ; loop3
      lw  r9,96(r29)            ; r9(=i3) = N div 2
      add r10,r0,r2             ; nastavi na zacatek 1.radku z bloku v matici A
      add r11,r10,r6            ; nastavi na zacatek 2.radku z bloku v matici A
      add r12,r0,r3             ; nastavi na zacatek sloupce v matici B
      add r13,r12,r6            ; nastavi na zacatek sloupce v matici B

      add r14,r0,r0             ; vynulovani prubeznych souctu (1)
      add r15,r0,r0             ;                              (2)
      add r16,r0,r0             ;                              (3)
      add r17,r0,r0             ;                              (4)

nasob_loop3exec:		; instrukce jsou prehazeny, aby se redukovaly stally
				; vznikle tim, ze je pouze jeden blok MEM a WB (po poslednim mult)
        lw r18,0(r10)         ; A11
        lw r20,0(r12)         ; B11

      subui r9,r9,#1            ; R9--

        mult r22,r18,r20      ; mult (1a) = A11*B11
        lw r21,4(r12)         ; B12
        lw r19,0(r11)         ; A21
        mult r18,r18,r21      ; mult (2a) = A11*B12
        lw r30,0(r13)         ; B21
        lw r31,4(r13)         ; B22
        mult r23,r19,r20      ; mult (3a) = A21*B11
        lw r24,4(r10)         ; A12
        lw r25,4(r11)         ; A22
        mult r19,r19,r21      ; mult (4a) = A21*B12
        addu r14,r14,r22      ; (1a)
        addu r15,r15,r18      ; (2a)
        mult r20,r24,r30      ; mult (1b) = A12*B21
        addu r16,r16,r23      ; (3a)
        addu r17,r17,r19      ; (4a)
        mult r21,r24,r31      ; mult (2b) = A12*B22
      addui r10,r10,#8          ; dalsi sloupec v 1.radku matice A
      addui r11,r11,#8          ; dalsi sloupec v 2.radku matice A
        mult r24,r25,r30      ; mult (3b) = A22*B21
        addu r14,r14,r20      ; (1b)
        addu r15,r15,r21      ; (2b)
        mult r25,r25,r31      ; mult (4b) = A22*B22
        
      addu  r12,r13,r6          ; dalsi radek ve sloupci matice B
      addu  r13,r12,r6

        addu r16,r16,r24      ; (3b)
        addu r17,r17,r25      ; (4b)

      bnez r9,nasob_loop3exec
      ; end of loop3
      
      ; pokud je matice licha, musim jeste pricist posledni lichy soucin
      bnez r1,nasob_lichy_loop3
      nasob_lichy_loop3_cont:

      ; ulozi vysledek predchozi operace do matice C
      sw 0(r4),r14
      sw 4(r4),r15
      sw 0(r5),r16
      sw 4(r5),r17
      addui r4,r4,#8           ; dalsi blok matice C
      addui r5,r5,#8

    subui r8,r8,#1
      addui r3,r3,#8           ; dalsi blok v matici B
    bnez r8,nasob_loop2exec
    ; end of loop2
    
    ; pokud ma matice lichy pocet sloupcu, musim se jeste postarat o posledni, lichy
    bnez r1,nasob_lichy_loop2
nasob_lichy_loop2_cont:

    ; dojelo se na konec sloupce -> dalsi radek i v C (blok po 2 radkach)
    addu r4,r4,r6             ; preskocit radek matice C (abych se dostal na dalsi blok, ne radek)
    addu r5,r5,r6

  subui r7,r7,#1
    addu r2,r2,r6             ; dalsi blok radek v matici A (2 radky)
    addu r2,r2,r6             ; dalsi blok radek v matici A (2 radky)
  bnez  r7,nasob_loop1exec
  ; end of loop1

nasob_pre_cisti:
  ; pokud je matice licha, musim jeste dopocitat posledni radek
  bnez r1,nasob_lichy_last

nasob_cisti:
  ; obnova regitru
  lw r5,0(r29)
  lw r6,4(r29)
  lw r7,8(r29)
  lw r8,12(r29)
  lw r9,16(r29)
  lw r10,20(r29)
  lw r11,24(r29)
  lw r12,28(r29)
  lw r13,32(r29)
  lw r14,36(r29)
  lw r15,40(r29)
  lw r16,44(r29)
  lw r17,48(r29)
  lw r18,52(r29)
  lw r19,56(r29)
  lw r20,60(r29)
  lw r21,64(r29)
  lw r22,68(r29)
  lw r23,72(r29)
  lw r24,76(r29)
  lw r25,80(r29)
  lw r31,88(r29)
  lw r30,84(r29)
  ; obnova zasobniku a navrat
  addui r29,r29,#104
  jr r31
  nop

;---------------------------------------------------

nasob_lichy_loop3:

  ; posledni clen, kt. se ma vynasobit a pricist
  lw r18,0(r10)         ; A11
  lw r19,0(r11)         ; A21
  lw r20,0(r12)         ; B11
  lw r21,4(r12)         ; B12

  mult r22,r18,r20      ; mult (1a) = A11*B11
  mult r18,r18,r21      ; mult (2a) = A11*B12
  mult r23,r19,r20      ; mult (3a) = A21*B11
  mult r19,r19,r21      ; mult (4a) = A21*B12

  addu r14,r14,r22      ; (1)
  addu r15,r15,r18      ; (2)
  addu r16,r16,r23      ; (3)
  addu r17,r17,r19      ; (4)

  j nasob_lichy_loop3_cont

;---------------------------------------------------

; vyridi lichy sloupec matice
nasob_lichy_loop2:

  ; lichy loop2
  lw r9,100(r29)            ; r9(=i3) = N
  add r10,r0,r2             ; nastavi na zacatek 1.radku z bloku v matici A
  add r11,r10,r6            ; nastavi na zacatek 2.radku z bloku v matici A
  add r12,r0,r3             ; nastavi na zacatek sloupce v matici B

  add r14,r0,r0             ; vynulovani prubeznych souctu (1)
  add r16,r0,r0             ;                              (3)

nasob_lichy_loop2exec:
    lw r18,0(r10)         ; A1
    lw r19,0(r11)         ; A2
    lw r20,0(r12)         ; B1

    mult r22,r18,r20      ; mult (1) = A1*B1
    mult r23,r19,r20      ; mult (3) = A2*B1

    addu r14,r14,r22      ; (1)
    addu r16,r16,r23      ; (3)

  subui r9,r9,#1
  addui r10,r10,#4          ; dalsi sloupec v 1.radku matice A
  addui r11,r11,#4          ; dalsi sloupec v 2.radku matice A
  addu  r12,r12,r6          ; dalsi radek ve sloupci matice B
  bnez r9,nasob_lichy_loop2exec
  ; end of lichy loop2

  ; ulozi vysledek predchozi operace do matice C
  sw 0(r4),r14
  sw 0(r5),r16
  addui r4,r4,#4           ; dalsi sloupec matice C (tj. skoci na dalsi radek)
  addui r5,r5,#4

  j nasob_lichy_loop2_cont     ; pokracovani pro sude i liche matice (dalsi normalni blok)

;---------------------------------------------------

; vyridi posledni radek liche matice; blok je zde 2x1
nasob_lichy_last:
 
  ; loop2 last
  lw r3,92(r29)               ; prvni sloupec matice B
  lw r8,96(r29)               ; r8(=i2) = N div 2

nasob_last_loop2exec:
    ; loop3 last
    lw r9,100(r29)            ; r9(=i3) = N
    add r10,r0,r2             ; nastavi na zacatek 1.radku z bloku v matici A
    add r12,r0,r3             ; nastavi na zacatek sloupce v matici B

    add r14,r0,r0             ; vynulovani prubeznych souctu (1)
    add r15,r0,r0             ;                              (2)

nasob_last_loop3exec:
      lw r18,0(r10)         ; A1
      lw r20,0(r12)         ; B1
      lw r21,4(r12)         ; B2

      mult r22,r18,r20      ; mult (1) = A1*B1
      mult r18,r18,r21      ; mult (2) = A1*B2

      addu r14,r14,r22      ; (1)
      addu r15,r15,r18      ; (2)

    subui r9,r9,#1
    addui r10,r10,#4          ; dalsi sloupec v 1.radku matice A
    addu  r12,r12,r6          ; dalsi radek ve sloupci matice B
    bnez r9,nasob_last_loop3exec
    ; end of last loop3

    ; ulozi vysledek predchozi operace do matice C
    sw 0(r4),r14
    sw 4(r4),r15
    addui r4,r4,#8           ; dalsi blok matice C

  subui r8,r8,#1
    addui r3,r3,#8           ; dalsi blok v matici B
  bnez r8,nasob_last_loop2exec
  ; end of last loop2

  ; matice ma lichy pocet sloupcu --> posledni cislo se dopocte
  ; last loop3 lichy
  lw r9,100(r29)            ; r9(=i3) = N
  add r10,r0,r2             ; nastavi na zacatek 1.radku z bloku v matici A
  add r12,r0,r3             ; nastavi na zacatek sloupce v matici B

  add r14,r0,r0             ; vynulovani prubeznych souctu (1)

nasob_last_lichy_loop3ex:
    lw r18,0(r10)         ; A1
    lw r20,0(r12)         ; B1

    mult r22,r18,r20      ; mult (1) = A1*B1

    addu r14,r14,r22      ; (1)

  subui r9,r9,#1
  addui r10,r10,#4          ; dalsi sloupec v 1.radku matice A
  addu  r12,r12,r6          ; dalsi radek ve sloupci matice B
  bnez r9,nasob_last_lichy_loop3ex
  ; end of last loop3 lichy

  ; ulozi vysledek predchozi operace do matice C
  sw 0(r4),r14
  ;sw 4(r4),r15

  j nasob_cisti
;************************************************************************************

