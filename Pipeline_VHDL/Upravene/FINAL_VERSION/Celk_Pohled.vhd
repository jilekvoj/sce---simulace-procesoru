-- Changes report:
-- 2006/04/06   MK  Pridan signal Trap_Detected (predevsim) pro potreby ukonceni simulace
-- 2007/05/18   JF  Pridana entita Dlx_Synt (instrukcni cache + core) a patricne signaly
-- 2007/05/21   MJ  Vlozena datova cache
-- 2007/05/26   MJ  Velikost bloku datove cache je nyni generic

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

library work;
use work.DLX_pkg.all;

entity Celk_Pohled is

  port(Clk           : in std_logic;
       Reset         : in std_logic;
       Trap_Detected : out std_logic
  );

end Celk_Pohled;

architecture behav of Celk_Pohled is
    
  component Dlx_Synt is
    GENERIC( 
		DC_WidthB : natural := DCache_WidthB;	-- width of data cache block offset. Block size=4B*(2**WidthB), default=32B
      WidthA    : natural :=20   -- Width of Address Bus
    );
    port(
      Clk           : in std_logic;
      Reset         : in std_logic;
      Trap_Detected : out std_logic;
--Instruction (MemSub)    
      I_AB          : OUT    std_logic_vector (WidthA-1 DOWNTO 2);  -- address bus
      I_DB          : INOUT  std_logic_vector (31 DOWNTO 0);   -- data bus
      I_START       : OUT    std_logic;                       -- signal indicating start of new read/write transaction
      I_Rd          : OUT    std_logic;                       -- read from memory
      I_Wr          : OUT    std_logic;                       -- write to memory
      I_Ack         : IN     std_logic;                       -- acknowledges read/write data
--Data (MemSub interface)
      D_AB          : OUT std_logic_vector (WidthA-1 DOWNTO 2);
      D_DB          : INOUT std_logic_vector (31 DOWNTO 0);
      D_Start       : OUT std_logic;        
      D_Rd          : OUT std_logic;
      D_Wr          : OUT std_logic;
      D_Ack         : IN  std_logic
    );
  end component;

  component MemSub IS
    GENERIC( 
      WidthB    : natural :=3  ; -- width of block offset. Block size=4B*(2**WidthB), default=32B
      DelayRd   : natural :=10 ; -- delay of first word in block during read [cycles]
      DelayWr   : natural :=10 ; -- delay of first write of word
      WidthA    : natural :=20 ; -- Width of Address Bus
      preloadfile : string;
      dumpfile : string
    );
    PORT( 
      AB      : IN     std_logic_vector (WidthA-1 DOWNTO 2);  -- address bus
      DB      : INOUT  std_logic_vector (31 DOWNTO 0);   -- data bus
      START   : IN     std_logic;                       -- signal indicating start of new read/write transaction
      RD      : IN     std_logic;                       -- read from memory
      WR      : IN     std_logic;                       -- write to memory
      ACK     : OUT    std_logic;                       -- acknowledges read/write data
      CLK     : IN     std_logic                        -- clock
    );
  end component;

  SIGNAL D_AB       : std_logic_vector (WidthA-1 DOWNTO 2);
  SIGNAL D_DB       : std_logic_vector (31 DOWNTO 0); 
  SIGNAL D_RD       : std_logic;                     
  SIGNAL D_WR       : std_logic;                     
  SIGNAL D_START    : std_logic;                       
  SIGNAL D_ACK      : std_logic;                       
  
  SIGNAL I_AB       : std_logic_vector (WidthA-1 DOWNTO 2);
  SIGNAL I_DB       : std_logic_vector (31 DOWNTO 0); 
  SIGNAL I_RD       : std_logic;                     
  SIGNAL I_WR       : std_logic;                     
  SIGNAL I_START    : std_logic;                      
  SIGNAL I_ACK      : std_logic;                      

begin

  Dlx_Inst : Dlx_Synt
    generic map( DC_WidthB => DCache_WidthB )
    port map(
      Clk            => Clk,
      Reset          => Reset,
      D_AB           => D_AB,
      D_DB           => D_DB,
      D_Start        => D_start,
      D_Rd           => D_rd,
      D_Wr           => D_wr,
      D_Ack          => D_Ack,
      I_AB           => I_AB,
      I_DB           => I_DB,
      I_START        => I_START,
      I_RD           => I_RD,
      I_WR           => I_WR,
      I_ACK          => I_ACK,
      Trap_Detected  => Trap_Detected
    );


  Data_mem: MemSub 
    GENERIC MAP( 
      WidthB  => DCache_WidthB,-- width of block offset. Block size=4B*(2**WidthB), default=32B
      DelayRd => 10,     -- delay of first word in block during read [cycles]
      DelayWr => 10,     -- delay of first write of word
      WidthA  => WidthA, -- Width of Address Bus
      preloadfile => "data_mem.txt",
      dumpfile => "data_mem_out.txt"
    )
    PORT MAP(
      CLK   => CLK,
      AB    => D_AB,
      DB    => D_DB,
      RD    => D_RD,
      WR    => D_WR,
      START => D_START,
      ACK   => D_ACK
    );

  Inst_mem: MemSub 
    GENERIC MAP( 
      WidthB  => 2,      -- width of block offset. Block size=4B*(2**WidthB), default=32B
      DelayRd => 10,     -- delay of first word in block during read [cycles]
      DelayWr => 10,     -- delay of first write of word
      WidthA  => WidthA, -- Width of Address Bus
      preloadfile => "program_mem.txt",
      dumpfile => "program_mem_out.txt"
    )
    PORT MAP(
      CLK   => CLK,
      AB    => I_AB,
      DB    => I_DB,
      RD    => I_RD,
      WR    => I_WR,
      START => I_START,
      ACK   => I_ACK
    );


end behav;
