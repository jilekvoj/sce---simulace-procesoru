LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_unsigned.ALL;

LIBRARY STD;
Use     STD.TEXTIO.all;

LIBRARY work;
USE work.all;
USE work.DLX_pkg.all;
--USE work.STD_LOGIC_1164_additions.to_hstring;


ENTITY dcache_tb IS
END dcache_tb;


ARCHITECTURE hybrid OF dcache_tb IS

COMPONENT DCache IS
   PORT( 
		Clk       	: IN  std_logic;
      Reset       : IN  std_logic;
      -- interface with CPU core
		Load			: IN	std_logic;									-- load req
		Store			: IN	std_logic;									-- store req
      Addr     	: IN  std_logic_vector (31 DOWNTO 0);		-- address
      Din			: IN	std_logic_vector (31 DOWNTO 0);		-- data in (when saving)
		Dout			: OUT	std_logic_vector (31 DOWNTO 0);		-- data out (when loading)
      DCache_hit  : OUT std_logic;					-- cache hit when loading, cache updated when storing
      -- interface to lower level memory (MemSub)
      mem_data    : INOUT std_logic_vector (31 DOWNTO 0);
      mem_addr    : OUT std_logic_vector (31 DOWNTO 0);
      mem_start   : OUT std_logic;					-- to memory
      mem_Rd      : OUT std_logic;					--    -||-
		mem_Wr		: OUT std_logic;					--    -||-
      mem_Ack		: IN  std_logic					-- ack from memory
   );
END COMPONENT;

COMPONENT MemSub IS
   GENERIC( 
      WidthB	: natural :=3  ; -- width of block offset. Block size=4B*(2**WidthB), default=32B
      DelayRd   : natural :=10 ; -- delay of first word in block during read [cycles]
      DelayWr   : natural :=10 ; -- delay of first write of word
      WidthA    : natural :=10 ; -- Width of Address Bus
      preloadfile : string;
      dumpfile : string
   );
   PORT( 
      AB      : IN     std_logic_vector (WidthA-1 DOWNTO 2);  -- address bus
      DB      : INOUT  std_logic_vector (31 DOWNTO 0);  -- data bus
      START   : IN     std_logic;                       -- signal indicating start of new read/write transaction
      RD      : IN     std_logic;                       -- read from memory
      WR      : IN     std_logic;                       -- write to memory
      ACK     : OUT    std_logic;                       -- acknowledges read/write data
      CLK     : IN     std_logic									-- clock
   );
END COMPONENT; -- MemSub 

TYPE test_ut IS RECORD
	addr : std_logic_vector(31 downto 0);		-- address
	data : std_logic_vector(31 downto 0);		-- data to write / expected data
	load : std_logic;									-- load if = '1' 
	save : std_logic;									-- save if = '1' 
END RECORD;

TYPE test_array is array (natural range <>) of test_ut;

CONSTANT ta : test_array := (
--     Addr        Data    Load/Save
	(X"00000000",X"ABCD1234",'1','0'), 	-- Read
	(X"00000004",X"F0F0EEEE",'1','0'), 
	(X"00000008",X"10100101",'1','0'), 
	(X"0000000C",X"12345678",'1','0'), 
	(X"00000010",X"11112222",'1','0'), 
	(X"00000014",X"55223344",'1','0'), 
	(X"00000018",X"123589AB",'1','0'), 
	(X"0000001C",X"00FFFF00",'1','0'), 
	(X"00000004",X"12345678",'0','1'),	-- Write
	(X"00000000",X"00000000",'0','0'),	-- NOP
	(X"00000000",X"ABCD1234",'1','0'),	-- Read
	(X"00000000",X"01101001",'0','1'),	-- Write
	(X"00000004",X"12345678",'1','0'),	-- Read
	(X"00000000",X"01101001",'1','0'),	-- Read
	(X"00000020",X"FF0000FF",'1','0'),	-- Read
	(X"00000000",X"00000000",'0','0'),	-- NOP
	(X"00000030",X"F0F0EEEE",'1','0'),	-- Read
	(X"00000000",X"00000000",'0','0')	 -- NOP
	);

SIGNAL   AB      : std_logic_vector (9 DOWNTO 2);
SIGNAL   DB      : std_logic_vector (31 DOWNTO 0); 
SIGNAL   CLK     : std_logic;                     
SIGNAL   RD      : std_logic;                     
SIGNAL   WR      : std_logic;                     
SIGNAL   START,ACK : std_logic;                      

SIGNAL	Load, Store	: std_logic;
SIGNAL   Addr			: std_logic_vector (31 DOWNTO 0);
SIGNAL   Data_in		: std_logic_vector (31 DOWNTO 0);
SIGNAL   Data_out		: std_logic_vector (31 DOWNTO 0);
SIGNAL   DCache_hit  : std_logic;
      -- interface to lower level memory 
SIGNAL   Miss_addr   : std_logic_vector (31 DOWNTO 0);
SIGNAL   Reset       : std_logic;

BEGIN

clkp: PROCESS
BEGIN
  clk<='0';
 WAIT for 10 ns;
  clk<='1';
 WAIT for 10 ns;
END PROCESS;


memory: MemSub 
GENERIC MAP( preloadfile => "data_mem.txt",
				 dumpfile    => "data_mem_out.txt" )
PORT MAP( AB=>AB,
          DB=>DB,
          CLK=>CLK,
          RD=>RD,
          WR=>WR,
          START=>START,
          ACK=>ACK
          );
 
uut: DCache
   PORT MAP( 
		Clk         => clk,
      Reset       => reset,
      -- interface with CPU core
		Load			=> Load,
		Store			=> Store,
      Addr     	=> Addr,
		Din			=> Data_in,
      Dout 			=> Data_out,
      DCache_hit  => Dcache_hit,
      -- interface to lower level memory 
      mem_data		=> DB,
      mem_addr    => Miss_addr,
      mem_start	=> start,
      mem_rd		=> rd,
		mem_wr		=> wr,
      mem_Ack	   => Ack
   );

-- wiring
AB<=Miss_addr(9 downto 2);


respr: PROCESS
BEGIN
	reset<='1';
	wait for 15 ns;
	reset<='0';
	wait;
END PROCESS;
 
testpr: PROCESS
BEGIN
	Store   <= '0';
	Load    <= '0';
	Addr	  <= (OTHERS=>'0');
	Data_in <= (others=>'0');

	wait until reset='0';
	
	for i in ta'range loop
		
		Addr    <= ta(i).addr;
		Store   <= ta(i).save;
		Load    <= ta(i).load;
		if (ta(i).save='1') then
			Data_in <= ta(i).data;
		end if;

		if (ta(i).load='1') then  		-- loading
			ASSERT false
				REPORT "Reading from 0x" & to_hstring(ta(i).addr) & 
						 ", expecting 0x" & to_hstring(ta(i).data)
				SEVERITY note;
		elsif (ta(i).save='1') then
			ASSERT false
				REPORT "Writing to 0x" & to_hstring(ta(i).addr) & 
						 ", data = 0x" & to_hstring(ta(i).data)
				SEVERITY note;
		end if;
		
		if ta(i).load='1' or ta(i).save='1' then		-- load or save
			Wait until clk'event AND clk='1' AND Dcache_hit='1';
		else
			wait until clk='1';				-- just wait
		end if;
		
		-- check for correct value
		if (ta(i).load='1') then
			ASSERT Data_out = ta(i).data
				REPORT "READ ERROR @ 0x" & to_hstring(ta(i).addr) & ", got 0x" & 
						 to_hstring(Data_out) & ", expected 0x" & to_hstring(ta(i).data)
				SEVERITY error;

			if Data_out = ta(i).data then
				ASSERT false
					REPORT "Read from 0x" & to_hstring(ta(i).addr) & " OK"
					SEVERITY note;
			end if;
		end if;
		
	end loop;

	Store <= '0';
	Load  <= '0';

	-- wait for write buffer to be written to main memory
	if WRITEBUF_EMPTY='0' then
		wait until WRITEBUF_EMPTY='1';
	end if;

	Wait until clk='1';
	
	DUMPNOW <= '1';
	wait until DUMPACK = '1';
	
	ASSERT false REPORT "SIMULATION FINISHED, this is not a failure."
	SEVERITY failure;

END PROCESS; 

stoppr: process
begin
	wait for 1 ms;
	ASSERT false REPORT "Time is OUT!"
	SEVERITY failure;
end process;


END HYBRID;
