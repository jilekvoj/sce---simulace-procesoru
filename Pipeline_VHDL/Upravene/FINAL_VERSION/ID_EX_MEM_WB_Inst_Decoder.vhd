-- Changes report:
-- 2007/04/15   MK  Prvni verze - Slouceni dekoderu ID_Comb_Logic, EX_Comb_Logic, MEM_Comb_Logic a WB_Comb_Logic
--                  Kompletni nahrada uvedenych bloku vcetne potrebnych pipeline registru
--                  Pojmenovani signalu: 
--                  Nazvy odpovidaji puvodnim signalum z bloku XX_Comb_Logic (pouze signal WB_Load je prejmenovan na WB_Select)
--                  Stejne signaly z ruznych fazi jsou slouceny
--                  Napr.: Vnit_EX_MEM_WB_Store_v_ID - Signal Store, ktery je treba ve fazich EX, MEM a WB a nachazi se ve fazi ID
--                  Komponenty Id_Inst_Decode, Id_Inst_Decode_ROM a Id_Inst_Decode_Big_Mux jsou vzajemne funkcne ekvivalentni - lze pouzit libovolnou z nich
-- 2007/05/01   MK  Signal ID_Do_Trap presunut do faze WB. 
--                  Opravena chyba, kdy TRAP zastavoval procesor, kdyz se pred nim nachazel skok, ktery byl vykonan a TRAP mel byt zahozen.
-- 2007/05/11   MK  Pridan signal MEM_Load

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity ID_EX_MEM_WB_Inst_Decoder is
  
  port( 
      ID_IR          : IN  std_logic_vector (31 DOWNTO 0);
      Clk            : IN  std_logic;
      Reset          : IN  std_logic;
      
      EX_clear       : IN  std_logic;
      EX_stall       : IN  std_logic;
      MEM_clear      : IN  std_logic;
      MEM_stall      : IN  std_logic;
      WB_clear       : IN  std_logic;
      WB_stall       : IN  std_logic;
      
      ID_Ra          : OUT std_logic_vector (4 DOWNTO 0);
      ID_Rb          : OUT std_logic_vector (4 DOWNTO 0);
      ID_Rw          : OUT std_logic_vector (4 DOWNTO 0);
      ID_Imm_type    : OUT std_logic_vector (2 DOWNTO 0);
      EX_Branch      : OUT std_logic_vector (1 DOWNTO 0);
      EX_Ra          : OUT std_logic_vector (4 DOWNTO 0);
      EX_Rb          : OUT std_logic_vector (4 DOWNTO 0);
      EX_ALUSrc      : OUT std_logic;
      EX_ALU_OP      : OUT std_logic_vector (2 DOWNTO 0);
      EX_Rw          : OUT std_logic_vector (4 DOWNTO 0);
      EX_Jmp_reg     : OUT std_logic;
      EX_Unsigned    : OUT std_logic;
      MEM_Load       : OUT std_logic;
      MEM_Store      : OUT std_logic;
      MEM_dual_cycle : OUT std_logic;
      MEM_Rw         : OUT std_logic_vector (4 DOWNTO 0);
      WB_Select      : OUT std_logic_vector(2 DOWNTO 0);
      WB_Do_Trap     : OUT std_logic;
      WB_Rw          : OUT std_logic_vector (4 DOWNTO 0)
   );
end ID_EX_MEM_WB_Inst_Decoder;

architecture Behavioral of ID_EX_MEM_WB_Inst_Decoder is

component Id_Inst_Decode
   PORT( 
      ID_IR          : IN  std_logic_vector (31 DOWNTO 0);
      ID_EX_Branch   : OUT std_logic_vector (1 DOWNTO 0);
      ID_EX_Ra       : OUT std_logic_vector (4 DOWNTO 0);
      ID_EX_Rb       : OUT std_logic_vector (4 DOWNTO 0);
      ID_EX_MEM_WB_Rw: OUT std_logic_vector (4 DOWNTO 0);
      ID_Imm_type    : OUT std_logic_vector (2 DOWNTO 0);
      EX_ALUSrc      : OUT std_logic;
      EX_ALU_OP      : OUT std_logic_vector (2 DOWNTO 0);
      EX_Jmp_reg     : OUT std_logic;
      EX_Unsigned    : OUT std_logic;
      MEM_Load       : OUT std_logic;
      MEM_Store      : OUT std_logic;
      MEM_dual_cycle : OUT std_logic;
      WB_Select      : OUT std_logic_vector(2 DOWNTO 0);
      WB_Do_Trap     : OUT std_logic
   );
end component;

COMPONENT Pipeline_reg
   GENERIC (
      N : integer := 32
   );
   PORT (
      Clear : IN     std_logic ;
      Clk   : IN     std_logic ;
      DIN   : IN     std_logic_vector (N-1 DOWNTO 0);
      Reset : IN     std_logic ;
      Stall : IN     std_logic ;
      DOUT  : OUT    std_logic_vector (N-1 DOWNTO 0)
   );
END COMPONENT;

SIGNAL Vnit_ID_EX_Branch_v_ID :  std_logic_vector(1 downto 0);
SIGNAL Vnit_ID_EX_Ra_v_ID :  std_logic_vector(4 downto 0);
SIGNAL Vnit_ID_EX_Rb_v_ID :  std_logic_vector(4 downto 0);
SIGNAL Vnit_ID_EX_MEM_WB_Rw_v_ID :  std_logic_vector(4 downto 0);
SIGNAL Vnit_ID_Imm_type_v_ID :  std_logic_vector(2 downto 0);
SIGNAL Vnit_EX_ALUSrc_v_ID :  std_logic;
SIGNAL Vnit_EX_ALU_OP_v_ID :  std_logic_vector(2 downto 0);
SIGNAL Vnit_EX_Jmp_reg_v_ID :  std_logic;
SIGNAL Vnit_EX_Unsigned_v_ID :  std_logic;
SIGNAL Vnit_MEM_Store_v_ID :  std_logic;
SIGNAL Vnit_MEM_Load_v_ID :  std_logic;
SIGNAL Vnit_MEM_dual_cycle_v_ID :  std_logic;
SIGNAL Vnit_WB_Select_v_ID :  std_logic_vector(2 downto 0);
SIGNAL Vnit_WB_Do_Trap_v_ID :  std_logic;

SIGNAL Vnit_ID_EX_Branch_v_EX :  std_logic_vector(1 downto 0);
SIGNAL Vnit_ID_EX_Ra_v_EX :  std_logic_vector(4 downto 0);
SIGNAL Vnit_ID_EX_Rb_v_EX :  std_logic_vector(4 downto 0);
SIGNAL Vnit_ID_EX_MEM_WB_Rw_v_EX :  std_logic_vector(4 downto 0);
SIGNAL Vnit_EX_ALUSrc_v_EX :  std_logic;
SIGNAL Vnit_EX_ALU_OP_v_EX :  std_logic_vector(2 downto 0);
SIGNAL Vnit_EX_Jmp_reg_v_EX :  std_logic;
SIGNAL Vnit_EX_Unsigned_v_EX :  std_logic;
SIGNAL Vnit_MEM_Store_v_EX :  std_logic;
SIGNAL Vnit_MEM_Load_v_EX :  std_logic;
SIGNAL Vnit_MEM_dual_cycle_v_EX :  std_logic;
SIGNAL Vnit_WB_Select_v_EX :  std_logic_vector(2 downto 0);
SIGNAL Vnit_WB_Do_Trap_v_EX :  std_logic;

SIGNAL Vnit_MEM_Store_v_MEM :  std_logic;
SIGNAL Vnit_MEM_Load_v_MEM :  std_logic;
SIGNAL Vnit_MEM_dual_cycle_v_MEM :  std_logic;
SIGNAL Vnit_WB_Select_v_MEM :  std_logic_vector(2 downto 0);
SIGNAL Vnit_WB_Do_Trap_v_MEM :  std_logic;
SIGNAL Vnit_ID_EX_MEM_WB_Rw_v_MEM :  std_logic_vector(4 downto 0);

SIGNAL Vnit_WB_Select_v_WB :  std_logic_vector(2 downto 0);
SIGNAL Vnit_WB_Do_Trap_v_WB :  std_logic;
SIGNAL Vnit_ID_EX_MEM_WB_Rw_v_WB :  std_logic_vector(4 downto 0);

begin

	Id_Inst_Decode_Inst: Id_Inst_Decode
  PORT MAP(
		ID_IR => ID_IR,
		ID_EX_Branch => Vnit_ID_EX_Branch_v_ID,
		ID_EX_Ra => Vnit_ID_EX_Ra_v_ID,
		ID_EX_Rb => Vnit_ID_EX_Rb_v_ID,
		ID_EX_MEM_WB_Rw => Vnit_ID_EX_MEM_WB_Rw_v_ID,
		ID_Imm_type => Vnit_ID_Imm_type_v_ID,
		EX_ALUSrc => Vnit_EX_ALUSrc_v_ID,
		EX_ALU_OP => Vnit_EX_ALU_OP_v_ID,
		EX_Jmp_reg => Vnit_EX_Jmp_reg_v_ID,
		EX_Unsigned => Vnit_EX_Unsigned_v_ID,
		MEM_Store => Vnit_MEM_Store_v_ID,
		MEM_Load => Vnit_MEM_Load_v_ID,
		MEM_dual_cycle => Vnit_MEM_dual_cycle_v_ID,
		WB_Select => Vnit_WB_Select_v_ID,
		WB_Do_Trap => Vnit_WB_Do_Trap_v_ID
	);
  
  -------------------------------------------------------------
  --ID
  -------------------------------------------------------------
		ID_Ra <= Vnit_ID_EX_Ra_v_ID;
		ID_Rb <= Vnit_ID_EX_Rb_v_ID;
		ID_Rw <= Vnit_ID_EX_MEM_WB_Rw_v_ID;
		ID_Imm_type <= Vnit_ID_Imm_type_v_ID;

  -------------------------------------------------------------
  --Registry ID -> EX
  -------------------------------------------------------------
  
    ID_EX_Branch_v_EX_Reg : Pipeline_reg
    GENERIC MAP (
       N => 2
    )
    PORT MAP (
       Clear => EX_clear,
       Clk   => Clk,
       DIN   => Vnit_ID_EX_Branch_v_ID,
       Reset => Reset,
       Stall => EX_stall,
       DOUT  => Vnit_ID_EX_Branch_v_EX
    );

    ID_EX_Ra_v_EX_Reg : Pipeline_reg
    GENERIC MAP (
       N => 5
    )
    PORT MAP (
       Clear => EX_clear,
       Clk   => Clk,
       DIN   => Vnit_ID_EX_Ra_v_ID,
       Reset => Reset,
       Stall => EX_stall,
       DOUT  => Vnit_ID_EX_Ra_v_EX
    );

    ID_EX_Rb_v_EX_Reg : Pipeline_reg
    GENERIC MAP (
       N => 5
    )
    PORT MAP (
       Clear => EX_clear,
       Clk   => Clk,
       DIN   => Vnit_ID_EX_Rb_v_ID,
       Reset => Reset,
       Stall => EX_stall,
       DOUT  => Vnit_ID_EX_Rb_v_EX
    );

    ID_EX_MEM_WB_Rw_v_EX_Reg : Pipeline_reg
    GENERIC MAP (
       N => 5
    )
    PORT MAP (
       Clear => EX_clear,
       Clk   => Clk,
       DIN   => Vnit_ID_EX_MEM_WB_Rw_v_ID,
       Reset => Reset,
       Stall => EX_stall,
       DOUT  => Vnit_ID_EX_MEM_WB_Rw_v_EX
    );

    EX_ALUSrc_v_EX_Reg : Pipeline_reg
    GENERIC MAP (
       N => 1
    )
    PORT MAP (
       Clear => EX_clear,
       Clk   => Clk,
       DIN(0)   => Vnit_EX_ALUSrc_v_ID,
       Reset => Reset,
       Stall => EX_stall,
       DOUT(0)  => Vnit_EX_ALUSrc_v_EX
    );

    EX_ALU_OP_v_EX_Reg : Pipeline_reg
    GENERIC MAP (
       N => 3
    )
    PORT MAP (
       Clear => EX_clear,
       Clk   => Clk,
       DIN   => Vnit_EX_ALU_OP_v_ID,
       Reset => Reset,
       Stall => EX_stall,
       DOUT  => Vnit_EX_ALU_OP_v_EX
    );

    EX_Jmp_reg_v_EX_Reg : Pipeline_reg
    GENERIC MAP (
       N => 1
    )
    PORT MAP (
       Clear => EX_clear,
       Clk   => Clk,
       DIN(0)   => Vnit_EX_Jmp_reg_v_ID,
       Reset => Reset,
       Stall => EX_stall,
       DOUT(0)  => Vnit_EX_Jmp_reg_v_EX
    );

    EX_Unsigned_v_EX_Reg : Pipeline_reg
    GENERIC MAP (
       N => 1
    )
    PORT MAP (
       Clear => EX_clear,
       Clk   => Clk,
       DIN(0)   => Vnit_EX_Unsigned_v_ID,
       Reset => Reset,
       Stall => EX_stall,
       DOUT(0)  => Vnit_EX_Unsigned_v_EX
    );

    MEM_Store_v_EX_Reg : Pipeline_reg
    GENERIC MAP (
       N => 1
    )
    PORT MAP (
       Clear => EX_clear,
       Clk   => Clk,
       DIN(0)   => Vnit_MEM_Store_v_ID,
       Reset => Reset,
       Stall => EX_stall,
       DOUT(0)  => Vnit_MEM_Store_v_EX
    );

    MEM_Load_v_EX_Reg : Pipeline_reg
    GENERIC MAP (
       N => 1
    )
    PORT MAP (
       Clear => EX_clear,
       Clk   => Clk,
       DIN(0)   => Vnit_MEM_Load_v_ID,
       Reset => Reset,
       Stall => EX_stall,
       DOUT(0)  => Vnit_MEM_Load_v_EX
    );

    MEM_dual_cycle_v_EX_Reg : Pipeline_reg
    GENERIC MAP (
       N => 1
    )
    PORT MAP (
       Clear => EX_clear,
       Clk   => Clk,
       DIN(0)   => Vnit_MEM_dual_cycle_v_ID,
       Reset => Reset,
       Stall => EX_stall,
       DOUT(0)  => Vnit_MEM_dual_cycle_v_EX
    );

    WB_Select_v_EX_Reg : Pipeline_reg
    GENERIC MAP (
       N => 3
    )
    PORT MAP (
       Clear => EX_clear,
       Clk   => Clk,
       DIN   => Vnit_WB_Select_v_ID,
       Reset => Reset,
       Stall => EX_stall,
       DOUT  => Vnit_WB_Select_v_EX
    );

    WB_Do_Trap_v_EX_Reg : Pipeline_reg
    GENERIC MAP (
       N => 1
    )
    PORT MAP (
       Clear => EX_clear,
       Clk   => Clk,
       DIN(0)   => Vnit_WB_Do_Trap_v_ID,
       Reset => Reset,
       Stall => EX_stall,
       DOUT(0)  => Vnit_WB_Do_Trap_v_EX
    );

  -------------------------------------------------------------
  --EX
  -------------------------------------------------------------

    EX_Branch <= Vnit_ID_EX_Branch_v_EX;
    EX_Ra <= Vnit_ID_EX_Ra_v_EX;
    EX_Rb <= Vnit_ID_EX_Rb_v_EX;
    EX_ALUSrc <= Vnit_EX_ALUSrc_v_EX;
    EX_ALU_OP <= Vnit_EX_ALU_OP_v_EX;
    EX_Rw <= Vnit_ID_EX_MEM_WB_Rw_v_EX;
    EX_Jmp_reg <= Vnit_EX_Jmp_reg_v_EX;
    EX_Unsigned <= Vnit_EX_Unsigned_v_EX;

  -------------------------------------------------------------
  --Registry EX -> MEM
  -------------------------------------------------------------


    MEM_Store_v_MEM_Reg : Pipeline_reg
    GENERIC MAP (
       N => 1
    )
    PORT MAP (
       Clear => MEM_clear,
       Clk   => Clk,
       DIN(0)   => Vnit_MEM_Store_v_EX,
       Reset => Reset,
       Stall => MEM_stall,
       DOUT(0)  => Vnit_MEM_Store_v_MEM
    );

    MEM_Load_v_MEM_Reg : Pipeline_reg
    GENERIC MAP (
       N => 1
    )
    PORT MAP (
       Clear => MEM_clear,
       Clk   => Clk,
       DIN(0)   => Vnit_MEM_Load_v_EX,
       Reset => Reset,
       Stall => MEM_stall,
       DOUT(0)  => Vnit_MEM_Load_v_MEM
    );

    MEM_dual_cycle_v_MEM_Reg : Pipeline_reg
    GENERIC MAP (
       N => 1
    )
    PORT MAP (
       Clear => MEM_clear,
       Clk   => Clk,
       DIN(0)   => Vnit_MEM_dual_cycle_v_EX,
       Reset => Reset,
       Stall => MEM_stall,
       DOUT(0)  => Vnit_MEM_dual_cycle_v_MEM
    );

    WB_Select_v_MEM_Reg : Pipeline_reg
    GENERIC MAP (
       N => 3
    )
    PORT MAP (
       Clear => MEM_clear,
       Clk   => Clk,
       DIN   => Vnit_WB_Select_v_EX,
       Reset => Reset,
       Stall => MEM_stall,
       DOUT  => Vnit_WB_Select_v_MEM
    );

    WB_Do_Trap_v_MEM_Reg : Pipeline_reg
    GENERIC MAP (
       N => 1
    )
    PORT MAP (
       Clear => MEM_clear,
       Clk   => Clk,
       DIN(0)   => Vnit_WB_Do_Trap_v_EX,
       Reset => Reset,
       Stall => MEM_stall,
       DOUT(0)  => Vnit_WB_Do_Trap_v_MEM
    );
    
    ID_EX_MEM_WB_Rw_v_MEM_Reg : Pipeline_reg
    GENERIC MAP (
       N => 5
    )
    PORT MAP (
       Clear => MEM_clear,
       Clk   => Clk,
       DIN   => Vnit_ID_EX_MEM_WB_Rw_v_EX,
       Reset => Reset,
       Stall => MEM_stall,
       DOUT  => Vnit_ID_EX_MEM_WB_Rw_v_MEM
    );


  -------------------------------------------------------------
  --MEM
  -------------------------------------------------------------

    MEM_Load <= Vnit_MEM_Load_v_MEM;
    MEM_Store <= Vnit_MEM_Store_v_MEM;
    MEM_dual_cycle <= Vnit_MEM_dual_cycle_v_MEM;
    MEM_Rw <= Vnit_ID_EX_MEM_WB_Rw_v_MEM;

  -------------------------------------------------------------
  --Registry MEM -> WB
  -------------------------------------------------------------

    WB_Select_v_WB_Reg : Pipeline_reg
    GENERIC MAP (
       N => 3
    )
    PORT MAP (
       Clear => WB_clear,
       Clk   => Clk,
       DIN   => Vnit_WB_Select_v_MEM,
       Reset => Reset,
       Stall => WB_stall,
       DOUT  => Vnit_WB_Select_v_WB
    );

    WB_Do_Trap_v_WB_Reg : Pipeline_reg
    GENERIC MAP (
       N => 1
    )
    PORT MAP (
       Clear => WB_clear,
       Clk   => Clk,
       DIN(0)   => Vnit_WB_Do_Trap_v_MEM,
       Reset => Reset,
       Stall => WB_stall,
       DOUT(0)  => Vnit_WB_Do_Trap_v_WB
    );

    ID_EX_MEM_WB_Rw_v_WB_Reg : Pipeline_reg
    GENERIC MAP (
       N => 5
    )
    PORT MAP (
       Clear => WB_clear,
       Clk   => Clk,
       DIN   => Vnit_ID_EX_MEM_WB_Rw_v_MEM,
       Reset => Reset,
       Stall => WB_stall,
       DOUT  => Vnit_ID_EX_MEM_WB_Rw_v_WB
    );

  -------------------------------------------------------------
  --WB
  -------------------------------------------------------------

    WB_Select <= Vnit_WB_Select_v_WB;
    WB_Do_Trap <= Vnit_WB_Do_Trap_v_WB;
    WB_Rw <= Vnit_ID_EX_MEM_WB_Rw_v_WB;

end Behavioral;

