-- -------------------------------------------------------------------------------------------------
--  Block Name : Memory Model
-- 
--  Designer : Milos Becvar
-- 
--  Company  : CTU in Prague, Faculty of Electrical Engineering
-- 
--  Description : Simplified model of 32-bit memory subsystem for DLX core evaluation
-- 
--  Date Created : 19.3.2007
-- 
--  History :
-- 
-- -------------------------------------------------------------------------------------------------
-- 
-- 
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

LIBRARY std_developerskit;
USE std_developerskit.Std_Mempak.all;
USE std_developerskit.Std_IOpak.all;
USE std_developerskit.Std_timing.all;

library work;
use work.DLX_pkg.all;

ENTITY MemSub IS
   GENERIC( 
      WidthB  : natural :=3  ; -- width of block offset. Block size=4B*(2**WidthB), default=32B
      DelayRd   : natural :=10 ; -- delay of first word in block during read [cycles]
      DelayWr   : natural :=10 ; -- delay of first write of word
      WidthA    : natural :=20 ; -- Width of Address Bus
      preloadfile : string:="program_mem.txt";
      dumpfile : string:="dump_mem.txt"
   );
   PORT( 
      AB      : IN     std_logic_vector (WidthA-1 DOWNTO 2);  -- address bus
      DB      : INOUT  std_logic_vector (31 DOWNTO 0);   -- data bus
      START   : IN     std_logic;                       -- signal indicating start of new read/write transaction
      RD      : IN     std_logic;                       -- read from memory
      WR      : IN     std_logic;                       -- write to memory
      ACK     : OUT    std_logic;                       -- acknowledges read/write data
      CLK     : IN     std_logic                        -- clock
   );

-- Declarations

END MemSub ;


ARCHITECTURE behav OF MemSub IS
   
   CONSTANT t_RD : time:=2 ns; -- delay of data on DB after rising edge of clk
             -- this is useful for simulation and debug

BEGIN

   ---------------------------------------------------------------------------
   model : PROCESS
   ---------------------------------------------------------------------------

   -- Process declarations
   -- memory data structure pointer
   variable mem: mem_id_type;
   variable data_read : std_logic_vector(31 downto 0);
   variable addr_block : std_logic_vector(WidthA-1 DOWNTO WidthB+2);
   variable data_to_write: std_logic_vector(31 downto 0);
   variable addr_word : std_logic_vector(WidthA-1 DOWNTO 2);
    
   BEGIN
      mem:=SRAM_INITIALIZE (name => "MainMemory",
                          length => 2**WidthA,
                           width => 8,
                           default_word => std_logic_vector'("")
                           );
      MEM_LOAD ( mem_id => mem,
               file_name => preloadfile
              );
       
      DB<=(OTHERS=>'Z');
      ACK<='0';
      
      main_loop:  LOOP
         
   WAIT UNTIL (clk'event) AND (clk='1') AND ((Start='1') OR (DUMPNOW='1'));
 
         IF  (DUMPNOW='1')  THEN
             MEM_DUMP ( mem_id => mem,
                        file_name => dumpfile,
            start_addr => 0,
            end_addr => 2**WidthA-1,
            header_flag => TRUE 
            );
             DUMPACK <= '1';
             wait;
         END IF;  
     
         IF (RD='1') THEN -- start of Read Burst  
            Addr_block:=AB(WidthA-1 DOWNTO WidthB+2); -- latch address of block
           
             -- waiting of read block delay
             FOR i IN 1 TO DelayRD LOOP
               WAIT UNTIL clk'event AND (clk='1');
                ASSERT (RD='1') REPORT "Error: Read deasserted during incomplete read burst." SEVERITY error;
                ASSERT (WR='0') REPORT "Error: Write asserted during incomplete read burst." SEVERITY error;
                ASSERT (Addr_block=AB(WidthA-1 DOWNTO WidthB+2)) 
                    REPORT "Error: Address of block changed during incomplete read burst." SEVERITY error;
             END LOOP;
              
             WAIT UNTIL clk'event AND clk='0'; -- falling edge is sampling
              
             WHILE (RD='1') LOOP
                    
                ACK<='1' after t_RD;
                Addr_word:=Addr_block & AB(WidthB+2-1 DOWNTO 2);
                     
              ASSERT (Addr_block=AB(WidthA-1 DOWNTO WidthB+2)) 
                REPORT "Error: Address of block changed during read burst." SEVERITY note;
              ASSERT (WR='0') REPORT "Error: Write asserted during read burst." SEVERITY note;
           
              MEM_READ( mem_id => mem,
                          address => Addr_word & "11",
                          data => data_read(7 downto 0)
                         ); 
                MEM_READ( mem_id => mem,
                          address => Addr_word & "10",
                          data => data_read(15 downto 8)
                         ); 
                MEM_READ( mem_id => mem,
                          address => Addr_word & "01",
                          data => data_read(23 downto 16)
                         );
                MEM_READ( mem_id => mem,
                          address => Addr_word & "00",
                          data => data_read(31 downto 24)
                         ); 
 
                 DB<=data_read AFTER t_RD;

                 WAIT UNTIL clk'event AND (clk='0');

            END LOOP;
                ACK<='0' after t_RD;
                DB<=(OTHERS=>'Z') AFTER t_RD;
                
        END IF; -- end of read burst cycle
         
        IF (WR='1') THEN -- start of Write Burst  
          
       Addr_block:=AB(WidthA-1 DOWNTO WidthB+2); -- latch address of block
           
             -- waiting of write block delay
             FOR i IN 1 TO DelayRD LOOP
               Addr_word:=Addr_block & AB(WidthB+2-1 DOWNTO 2);
               data_to_write:=DB;
               WAIT UNTIL clk'event AND (clk='1');
                ASSERT (WR='1') REPORT "Error: Write deasserted during incomplete write burst." SEVERITY note;
                ASSERT (RD='0') REPORT "Error: Read asserted during incomplete write burst." SEVERITY note;
                ASSERT (Addr_block=AB(WidthA-1 DOWNTO WidthB+2)) 
                    REPORT "Error: Address of block changed during incomplete write burst." SEVERITY note;
             END LOOP;
              
             WHILE (WR='1') LOOP
                
                ACK<='1' after t_RD;     
      
      ASSERT (Addr_block=AB(WidthA-1 DOWNTO WidthB+2)) 
                  REPORT "Error: Address of block changed during write burst." SEVERITY note;
      ASSERT (RD='0') REPORT "Error: Read asserted during write burst." SEVERITY note;
      
             MEM_WRITE( mem_id => mem,
                          address => Addr_word & "11",
                          data => data_to_write(7 DOWNTO 0),
                          write_per_bit => FALSE
                         ); 
                MEM_WRITE( mem_id => mem,
                          address => Addr_word & "10",
                          data => data_to_write(15 DOWNTO 8),
                          write_per_bit => FALSE
                         ); 
                MEM_WRITE( mem_id => mem,
                          address => Addr_word & "01",
                          data => data_to_write(23 DOWNTO 16),
                          write_per_bit => FALSE
                         ); 
                MEM_WRITE( mem_id => mem,
                          address => Addr_word & "00",
                          data => data_to_write(31 DOWNTO 24),
                          write_per_bit => FALSE
                         ); 
             
              WAIT UNTIL clk'event AND (clk='1');

               Addr_word:=Addr_block & AB(WidthB+2-1 DOWNTO 2);
               data_to_write:=DB;
               
            END LOOP;
            ACK<='0' after t_RD;  
         END IF ; -- end of write burst
      END LOOP main_loop;
   END PROCESS model;
   



END Behav;
