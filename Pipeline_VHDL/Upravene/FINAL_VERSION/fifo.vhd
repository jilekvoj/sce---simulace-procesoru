library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity fifo is

  generic (
    WIDTH : natural := 5;  -- pocet polozek (bitove)
    Aw    : natural := 20; -- adresa
    Dw    : natural := 32  -- data
  );

  port (
    clk         : in std_logic;
    reset       : in std_logic;
  --------------------------------------------------
  -- bus controller interface
    grant_req   : out std_logic; -- grant request, FIFO wants to use the MEM bus
    grant_ack   : in std_logic;  -- grant ack, only then can use the MEM bus
  --------------------------------------------------
    fifo_full   : out std_logic;
    fifo_empty  : out std_logic;
  --------------------------------------------------
    addrIN      : in std_logic_vector(Aw-1 downto 0);
    dataIN      : in std_logic_vector(Dw-1 downto 0);
    write_fifo  : in std_logic;
  --------------------------------------------------
    ack         : in std_logic; -- potvrzeni zapisu z MemSub
    addrOUT     : out std_logic_vector(Aw-1 downto 0);
    dataOUT     : out std_logic_vector(Dw-1 downto 0);
    wr_en       : out std_logic;
    start       : out std_logic
  );
  
end entity fifo;

architecture behav of fifo is

  type t_mem is array (0 to 2**WIDTH-1) of std_logic_vector(Aw+Dw-1 downto 2);
  signal mem : t_mem := (others => (others => '0')); -- inicializace pameti

  signal START_SENT : std_logic;
  signal G_ACK : std_logic;
  
  signal fifo_plna : std_logic := '0';
  signal fifo_prazdna : std_logic := '1';
  
  signal grant_req_a : std_logic;
  

begin

  fifo : process (clk, reset)

    variable zac : std_logic_vector(WIDTH-1 downto 0) := (others => '0'); -- ukazatel na zacatek fifo
    variable kon : std_logic_vector(WIDTH-1 downto 0) := (others => '0'); -- ukazatel na konec fifo 

  begin

    if reset = '1' then 
      dataOUT <= (others => '0');
      addrOUT <= (others => '0');
      zac := (others => '0');             
      kon := (others => '0');             
      wr_en <= '0';
      start <= '0';
      start_sent <= '0';
      grant_req <= '0';
      grant_req_a <= '0';
      G_ACK <= '0';
      fifo_plna <= '0';
      fifo_prazdna <= '1';

    elsif clk = '1' and clk'event then  -- jeden takt
  -------------------------------------------------------------------------
      if grant_ack = '1' and ack = '0' and grant_req_a = '1'  then 
        G_ACK <= '1';                     -- povoleni zapisovat, dostavam jen zde
		--else ---!
        --G_ACK <= '0'; ---!
      end if;   
      
      if fifo_prazdna = '0' and (G_ACK = '1' or (grant_ack = '1' and ack='0')) then  -- mam zapisovat do MemSub? 
        wr_en <= '1';
        if start_sent = '0' then  -- zajisti impuls na jeden tak, zacne se zapisovat 
          start <= '1';
          start_sent <= '1';
        else
          start <= '0';
        end if;
        dataOUT <= mem(conv_integer(zac))(Aw+Dw-1 downto Aw);
        addrOUT <= mem(conv_integer(zac))(Aw-1 downto 2) & "00";
        if ack = '1' then
          zac := zac + 1;
          G_ACK <= '0';
          grant_req <= '0';
          grant_req_a <= '0';
          dataOUT <= (others => '0');
          addrOUT <= (others => '0');
          wr_en <= '0';
          start_sent <= '0';
        end if;
      --else
        --wr_en <= '0';
        --start_sent <= '0';
		  --start <= '0'; ---!
      end if;
  -------------------------------------------------------------------------
      if write_fifo = '1' and fifo_plna = '0' then  -- zapis do fifo kdyz neni plno
        mem(conv_integer(kon)) <= dataIN & addrIN(Aw-1 downto 2); 
        kon := kon + 1;                             -- inkrementace
      end if;
  -------------------------------------------------------------------------
      if kon = zac then        -- je FIFO prazdna?
        fifo_prazdna <= '1';
        grant_req <= '0';      -- pozadavek na zapis
        grant_req_a <= '0';
      else
        fifo_prazdna <= '0';
		  if (grant_ack = '0') then ---!
			 grant_req <= '1';      -- pozadavek na zapis
			 grant_req_a <= '1';      
		  end if; ---!
      end if;
  -------------------------------------------------------------------------
      if (zac = kon+1) then 
        fifo_plna <= '1';
      else
        fifo_plna <= '0';
      end if;
      
    end if;
    
  end process;
  
  fifo_full <= fifo_plna;
  fifo_empty <= fifo_prazdna;
  
end behav;