-- Changes report:
-- 2007/04/16   KK  Odstraneny nepotrebne signaly EX_Stall a MEM_Stall z registru MEM a WB.
-- 2007/05/22   MK  Vstupy rozsireny na 32 bitu


library ieee;
use ieee.std_logic_1164.all;
--use ieee.std_logic_arith.all;
use ieee.std_logic_signed.all;

entity Multiplier is
  port(A : in std_logic_vector(31 downto 0);
       B : in std_logic_vector(31 downto 0);
       Y : out std_logic_vector(31 downto 0);
       clk : in std_logic);
end Multiplier;

architecture behav of Multiplier is

  signal Y1 : std_logic_vector(63 downto 0);

begin

  PipeMult : process
  begin
    wait until clk = '1';
    Y1 <= A * B;          -- MEM reg
    Y <= Y1(31 downto 0); -- WB reg
  end process;

end behav;