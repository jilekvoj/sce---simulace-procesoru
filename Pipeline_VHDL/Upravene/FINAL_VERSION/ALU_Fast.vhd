library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity ALU_Fast is

  port(F_A  : in  std_logic_vector (31 downto 0);
       F_B  : in  std_logic_vector (31 downto 0);
       F_C  : out std_logic_vector (31 downto 0));

end ALU_Fast;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

architecture behav of ALU_Fast is
   
begin
  
  F_C <= F_A + F_B;

end behav;
