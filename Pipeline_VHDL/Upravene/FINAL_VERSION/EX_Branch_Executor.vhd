-- Changes report:
-- 2007/05/02   KK  Prvni verze. Blok pro detekci & obsluhu skoku jiz ve fazi EX.
-- 2007/0x/xx   MK  Prepracovano na dvoutaktovou verzi
-- 2007/05/10   MK  Vraceno zpet na jednotaktovou verzi. Dvoutaktova verze nebyla schopna dukladne vycistit pipeline, 
--                  takze instrukce jdouci tesne po vykonanem skoku mohla byt dokoncena, i kdyz by mela byt zahozena
--                  Pokud bude treba vratit se k dvoutaktove verzi (ma lepsi casovani), pak je treba generovat dalsi signal "pseudo_MEM_Clear", 
--                  ktery v nasledujicim taktu nedovoli nahozeni signalu Jump_Now (nedovoli skok) a 
--                  v dalsim (tj. prespristim) taktu nahodi signal WB_Clear (nedovoli provedeni operaci z dvoutaktovych bloku)
-- 2007/05/22   MK  Signal Jump_Now se negeneruje dokud je instrukce skoku ve fazi EX Stallovana

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity EX_Branch_Executor is
   PORT( 
      Clk           : IN     std_logic;
      EX_Branch     : IN     std_logic_vector (1 DOWNTO 0);
      EX_Jmp_Reg    : IN     std_logic;
      EX_Stall      : IN     std_logic;
      Register_In_A : IN     std_logic_vector (31 DOWNTO 0);
      Register_In_B : IN     std_logic_vector (31 DOWNTO 0);
      PC_In         : IN     std_logic_vector (31 DOWNTO 0);
      Imm_In        : IN     std_logic_vector (31 DOWNTO 0);
      Address       : OUT    std_logic_vector (31 DOWNTO 0);
      Jump_now      : OUT    std_logic
   );
end EX_Branch_Executor;

architecture Behavioral of EX_Branch_Executor is

signal Branch : std_logic_vector (3 DOWNTO 0);

signal Address_Vnit : std_logic_vector (31 DOWNTO 0);
signal Jump_now_Vnit : std_logic;

signal PC_plus_Imm : std_logic_vector (31 DOWNTO 0);


begin

   Branch(1 downto 0) <= EX_Branch;
   Branch(3) <= EX_Jmp_Reg;
   PC_plus_Imm <= PC_In + Imm_In;

   ---------------------------------------------------------------------------
   Comparator : process(Register_In_A, Register_In_B)
   ---------------------------------------------------------------------------
   begin
      if (Register_In_A = Register_In_B) then
        Branch(2) <= '1';
      else
        Branch(2) <= '0';
      end if;   
   end process;      

   ---------------------------------------------------------------------------
   Jump_Now_Vnit_Gen: PROCESS(Branch)
   ---------------------------------------------------------------------------
   BEGIN
      case (Branch) is
         when "0101" =>
            Jump_now_Vnit <= '1';
            -- BEQZ
         when "0010" =>
            Jump_now_Vnit <= '1';
            -- BNEZ
         when "0011" =>
            Jump_now_Vnit <= '1';
            -- uncoditional non Register
         when "0111" =>
            Jump_now_Vnit <= '1';
            -- uncoditional non Register
         when "1011" =>
            Jump_now_Vnit <= '1';
            -- uncoditional Register
         when "1111" =>
            Jump_now_Vnit <= '1';
            -- uncoditional Register
         when others =>
            Jump_now_Vnit <= '0';
            -- Other situations
      end case;

   END PROCESS;

   ---------------------------------------------------------------------------
   Address_Vnit_Gen: PROCESS(Branch,PC_plus_Imm,Register_In_A)
   ---------------------------------------------------------------------------
   BEGIN
      case (Branch) is
         when "0101" =>
            Address_Vnit <= PC_plus_Imm;
            -- BEQZ
         when "0010" =>
            Address_Vnit <= PC_plus_Imm;
            -- BNEZ
         when "0011" =>
            Address_Vnit <= PC_plus_Imm;
            -- uncoditional non Register
         when "0111" =>
            Address_Vnit <= PC_plus_Imm;
            -- uncoditional non Register
         when "1011" =>
            Address_Vnit <= Register_In_A;
            -- uncoditional Register
         when "1111" =>
            Address_Vnit <= Register_In_A;
            -- uncoditional Register
         when others =>
            Address_Vnit <= (others => '0');
            -- Other situations
      end case;

   END PROCESS;
   
--  DFF: process (Clk)
--  begin
--    if Clk = '1' and Clk'event then
      Address <= Address_Vnit;
      Jump_now <= Jump_now_Vnit and not EX_Stall;
--    end if;
--  end process;



end Behavioral;

