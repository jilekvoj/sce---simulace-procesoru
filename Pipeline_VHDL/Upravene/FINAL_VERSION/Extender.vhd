--
-- VHDL Architecture integerdlx.Imm_Ext.rtl
--
-- Created:
--          by - becvarm.UNKNOWN (BECVAR)
--          at - 17:42:58 21.02.2004
--
-- using Mentor Graphics HDL Designer(TM) 2003.3 (Build 60)
--
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all;

ENTITY Extender IS
   PORT( 
      PCOffset26 : IN     std_logic_vector (25 DOWNTO 0);
      Imm_type   : IN     std_logic_vector (2 DOWNTO 0);
      Imm32      : OUT    std_logic_vector (31 DOWNTO 0)
   );

-- Declarations

END Extender ;

--
ARCHITECTURE rtl OF Extender IS

SIGNAL Imm16 : std_logic_vector(15 downto 0);
SIGNAL PCOffset26_ext : std_logic_vector(15 downto 0);
SIGNAL Imm16_ext    : std_logic_vector(15 downto 0);
SIGNAL PCOffsign6   : std_logic_vector(5 downto 0);

BEGIN

Imm16<= PCoffset26(15 downto 0) when (Imm_type(2) = '0')
        else "00000000000" & PCOffset26 (10 downto 6);

-- Lower 16 bits are always the same as Imm16 part of instruction code
Imm32(15 downto 0)<=Imm16(15 downto 0);

-- Imm_type = "000" zero extended Imm16 (e.g. ADDUI, ORI)
--            "001" sign extended Imm16/Disp16 (e.g. ADDI, BEQZ, BNEZ)
--            "010" zero extended PCOffset26 (not used)
--            "011" sign extended PCOffset26 (J, JAL) 
--            "100" zero extended Imm5 (I-Type shifts)

-- Imm16 zero or sign extension
Imm16_ext     <=  (OTHERS=>'0') WHEN (Imm_type(0)='0') ELSE (OTHERS=>Imm16(15));

PCOffsign6<=(OTHERS=>PCOffset26(25)); -- sign extension of 26bit PC offset 

-- PCOffset26 zero or sign extension
PCOffset26_ext<= "000000" & PCOffset26(25 DOWNTO 16) WHEN (Imm_type(0)='0')
         ELSE    PCOffsign6 & PCOffset26(25 DOWNTO 16);          
  
Imm32(31 downto 16)<= Imm16_ext when (Imm_type(1)='0') ELSE 
                      PCOffset26_ext;  
 

END rtl;

