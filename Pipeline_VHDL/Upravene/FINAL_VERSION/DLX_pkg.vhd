library ieee;
use ieee.std_logic_1164.all;


package DLX_pkg is

-- There be random stuff

   -------------------------------------------------------------------------------
   -- Simulation ONLY
   -------------------------------------------------------------------------------
-- synthesis translate_off
   signal DUMPNOW : std_logic := ('0');
   signal DUMPACK : std_logic := ('0');
   signal WRITEBUF_EMPTY : std_logic := '0';			-- cache write buffer is empty
   
-- synthesis translate_on

   constant WidthA : natural := 20;
	
	-- width of data cache block offset. Block size=4B*(2**WidthB), default=32B
	constant DCache_WidthB : natural := 7;	-- width of block offset
	
end DLX_pkg;
