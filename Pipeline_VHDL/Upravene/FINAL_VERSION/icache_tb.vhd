
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_unsigned.ALL;

LIBRARY work;
USE work.all;

ENTITY icache_tb IS
END icache_tb;


ARCHITECTURE hybrid OF icache_tb IS

COMPONENT ICache_core 
   PORT( 
      -- interface with CPU core
      PC_Addr     : IN  std_logic_vector (31 DOWNTO 0);
      Instruction : OUT std_logic_vector (31 DOWNTO 0);
      ICache_hit  : OUT std_logic;
      -- interface to lower level memory 
      Miss_data   : IN  std_logic_vector (31 DOWNTO 0);
      Miss_addr   : OUT std_logic_vector (31 DOWNTO 0);
      start       : OUT std_logic;
      Rd          : OUT std_logic;
      Wr_En       : IN  std_logic;
		Clk      	: IN  std_logic;
      Reset       : IN  std_logic     
   );
END COMPONENT;

COMPONENT MemSub 
   GENERIC( 
      WidthB	: natural :=2  ; -- width of block offset. Block size=4B*(2**WidthB), default=32B
      DelayRd   : natural :=10 ; -- delay of first word in block during read [cycles]
      DelayWr   : natural :=10 ; -- delay of first write of word
      WidthA    : natural :=20  -- Width of Address Bus
   );
   PORT( 
      AB      : IN     std_logic_vector (WidthA-1 DOWNTO 2);  -- address bus
      DB      : INOUT  std_logic_vector (31 DOWNTO 0);   -- data bus
      START   : IN     std_logic;                       -- signal indicating start of new read/write transaction
      RD      : IN     std_logic;                       -- read from memory
      WR      : IN     std_logic;                       -- write to memory
      ACK     : OUT    std_logic;                       -- acknowledges read/write data
      CLK     : IN     std_logic;							-- clock
      DUMP_IT : IN     std_logic                        -- signal for dumping memory content to a file
   );

-- Declarations

END COMPONENT; -- MemSub 


SIGNAL   AB      : std_logic_vector (19 DOWNTO 2);
SIGNAL   DB      : std_logic_vector (31 DOWNTO 0); 
SIGNAL   CLK     : std_logic;                     
SIGNAL   RD      : std_logic;                     
SIGNAL   WR      : std_logic;                     
SIGNAL   DUMP_IT : std_logic;
SIGNAL   START,ACK : std_logic;                      

SIGNAL   PC_Addr     :  std_logic_vector (31 DOWNTO 0);
SIGNAL   Instruction :  std_logic_vector (31 DOWNTO 0);
SIGNAL   ICache_hit  :  std_logic;
      -- interface to lower level memory 
SIGNAL   Miss_addr   :  std_logic_vector (31 DOWNTO 0);
SIGNAL   Wr_En       :  std_logic;
SIGNAL   Reset       : std_logic;

BEGIN

clkp: PROCESS
BEGIN
  clk<='0';
 WAIT for 10 ns;
  clk<='1';
 WAIT for 10 ns;
END PROCESS;


memory: MemSub 
PORT MAP( AB=>AB,
          DB=>DB,
          CLK=>CLK,
          RD=>RD,
          WR=>WR,
          START=>START,
          ACK=>ACK,
          DUMP_IT=>DUMP_IT
          );
 
icache:ICache_core 
   PORT MAP( 
      -- interface with CPU core
      PC_Addr     => PC_addr,
      Instruction => Instruction,
      ICache_hit  => Icache_hit,
      -- interface to lower level memory 
      Miss_data   => DB,
      Miss_addr   => Miss_addr,
      start       => start,
      Rd          => rd,
      Wr_En       => wr_en,
	Clk         => clk,
      Reset       => reset
   );

-- wiring
AB<=Miss_addr(19 downto 2);
wr_en<=ack;

 
testpr: PROCESS
BEGIN
  dump_it<='0';
  WR<='0';
  PC_Addr<=(OTHERS=>'0');
  reset<='1';
  wait for 15 ns;
  reset<='0';  
  
FOR i IN 1 to 200 LOOP   
  Wait until clk'event AND clk='1' AND Icache_hit='1';
  PC_Addr<=PC_Addr+4;
  Wait until clk'event AND clk='1' AND Icache_hit='1';
  PC_Addr<=PC_Addr+4;
  Wait until clk'event AND clk='1' AND Icache_hit='1';
  PC_Addr<=PC_Addr+4;
  Wait until clk'event AND clk='1' AND Icache_hit='1';
  PC_Addr<=PC_Addr+4;
END LOOP;

 wait for 20 ns;
  
  ASSERT false REPORT "Simulation finished, this is not a failure."
  SEVERITY failure;

END PROCESS; 
 
  
  



END HYBRID;
