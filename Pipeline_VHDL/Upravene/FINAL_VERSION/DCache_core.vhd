-- 2007/05/26   MJ  Velikost cache nastavena na 2kB, velikost bloku datove cache je nyni generic

LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_unsigned.all;
USE ieee.std_logic_arith.all;

ENTITY DCache_core IS
  GENERIC( Aw : natural := 20;
			  Dw : natural := 32;
           WidthB : natural := 3
		   );
   PORT( 
      Clk         : IN  std_logic;
      Reset       : IN  std_logic;
    -- bus controller interface
      Grant_req   : OUT  std_logic;      -- grant request, when want to use mem bus
      Grant_ack   : IN  std_logic;      -- grant ack - only then can use bus
      -- interface with CPU core
      Load        : IN  std_logic;                  -- load req
      Store       : IN  std_logic;                  -- store req
      Addr        : IN  std_logic_vector (Aw-1 DOWNTO 0);  -- address
      Din         : IN  std_logic_vector (Dw-1 DOWNTO 0);  -- data in (when saving)
      Dout        : OUT std_logic_vector (Dw-1 DOWNTO 0);  -- data out (when loading)
      DCache_hit  : OUT std_logic;          -- cache hit when loading, cache updated when storing
      -- interface to lower level memory (MemSub)
      Miss_data   : IN  std_logic_vector (Dw-1 DOWNTO 0);
      Miss_addr   : OUT std_logic_vector (Aw-1 DOWNTO 0);
      start       : OUT std_logic;          -- to memory
      Rd          : OUT std_logic;          --    -||-
      Ack         : IN  std_logic          -- ack from memory
   );
END DCache_core;

--
ARCHITECTURE rtl OF DCache_core IS

-- cache size is 2kB -- width = 11
CONSTANT size_width  : natural := 11;	-- cache size = (2**size_width) B

CONSTANT off_width   : NATURAL := WidthB; -- block size=2**off_width B
CONSTANT index_width : NATURAL := size_width-WidthB;
CONSTANT tag_width   : NATURAL := Aw-(index_width+off_width);
CONSTANT num_tags    : NATURAL := 2**index_width;
CONSTANT num_words   : NATURAL := 2**(index_width + off_width-2);


-- block ram must be in separated entity to make bram inference reliable

COMPONENT Bram_array 
   GENERIC (Aw: IN NATURAL:=8;
            Dw: IN NATURAL:=8);
   PORT( 
      addr_read   : IN  std_logic_vector(Aw-1 DOWNTO 0);
      addr_write  : IN  std_logic_vector(Aw-1 DOWNTO 0);
      Data_in     : IN  std_logic_vector(Dw-1 DOWNTO 0);
      Data_out    : OUT std_logic_vector(Dw-1 DOWNTO 0);
      Wr_En       : IN   std_logic;
      Clk_read    : IN   std_logic;
      Clk_write   : IN   std_logic
        );
END COMPONENT; -- Bram_array

COMPONENT MUX is
generic (
    dw : natural      -- data width (bits)
  );
port (
    i0:     in std_logic_vector(dw-1 downto 0);
    i1:     in std_logic_vector(dw-1 downto 0);
    vyber:  in std_logic;
    vystup: out std_logic_vector(dw-1 downto 0)
  );
END COMPONENT MUX;

CONSTANT max_cnt : std_logic_vector(off_width-1 DOWNTO 2) := conv_std_logic_vector((2**(off_width-2))-1,(off_width-2)); -- max.cnt addr ("11" for off_width=4)

SIGNAL miss_addr_reg : std_logic_vector(Aw-1 DOWNTO 0);
SIGNAL miss_int  : std_logic;
SIGNAL miss_rd    : std_logic;      -- miss and Load
SIGNAL miss_reg  : std_logic;
SIGNAL miss_word_cnt : std_logic_vector(off_width-1 DOWNTO 2);
SIGNAL rdo      : std_logic;    -- rd output signal branch

SIGNAL tag_we, data_we : std_logic;            -- tag & data BRAM write enable
SIGNAL sig_release : std_logic;
SIGNAL tag       : std_logic_vector(tag_width-1 DOWNTO 0);
SIGNAL index_addr : std_logic_vector(index_width-1 DOWNTO 0);
SIGNAL word_addr  : std_logic_vector(off_width-1 DOWNTO 2);
SIGNAL tag_read, tag_write   : std_logic_vector(tag_width DOWNTO 0); 
SIGNAL word_read  : std_logic_vector(Dw-1 DOWNTO 0); 
SIGNAL data_addr : std_logic_vector(index_width+off_width-1 DOWNTO 2);
SIGNAL nclk : std_logic; 

SIGNAL bram_data  : std_logic_vector (Dw-1 DOWNTO 0);  -- data on the input of data BRAM


BEGIN

-- test process
   -------------------------------------------------------------------------------
   -- Simulation ONLY
   -------------------------------------------------------------------------------
-- synthesis translate_off
assert_pr: PROCESS(Load, Store, clk)
begin
  if (clk'event and clk='1') then
    ASSERT not (Load='1' and Store='1')
      REPORT "Both Load and Store are set to '1'!"
      SEVERITY failure;
  end if;
end process;
-- synthesis translate_on

index_addr_mux : MUX
  GENERIC MAP ( dw => index_width )
  PORT MAP (
    i0     => Addr(index_width+off_width-1 DOWNTO off_width),
    i1     => miss_addr_reg(index_width+off_width-1 DOWNTO off_width),
    vyber  => miss_reg,
    vystup => index_addr
  );

word_addr_mux : MUX
  GENERIC MAP ( dw => off_width-2 )
  PORT MAP (
      i0     => Addr(off_width-1 DOWNTO 2),
    i1     => miss_word_cnt,
    vyber   => miss_reg,
    vystup => word_addr
  );

--index_addr <= Addr(index_width+off_width-1 DOWNTO off_width) WHEN miss_reg='0'
--              ELSE miss_addr_reg(index_width+off_width-1 DOWNTO off_width) WHEN miss_reg='1';

--word_addr<=Addr(off_width-1 DOWNTO 2) WHEN miss_reg='0' 
--           ELSE miss_word_cnt;

miss_regp: PROCESS(clk, reset)
BEGIN
  IF reset='1' THEN
    miss_addr_reg<=(OTHERS=>'0');
    miss_reg<='0';
  ELSIF clk'event AND clk='1' THEN
     IF miss_reg='0' AND miss_rd='1' AND rdo='1' THEN    -- will be reading from mem
       miss_addr_reg<=Addr;
       miss_reg<=miss_rd;
     ELSE
       miss_reg<=miss_reg AND not(sig_release);
     END IF;
  END IF;
END PROCESS miss_regp; 

startpr: PROCESS(clk, reset)
BEGIN
  IF reset='1' THEN
    start<='0';
  ELSIF clk'event AND clk='1' THEN
    start<=miss_rd AND not(miss_reg) AND rdo;
  END IF;
END PROCESS;

DCache_hit<=not(miss_int OR miss_reg);
Miss_addr<=miss_addr_reg(Aw-1 DOWNTO off_width) & miss_word_cnt & "00";

miss_wcp: PROCESS(clk,reset)
BEGIN
  IF reset='1' THEN
    miss_word_cnt<=(OTHERS=>'0');
  ELSIF clk'event AND clk='1' THEN
    IF Ack='1' and rdo='1' THEN      -- increment only when reading from memory
      miss_word_cnt<=miss_word_cnt+1;
    END IF;
  END IF;
END PROCESS;

sig_release <= '1' WHEN (miss_word_cnt=max_cnt) AND (Ack='1') ELSE '0';

-- bus request
Grant_req <= '1' WHEN (reset='0') AND (miss_rd='1' OR miss_reg='1')
      ELSE '0';

rdo <= Grant_ack and (miss_rd or miss_reg);
rd  <= rdo;

-- setting BRAM write enables (Write Non Allocate)
bram_wepr: process(reset, Load, rdo, Store, Ack, miss_int)
begin
  if reset='1' then
    tag_we  <= '0';
    data_we <= '0';
  else
    if Load='1' and rdo='1' then
      tag_we  <= Ack;
      data_we <= Ack;
    elsif Store='1' then
      tag_we  <= '0';
      data_we <= not miss_int;
    else
      tag_we  <= '0';
      data_we <= '0';
    end if;
  end if;
end process bram_wepr;

-- tag array read /write access

tag_write <= miss_addr_reg(tag_width+index_width+off_width-1 DOWNTO index_width+off_width) & '1'; 

nclk <= not(clk);

tag_array : bram_array
GENERIC MAP( AW => index_width,
             DW => tag_width + 1)
PORT MAP(
      addr_read   => index_addr,
      addr_write  => index_addr,
      Data_in     => tag_write,
      Data_out    => tag_read, 
      Wr_En       => tag_we,
      Clk_read    => nclk, -- falling edge read
      Clk_write   => clk -- rising edge write
       );

tag <= Addr(tag_width+index_width+off_width-1 DOWNTO index_width+off_width);

miss_int<='1' WHEN (tag /= tag_read(tag_width DOWNTO 1)) OR (tag_read(0)='0') -- valid bit is LSB
              ELSE '0';
miss_rd <= miss_int and Load;

-- data array read/write access

bram_data <= miss_data WHEN rdo='1' and Ack='1'
       ELSE Din WHEN Store='1'
      ELSE X"00000000";

data_addr <= index_addr & word_addr;

data_array : bram_array
GENERIC MAP( AW => index_width+off_width-2,
             DW => Dw)
PORT MAP(
      addr_read   => data_addr,
      addr_write  => data_addr,
      Data_in     => bram_data,
      Data_out    => word_read, 
      Wr_En       => data_we,
      Clk_read    => nclk, -- falling edge read
      Clk_write   => clk -- rising edge write
       );

 
Dout<=word_read; 


END rtl;
 
