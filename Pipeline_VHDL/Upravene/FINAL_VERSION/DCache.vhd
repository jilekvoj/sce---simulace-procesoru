-- 2007/05/25  MJ  Pridany signaly full_write_buffer_stalls a load_stall_write_buffer_not_empty (pouze v simulaci)
--	                pro testovani vykonnosti - stally zpusobene pametovym systemem, resp. write bufferem
-- 2007/05/26   MJ  Velikost bloku datove cache je nyni generic


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

USE work.DLX_pkg.all;

entity DCache is
  GENERIC( 
      Aw : natural := 20;
      Dw : natural := 32;
		WidthB : natural :=3   	-- width of data cache block offset. Block size=4B*(2**WidthB), default=32B
    );
  PORT( 
      Clk        : IN  std_logic;
      Reset      : IN  std_logic;
      -- interface with CPU core
      Load       : IN  std_logic;                  -- load req
      Store      : IN  std_logic;                  -- store req
      Addr       : IN  std_logic_vector (Aw-1 DOWNTO 0);  -- address
      Din        : IN  std_logic_vector (Dw-1 DOWNTO 0);  -- data in (when saving)
      Dout       : OUT std_logic_vector (Dw-1 DOWNTO 0);  -- data out (when loading)
      DCache_hit : OUT std_logic;          -- cache hit when loading, cache updated when storing
      -- interface to lower level memory (MemSub)
      mem_data   : INOUT std_logic_vector (Dw-1 DOWNTO 0);
      mem_addr   : OUT std_logic_vector   (Aw-1 DOWNTO 0);
      mem_start  : OUT std_logic;          -- to memory
      mem_Rd     : OUT std_logic;          --    -||-
      mem_Wr     : OUT std_logic;          --    -||-
      mem_Ack    : IN  std_logic           -- ack from memory
   );
end DCache;


architecture hybrid of DCache is

COMPONENT DCache_core IS
  GENERIC( Aw : natural := 20;
           Dw : natural := 32;
           WidthB : natural := 3 );
   PORT( 
      Clk         : IN  std_logic;
      Reset       : IN  std_logic;
    -- bus controller interface
      Grant_req   : OUT  std_logic;      -- grant request, when want to use mem bus
      Grant_ack   : IN  std_logic;       -- grant ack - only then can use bus
      -- interface with CPU core
      Load       : IN  std_logic;                  -- load req
      Store      : IN  std_logic;                  -- store req
      Addr       : IN  std_logic_vector (Aw-1 DOWNTO 0);  -- address
      Din        : IN  std_logic_vector (Dw-1 DOWNTO 0);  -- data in (when saving)
      Dout       : OUT  std_logic_vector (Dw-1 DOWNTO 0);  -- data out (when loading)
      DCache_hit : OUT std_logic;          -- cache hit when loading, cache updated when storing
      -- interface to lower level memory (MemSub)
      Miss_data  : IN  std_logic_vector (Dw-1 DOWNTO 0);
      Miss_addr  : OUT std_logic_vector (Aw-1 DOWNTO 0);
      start      : OUT std_logic;          -- to memory
      Rd         : OUT std_logic;          --    -||-
      Ack        : IN  std_logic           -- ack from memory
   );
END COMPONENT DCache_core;

COMPONENT fifo IS

  generic (
    WIDTH : natural := 4;  -- pocet polozek (bitove)
    Aw    : natural := 18; -- adresa
    Dw    : natural := 32  -- data
  );

  port (
    clk         : in std_logic;
    reset       : in std_logic;
  --------------------------------------------------
  -- bus controller interface
    grant_req   : out std_logic; -- grant request, FIFO wants to use the MEM bus
    grant_ack   : in std_logic;  -- grant ack, only then can use the MEM bus
  --------------------------------------------------
    fifo_full   : out std_logic;
    fifo_empty  : out std_logic;
  --------------------------------------------------
    addrIN      : in std_logic_vector(Aw-1 downto 0);
    dataIN      : in std_logic_vector(Dw-1 downto 0);
    write_fifo  : in std_logic;
  --------------------------------------------------
    ack         : in std_logic; -- potvrzeni zapisu z MemSub
    addrOUT     : out std_logic_vector(Aw-1 downto 0);
    dataOUT     : out std_logic_vector(Dw-1 downto 0);
    wr_en       : out std_logic;
    start       : out std_logic
  );
  
END COMPONENT;

COMPONENT MUX is
generic (
    dw : natural      -- data width (bits)
  );
port (
    i0:     in std_logic_vector(dw-1 downto 0);
    i1:     in std_logic_vector(dw-1 downto 0);
    vyber:  in std_logic;
    vystup: out std_logic_vector(dw-1 downto 0)
  );
END COMPONENT MUX;

-- internal signals
SIGNAL store_ready : std_logic;      -- store and write to fifo (fifo not full)

-- to memory
SIGNAL mem_rd_net, mem_wr_net : std_logic;

-- to/from cache_core
SIGNAL core_grant_req, core_grant_ack, granted_core, granted_fifo : std_logic;    -- bus grant
SIGNAL core_start, core_hit : std_logic;
SIGNAL core_addr : std_logic_vector (Aw-1 DOWNTO 0);

-- to/from FIFO
SIGNAL fifo_grant_req, fifo_grant_ack : std_logic;    -- bus grant
SIGNAL fifo_Dout : std_logic_vector (Dw-1 DOWNTO 0); -- to memory
SIGNAL fifo_Aout : std_logic_vector (Aw-1 DOWNTO 0); -- to memory
SIGNAL fifo_start, fifo_full, fifo_empty : std_logic;

-- number of stalls caused by write buffer
-- synthesis translate_off
SIGNAL full_write_buffer_stalls : integer := 0;
SIGNAL load_stall_write_buffer_not_empty : integer := 0;
-- synthesis translate_on


begin

cache: DCache_core
   GENERIC MAP( Aw => Aw, Dw => Dw, WidthB => WidthB	)   	-- forward
   PORT MAP( 
      Clk         => clk,
      Reset       => reset,
    -- bus controller interface
      Grant_req   => core_grant_req,
      Grant_ack   => core_grant_ack,
      -- interface with CPU core
      Load        => load,
      Store       => store_ready,
      Addr        => addr,
      Din         => Din,
      Dout        => Dout,
      DCache_hit  => core_hit,
      -- interface to lower level memory (MemSub)
      Miss_data   => mem_data,
      Miss_addr   => core_addr,
      start       => core_start,
      Rd          => mem_rd_net,
      Ack         => mem_ack
   );

write_buffer: fifo
  generic map (
    WIDTH   => 5,
    Aw      => Aw,
    Dw      => Dw
  )
  PORT MAP(
    clk         => clk,
      reset       => reset,
    -- bus controller interface
    grant_req  => fifo_grant_req,
    grant_ack  => fifo_grant_ack,
    -- fifo state
    fifo_full   => fifo_full,
    fifo_empty  => fifo_empty,
      -- interface with CPU core
    addrIN      => addr,
    dataIN      => Din,
    write_fifo  => store_ready,
      -- interface to lower level memory (MemSub)
    ack         => mem_ack,
    addrOUT     => fifo_Aout,
    dataOUT     => fifo_Dout,
    wr_en       => mem_wr_net,
    start       => fifo_start
  );


------

store_ready <= store and (not fifo_full);
DCache_hit  <= (load and core_hit) or (store and (not fifo_full));

-- memory address MUX
mem_addr_mux : MUX
  GENERIC MAP ( dw => Aw )
  PORT MAP (
      i0     => core_addr,
    i1     => fifo_Aout,
    vyber   => fifo_grant_ack,
    vystup => mem_addr
  );
--mem_addr <= fifo_Aout WHEN fifo_grant_ack='1'
--     ELSE core_addr;

-- memory data 3S buf
mem_data <= fifo_Dout WHEN fifo_grant_ack='1'
     ELSE (others=>'Z');
     
-- memory start signal MUX
mem_start <= fifo_start WHEN fifo_grant_ack='1'
      ELSE core_start;

-- just connect
mem_rd <= mem_rd_net;
mem_wr <= mem_wr_net;

-- bus arbiter
ack_pr: process(core_grant_req, fifo_grant_req, core_grant_ack, fifo_grant_ack, granted_core, granted_fifo, fifo_empty, mem_ack)
begin
-- core has higher priority
--	core_grant_ack <= (core_grant_req and not granted_fifo and fifo_empty) or granted_core;
--	fifo_grant_ack <= (fifo_grant_req and not core_grant_ack) or granted_fifo;

-- fifo has higher priority
	fifo_grant_ack <= (fifo_grant_req and not granted_core) or (granted_fifo and mem_ack);
	core_grant_ack <= (core_grant_req and not fifo_grant_ack and fifo_empty) or (granted_core and mem_ack);
end process;

grant_reg_pr: process(reset, clk)
begin
  if reset='1' then
    granted_core <= '0';
    granted_fifo <= '0';
  elsif clk'event and clk='1' then
    granted_core <= (core_grant_req and core_grant_ack) or (granted_core and mem_Ack);
    granted_fifo <= (fifo_grant_req and fifo_grant_ack) or (granted_fifo and mem_Ack);
  end if;
end process;


-- synthesis translate_off

-- write buffer empty (sim only)
WRITEBUF_EMPTY <= fifo_empty;

-- number of stalls caused by full write buffer
wbfs_pr: process(clk)
begin
	if clk'event and clk='1' then
		if (load='1') and (core_hit='0') and (fifo_empty='0') then
			load_stall_write_buffer_not_empty <= load_stall_write_buffer_not_empty + 1;
		end if;
		if (store='1') and (fifo_full='1') then
			full_write_buffer_stalls <= full_write_buffer_stalls + 1;
		end if;
	end if;
end process;

-- synthesis translate_on


end hybrid;

