library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity Barrel_shifter_tb is
end Barrel_Shifter_tb;

architecture behav of Barrel_shifter_tb is 

  -- Component Declaration for the Unit Under Test (UUT)
  component Barrel_shifter2
    port(left    : IN std_logic;
         logical : IN std_logic;
         shift   : IN std_logic_vector(31 downto 0);
         input   : IN std_logic_vector(31 downto 0);
         output  : OUT std_logic_vector(31 downto 0);
         clk     : IN std_logic);
  end component;

  --Inputs
  signal left    : std_logic := '0';
  signal logical : std_logic := '0';  -- arithmetic shift right (ASHR)
  signal shift   : std_logic_vector(31 downto 0) := (others=>'0');
  signal input   : std_logic_vector(31 downto 0) := (others=>'0');
  signal clk     : std_logic;
  
  --Outputs
  signal output  : std_logic_vector(31 downto 0);

begin

  -- Instantiate the Unit Under Test (UUT)
  uut: Barrel_shifter2 
  port map(left => left,
           logical => logical,
           shift => shift,
           input => input,
           output => output,
           clk => clk);

  gen_clk : process
  begin
    wait for 2 ns;
    clk <= '0';
    wait for 2 ns;
    clk <= '1';
  end process;
  
  tb : process
  begin

    input <= "10010110100101101001011010010110";
    wait for 10 ns;

    for i in 0 to 63 loop
      shift <= shift+1;
      wait for 10 ns;
    end loop;

    -- logical shift right (LSHR)
    shift <= (others=>'0');
    left <= '0';
    logical <= '1';
    wait for 10 ns;

    for i in 0 to 63 loop
      shift <= shift+1;
      wait for 10 ns;
    end loop;

    -- arithmetic shift left (ASHL)
    shift <= (others=>'0');
    left <= '1';
    logical <= '0';
    wait for 10 ns;

    for i in 0 to 63 loop
      shift <= shift+1;
      wait for 10 ns;
    end loop;

    -- logical shift left (LSHL)
    shift <= (others=>'0');
    left <= '1';
    logical <= '1';
    wait for 10 ns;

    for i in 0 to 63 loop
      shift <= shift+1;
      wait for 10 ns;
    end loop;

    assert false
      report ("Konec")
      severity failure;

  end process;

end;