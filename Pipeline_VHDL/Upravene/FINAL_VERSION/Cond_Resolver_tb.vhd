library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_arith.all;

entity Cond_Resolver_tb is
end Cond_Resolver_tb;

architecture behav of Cond_Resolver_tb is 

  -- Component Declaration for the Unit Under Test (UUT)
  component cond_resolver
  port (Clk          : in std_logic;
        Input1       : in std_logic_vector (31 downto 0);
        Input2       : in std_logic_vector (31 downto 0);
        Unsigned     : in std_logic;
        Test_Equal   : in std_logic;
        Test_Lesser  : in std_logic;
        Test_Greater : in std_logic;
        Output_Reg   : out std_logic_vector (31 downto 0));
  end component;

  --Inputs
  signal Unsigned     : std_logic := '0';
  signal Test_Equal   : std_logic := '0';
  signal Test_Lesser  : std_logic := '0';
  signal Test_Greater : std_logic := '0';
  signal Input1 : std_logic_vector(31 downto 0) := (others=>'0');
  signal Input2 : std_logic_vector(31 downto 0) := (others=>'0');
  signal Clk : std_logic;

  --Outputs
  signal Output_Reg : std_logic_vector(31 downto 0);

begin

  -- Instantiate the Unit Under Test (UUT)
  uut : cond_resolver
  port map (
         Clk => Clk,
         Input1 => Input1,
         Input2 => Input2,
         Unsigned => Unsigned,
         Test_Equal => Test_Equal,
         Test_Lesser => Test_Lesser,
         Test_Greater => Test_Greater,
         Output_Reg => Output_Reg
       );

  clk_gen : process
  begin
    wait for 10 ns;
    Clk <= '1';
    wait for 10 ns;
    Clk <= '0';
  end process;

  testb : process
  begin

    wait for 5 ns;

    Unsigned <= '0';
    Test_Equal <= '0';
    Test_Lesser <= '1';
    Test_Greater <= '0';

    Input1 <= "11111111111111111111111111111111";
    Input2 <= "11111111111111111111111111111111";

    wait for 40 ns;

    Input1 <= "11111111111111111111111111111111";
    Input2 <= "11111111111111111111111111000000";

    wait for 40 ns;

    Input1 <= "11111111111111111111111110000000";
    Input2 <= "11111111111111111111111111111111";

    wait for 40 ns;

    Input1 <= "11111111111111111111111111111111";
    Input2 <= CONV_STD_LOGIC_VECTOR(100, 32);

    wait for 40 ns;

    Input1 <= CONV_STD_LOGIC_VECTOR(100, 32);
    Input2 <= "11111111111111111111111111111111";

    wait for 40 ns;

    Input1 <= CONV_STD_LOGIC_VECTOR(100, 32);
    Input2 <= CONV_STD_LOGIC_VECTOR(100, 32);

    wait for 40 ns;

    Input1 <= "11111111111111111111111111111111";
    Input2 <= CONV_STD_LOGIC_VECTOR(0, 32);

    wait for 40 ns;

    Input1 <= CONV_STD_LOGIC_VECTOR(0, 32);
    Input2 <= "11111111111111111111111111111111";

    wait for 40 ns;

    Input1 <= CONV_STD_LOGIC_VECTOR(100, 32);
    Input2 <= CONV_STD_LOGIC_VECTOR(50, 32);

    wait for 40 ns;

    Input1 <= CONV_STD_LOGIC_VECTOR(50, 32);
    Input2 <= CONV_STD_LOGIC_VECTOR(100, 32);

    wait for 40 ns;


    Unsigned <= '1';


    wait for 40 ns;

    Input1 <= CONV_STD_LOGIC_VECTOR(-100, 32);
    Input2 <= CONV_STD_LOGIC_VECTOR(-100, 32);

    wait for 40 ns;

    Input1 <= CONV_STD_LOGIC_VECTOR(-10, 32);
    Input2 <= CONV_STD_LOGIC_VECTOR(-100, 32);

    wait for 40 ns;

    Input1 <= CONV_STD_LOGIC_VECTOR(-100, 32);
    Input2 <= CONV_STD_LOGIC_VECTOR(-10, 32);

    wait for 40 ns;

    Input1 <= CONV_STD_LOGIC_VECTOR(-10, 32);
    Input2 <= CONV_STD_LOGIC_VECTOR(100, 32);

    wait for 40 ns;

    Input1 <= CONV_STD_LOGIC_VECTOR(100, 32);
    Input2 <= CONV_STD_LOGIC_VECTOR(-10, 32);

    wait for 40 ns;

    Input1 <= CONV_STD_LOGIC_VECTOR(100, 32);
    Input2 <= CONV_STD_LOGIC_VECTOR(100, 32);

    wait for 40 ns;

    Input1 <= CONV_STD_LOGIC_VECTOR(-10, 32);
    Input2 <= CONV_STD_LOGIC_VECTOR(0, 32);

    wait for 40 ns;

    Input1 <= CONV_STD_LOGIC_VECTOR(0, 32);
    Input2 <= CONV_STD_LOGIC_VECTOR(-10, 32);

    wait for 40 ns;

    Input1 <= CONV_STD_LOGIC_VECTOR(100, 32);
    Input2 <= CONV_STD_LOGIC_VECTOR(50, 32);

    wait for 40 ns;

    Input1 <= CONV_STD_LOGIC_VECTOR(50, 32);
    Input2 <= CONV_STD_LOGIC_VECTOR(100, 32);

    wait for 40 ns;


    assert false
      report ("Konec")
      severity failure;
  end process;

end behav;
