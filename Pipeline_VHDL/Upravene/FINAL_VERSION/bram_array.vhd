LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_unsigned.all;

ENTITY Bram_array IS
   GENERIC (Aw: NATURAL:=8;
            Dw: NATURAL:=8);
   PORT( 
      addr_read   : IN  std_logic_vector(Aw-1 DOWNTO 0);
      addr_write  : IN  std_logic_vector(Aw-1 DOWNTO 0);
      Data_in     : IN  std_logic_vector(Dw-1 DOWNTO 0);
      Data_out    : OUT std_logic_vector(Dw-1 DOWNTO 0);
      Wr_En       : IN   std_logic;
      Clk_read    : IN   std_logic;
      Clk_write   : IN   std_logic
        );

-- Declarations

END Bram_array;

--
ARCHITECTURE rtl OF Bram_array IS

TYPE t_mem_array IS ARRAY (0 to 2**Aw-1) OF std_logic_vector(Dw-1 DOWNTO 0); 

SIGNAL mem_array  : t_mem_array :=(OTHERS=>(OTHERS=>'0')); -- initialize array to zero

-- uncomment for XST
SIGNAL reada : std_logic_vector(Aw-1 DOWNTO 0);
--

BEGIN


-- memory read port

mem_rdp: PROCESS(clk_read) -- no reset here !
BEGIN
  IF clk_read'event AND clk_read='1' THEN -- falling edge
-- uncomment for XST
    reada <= addr_read;
--
-- uncomment for others
--	 Data_out<=mem_array(conv_integer(addr_read)); 
--
  END IF;
END PROCESS mem_rdp;

-- uncomment for XST
Data_out<=mem_array(conv_integer(reada)); 
--

-- memory write port

mem_wrp: PROCESS(clk_write)
BEGIN
  IF clk_write'event AND clk_write='1' THEN -- falling edge
    IF (Wr_En='1') THEN
      mem_array(conv_integer(addr_write))<=Data_in;
    END IF;
  END IF;
END PROCESS mem_wrp;
 

END rtl;
 
