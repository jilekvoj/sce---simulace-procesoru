--
-- VHDL Architecture integerdlx.PC.rtl
--
-- Created:
--          by - becvarm.UNKNOWN (BECVAR)
--          at - 15:25:52 21.02.2004
--
-- using Mentor Graphics HDL Designer(TM) 2003.3 (Build 60)
--
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all;

ENTITY PC IS
   PORT( 
      Clk        : IN     std_logic;
      NextPC     : IN     std_logic_vector (31 DOWNTO 0);
      Reset      : IN     std_logic;
      Stall      : IN     std_logic;
      Instr_Addr : OUT    std_logic_vector (31 DOWNTO 0)
   );

-- Declarations

END PC ;

--
ARCHITECTURE rtl OF PC IS

SIGNAL Current_PC : std_logic_vector(31 downto 0);

BEGIN

clkp: PROCESS(clk, reset)
BEGIN
  IF reset='1' THEN Current_PC<=(OTHERS=>'0'); -- async. reset
    ELSIF clk'event AND clk='1' THEN
      -- normal operation after rising edge of clock
      IF stall='1' THEN Current_PC<=Current_PC;
       -- no change of PC when in stall
        ELSE Current_PC<=NextPC;
        -- normal operation means that PC goes to next address
      END IF;     
  END IF;    
END PROCESS clkp;
  
  Instr_Addr<=Current_PC;
  
END rtl;

