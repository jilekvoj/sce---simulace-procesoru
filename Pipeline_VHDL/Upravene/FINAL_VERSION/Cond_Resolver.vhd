library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity Cond_Resolver is

  port (Clk          : in  std_logic;
        Input1       : in  std_logic_vector (31 downto 0);
        Input2       : in  std_logic_vector (31 downto 0);
        Unsigned     : in  std_logic;
        Test_Equal   : in  std_logic;
        Test_Lesser  : in  std_logic;
        Test_Greater : in  std_logic;
        Output_Reg   : out std_logic_vector (31 downto 0));

end Cond_Resolver;

architecture behav of Cond_Resolver is

  signal Equal_Vnit   : std_logic;
  signal Lesser_Vnit  : std_logic;
  signal Greater_Vnit : std_logic;

  signal Result,Result2 : std_logic;

begin

  Resolve : process (Input1, Input2, Unsigned)
    
    variable Lesser_Vnit_Var: std_logic;
    variable Greater_Vnit_Var: std_logic;
  
  begin
    
    Equal_Vnit <= '0';
    Lesser_Vnit_Var := '0';
    Greater_Vnit_Var := '0';

    if (Input1 = Input2) then 
      Equal_Vnit <= '1';
    elsif (Input1 > Input2) then
      Greater_Vnit_Var := '1';
    else
      Lesser_Vnit_Var := '1';
    end if;

    if (Unsigned = '0' and ((Input1(31) = '1') xor (Input2(31) = '1'))) then
      Lesser_Vnit_Var := not Lesser_Vnit_Var;
      Greater_Vnit_Var := not Greater_Vnit_Var;
    end if;

    Lesser_Vnit <= Lesser_Vnit_Var;
    Greater_Vnit <= Greater_Vnit_Var;

  end process;  

  Result <= (Test_Greater and Greater_Vnit) or
            (Test_Lesser and Lesser_Vnit) or 
            (Test_Equal and Equal_Vnit);

  DFF1 : process (Clk)
  begin
    if Clk = '1' and Clk'event then
      Result2 <= Result; 
    end if;
  end process;

  DFF2 : process (Clk)
  begin
    if Clk = '1' and Clk'event then
      Output_Reg(0) <= Result2;
    end if;
  end process;

  Output_Reg(31 downto 1) <= (others => '0');

end behav;
