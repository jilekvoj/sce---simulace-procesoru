-- Changes report:
-- 2007/04/06   MK  Pridan signal Trap_Detected (predevsim) pro potreby ukonceni simulace
-- 2007/05/22 MJ+MK Pridano cekani na vyprazdneni zapisove FIFO, 
--                  vypsani obsahu pameti do souboru a cekani na dokonceni vypisu
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

library work;
use work.DLX_pkg.all;


entity Celk_Pohled_tb is
  
  generic(Period : time := 10 ns);

end Celk_Pohled_tb;

architecture behav of Celk_Pohled_tb is 
  
  -- Component Declaration for the Unit Under Test (UUT)
  component Celk_pohled
    port(Clk            : in std_logic;
         Reset          : in std_logic;
         Trap_Detected  : out std_logic);
  end component;

  --Inputs
  signal Clk   : std_logic := '0';
  signal Reset : std_logic := '0';

  signal Trap_Detected : std_logic;

  signal cycle_number : natural := 0;

  signal Local_WRITEBUF_EMPTY : std_logic;
  signal Local_DUMPNOW : std_logic;
  signal Local_DUMPACK : std_logic;

begin

  Local_WRITEBUF_EMPTY <= WRITEBUF_EMPTY;
  Local_DUMPNOW <= DUMPNOW;
  Local_DUMPACK <= DUMPACK;

  -- Instantiate the Unit Under Test (UUT)
  uut: Celk_pohled
    port map(Clk => Clk,
             Reset => Reset,
             Trap_Detected => Trap_Detected);

  clkpr : process
  begin
    clk <= '0';
    wait for period /2;
    clk <= '1';
    wait for period /2;
  end process clkpr;
  
  resetpr : process
  begin
    reset <= '1';
    wait for 2 ns; -- reset time is fixed to 2 ns
    reset <= '0';
    wait;
  end process resetpr;    
  
  countp : process(clk, reset)
  begin
    if reset = '1' then cycle_number <= 0;
      elsif clk'event and clk='1' then
        cycle_number <= cycle_number + 1;
    end if;
  end process countp;
  
  stoppr : process
  begin
    wait until Trap_Detected = '1';

    assert false
      report ("Trap detected")
      severity note;

    wait until Clk='1';
    wait until Clk='1';
    wait until Clk='1';
    wait until Clk='1';
    wait until Clk='1';
    
    if (WRITEBUF_EMPTY = '0') then
      wait until WRITEBUF_EMPTY='1';
      wait until Clk='1';
    end if;  

    DUMPNOW <= '1';
    
    wait until DUMPACK='1';
    DUMPNOW <= '0';
    
    wait until Clk='1';
    wait until Clk='1';
    wait until Clk='1';
    wait until Clk='1';
    
    assert false
      report ("Konec simulace")
      severity failure;


  end process stoppr; 

end behav;
