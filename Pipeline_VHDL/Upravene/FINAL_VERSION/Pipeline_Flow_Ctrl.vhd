-- 2007/0x/xx   MK  Prvni verze. Shrnuti kontroly signalu stall a clear do jednoho bloku
-- 2007/05/10   MK  Uprava MEM_Clear, aby odpovidal jednotaktovemu vykonavani skoku v EX
-- 2007/05/18   JF  Upraveno generovani IF_Stall (pozastaveni pri not ICache_hit)
-- 2007/05/21   MJ  Pridany signaly pro datovou cache - MEM_Load, MEM_Store, DCache_hit
--              a stallovani pipeline na zaklade DCache_hit
-- 2007/05/22   MK  Slouceny zmeny tak, aby spolupracovaly obe cache
--                  Pridana kontrola (pro simulaci), jestli nekoliduji Stall a Clear ve stejne fazi

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity Pipeline_Flow_Ctrl is
  Port (
    Clk               : in  STD_LOGIC;
    Reset             : in  STD_LOGIC;
    WB_Trap_Detected  : in  STD_LOGIC;
    Jump_Now          : in  STD_LOGIC;
   
    ICache_hit        : in  STD_LOGIC; 
    DCache_hit        : in  STD_LOGIC;
    MEM_Load          : in  STD_LOGIC;
    MEM_Store         : in  STD_LOGIC;
    
    ID_Ra             : in  STD_LOGIC_VECTOR (4 downto 0);
    ID_Rb             : in  STD_LOGIC_VECTOR (4 downto 0);
    ID_Rw             : in  STD_LOGIC_VECTOR (4 downto 0);
    EX_Ra             : in  STD_LOGIC_VECTOR (4 downto 0);
    EX_Rb             : in  STD_LOGIC_VECTOR (4 downto 0);
    EX_Rw             : in  STD_LOGIC_VECTOR (4 downto 0);
    MEM_Rw            : in  STD_LOGIC_VECTOR (4 downto 0);
    WB_Rw             : in  STD_LOGIC_VECTOR (4 downto 0);
    IF_Stall          : out  STD_LOGIC;
    ID_Stall          : out  STD_LOGIC;
    EX_Stall          : out  STD_LOGIC;
    MEM_Stall         : out  STD_LOGIC;
    WB_Stall          : out  STD_LOGIC;

    ID_Clear          : out  STD_LOGIC;
    EX_Clear          : out  STD_LOGIC;
    MEM_Clear         : out  STD_LOGIC;
    WB_Clear          : out  STD_LOGIC
  );
end Pipeline_Flow_Ctrl;

architecture Behavioral of Pipeline_Flow_Ctrl is

signal IF_Stall_Vnit  : STD_LOGIC;
signal ID_Stall_Vnit  : STD_LOGIC;
signal EX_Stall_Vnit  : STD_LOGIC;
signal MEM_Stall_Vnit : STD_LOGIC;
signal WB_Stall_Vnit  : STD_LOGIC;

signal ID_Clear_Vnit  : STD_LOGIC;
signal EX_Clear_Vnit  : STD_LOGIC;
signal MEM_Clear_Vnit : STD_LOGIC;
signal WB_Clear_Vnit  : STD_LOGIC;

signal Operands_not_Ready : STD_LOGIC;
signal DCache_Miss : STD_LOGIC;

component Ctrl_Table
  Port (
    Clk   : in  std_logic;
    Reset : in  std_logic;
    ID_Ra : in  STD_LOGIC_VECTOR (4 downto 0);
    ID_Rb : in  STD_LOGIC_VECTOR (4 downto 0);
    ID_Rw : in  STD_LOGIC_VECTOR (4 downto 0);
    WB_Rw : in  STD_LOGIC_VECTOR (4 downto 0);
    Ready : out std_logic
   );
end component;


begin

   IF_Stall <= IF_Stall_Vnit;
   ID_Stall <= ID_Stall_Vnit;
   EX_Stall <= EX_Stall_Vnit;
   MEM_Stall <= MEM_Stall_Vnit;
   WB_Stall <= WB_Stall_Vnit;

   ID_Clear <= ID_Clear_Vnit;
   EX_Clear <= EX_Clear_Vnit;
   MEM_Clear <= MEM_Clear_Vnit;
   WB_Clear <= WB_Clear_Vnit;
   
   Operands_not_Ready <= '1' when (ID_Ra=EX_rw) and not(ID_Ra=0) else
                    '1' when (ID_Rb=EX_rw) and not(ID_Rb=0) else
                    '1' when (ID_Ra=MEM_rw) and not(ID_Ra=0) else
                    '1' when (ID_Rb=MEM_rw) and not(ID_Rb=0) else
                    '0';
                    
   DCache_Miss <= (MEM_Load or MEM_Store) and (not DCache_Hit);

--   IF_stall_Vnit <= ((Operands_not_Ready or not ICache_hit) and not Jump_Now) or DCache_Miss;
--   ID_stall_Vnit <= (Operands_not_Ready and not Jump_Now) or DCache_Miss;
--
--   EX_stall_Vnit <= DCache_Miss;
--   MEM_stall_Vnit <= DCache_Miss;
--   WB_stall_Vnit <= WB_Trap_Detected;
--   
--   ID_clear_Vnit <= (not ICache_hit and not Operands_not_Ready) or jump_now or WB_Trap_Detected;
--   EX_clear_Vnit <= (jump_now or WB_Trap_Detected or Operands_not_Ready) and not DCache_Miss; -- ID_Stall je zahrnut v Operands_not_Ready
----   MEM_clear_Vnit <= jump_now or WB_Trap_Detected; -- Jump_Now v MEM
--   MEM_clear_Vnit <= WB_Trap_Detected; -- Jump_Now v EX
--   WB_clear_Vnit <= DCache_Miss;

                                                                                                           
   IF_stall_Vnit <=            DCache_Miss or                    ((not ICache_hit or Operands_not_Ready) and not Jump_Now);
   ID_stall_Vnit <=            DCache_Miss or                                        (Operands_not_Ready and not Jump_Now);
   EX_stall_Vnit <=            DCache_Miss;
   MEM_stall_Vnit <=           DCache_Miss;
   WB_stall_Vnit <=                          WB_Trap_Detected;
                   
   ID_clear_Vnit <=  jump_now or             WB_Trap_Detected or (not ICache_hit and not Operands_not_Ready and not DCache_Miss);
   EX_clear_Vnit <=  jump_now or             WB_Trap_Detected or                     (Operands_not_Ready and not DCache_Miss);
   MEM_clear_Vnit <=                         WB_Trap_Detected;
   WB_clear_Vnit <=            DCache_Miss;

-- synthesis translate_off
  Controlpr : process
  begin
      wait on clk;
      wait for 1 ns;

      assert not (ID_Stall_Vnit = '1' and ID_Clear_Vnit = '1')
        report ("ID_Stall a ID_Clear zaroven!")
        severity error;
        
      assert not (EX_Stall_Vnit = '1' and EX_Clear_Vnit = '1')
        report ("EX_Stall a EX_Clear zaroven!")
        severity error;
        
      assert not (MEM_Stall_Vnit = '1' and MEM_Clear_Vnit = '1')
        report ("MEM_Stall a MEM_Clear zaroven!")
        severity error;
        
      assert not (WB_Stall_Vnit = '1' and WB_Clear_Vnit = '1')
        report ("WB_Stall a WB_Clear zaroven!")
        severity error;
  end process;
-- synthesis translate_on
  

end Behavioral;

