library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity Pipeline_reg is
  generic( 
    N : integer := 32
  );
  port( 
     Clear : in std_logic;
     Clk   : in std_logic;
     DIN   : in std_logic_vector (N-1 downto 0);
     Reset : in std_logic;
     Stall : in std_logic;
     DOUT  : out std_logic_vector (N-1 downto 0)
   );

end Pipeline_reg ;

--
architecture rtl of Pipeline_reg is

  signal reg_output : std_logic_vector(N-1 downto 0);

begin

  clkp : process(clk, reset)
  begin
    if reset = '1' then reg_output <= (others => '0'); -- async. initialization
    elsif clk'event and clk = '1' then
      if Clear = '1' then reg_output <= (others => '0'); -- forcing zeros (NOP)
      elsif Stall = '1' then reg_output <= reg_output; -- stall 
        else reg_output <= DIN; -- normal operation
      end if;
    end if;  
  end process clkp;
  
  DOUT <= reg_output;
  
end rtl;

