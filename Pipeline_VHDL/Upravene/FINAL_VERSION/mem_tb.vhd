
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_unsigned.ALL;

LIBRARY work;
USE work.all;

ENTITY mem_tb IS
END mem_tb;


ARCHITECTURE hybrid OF mem_tb IS

--CONSTANT preloadfile: 	string:="program_mem.txt";
--CONSTANT dumpfile: 		string:="dump_mem.txt";

COMPONENT MemSub 
   GENERIC( 
      WidthB	: natural :=3  ; -- width of block offset. Block size=4B*(2**WidthB), default=32B
      DelayRd   : natural :=10 ; -- delay of first word in block during read [cycles]
      DelayWr   : natural :=10 ; -- delay of first write of word
      WidthA    : natural :=20 ; -- Width of Address Bus
      preloadfile : string:="jmp-testmuj.hdl";
      dumpfile : string:="dump_mem.txt"		
   );
   PORT( 
      AB      : IN     std_logic_vector (WidthA-1 DOWNTO 2);  -- address bus
      DB      : INOUT  std_logic_vector (31 DOWNTO 0);   -- data bus
      START   : IN     std_logic;                       -- signal indicating start of new read/write transaction
      RD      : IN     std_logic;                       -- read from memory
      WR      : IN     std_logic;                       -- write to memory
      ACK     : OUT    std_logic;                       -- acknowledges read/write data
      CLK     : IN     std_logic;			-- clock
      DUMP_IT : IN     std_logic                        -- signal for dumping memory content to a file
   );

-- Declarations

END COMPONENT; -- MemSub 


SIGNAL   AB      : std_logic_vector (19 DOWNTO 2);
SIGNAL   DB      : std_logic_vector (31 DOWNTO 0); 
SIGNAL   CLK     : std_logic;                     
SIGNAL   RD      : std_logic;                     
SIGNAL   WR      : std_logic;                     
SIGNAL   DUMP_IT : std_logic;
SIGNAL   START,ACK : std_logic;                      


BEGIN

clkp: PROCESS
BEGIN
  clk<='0';
 WAIT for 10 ns;
  clk<='1';
 WAIT for 10 ns;
END PROCESS;


memory: MemSub 
PORT MAP( AB=>AB,
          DB=>DB,
          CLK=>CLK,
          RD=>RD,
          WR=>WR,
          START=>START,
          ACK=>ACK,
          DUMP_IT=>DUMP_IT
          );
 
 
testpr: PROCESS
BEGIN
  dump_it<='0';
  AB<=(OTHERS=>'0');
  DB<=(OTHERS=>'Z');
  START<='0';
  RD<='0';
  WR<='0';
  ASSERT false REPORT "Starting read burst on address 0x00000" severity NOTE;
  WAIT UNTIL (clk'event) and (clk='1');

  START<='1' after 2 ns;
  RD<='1' after 2 ns;
  
  WAIT UNTIL (clk'event) and (clk='1');
  START<='0' after 2 ns;

  FOR i IN 1 to 8 LOOP
  WAIT UNTIL (clk'event) and (clk='1') and (ACK='1');
  AB<=AB + 1 after 2 ns;
  END LOOP;
  RD<='0' after 2 ns;
  
  
  
  WAIT UNTIL (clk'event) and (clk='1') and (ACK='0');
  ASSERT false REPORT "Starting write burst on address 0x00400" severity NOTE;
  AB<=(OTHERS=>'0');
  AB(12)<='1' after 2 ns;
  START<='1' after 2 ns;
  WR<='1' after 2 ns;
  DB<=X"ABCD1234" after 2 ns;
  
  WAIT UNTIL (clk'event) and (clk='1');

  START<='0' after 2 ns;
  
   
  FOR i IN 1 to 8 LOOP
  WAIT UNTIL (clk'event) and (clk='1') and (ACK='1');
  AB<=AB + 1 after 2 ns;
  DB<=DB + 1 after 2 ns;
  END LOOP;
  WR<='0' after 2 ns;
  DB<=(OTHERS=>'Z');
  
 WAIT UNTIL (clk'event) and (clk='1') and (ACK='0');
  ASSERT false REPORT "Starting read burst on address 0x00400" severity NOTE;
  AB<=(OTHERS=>'0');
  AB(12)<='1' after 2 ns;
  START<='1' after 2 ns;
  RD<='1' after 2 ns;
  
  WAIT UNTIL (clk'event) and (clk='1');
  START<='0' after 2 ns;

  FOR i IN 1 to 8 LOOP
  WAIT UNTIL (clk'event) and (clk='1') and (ACK='1');
  AB<=AB + 1 after 2 ns;
  END LOOP;
  RD<='0' after 2 ns;
  
 wait for 100 ns;

 DUMP_IT<='1' after 2 ns;

 wait UNTIL clk'event AND clk='1';
 wait UNTIL clk'event AND clk='1';
 wait UNTIL clk'event AND clk='1';
 wait UNTIL clk'event AND clk='1';

 DUMP_IT<='0' after 2 ns;

 wait UNTIL clk'event AND clk='1';
 wait UNTIL clk'event AND clk='1';
 wait UNTIL clk'event AND clk='1';
 wait UNTIL clk'event AND clk='1';


 wait for 20 ns;

 ASSERT false REPORT" Simulation finished, this is not a failure."
 SEVERITY failure;
  

END PROCESS; 
 
  
  



END HYBRID;
