-- Changes report:
-- 2007/04/15   KK  Odstranen prupis hodnoty z W na A/B pri shode indexu registru, nyni obstarava forwarding z WB2_Stage.
-- 2007/05/03   KK  Odstranen asynchronni reset, misto nej jsou registry inicializovany '0' => syntetizuje jako distribuovanou RAM. 
-- 2007/05/06   MK  Obnoven prupis hodnoty z W na A/B

LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_unsigned.all;

ENTITY Registers IS
   PORT( 
      Clk   : IN     std_logic;
      Ra    : IN     std_logic_vector (4 DOWNTO 0);
      Rb    : IN     std_logic_vector (4 DOWNTO 0);
      Rw    : IN     std_logic_vector (4 DOWNTO 0);
      W     : IN     std_logic_vector (31 DOWNTO 0);
      A     : OUT    std_logic_vector (31 DOWNTO 0);
      B     : OUT    std_logic_vector (31 DOWNTO 0)
   );

-- Declarations

END Registers ;

--
ARCHITECTURE rtl OF Registers IS

  TYPE t_regfile IS ARRAY (0 to 31) of std_logic_vector(31 downto 0);

  SIGNAL reg_file : t_regfile := (OTHERS=>(OTHERS=>'0'));
  
  SIGNAL write_enable : std_logic; -- internal write enable signal

BEGIN

  write_enable<='1' WHEN (Rw/="00000") ELSE '0';

  writepr : PROCESS(clk)
  BEGIN
    IF clk'event AND clk='1' THEN
      IF write_enable='1' THEN
        reg_file(conv_integer(Rw))<=W;
      END IF;  
    END IF; 
  END PROCESS writepr;

  read_a_pr: PROCESS(W, Ra, Rw, reg_file, write_enable)
  BEGIN
    IF Ra=Rw AND write_enable='1' then 
      A<=W after 2 ns; -- internal reg. file bypass
    ELSE
      A<=reg_file(conv_integer(Ra)) after 2 ns; -- otherwise read from reg_file
    END IF; 
  END PROCESS read_a_pr;

  read_b_pr: PROCESS(W, Rb, Rw, reg_file, write_enable)
  BEGIN
    IF Rb=Rw AND write_enable='1' then 
      B<=W after 2 ns; -- internal reg. file bypass
    ELSE
      B<=reg_file(conv_integer(Rb)) after 2 ns; -- otherwise read from reg_file
    END IF; 
  END PROCESS read_b_pr;
  
--  A <= reg_file(conv_integer(Ra)) after 2 ns; -- read from reg_file
--
--  B <= reg_file(conv_integer(Rb)) after 2 ns; -- read from reg_file  
  
END rtl;

