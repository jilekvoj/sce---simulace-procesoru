library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity ALU_Slow is

  port(S_A  : in  std_logic_vector (31 downto 0);
       S_B  : in  std_logic_vector (31 downto 0);
       S_OP : in  std_logic_vector (2 downto 0);
       S_C  : out std_logic_vector (31 downto 0);
       CLK  : in  std_logic);

end ALU_Slow;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

architecture behav of ALU_Slow is
   
  signal Vysl_Vnit : std_logic_vector(31 downto 0);
  signal Vysl_Vnit_2 : std_logic_vector(31 downto 0);

begin
  
  truth_proc : process (S_A, S_B, S_OP)
  begin
    case S_OP is
      when "000" =>
         Vysl_Vnit <= S_A + S_B;
      when "001" =>
         Vysl_Vnit <= S_A + S_B;
      when "010" =>
         Vysl_Vnit <= S_A - S_B;
      when "011" =>
         Vysl_Vnit <= S_A - S_B;
      when "100" =>
         Vysl_Vnit <= S_A and S_B;
      when "101" =>
         Vysl_Vnit <= S_A or S_B;
      when "110" =>
         Vysl_Vnit <= S_A xor S_B;
      when "111" =>
         Vysl_Vnit <= S_B(15 downto 0) & S_A(15 downto 0);
      when others =>
         Vysl_Vnit <= (others => '0');
    end case;
  end process truth_proc;

  DFF1: process (Clk)
  begin
    if Clk = '1' and Clk'event then
      Vysl_Vnit_2 <= Vysl_Vnit;
    end if;
  end process;

  DFF2: process (Clk)
  begin
    if Clk = '1' and Clk'event then
      S_C <= Vysl_Vnit_2;
    end if;
  end process;


end architecture behav;
