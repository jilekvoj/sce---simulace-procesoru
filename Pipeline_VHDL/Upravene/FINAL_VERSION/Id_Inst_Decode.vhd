-- Changes report:
-- 2007/04/15   MK  Prvni verze - Slouceni dekoderu ID_Comb_Logic, EX_Comb_Logic, MEM_Comb_Logic a WB_Comb_Logic
--                    Tato verze dekoduje signaly pomoci multiplexeru - jeden signal odpovida jednomu procesu (s vyjimkou signalu XX_Ra, XX_Rb a XX_Rw, ktere jsou dohromady)
--                    Pojmenovani signalu: 
--                      Nazvy odpovidaji puvodnim signalum z bloku XX_Comb_Logic (pouze signal WB_Load je prejmenovan na WB_Select)
--                      Stejne signaly z ruznych fazi jsou slouceny
--                      Napr.: EX_MEM_WB_Store - Signal Store, ktery je treba ve fazich EX, MEM a WB
-- 23007/04/26  MK  Verze upravena pro kompatibilitu s dlx2hdl (zmeneny I-type shifty)
-- 2007/05/01   MK  Signal ID_Do_Trap presunut do faze WB. 
--                  Opravena chyba, kdy TRAP zastavoval procesor, kdyz se pred nim nachazel skok, ktery byl vykonan a TRAP mel byt zahozen.
-- 2007/05/11   MK  Pridan signal MEM_Load
-- 2007/05/14   MK  Opraveno zpracovani znamenkovych I-Type cond instrukci
--                  Lze jednoduse vybrat typ kodovani Tabulka vs. dlx2hdl pouze za/odkomentovanim prislusne sady konstant (radky 48-60)


library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity Id_Inst_Decode is
   PORT( 
      ID_IR          : IN  std_logic_vector (31 DOWNTO 0);
      ID_EX_Branch   : OUT std_logic_vector (1 DOWNTO 0);
      ID_EX_Ra       : OUT std_logic_vector (4 DOWNTO 0);
      ID_EX_Rb       : OUT std_logic_vector (4 DOWNTO 0);
      ID_EX_MEM_WB_Rw    : OUT std_logic_vector (4 DOWNTO 0);
      ID_Imm_type    : OUT std_logic_vector (2 DOWNTO 0);
      
      EX_ALUSrc      : OUT std_logic;                     -- i pro Barrel, Compare
      EX_ALU_OP      : OUT std_logic_vector (2 DOWNTO 0); -- i pro Compare, Barrel (Barrel - jen bity 1:0)
      EX_Jmp_reg     : OUT std_logic;
      EX_Unsigned    : OUT std_logic;                     

      MEM_Load       : OUT std_logic;
      MEM_Store      : OUT std_logic;
      MEM_dual_cycle : OUT std_logic;
      
      WB_Select      : OUT std_logic_vector(2 DOWNTO 0);
      WB_Do_Trap     : OUT std_logic
   );
end Id_Inst_Decode;

architecture Behavioral of Id_Inst_Decode is

signal addr: std_logic_vector(6 downto 0);

---------------------------------------------------------------
-- Dle tabulky
--constant SLLI_ADDR: std_logic_vector(6 downto 0) := "1010100";
--constant SRLI_ADDR: std_logic_vector(6 downto 0) := "1010110";
--constant SRAI_ADDR: std_logic_vector(6 downto 0) := "1010111";
---------------------------------------------------------------

---------------------------------------------------------------
-- Dle dlx2hdl
constant SLLI_ADDR: std_logic_vector(6 downto 0) := "0000000";
constant SRLI_ADDR: std_logic_vector(6 downto 0) := "0000010";
constant SRAI_ADDR: std_logic_vector(6 downto 0) := "0000011";
---------------------------------------------------------------

begin


   Gen_Addr: process (ID_IR)
   begin
      if (ID_IR(31 downto 26) = "000000") then
         addr <= '0' & ID_IR(5 downto 0);
      else
         addr <= '1' & ID_IR(31 downto 26);
      end if;
   end process;



   ---------------------------------------------------------------------------
   ID_EX_Ra_ID_EX_Rb_ID_EX_MEM_WB_Rw_Gen : PROCESS(addr, ID_IR)
   ---------------------------------------------------------------------------
   begin
      case (addr) is
------------------------------------------------
         when SLLI_ADDR =>
            ID_EX_Ra <= ID_IR(25 downto 21);
            ID_EX_Rb <= "00000";
            ID_EX_MEM_WB_Rw <= ID_IR(15 downto 11);
            -- I-type SLLI
         when SRLI_ADDR =>
            ID_EX_Ra <= ID_IR(25 downto 21);
            ID_EX_Rb <= "00000";
            ID_EX_MEM_WB_Rw <= ID_IR(15 downto 11);
            -- I-type SRLI
         when SRAI_ADDR =>
            ID_EX_Ra <= ID_IR(25 downto 21);
            ID_EX_Rb <= "00000";
            ID_EX_MEM_WB_Rw <= ID_IR(15 downto 11);
            -- I-type SRAI
------------------------------------------------

         when "0000100" =>
            ID_EX_Ra <= ID_IR(25 downto 21);
            ID_EX_Rb <= ID_IR(20 downto 16);
            ID_EX_MEM_WB_Rw <= ID_IR(15 downto 11);
            -- R-type SLL
         when "0000110" =>
            ID_EX_Ra <= ID_IR(25 downto 21);
            ID_EX_Rb <= ID_IR(20 downto 16);
            ID_EX_MEM_WB_Rw <= ID_IR(15 downto 11);
            -- R-type SRL
         when "0000111" =>
            ID_EX_Ra <= ID_IR(25 downto 21);
            ID_EX_Rb <= ID_IR(20 downto 16);
            ID_EX_MEM_WB_Rw <= ID_IR(15 downto 11);
            -- R-type SRA

         when "0010000" =>
            ID_EX_Ra <= ID_IR(25 downto 21);
            ID_EX_Rb <= ID_IR(20 downto 16);
            ID_EX_MEM_WB_Rw <= ID_IR(15 downto 11);
            -- R-type SEQU
         when "0010001" =>
            ID_EX_Ra <= ID_IR(25 downto 21);
            ID_EX_Rb <= ID_IR(20 downto 16);
            ID_EX_MEM_WB_Rw <= ID_IR(15 downto 11);
            -- R-type SNEU
         when "0010010" =>
            ID_EX_Ra <= ID_IR(25 downto 21);
            ID_EX_Rb <= ID_IR(20 downto 16);
            ID_EX_MEM_WB_Rw <= ID_IR(15 downto 11);
            -- R-type SLTU
         when "0010011" =>
            ID_EX_Ra <= ID_IR(25 downto 21);
            ID_EX_Rb <= ID_IR(20 downto 16);
            ID_EX_MEM_WB_Rw <= ID_IR(15 downto 11);
            -- R-type SGTU
         when "0010100" =>
            ID_EX_Ra <= ID_IR(25 downto 21);
            ID_EX_Rb <= ID_IR(20 downto 16);
            ID_EX_MEM_WB_Rw <= ID_IR(15 downto 11);
            -- R-type SLEU
         when "0010101" =>
            ID_EX_Ra <= ID_IR(25 downto 21);
            ID_EX_Rb <= ID_IR(20 downto 16);
            ID_EX_MEM_WB_Rw <= ID_IR(15 downto 11);
            -- R-type SGEU

         when "0011000" =>
            ID_EX_Ra <= ID_IR(25 downto 21);
            ID_EX_Rb <= ID_IR(20 downto 16);
            ID_EX_MEM_WB_Rw <= ID_IR(15 downto 11);
            -- R-type MULT

         when "0100000" =>
            ID_EX_Ra <= ID_IR(25 downto 21);
            ID_EX_Rb <= ID_IR(20 downto 16);
            ID_EX_MEM_WB_Rw <= ID_IR(15 downto 11);
            -- R-type ADD
         when "0100001" =>
            ID_EX_Ra <= ID_IR(25 downto 21);
            ID_EX_Rb <= ID_IR(20 downto 16);
            ID_EX_MEM_WB_Rw <= ID_IR(15 downto 11);
            -- R-type ADDU
         when "0100010" =>
            ID_EX_Ra <= ID_IR(25 downto 21);
            ID_EX_Rb <= ID_IR(20 downto 16);
            ID_EX_MEM_WB_Rw <= ID_IR(15 downto 11);
            -- R-type SUB
         when "0100011" =>
            ID_EX_Ra <= ID_IR(25 downto 21);
            ID_EX_Rb <= ID_IR(20 downto 16);
            ID_EX_MEM_WB_Rw <= ID_IR(15 downto 11);
            -- R-type SUBU
         when "0100100" =>
            ID_EX_Ra <= ID_IR(25 downto 21);
            ID_EX_Rb <= ID_IR(20 downto 16);
            ID_EX_MEM_WB_Rw <= ID_IR(15 downto 11);
            -- R-type AND
         when "0100101" =>
            ID_EX_Ra <= ID_IR(25 downto 21);
            ID_EX_Rb <= ID_IR(20 downto 16);
            ID_EX_MEM_WB_Rw <= ID_IR(15 downto 11);
            -- R-type OR
         when "0100110" =>
            ID_EX_Ra <= ID_IR(25 downto 21);
            ID_EX_Rb <= ID_IR(20 downto 16);
            ID_EX_MEM_WB_Rw <= ID_IR(15 downto 11);
            -- R-type XOR

         when "0101000" =>
            ID_EX_Ra <= ID_IR(25 downto 21);
            ID_EX_Rb <= ID_IR(20 downto 16);
            ID_EX_MEM_WB_Rw <= ID_IR(15 downto 11);
            -- R-type SEQ
         when "0101001" =>
            ID_EX_Ra <= ID_IR(25 downto 21);
            ID_EX_Rb <= ID_IR(20 downto 16);
            ID_EX_MEM_WB_Rw <= ID_IR(15 downto 11);
            -- R-type SNE
         when "0101010" =>
            ID_EX_Ra <= ID_IR(25 downto 21);
            ID_EX_Rb <= ID_IR(20 downto 16);
            ID_EX_MEM_WB_Rw <= ID_IR(15 downto 11);
            -- R-type SLT
         when "0101011" =>
            ID_EX_Ra <= ID_IR(25 downto 21);
            ID_EX_Rb <= ID_IR(20 downto 16);
            ID_EX_MEM_WB_Rw <= ID_IR(15 downto 11);
            -- R-type SGT
         when "0101100" =>
            ID_EX_Ra <= ID_IR(25 downto 21);
            ID_EX_Rb <= ID_IR(20 downto 16);
            ID_EX_MEM_WB_Rw <= ID_IR(15 downto 11);
            -- R-type SLE
         when "0101101" =>
            ID_EX_Ra <= ID_IR(25 downto 21);
            ID_EX_Rb <= ID_IR(20 downto 16);
            ID_EX_MEM_WB_Rw <= ID_IR(15 downto 11);
            -- R-type SGE


         when "1000011" =>
            ID_EX_Ra <= "00000";
            ID_EX_Rb <= "00000";
            ID_EX_MEM_WB_Rw <= "11111";
            -- J-type JAL
         when "1000100" =>
            ID_EX_Ra <= ID_IR(25 downto 21);
            ID_EX_Rb <= "00000";
            ID_EX_MEM_WB_Rw <= "00000";
            -- I-type BEQZ
         when "1000101" =>
            ID_EX_Ra <= ID_IR(25 downto 21);
            ID_EX_Rb <= "00000";
            ID_EX_MEM_WB_Rw <= "00000";
            -- I-type BNEZ
      
         when "1001000" =>
            ID_EX_Ra <= ID_IR(25 downto 21);
            ID_EX_Rb <= "00000";
            ID_EX_MEM_WB_Rw <= ID_IR(20 downto 16);
            -- I-type ADDI
         when "1001001" =>
            ID_EX_Ra <= ID_IR(25 downto 21);
            ID_EX_Rb <= "00000";
            ID_EX_MEM_WB_Rw <= ID_IR(20 downto 16);
            -- I-type ADDUI
         when "1001010" =>
            ID_EX_Ra <= ID_IR(25 downto 21);
            ID_EX_Rb <= "00000";
            ID_EX_MEM_WB_Rw <= ID_IR(20 downto 16);
            -- I-type SUBI
         when "1001011" =>
            ID_EX_Ra <= ID_IR(25 downto 21);
            ID_EX_Rb <= "00000";
            ID_EX_MEM_WB_Rw <= ID_IR(20 downto 16);
            -- I-type SUBUI
         when "1001100" =>
            ID_EX_Ra <= ID_IR(25 downto 21);
            ID_EX_Rb <= "00000";
            ID_EX_MEM_WB_Rw <= ID_IR(20 downto 16);
            -- I-type ANDI
         when "1001101" =>
            ID_EX_Ra <= ID_IR(25 downto 21);
            ID_EX_Rb <= "00000";
            ID_EX_MEM_WB_Rw <= ID_IR(20 downto 16);
            -- I-type ORI
         when "1001110" =>
            ID_EX_Ra <= ID_IR(25 downto 21);
            ID_EX_Rb <= "00000";
            ID_EX_MEM_WB_Rw <= ID_IR(20 downto 16);
            -- I-type XORI
         when "1001111" =>
            ID_EX_Ra <= ID_IR(25 downto 21);
            ID_EX_Rb <= "00000";
            ID_EX_MEM_WB_Rw <= ID_IR(20 downto 16);
            -- I-type LHI
      
         when "1010010" =>
            ID_EX_Ra <= ID_IR(25 downto 21);
            ID_EX_Rb <= "00000";
            ID_EX_MEM_WB_Rw <= "00000";
            -- I-type JR
         when "1010011" =>
            ID_EX_Ra <= ID_IR(25 downto 21);
            ID_EX_Rb <= "00000";
            ID_EX_MEM_WB_Rw <= "11111";
            -- I-type JALR
      
         when "1011000" =>
            ID_EX_Ra <= ID_IR(25 downto 21);
            ID_EX_Rb <= "00000";
            ID_EX_MEM_WB_Rw <= ID_IR(20 downto 16);
            -- I-type SEQI
         when "1011001" =>
            ID_EX_Ra <= ID_IR(25 downto 21);
            ID_EX_Rb <= "00000";
            ID_EX_MEM_WB_Rw <= ID_IR(20 downto 16);
            -- I-type SNEI
         when "1011010" =>
            ID_EX_Ra <= ID_IR(25 downto 21);
            ID_EX_Rb <= "00000";
            ID_EX_MEM_WB_Rw <= ID_IR(20 downto 16);
            -- I-type SLTI
         when "1011011" =>
            ID_EX_Ra <= ID_IR(25 downto 21);
            ID_EX_Rb <= "00000";
            ID_EX_MEM_WB_Rw <= ID_IR(20 downto 16);
            -- I-type SGTI
         when "1011100" =>
            ID_EX_Ra <= ID_IR(25 downto 21);
            ID_EX_Rb <= "00000";
            ID_EX_MEM_WB_Rw <= ID_IR(20 downto 16);
            -- I-type SLEI
         when "1011101" =>
            ID_EX_Ra <= ID_IR(25 downto 21);
            ID_EX_Rb <= "00000";
            ID_EX_MEM_WB_Rw <= ID_IR(20 downto 16);
            -- I-type SGEI
      
         when "1100011" =>
            ID_EX_Ra <= ID_IR(25 downto 21);
            ID_EX_Rb <= "00000";
            ID_EX_MEM_WB_Rw <= ID_IR(20 downto 16);
            
         when "1101011" =>
            ID_EX_Ra <= ID_IR(25 downto 21);
            ID_EX_Rb <= ID_IR(20 downto 16);
            ID_EX_MEM_WB_Rw <= "00000";
            -- I-type SW
            
         when "1110000" =>
            ID_EX_Ra <= ID_IR(25 downto 21);
            ID_EX_Rb <= "00000";
            ID_EX_MEM_WB_Rw <= ID_IR(20 downto 16);
            -- I-type SEQUI
         when "1110001" =>
            ID_EX_Ra <= ID_IR(25 downto 21);
            ID_EX_Rb <= "00000";
            ID_EX_MEM_WB_Rw <= ID_IR(20 downto 16);
            -- I-type SNEUI
         when "1110010" =>
            ID_EX_Ra <= ID_IR(25 downto 21);
            ID_EX_Rb <= "00000";
            ID_EX_MEM_WB_Rw <= ID_IR(20 downto 16);
            -- I-type SLTUI
         when "1110011" =>
            ID_EX_Ra <= ID_IR(25 downto 21);
            ID_EX_Rb <= "00000";
            ID_EX_MEM_WB_Rw <= ID_IR(20 downto 16);
            -- I-type SGTUI
         when "1110100" =>
            ID_EX_Ra <= ID_IR(25 downto 21);
            ID_EX_Rb <= "00000";
            ID_EX_MEM_WB_Rw <= ID_IR(20 downto 16);
            -- I-type SLEUI
         when "1110101" =>
            ID_EX_Ra <= ID_IR(25 downto 21);
            ID_EX_Rb <= "00000";
            ID_EX_MEM_WB_Rw <= ID_IR(20 downto 16);
            -- I-type SGEUI
      
         when others =>
            ID_EX_Ra <= "00000";
            ID_EX_Rb <= "00000";
            ID_EX_MEM_WB_Rw <= "00000";
         END CASE;
   END PROCESS ID_EX_Ra_ID_EX_Rb_ID_EX_MEM_WB_Rw_Gen;

   ---------------------------------------------------------------------------
   ID_EX_Branch_Gen: process(addr)
   ---------------------------------------------------------------------------
   begin
      case (addr) is
         when "1000010" =>
            ID_EX_Branch <= "11";
            -- J
         when "1000011" =>
            ID_EX_Branch <= "11";
            -- JAL
         when "1000100" =>
            ID_EX_Branch <= "01";
            -- BEQZ
         when "1000101" =>
            ID_EX_Branch <= "10";
            -- BNEZ
         when "1010010" =>
            ID_EX_Branch <= "11";
            -- JR
         when "1010011" =>
            ID_EX_Branch <= "11";
            -- JALR
         when others =>
            ID_EX_Branch <= "00";
      end case;
   end process ID_EX_Branch_Gen;
   
   ---------------------------------------------------------------------------
   ID_Imm_type_Gen: process(addr)
   ---------------------------------------------------------------------------
   begin
      case (addr) is
------------------------------------------------
         when SLLI_ADDR =>
            ID_Imm_type <= "100";
            -- I-type SLLI
         when SRLI_ADDR =>
            ID_Imm_type <= "100";
            -- I-type SRLI
         when SRAI_ADDR =>
            ID_Imm_type <= "100";
            -- I-type SRAI
------------------------------------------------

         when "1011000" =>
            ID_Imm_type <= "001";
            -- I-type SEQI
         when "1011001" =>
            ID_Imm_type <= "001";
            -- I-type SNEI
         when "1011010" =>
            ID_Imm_type <= "001";
            -- I-type SLTI
         when "1011011" =>
            ID_Imm_type <= "001";
            -- I-type SGTI
         when "1011100" =>
            ID_Imm_type <= "001";
            -- I-type SLEI
         when "1011101" =>
            ID_Imm_type <= "001";
            -- I-type SGEI

------------------------------------------------


         when "1000010" =>
            ID_Imm_type <= "011";
            -- J
         when "1000011" =>
            ID_Imm_type <= "011";
            -- JAL
         when "1000100" =>
            ID_Imm_type <= "001";
            -- BEQZ
         when "1000101" =>
            ID_Imm_type <= "001";
            -- BNEZ
         when "1001000" =>
            ID_Imm_type <= "001";
            -- I-type ADDI
         when "1001010" =>
            ID_Imm_type <= "001";
            -- I-type SUBI
         when "1100011" =>
            ID_Imm_type <= "001";
            -- I-type LW
         when "1101011" =>
            ID_Imm_type <= "001";
            -- I-type SW
         when others =>
            ID_Imm_type <= "000";
      end case;
   end process ID_Imm_type_Gen;
   
   ---------------------------------------------------------------------------
   WB_Do_Trap_Gen: process(addr)
   ---------------------------------------------------------------------------
   begin
      case (addr) is
         when "1010001" =>
            WB_Do_Trap <= '1';
            -- J-type Trap
         when others =>
            WB_Do_Trap <= '0';
      end case;
   end process WB_Do_Trap_Gen;
   
   ---------------------------------------------------------------------------
   EX_ALU_Op_Gen: process(addr)
   ---------------------------------------------------------------------------
   begin
      case (addr) is
-------------------------------------------------
         when SLLI_ADDR =>
            EX_ALU_OP <= "011";
            -- I-type SLLI
         when SRLI_ADDR =>
            EX_ALU_OP <= "001";
            -- I-type SRLI
-------------------------------------------------
      
         when "0000100" =>
            EX_ALU_OP <= "011";
            -- R-type SLL
         when "0000110" =>
            EX_ALU_OP <= "001";
            -- R-type SRL

         when "0010000" =>
            EX_ALU_OP <= "001";
            -- R-type SEQU
         when "0010001" =>
            EX_ALU_OP <= "110";
            -- R-type SNEU
         when "0010010" =>
            EX_ALU_OP <= "010";
            -- R-type SLTU
         when "0010011" =>
            EX_ALU_OP <= "100";
            -- R-type SGTU
         when "0010100" =>
            EX_ALU_OP <= "011";
            -- R-type SLEU
         when "0010101" =>
            EX_ALU_OP <= "101";
            -- R-type SGEU

         when "0100001" =>
            EX_ALU_OP <= "001";
            -- R-type ADDU
         when "0100010" =>
            EX_ALU_OP <= "010";
            -- R-type SUB
         when "0100011" =>
            EX_ALU_OP <= "011";
            -- R-type SUBU
         when "0100100" =>
            EX_ALU_OP <= "100";
            -- R-type AND
         when "0100101" =>
            EX_ALU_OP <= "101";
            -- R-type OR
         when "0100110" =>
            EX_ALU_OP <= "110";
            -- R-type XOR

         when "0101000" =>
            EX_ALU_OP <= "001";
            -- R-type SEQ
         when "0101001" =>
            EX_ALU_OP <= "110";
            -- R-type SNE
         when "0101010" =>
            EX_ALU_OP <= "010";
            -- R-type SLT
         when "0101011" =>
            EX_ALU_OP <= "100";
            -- R-type SGT
         when "0101100" =>
            EX_ALU_OP <= "011";
            -- R-type SLE
         when "0101101" =>
            EX_ALU_OP <= "101";
            -- R-type SGE


         when "1000100" =>
            EX_ALU_OP <= "010";
            -- I-type BEQZ
         when "1000101" =>
            EX_ALU_OP <= "010";
            -- I-type BNEZ
      
         when "1001001" =>
            EX_ALU_OP <= "001";
            -- I-type ADDUI
         when "1001010" =>
            EX_ALU_OP <= "010";
            -- I-type SUBI
         when "1001011" =>
            EX_ALU_OP <= "011";
            -- I-type SUBUI
         when "1001100" =>
            EX_ALU_OP <= "100";
            -- I-type ANDI
         when "1001101" =>
            EX_ALU_OP <= "101";
            -- I-type ORI
         when "1001110" =>
            EX_ALU_OP <= "110";
            -- I-type XORI
         when "1001111" =>
            EX_ALU_OP <= "111";
            -- I-type LHI
      
         when "1011000" =>
            EX_ALU_OP <= "001";
            -- I-type SEQI
         when "1011001" =>
            EX_ALU_OP <= "110";
            -- I-type SNEI
         when "1011010" =>
            EX_ALU_OP <= "010";
            -- I-type SLTI
         when "1011011" =>
            EX_ALU_OP <= "100";
            -- I-type SGTI
         when "1011100" =>
            EX_ALU_OP <= "011";
            -- I-type SLEI
         when "1011101" =>
            EX_ALU_OP <= "101";
            -- I-type SGEI

         when "1110000" =>
            EX_ALU_OP <= "001";
            -- I-type SEQUI
         when "1110001" =>
            EX_ALU_OP <= "110";
            -- I-type SNEUI
         when "1110010" =>
            EX_ALU_OP <= "010";
            -- I-type SLTUI
         when "1110011" =>
            EX_ALU_OP <= "100";
            -- I-type SGTUI
         when "1110100" =>
            EX_ALU_OP <= "011";
            -- I-type SLEUI
         when "1110101" =>
            EX_ALU_OP <= "101";
            -- I-type SGEUI
      
         when others =>
            EX_ALU_Op <= "000";
      end case;
   end process EX_ALU_Op_Gen;
   
   ---------------------------------------------------------------------------
   EX_ALUSrc_Gen: process(addr)
   ---------------------------------------------------------------------------
   begin
      case (addr) is
-------------------------------------------------
         when SLLI_ADDR =>
            EX_ALUSrc <= '1';
            -- I-type SLLI
         when SRLI_ADDR =>
            EX_ALUSrc <= '1';
            -- I-type SRLI
         when SRAI_ADDR =>
            EX_ALUSrc <= '1';
            -- I-type SRAI
-------------------------------------------------
      
         when "1001000" =>
            EX_ALUSrc <= '1';
            -- I-type ADDI
         when "1001001" =>
            EX_ALUSrc <= '1';
            -- I-type ADDUI
         when "1001010" =>
            EX_ALUSrc <= '1';
            -- I-type SUBI
         when "1001011" =>
            EX_ALUSrc <= '1';
            -- I-type SUBUI
         when "1001100" =>
            EX_ALUSrc <= '1';
            -- I-type ANDI
         when "1001101" =>
            EX_ALUSrc <= '1';
            -- I-type ORI
         when "1001110" =>
            EX_ALUSrc <= '1';
            -- I-type XORI
         when "1001111" =>
            EX_ALUSrc <= '1';
            -- I-type LHI
      
         when "1011000" =>
            EX_ALUSrc <= '1';
            -- I-type SEQI
         when "1011001" =>
            EX_ALUSrc <= '1';
            -- I-type SNEI
         when "1011010" =>
            EX_ALUSrc <= '1';
            -- I-type SLTI
         when "1011011" =>
            EX_ALUSrc <= '1';
            -- I-type SGTI
         when "1011100" =>
            EX_ALUSrc <= '1';
            -- I-type SLEI
         when "1011101" =>
            EX_ALUSrc <= '1';
            -- I-type SGEI

         when "1100011" =>
            EX_ALUSrc <= '1';
            -- I-type LW

         when "1101011" =>
            EX_ALUSrc <= '1';
            -- I-type SW
            
         when "1110000" =>
            EX_ALUSrc <= '1';
            -- I-type SEQUI
         when "1110001" =>
            EX_ALUSrc <= '1';
            -- I-type SNEUI
         when "1110010" =>
            EX_ALUSrc <= '1';
            -- I-type SLTUI
         when "1110011" =>
            EX_ALUSrc <= '1';
            -- I-type SGTUI
         when "1110100" =>
            EX_ALUSrc <= '1';
            -- I-type SLEUI
         when "1110101" =>
            EX_ALUSrc <= '1';
            -- I-type SGEUI
         when others =>
            EX_ALUSrc <= '0';
      end case;
   end process EX_ALUSrc_Gen;
   
   ---------------------------------------------------------------------------
   EX_Jmp_reg_Gen: process(addr)
   ---------------------------------------------------------------------------
   begin
      case (addr) is
         when "1010010" =>
            EX_Jmp_reg <= '1';
            -- I-type JR
         when "1010011" =>
            EX_Jmp_reg <= '1';
            -- I-type JALR
         when others =>
            EX_Jmp_reg <= '0';
      end case;
   end process EX_Jmp_reg_Gen;
   
   ---------------------------------------------------------------------------
   MEM_Store_Gen: process(addr)
   ---------------------------------------------------------------------------
   begin
      case (addr) is
         when "1101011" =>
            MEM_Store <= '1';
            -- I-type SW
         when others =>
            MEM_Store <= '0';
      end case;
   end process MEM_Store_Gen;
   
   ---------------------------------------------------------------------------
   EX_Unsigned_Gen: process(addr)
   ---------------------------------------------------------------------------
   begin
      case (addr) is
         when "0010000" =>
            EX_Unsigned <= '1';
            -- R-type SEQU
         when "0010001" =>
            EX_Unsigned <= '1';
            -- R-type SNEU
         when "0010010" =>
            EX_Unsigned <= '1';
            -- R-type SLTU
         when "0010011" =>
            EX_Unsigned <= '1';
            -- R-type SGTU
         when "0010100" =>
            EX_Unsigned <= '1';
            -- R-type SLEU
         when "0010101" =>
            EX_Unsigned <= '1';
            -- R-type SGEU


         when "1110000" =>
            EX_Unsigned <= '1';
            -- I-type SEQUI
         when "1110001" =>
            EX_Unsigned <= '1';
            -- I-type SNEUI
         when "1110010" =>
            EX_Unsigned <= '1';
            -- I-type SLTUI
         when "1110011" =>
            EX_Unsigned <= '1';
            -- I-type SGTUI
         when "1110100" =>
            EX_Unsigned <= '1';
            -- I-type SLEUI
         when "1110101" =>
            EX_Unsigned <= '1';
            -- I-type SGEUI

         when others =>
            EX_Unsigned <= '0';
      end case;
   end process EX_Unsigned_Gen;


   ---------------------------------------------------------------------------
   MEM_Load_Gen: process(addr)
   ---------------------------------------------------------------------------
   begin
      case (addr) is
         when "1100011" =>
            MEM_Load <= '1';
            -- I-type LW
         when others =>
            MEM_Load <= '0';
      end case;
   end process MEM_Load_Gen;
   
   ---------------------------------------------------------------------------
   MEM_WB_Select_Gen: process(addr)
   ---------------------------------------------------------------------------
   begin
      case (addr) is
-------------------------------------------------
         when SLLI_ADDR =>
            WB_Select <= "011";
            -- I-type SLLI
         when SRLI_ADDR =>
            WB_Select <= "011";
            -- I-type SRLI
         when SRAI_ADDR =>
            WB_Select <= "011";
            -- I-type SRAI
-------------------------------------------------

         when "0010000" =>
            WB_Select <= "101";
            -- R-type SEQU
         when "0010001" =>
            WB_Select <= "101";
            -- R-type SNEU
         when "0010010" =>
            WB_Select <= "101";
            -- R-type SLTU
         when "0010011" =>
            WB_Select <= "101";
            -- R-type SGTU
         when "0010100" =>
            WB_Select <= "101";
            -- R-type SLEU
         when "0010101" =>
            WB_Select <= "101";
            -- R-type SGEU
-------------------------------------------------
         when "0100000" =>
            WB_Select <= "100";
            -- R-type ADD
         when "0100001" =>
            WB_Select <= "100";
            -- R-type ADDU
         when "0100010" =>
            WB_Select <= "100";
            -- R-type SUB
         when "0100011" =>
            WB_Select <= "100";
            -- R-type SUBU
         when "0100100" =>
            WB_Select <= "100";
            -- R-type AND
         when "0100101" =>
            WB_Select <= "100";
            -- R-type OR
         when "0100110" =>
            WB_Select <= "100";
            -- R-type XOR
-------------------------------------------------
         when "0101000" =>
            WB_Select <= "101";
            -- R-type SEQ
         when "0101001" =>
            WB_Select <= "101";
            -- R-type SNE
         when "0101010" =>
            WB_Select <= "101";
            -- R-type SLT
         when "0101011" =>
            WB_Select <= "101";
            -- R-type SGT
         when "0101100" =>
            WB_Select <= "101";
            -- R-type SLE
         when "0101101" =>
            WB_Select <= "101";
            -- R-type SGE
-------------------------------------------------
         when "1001000" =>
            WB_Select <= "100";
            -- I-type ADDI
         when "1001001" =>
            WB_Select <= "100";
            -- I-type ADDUI
         when "1001010" =>
            WB_Select <= "100";
            -- I-type SUBI
         when "1001011" =>
            WB_Select <= "100";
            -- I-type SUBUI
         when "1001100" =>
            WB_Select <= "100";
            -- I-type ANDI
         when "1001101" =>
            WB_Select <= "100";
            -- I-type ORI
         when "1001110" =>
            WB_Select <= "100";
            -- I-type XORI
         when "1001111" =>
            WB_Select <= "100";
            -- I-type LHI
-------------------------------------------------
         when "1011000" =>
            WB_Select <= "101";
            -- I-type SEQI
         when "1011001" =>
            WB_Select <= "101";
            -- I-type SNEI
         when "1011010" =>
            WB_Select <= "101";
            -- I-type SLTI
         when "1011011" =>
            WB_Select <= "101";
            -- I-type SGTI
         when "1011100" =>
            WB_Select <= "101";
            -- I-type SLEI
         when "1011101" =>
            WB_Select <= "101";
            -- I-type SGEI
-------------------------------------------------
         when "1110000" =>
            WB_Select <= "101";
            -- I-type SEQUI
         when "1110001" =>
            WB_Select <= "101";
            -- I-type SNEUI
         when "1110010" =>
            WB_Select <= "101";
            -- I-type SLTUI
         when "1110011" =>
            WB_Select <= "101";
            -- I-type SGTUI
         when "1110100" =>
            WB_Select <= "101";
            -- I-type SLEUI
         when "1110101" =>
            WB_Select <= "101";
            -- I-type SGEUI		
-------------------------------------------------
         when "1000011" =>
            WB_Select <= "110";
            -- J-type JAL
         when "1010011" =>
            WB_Select <= "110";
            -- I-type JALR
-------------------------------------------------
         when "1100011" =>
            WB_Select <= "001";
            -- I-type LW
         when "0011000" =>
            WB_Select <= "010";
            -- R-type MULT
-------------------------------------------------
         when "0000100" =>
            WB_Select <= "011";
            -- R-type SLL
         when "0000110" =>
            WB_Select <= "011";
            -- R-type SRL
         when "0000111" =>
            WB_Select <= "011";
            -- R-type SRA
         when others =>
            WB_Select <= "000";
      end case;
   end process MEM_WB_Select_Gen;

   ---------------------------------------------------------------------------
   MEM_dual_cycle_Gen: process(addr)
   ---------------------------------------------------------------------------
   begin
      case (addr) is
-------------------------------------------------
         when SLLI_ADDR =>
            MEM_dual_cycle <= '1';
            -- I-type SLLI
         when SRLI_ADDR =>
            MEM_dual_cycle <= '1';
            -- I-type SRLI
         when SRAI_ADDR =>
            MEM_dual_cycle <= '1';
            -- I-type SRAI
-------------------------------------------------

         when "0010000" =>
            MEM_dual_cycle <= '1';
            -- R-type SEQU
         when "0010001" =>
            MEM_dual_cycle <= '1';
            -- R-type SNEU
         when "0010010" =>
            MEM_dual_cycle <= '1';
            -- R-type SLTU
         when "0010011" =>
            MEM_dual_cycle <= '1';
            -- R-type SGTU
         when "0010100" =>
            MEM_dual_cycle <= '1';
            -- R-type SLEU
         when "0010101" =>
            MEM_dual_cycle <= '1';
            -- R-type SGEU
-------------------------------------------------
         when "0100010" =>
            MEM_dual_cycle <= '1';
            -- R-type SUB
         when "0100011" =>
            MEM_dual_cycle <= '1';
            -- R-type SUBU
         when "0100100" =>
            MEM_dual_cycle <= '1';
            -- R-type AND
         when "0100101" =>
            MEM_dual_cycle <= '1';
            -- R-type OR
         when "0100110" =>
            MEM_dual_cycle <= '1';
            -- R-type XOR
-------------------------------------------------
         when "0101000" =>
            MEM_dual_cycle <= '1';
            -- R-type SEQ
         when "0101001" =>
            MEM_dual_cycle <= '1';
            -- R-type SNE
         when "0101010" =>
            MEM_dual_cycle <= '1';
            -- R-type SLT
         when "0101011" =>
            MEM_dual_cycle <= '1';
            -- R-type SGT
         when "0101100" =>
            MEM_dual_cycle <= '1';
            -- R-type SLE
         when "0101101" =>
            MEM_dual_cycle <= '1';
            -- R-type SGE
-------------------------------------------------
         when "1001010" =>
            MEM_dual_cycle <= '1';
            -- I-type SUBI
         when "1001011" =>
            MEM_dual_cycle <= '1';
            -- I-type SUBUI
         when "1001100" =>
            MEM_dual_cycle <= '1';
            -- I-type ANDI
         when "1001101" =>
            MEM_dual_cycle <= '1';
            -- I-type ORI
         when "1001110" =>
            MEM_dual_cycle <= '1';
            -- I-type XORI
         when "1001111" =>
            MEM_dual_cycle <= '1';
            -- I-type LHI
-------------------------------------------------
         when "1011000" =>
            MEM_dual_cycle <= '1';
            -- I-type SEQI
         when "1011001" =>
            MEM_dual_cycle <= '1';
            -- I-type SNEI
         when "1011010" =>
            MEM_dual_cycle <= '1';
            -- I-type SLTI
         when "1011011" =>
            MEM_dual_cycle <= '1';
            -- I-type SGTI
         when "1011100" =>
            MEM_dual_cycle <= '1';
            -- I-type SLEI
         when "1011101" =>
            MEM_dual_cycle <= '1';
            -- I-type SGEI
-------------------------------------------------
         when "1110000" =>
            MEM_dual_cycle <= '1';
            -- I-type SEQUI
         when "1110001" =>
            MEM_dual_cycle <= '1';
            -- I-type SNEUI
         when "1110010" =>
            MEM_dual_cycle <= '1';
            -- I-type SLTUI
         when "1110011" =>
            MEM_dual_cycle <= '1';
            -- I-type SGTUI
         when "1110100" =>
            MEM_dual_cycle <= '1';
            -- I-type SLEUI
         when "1110101" =>
            MEM_dual_cycle <= '1';
            -- I-type SGEUI		
-------------------------------------------------
         when "1100011" =>
            MEM_dual_cycle <= '1';
            -- I-type LW
         when "0011000" =>
            MEM_dual_cycle <= '1';
            -- R-type MULT
-------------------------------------------------
         when "0000100" =>
            MEM_dual_cycle <= '1';
            -- R-type SLL
         when "0000110" =>
            MEM_dual_cycle <= '1';
            -- R-type SRL
         when "0000111" =>
            MEM_dual_cycle <= '1';
            -- R-type SRA
         when others =>
            MEM_dual_cycle <= '0';
      end case;
   end process MEM_dual_cycle_Gen;


end Behavioral;

