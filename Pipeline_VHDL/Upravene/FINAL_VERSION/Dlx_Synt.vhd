-- 2007/05/22   MK  Novy syntetizovatelny top-modul jadra DLX vcetne cache - slouceni dvou verzi s jednou cache
-- 2007/05/26   MJ  Velikost bloku datove cache je nyni generic

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

library work;
use work.DLX_pkg.all;

entity Dlx_Synt is
  GENERIC( 
		DC_WidthB : natural :=DCache_WidthB; -- width of data cache block offset. Block size=4B*(2**WidthB), default=32B
      WidthA    : natural :=20   -- Width of Address Bus
  );
  port(
      Clk             : in     std_logic;
      Reset           : in     std_logic;
      Trap_Detected   : out    std_logic;
--Instruction (MemSub interface)    
      I_AB            : OUT    std_logic_vector (WidthA-1 DOWNTO 2);  -- address bus
      I_DB            : INOUT  std_logic_vector (31 DOWNTO 0);   -- data bus
      I_Start         : OUT    std_logic;                       -- signal indicating start of new read/write transaction
      I_Rd            : OUT    std_logic;                       -- read from memory
      I_Wr            : OUT    std_logic;                       -- write to memory
      I_Ack           : IN     std_logic;                       -- acknowledges read/write data
--Data (MemSub interface)
      D_AB            : OUT    std_logic_vector (WidthA-1 DOWNTO 2);
      D_DB            : INOUT  std_logic_vector (31 DOWNTO 0);
      D_Start         : OUT    std_logic;        
      D_Rd            : OUT    std_logic;
      D_Wr            : OUT    std_logic;
      D_Ack           : IN     std_logic
  );
end Dlx_Synt;

architecture struct of Dlx_Synt is

  component intdlx
  port(
      Clk            : in  std_logic;
      Reset          : in  std_logic;

      D_Addr         : out std_logic_vector(WidthA-1 DOWNTO 0);
      D_to_Mem       : out std_logic_vector(31 downto 0);
      D_Wr_En        : out std_logic;
      D_Load         : out std_logic;
      D_From_Mem     : in  std_logic_vector(31 downto 0);
      D_Cache_Hit    : in  std_logic;

      I_Addr         : out std_logic_vector(WidthA-1 downto 0);
      Instr_from_Mem : in  std_logic_vector(31 downto 0);
      ICache_hit     : in  std_logic;
      
      Trap_Detected  : out std_logic
      );
  end component;

  component ICache_core
    port( 
      Clk         : IN  std_logic;
      Reset       : IN  std_logic;  
      -- interface with CPU core
      PC_Addr     : IN  std_logic_vector (31 DOWNTO 0);
      Instruction : OUT std_logic_vector (31 DOWNTO 0);
      ICache_hit  : OUT std_logic;
      -- interface to lower level memory 
      Miss_data   : INOUT  std_logic_vector (31 DOWNTO 0);
      Miss_addr   : OUT std_logic_vector (31 DOWNTO 0);
      start       : OUT std_logic;
      Rd          : OUT std_logic;
      Wr_En       : IN  std_logic);
  end component; 

  component DCache is
	GENERIC( 
      Aw : natural := 20;
      Dw : natural := 32;
		WidthB : natural :=3   	-- width of data cache block offset. Block size=4B*(2**WidthB), default=32B
    );
    PORT( 
      Clk         : IN  std_logic;
      Reset       : IN  std_logic;
      -- interface with CPU core
      Load        : IN  std_logic;                  -- load req
      Store       : IN  std_logic;                  -- store req
      Addr        : IN  std_logic_vector (19 DOWNTO 0);    -- address
      Din         : IN  std_logic_vector (31 DOWNTO 0);    -- data in (when saving)
      Dout        : OUT std_logic_vector (31 DOWNTO 0);    -- data out (when loading)
      DCache_hit  : OUT std_logic;          -- cache hit when loading, cache updated when storing
      -- interface to lower level memory (MemSub)
      mem_data    : INOUT std_logic_vector (31 DOWNTO 0);
      mem_addr    : OUT std_logic_vector (19 DOWNTO 0);
      mem_start   : OUT std_logic;          -- to memory
      mem_Rd      : OUT std_logic;          --    -||-
      mem_Wr      : OUT std_logic;          --    -||-
      mem_Ack     : IN  std_logic           -- ack from memory
    );
  end component;

  SIGNAL PC_Addr           : std_logic_vector (31 DOWNTO 0);
  SIGNAL Zeros             : std_logic_vector (31 DOWNTO WidthA);
  SIGNAL Miss_addr         : std_logic_vector (31 DOWNTO 0);
  SIGNAL I_Wr_En           : std_logic;
  signal I_Addr            : std_logic_vector (WidthA-1 downto 0);
  signal Instr_from_Mem    : std_logic_vector (31 downto 0);
  SIGNAL ICache_hit        : std_logic;

  SIGNAL mem_addr         : std_logic_vector (WidthA-1 DOWNTO 0);
  SIGNAL D_addr           : std_logic_vector(WidthA-1 downto 0);
  SIGNAL D_to_mem         : std_logic_vector(31 downto 0);
  SIGNAL D_from_mem       : std_logic_vector(31 downto 0);
  SIGNAL D_load           : std_logic;
  SIGNAL D_store          : std_logic;
  SIGNAL D_hit            : std_logic;

begin

  IntDlx_Inst : intdlx 
    PORT MAP(
      Clk            => Clk,
      Reset          => Reset,
      D_Addr         => D_addr,
      D_to_Mem       => D_to_mem,
      D_Wr_En        => D_store,
      D_Load         => D_load,
      D_From_Mem     => D_from_mem,
      D_Cache_Hit    => D_hit,
      I_Addr         => I_Addr,
      Instr_from_Mem => Instr_from_Mem,
      ICache_hit     => ICache_hit,
      Trap_Detected  => Trap_Detected
  );
  
  ICache_Inst : ICache_core
    port map(      -- interface with CPU core
      Clk         => Clk,
      Reset       => Reset,
      -- interface with CPU core
      PC_Addr     => PC_Addr,
      Instruction => Instr_from_Mem,
      ICache_hit  => ICache_hit,
      -- interface to lower level memory (MemSub)
      Miss_data   => I_DB,
      Miss_addr   => Miss_addr, --zde nemuze byt primo AB, protoze vektory jsou ruzne velke,
                                --napojeni se provede az nize (AB<=Miss_addr(19 downto 2);)
      start       => I_START,
      Rd          => I_RD,
      Wr_En       => I_Wr_En  -- = ACK
    );

  Zeros   <= (others => '0');
  PC_Addr <= Zeros & I_Addr;
  I_AB    <= Miss_addr(WidthA-1 downto 2);
  I_Wr_En <= I_ACK;  --predani mezi submem a i_cache
  I_WR    <= '0';


  DCache_Inst : DCache
	 GENERIC MAP( WidthB => DCache_WidthB )
    PORT MAP(
      Clk         => Clk,
      Reset       => Reset,
      -- interface with CPU core
      Load        => D_load,
      Store       => D_store,
      Addr        => D_addr,
      Din         => D_to_mem,
      Dout        => D_from_mem,
      DCache_hit  => D_hit,
      -- interface to lower level memory (MemSub)
      mem_data    => D_DB,
      mem_addr    => mem_addr,
      mem_start   => D_Start,
      mem_Rd      => D_Rd,
      mem_Wr      => D_Wr,
      mem_Ack     => D_Ack
    );

  D_AB    <= mem_addr(WidthA-1 downto 2);

end struct;

