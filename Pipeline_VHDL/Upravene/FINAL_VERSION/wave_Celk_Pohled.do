onerror {resume}

quietly WaveActivateNextPane {} 0
add wave -noupdate -divider {Clocks, Trap, Reset}
add wave -noupdate -format Logic /Celk_Pohled_tb/clk
add wave -noupdate -format Logic /Celk_Pohled_tb/reset

add wave -noupdate -format Literal /Celk_Pohled_tb/cycle_number
add wave -noupdate -format Logic /Celk_Pohled_tb/Trap_Detected

add wave -noupdate -format Logic /Celk_Pohled_tb/Local_WRITEBUF_EMPTY
add wave -noupdate -format Logic /Celk_Pohled_tb/Local_DUMPNOW
add wave -noupdate -format Logic /Celk_Pohled_tb/Local_DUMPACK
add wave -noupdate -format Literal /celk_pohled_tb/uut/dlx_inst/dcache_inst/full_write_buffer_stalls
add wave -noupdate -format Literal /celk_pohled_tb/uut/dlx_inst/dcache_inst/load_stall_write_buffer_not_empty


add wave -noupdate -divider {Pipeline control registers}
add wave -noupdate -color Yellow -format Literal -itemcolor Yellow -label {IF PC Programm Counter} -radix hexadecimal /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/if_pc
add wave -noupdate -color Orange -format Literal -itemcolor Orange -label {ID Instruction register} -radix hexadecimal /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/id_ir
add wave -noupdate -color {Orange Red} -format Literal -itemcolor {Orange Red} -label {EX Instruction Register} -radix hexadecimal /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/ex_ir
add wave -noupdate -color Green -format Literal -itemcolor Green -label {MEM Instruction Register} -radix hexadecimal /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/mem_ir
add wave -noupdate -color Blue -format Literal -itemcolor Blue -label {WB Instruction Register} -radix hexadecimal /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/wb_ir

quietly virtual signal -install /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst { /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/instruction(31 downto 26)} IF_opcode
quietly virtual signal -install /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst { /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/instruction(5 downto 0)} IF_funct
quietly virtual signal -install /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst { /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/id_ir(31 downto 26)} ID_opcode
quietly virtual signal -install /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst { /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/id_ir(5 downto 0)} ID_funct
quietly virtual signal -install /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst { /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/ex_ir(31 downto 26)} EX_opcode
quietly virtual signal -install /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst { /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/ex_ir(5 downto 0)} EX_funct
quietly virtual signal -install /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst { /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/mem_ir(31 downto 26)} MEM_opcode
quietly virtual signal -install /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst { /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/mem_ir(5 downto 0)} MEM_funct
quietly virtual signal -install /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst { /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/wb_ir(31 downto 26)} WB_opcode
quietly virtual signal -install /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst { /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/wb_ir(5 downto 0)} WB_funct

quietly virtual type {Rtype undef J JAL BEQZ BNEZ undef undef 
              ADDI ADDUI SUBI SUBUI ANDI ORI XORI LHI
              undef TRAP JR JALR SLLI(Tab) undef SRLI(Tab) SRAI(Tab) 
              SEQI SNEI SLTI SGTI SLEI SGEI undef undef
              undef undef undef LW undef undef undef undef 
              undef undef undef SW undef undef undef undef 
              undef undef undef undef undef undef undef undef
              undef undef undef undef undef undef undef undef} dlx_opcode_t

quietly virtual type {NOP(SLLI)(dlx2hdl) undef SRLI(dlx2hdl) SRAI(dlx2hdl) SLL undef SRL SRA
             undef undef undef undef undef undef undef undef
             undef undef undef undef undef undef undef undef
             MULT undef undef undef undef undef undef undef
             ADD ADDU SUB SUBU AND OR XOR undef
             SEQ SNE SLT SGT SLE SGE undef undef
             undef undef undef undef undef undef undef undef
             undef undef undef undef undef undef undef undef} dlx_funct_t


quietly virtual function {(dlx_opcode_t)Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/IF_opcode} IF_opc_decoded
quietly virtual function {(dlx_funct_t)Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/IF_funct} IF_funct_decoded
quietly virtual function {(dlx_opcode_t)Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/ID_opcode} ID_opc_decoded
quietly virtual function {(dlx_funct_t)Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/ID_funct} ID_funct_decoded
quietly virtual function {(dlx_opcode_t)Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/EX_opcode} EX_opc_decoded
quietly virtual function {(dlx_funct_t)Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/EX_funct} EX_funct_decoded
quietly virtual function {(dlx_opcode_t)Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/MEM_opcode} MEM_opc_decoded
quietly virtual function {(dlx_funct_t)Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/MEM_funct} MEM_funct_decoded
quietly virtual function {(dlx_opcode_t)Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/WB_opcode} WB_opc_decoded
quietly virtual function {(dlx_funct_t)Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/WB_funct} WB_funct_decoded

quietly virtual function { &{/Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/IF_opc_decoded, /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/IF_funct_decoded}} IF_instr_decoded
quietly virtual function { &{/Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/ID_opc_decoded, /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/ID_funct_decoded}} ID_instr_decoded
quietly virtual function { &{/Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/EX_opc_decoded, /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/EX_funct_decoded}} EX_instr_decoded
quietly virtual function { &{/Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/MEM_opc_decoded, /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/MEM_funct_decoded}} MEM_instr_decoded
quietly virtual function { &{/Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/WB_opc_decoded, /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/WB_funct_decoded}} WB_instr_decoded

add wave -noupdate -divider {Decoded Instructions}

add wave -color Yellow -itemcolor Yellow Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/IF_instr_decoded
add wave -color Orange -itemcolor Orange Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/ID_instr_decoded
add wave -color {Orange Red} -itemcolor {Orange Red} Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/EX_instr_decoded
add wave -color Green -itemcolor Green Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/MEM_instr_decoded
add wave -color Blue  -itemcolor Blue Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/WB_instr_decoded

add wave -noupdate -divider {IF Stage}

add wave -noupdate -format Literal -radix hexadecimal /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/IF_PC
add wave -noupdate -format Literal -radix hexadecimal /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/instruction
add wave -noupdate -format Literal -radix hexadecimal /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/next_pc
add wave -noupdate -format Logic /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/jump_now
add wave -noupdate -format Logic /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/if_stall

add wave -noupdate -divider {ID Stage}

add wave -noupdate -format Literal -radix hexadecimal /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/id_ir
add wave -noupdate -format Literal -radix unsigned /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/ID_Ra
add wave -noupdate -format Literal -radix unsigned /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/id_Rb
add wave -noupdate -format Literal -radix unsigned /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/wb_rw
add wave -noupdate -format Logic /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/Registers_Inst/write_enable
add wave -noupdate -format Literal -radix hexadecimal /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/busw
add wave -noupdate -format Literal -label {Register File} -radix hexadecimal /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/Registers_Inst/reg_file
add wave -noupdate -format Literal -radix hexadecimal /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/id_busa
add wave -noupdate -format Literal -radix hexadecimal /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/id_busb
add wave -noupdate -format Literal /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/ID_imm_type
add wave -noupdate -format Literal -radix hexadecimal /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/id_imm32
add wave -noupdate -format Logic /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/Pipeline_Flow_Ctrl_Inst/Operands_not_Ready
add wave -noupdate -format Logic /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/id_clear
add wave -noupdate -format Logic /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/id_stall

add wave -noupdate -divider {EX Stage}

add wave -noupdate -format Literal -radix hexadecimal /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/ex_ir
add wave -noupdate -format Literal /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/EX_alu_op
add wave -noupdate -format Literal -radix unsigned /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/ex_Ra
add wave -noupdate -format Literal -radix unsigned /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/ex_Rb
add wave -noupdate -format Literal -radix unsigned /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/ex_rw
add wave -noupdate -format Logic /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/ex_alusrc
add wave -noupdate -format Literal -radix hexadecimal /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/ex_busa
add wave -noupdate -format Literal -radix hexadecimal /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/ex_busb
add wave -noupdate -format Literal -radix hexadecimal /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/alu_b
add wave -noupdate -format Literal -radix hexadecimal /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/ALU_C_Fast
add wave -noupdate -format Logic /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/EX_Unsigned_Comp
add wave -noupdate -format Literal -radix hexadecimal /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/EX_PC
add wave -noupdate -format Literal -radix hexadecimal /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/ex_imm32
add wave -noupdate -format Literal -radix hexadecimal /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/EX_Target_Addr
add wave -noupdate -format Literal /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/ex_branch
add wave -noupdate -format Logic /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/ex_jmp_reg
add wave -noupdate -format Logic /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/ex_stall
add wave -noupdate -format Logic /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/ex_clear

add wave -noupdate -divider {MEM Stage}

add wave -noupdate -format Literal -radix hexadecimal /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/mem_ir
add wave -noupdate -format Literal -radix hexadecimal /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/mem_s
add wave -noupdate -format Literal -radix hexadecimal /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/mem_busb
add wave -noupdate -format Literal -radix hexadecimal /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/MEM_PC
add wave -noupdate -format Logic /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/jump_now
add wave -noupdate -format Logic /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/mem_load
add wave -noupdate -format Logic /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/mem_store
add wave -noupdate -format Logic /celk_pohled_tb/uut/dlx_inst/intdlx_inst/d_cache_hit
add wave -noupdate -format Logic /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/MEM_dual_cycle
add wave -noupdate -format Literal -radix hexadecimal /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/mem_rw
add wave -noupdate -format Literal -radix hexadecimal /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/mem_m
add wave -noupdate -format Logic /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/mem_stall
add wave -noupdate -format Logic /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/mem_clear

add wave -noupdate -divider {WB Stage}

add wave -noupdate -format Literal -radix hexadecimal /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/wb_ir
add wave -noupdate -format Literal -radix unsigned /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/wb_rw
add wave -noupdate -format Logic /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/wb_Select
add wave -noupdate -format Literal -radix hexadecimal /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/WB_Mem
add wave -noupdate -format Literal -radix hexadecimal /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/WB_Mult
add wave -noupdate -format Literal -radix hexadecimal /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/WB_Barrel
add wave -noupdate -format Literal -radix hexadecimal /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/WB_ALU
add wave -noupdate -format Literal -radix hexadecimal /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/WB_Cond_Res
add wave -noupdate -format Literal -radix hexadecimal /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/WB_PC
add wave -noupdate -format Literal -radix hexadecimal /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/busw
add wave -noupdate -format Logic /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/wb_clear
add wave -noupdate -format Logic /Celk_Pohled_tb/uut/Dlx_Inst/IntDlx_Inst/wb_stall

add wave -noupdate -divider {Instruction Cache}

add wave -noupdate -format Literal -radix hexadecimal /celk_pohled_tb/uut/Dlx_Inst/ICache_Inst/pc_addr
add wave -noupdate -format Literal -radix hexadecimal /celk_pohled_tb/uut/Dlx_Inst/ICache_Inst/instruction
add wave -noupdate -format Logic /celk_pohled_tb/uut/Dlx_Inst/ICache_Inst/icache_hit
add wave -noupdate -format Literal -radix hexadecimal /celk_pohled_tb/uut/Dlx_Inst/ICache_Inst/miss_data
add wave -noupdate -format Literal -radix hexadecimal /celk_pohled_tb/uut/Dlx_Inst/ICache_Inst/miss_addr
add wave -noupdate -format Logic /celk_pohled_tb/uut/Dlx_Inst/ICache_Inst/start
add wave -noupdate -format Logic /celk_pohled_tb/uut/Dlx_Inst/ICache_Inst/rd
add wave -noupdate -format Logic /celk_pohled_tb/uut/Dlx_Inst/ICache_Inst/wr_en
add wave -noupdate -format Logic /celk_pohled_tb/uut/Dlx_Inst/ICache_Inst/clk
add wave -noupdate -format Logic /celk_pohled_tb/uut/Dlx_Inst/ICache_Inst/reset
add wave -noupdate -format Literal -radix hexadecimal /celk_pohled_tb/uut/Dlx_Inst/ICache_Inst/miss_addr_reg
add wave -noupdate -format Logic /celk_pohled_tb/uut/Dlx_Inst/ICache_Inst/miss_int
add wave -noupdate -format Logic /celk_pohled_tb/uut/Dlx_Inst/ICache_Inst/miss_reg
add wave -noupdate -format Literal -radix hexadecimal /celk_pohled_tb/uut/Dlx_Inst/ICache_Inst/miss_word_cnt
add wave -noupdate -format Logic /celk_pohled_tb/uut/Dlx_Inst/ICache_Inst/release
add wave -noupdate -format Literal -radix hexadecimal /celk_pohled_tb/uut/Dlx_Inst/ICache_Inst/pc_tag
add wave -noupdate -format Literal -radix hexadecimal /celk_pohled_tb/uut/Dlx_Inst/ICache_Inst/index_addr
add wave -noupdate -format Literal -radix hexadecimal /celk_pohled_tb/uut/Dlx_Inst/ICache_Inst/word_addr
add wave -noupdate -format Literal -radix hexadecimal /celk_pohled_tb/uut/Dlx_Inst/ICache_Inst/tag_read
add wave -noupdate -format Literal -radix hexadecimal /celk_pohled_tb/uut/Dlx_Inst/ICache_Inst/tag_write
add wave -noupdate -format Literal -radix hexadecimal /celk_pohled_tb/uut/Dlx_Inst/ICache_Inst/word_read
add wave -noupdate -format Literal -radix hexadecimal /celk_pohled_tb/uut/Dlx_Inst/ICache_Inst/data_addr
add wave -noupdate -format Logic /celk_pohled_tb/uut/Dlx_Inst/ICache_Inst/nclk

add wave -noupdate -divider {Instruction Memory}

add wave -noupdate -format Literal -radix hexadecimal /celk_pohled_tb/uut/Inst_Mem/ab 
add wave -noupdate -format Logic /celk_pohled_tb/uut/Inst_Mem/ack 
add wave -noupdate -format Logic /celk_pohled_tb/uut/Inst_Mem/clk 
add wave -noupdate -format Literal -radix hexadecimal /celk_pohled_tb/uut/Inst_Mem/db 
add wave -noupdate -format Logic /celk_pohled_tb/uut/Inst_Mem/delayrd 
add wave -noupdate -format Logic /celk_pohled_tb/uut/Inst_Mem/delaywr 
add wave -noupdate -format ASCII /celk_pohled_tb/uut/Inst_Mem/dumpfile 
add wave -noupdate -format ASCII /celk_pohled_tb/uut/Inst_Mem/preloadfile 
add wave -noupdate -format Logic /celk_pohled_tb/uut/Inst_Mem/rd 
add wave -noupdate -format Logic /celk_pohled_tb/uut/Inst_Mem/start 
add wave -noupdate -format Logic /celk_pohled_tb/uut/Inst_Mem/widtha 
add wave -noupdate -format Logic /celk_pohled_tb/uut/Inst_Mem/widthb 
add wave -noupdate -format Logic /celk_pohled_tb/uut/Inst_Mem/wr

add wave -noupdate -divider {Data cache}
add wave -noupdate -format Logic /celk_pohled_tb/uut/dlx_inst/dcache_inst/core_grant_req
add wave -noupdate -format Logic /celk_pohled_tb/uut/dlx_inst/dcache_inst/core_grant_ack
add wave -noupdate -format Logic /celk_pohled_tb/uut/dlx_inst/dcache_inst/fifo_grant_req
add wave -noupdate -format Logic /celk_pohled_tb/uut/dlx_inst/dcache_inst/fifo_grant_ack
add wave -noupdate -format Logic /celk_pohled_tb/uut/dlx_inst/dcache_inst/core_hit
add wave -noupdate -format Literal -radix hexadecimal /celk_pohled_tb/uut/dlx_inst/dcache_inst/cache/miss_word_cnt

add wave -noupdate -divider {Data cache - FIFO}
add wave -noupdate -format Logic /celk_pohled_tb/uut/dlx_inst/dcache_inst/fifo_empty
add wave -noupdate -format Logic /celk_pohled_tb/uut/dlx_inst/dcache_inst/fifo_full

add wave -noupdate -divider {Data cache - tag}
add wave -noupdate -format Literal -radix hexadecimal /celk_pohled_tb/uut/dlx_inst/dcache_inst/cache/tag_array/addr_read
add wave -noupdate -format Literal -radix hexadecimal /celk_pohled_tb/uut/dlx_inst/dcache_inst/cache/tag_array/mem_array
add wave -noupdate -format Literal -radix hexadecimal /celk_pohled_tb/uut/dlx_inst/dcache_inst/cache/tag_array/data_in
add wave -noupdate -format Literal -radix hexadecimal /celk_pohled_tb/uut/dlx_inst/dcache_inst/cache/tag_array/data_out
add wave -noupdate -format Logic /celk_pohled_tb/uut/dlx_inst/dcache_inst/cache/tag_array/wr_en

add wave -noupdate -divider {Data cache - data}
add wave -noupdate -format Literal -radix hexadecimal /celk_pohled_tb/uut/dlx_inst/dcache_inst/cache/data_array/addr_read
add wave -noupdate -format Literal -radix hexadecimal /celk_pohled_tb/uut/dlx_inst/dcache_inst/cache/data_array/mem_array
add wave -noupdate -format Literal -radix hexadecimal /celk_pohled_tb/uut/dlx_inst/dcache_inst/cache/data_array/data_in
add wave -noupdate -format Literal -radix hexadecimal /celk_pohled_tb/uut/dlx_inst/dcache_inst/cache/data_array/data_out
add wave -noupdate -format Logic /celk_pohled_tb/uut/dlx_inst/dcache_inst/cache/data_array/wr_en

add wave -noupdate -divider {Data memory}
add wave -noupdate -format Literal -radix hexadecimal /celk_pohled_tb/uut/data_mem/ab
add wave -noupdate -format Literal -radix hexadecimal /celk_pohled_tb/uut/data_mem/db
add wave -noupdate -format Logic -radix hexadecimal /celk_pohled_tb/uut/data_mem/start
add wave -noupdate -format Logic -radix hexadecimal /celk_pohled_tb/uut/data_mem/rd
add wave -noupdate -format Logic -radix hexadecimal /celk_pohled_tb/uut/data_mem/wr
add wave -noupdate -format Logic -radix hexadecimal /celk_pohled_tb/uut/data_mem/ack

TreeUpdate [SetDefaultTree]
configure wave -namecolwidth 182
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0