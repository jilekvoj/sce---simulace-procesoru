vlib work

vcom -explicit  -93 "to_Hstring.vhd"
vcom -explicit  -93 "multiplexor.vhd"
vcom -explicit  -93 "bram_array.vhd"
vcom -explicit  -93 "Pipeline_reg_rtl.vhd"
vcom -explicit  -93 "Id_Inst_Decode.vhd"
vcom -explicit  -93 "next_addr_logic_tbl.vhd"
vcom -explicit  -93 "fifo.vhd"
vcom -explicit  -93 "Registers_rtl.vhd"
vcom -explicit  -93 "Pipeline_Flow_Ctrl.vhd"
vcom -explicit  -93 "PC_rtl.vhd"
vcom -explicit  -93 "Multiplier.vhd"
vcom -explicit  -93 "ID_EX_MEM_WB_Inst_Decoder.vhd"
vcom -explicit  -93 "Extender.vhd"
vcom -explicit  -93 "EX_Branch_Executor.vhd"
vcom -explicit  -93 "DLX_pkg.vhd"
vcom -explicit  -93 "DCache_core.vhd"
vcom -explicit  -93 "Cond_Resolver.vhd"
vcom -explicit  -93 "Barrel_Shifter.vhd"
vcom -explicit  -93 "ALU_Slow.vhd"
vcom -explicit  -93 "ALU_Fast.vhd"
vcom -explicit  -93 "intdlx_struct.vhd"
vcom -explicit  -93 "ICache_core.vhd"
vcom -explicit  -93 "DCache.vhd"
vcom -explicit  -93 "Program_memory_behav.vhd"
vcom -explicit  -93 "MemSub.vhd"
vcom -explicit  -93 "Dlx_Synt.vhd"
vcom -explicit  -93 "Celk_Pohled.vhd"
vcom -explicit  -93 "Celk_Pohled_tb.vhd"
vsim -t 1ns -lib work celk_pohled_tb
view wave

view structure
view signals

do wave_Celk_Pohled.do
run -all
