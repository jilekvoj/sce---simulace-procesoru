// Instruction memory module
`timescale 1 ps / 100 fs
module InstructionMem(instruction, address);

input [31:0] address;
output [31:0] instruction;

	reg [31:0] Mem [0:my_pkg::MEM_SIZE];

	assign instruction = Mem[address % my_pkg::MEM_SIZE];

endmodule