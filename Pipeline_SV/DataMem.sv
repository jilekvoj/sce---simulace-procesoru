`timescale 1 ps / 100 fs
// Data Memory 
module dataMem(data, address, writedata, writeenable,MemRead,clk);

input [31:0] address, writedata;
input writeenable,MemRead, clk;
output [31:0] data;
reg [31:0] Mem [0:my_pkg::MEM_SIZE];


	assign data = Mem[address % my_pkg::MEM_SIZE];

	always @(posedge clk) begin
		if (writeenable == 1'b1) begin
			Mem[address % my_pkg::MEM_SIZE] = writedata;
		end
	end

endmodule