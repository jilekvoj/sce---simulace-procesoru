`timescale 1ns/10ps

import uvm_pkg::*;

`include "my_pkg.sv"
import my_pkg::*;

`include "InputInterface.sv"
`include "OutputInterface.sv"
`include "Processor.sv"

module uvm_top;
	bit T_CLK, T_RST;

	always
		#5 T_CLK = ~T_CLK;

	initial begin
		T_RST <= 1'b0;
		T_CLK <= 1'b1;
		#1 T_RST <= 1'b1;
		#21 T_RST <= 1'b0;
	end


	if_out inst_if_out (.clk(T_CLK), .reset(T_RST));
	if_in inst_if_in (.clk(T_CLK), .reset(T_RST));
	if_reg_acc inst_if_reg (.clk(T_CLK), .reset(T_RST));

	// disable access to registers in DUT (this interface is used for RAL simulation)
	assign inst_if_reg.reg_rd_en = 1'b0;
	assign inst_if_reg.reg_wr_en = 1'b0;
	assign inst_if_reg.PC_rd_en = 1'b0;
	assign inst_if_reg.PC_wr_en = 1'b0;

	Processor _dut (.CLK(T_CLK), .RESET(T_RST), .instruction(inst_if_in.instruction), .mem_data_out(inst_if_in.mem_data_out),
                        .PC(inst_if_out.PC), .mem_we(inst_if_out.mem_we), .mem_addr(inst_if_out.mem_addr), 
                        .mem_data_in(inst_if_out.mem_data_in), .if_reg_acc_inst(inst_if_reg.dut));

	initial begin
		// save references to DUT interfaces to uvm databese
		uvm_config_db#(virtual if_in)::set(uvm_root::get(), "*", "inst_if_in", inst_if_in);
		uvm_config_db#(virtual if_out)::set(uvm_root::get(), "*", "inst_if_out", inst_if_out);

		run_test("my_test");
	end
endmodule

