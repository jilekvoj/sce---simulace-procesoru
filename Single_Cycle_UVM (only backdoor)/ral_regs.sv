`include "uvm_macros.svh"
import uvm_pkg::*;

class my_reg extends uvm_reg;
	`uvm_object_utils(my_reg)
    
	rand uvm_reg_field data;
	string instr;

	function new (string name = "my_reg");
		super.new(name, 32, UVM_NO_COVERAGE);
	endfunction
 
	function void build;
		data = uvm_reg_field::type_id::create("data");  
		data.configure(this, 32, 0, "RW", 0, 0, 0, 1, 0);
    	endfunction

	task SetInstr (string instruction);
		instr = instruction;
	endtask

	// override do_check of the base class - custom error message
	function bit do_check(input uvm_reg_data_t expected, input uvm_reg_data_t actual, uvm_reg_map map);
		uvm_reg_data_t  valid_bits_mask = 0;
		foreach(m_fields[i]) begin
			string acc = m_fields[i].get_access(map);
			acc = acc.substr(0, 1);
			if (!(m_fields[i].get_compare() == UVM_NO_CHECK ||acc == "WO")) begin
				valid_bits_mask |= ((1 << m_fields[i].get_n_bits())-1)<< m_fields[i].get_lsb_pos();
			end
		end

		if ((actual&valid_bits_mask) === (expected&valid_bits_mask)) return 1;

		`uvm_error("RegModel", $sformatf("Error - instruction %s\nWrong value in DUT - %s\nExpected value:%b, got:%b", instr, get_full_name(), expected, actual))

		return 0;
	endfunction

endclass

class my_reg_model_gr extends uvm_reg_block;
	`uvm_object_utils(my_reg_model_gr)

	rand my_reg  my_reg_inst[31:0];
	rand my_reg my_PC;   

	function new (string name = "");
		super.new(name, build_coverage(UVM_NO_COVERAGE));
	endfunction
 
	function void build;
		default_map = create_map("my_map", 0, 132, UVM_LITTLE_ENDIAN);
		for (int i = 0; i < 32; i++) begin
			my_reg_inst[i] = my_reg::type_id::create($sformatf("my_reg_inst%d", i));
			my_reg_inst[i].build();
			my_reg_inst[i].configure(this);
			my_reg_inst[i].add_hdl_path_slice($sformatf("GPR_Set.rf[%d]", i), 0, my_reg_inst[i].get_n_bits());
			default_map.add_reg(my_reg_inst[i], 4 * i, "RW");
		end
		my_PC = my_reg::type_id::create("my_PC");
		my_PC.build();
		my_PC.configure(this);
		my_PC.add_hdl_path_slice("PC1.ADDR_OUT", 0, my_PC.get_n_bits());
		add_hdl_path("_dut");
	endfunction
endclass

class my_reg_model extends uvm_reg_block;
	`uvm_object_utils(my_reg_model)

	rand my_reg_model_gr m_reg_model_gr;
	
	function new (string name = "");
		super.new(name);
	endfunction

	function void build;
		default_map = create_map("", 0, 132, UVM_LITTLE_ENDIAN);
	
		m_reg_model_gr = my_reg_model_gr::type_id::create("m_reg_model_gr",,get_full_name());
		m_reg_model_gr.configure(this, "_dut");	
		m_reg_model_gr.build();

		add_hdl_path("uvm_top");
		default_map.add_submap(m_reg_model_gr.default_map, 0);
	endfunction
endclass