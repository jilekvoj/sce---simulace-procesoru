`timescale 1ns/10ps

`include "uvm_macros.svh"

import uvm_pkg::*;

class my_monitor extends uvm_monitor;
	`uvm_component_utils(my_monitor)
  
	virtual if_out inst_if_out;
	my_pkg::my_item inst_collected_item;
	// port for communication with scoreboard
	uvm_analysis_port #(my_pkg::my_item) inst_collected_item_port;
  
	function new (string name = "my_monitor", uvm_component parent = null);
		super.new(name, parent);
	endfunction
  
	virtual function void build_phase(uvm_phase phase);
		super.build_phase(phase);
		inst_collected_item_port = new("inst_collected_item_port", this);
  
		// get reference for a DUT input interface
		if(!uvm_config_db#(virtual if_out)::get(this, "", "inst_if_out", inst_if_out))
			`uvm_fatal("NOVIF", {"Missing inst_if_out: ", get_full_name(), ".inst_if_out"})
	endfunction
  
	virtual task run_phase(uvm_phase phase);
		forever begin
			while (inst_if_out.reset !== 1'b0) #1;
			@(negedge inst_if_out.clk);
			if (inst_if_out.reset !== 1'b0) continue;
			inst_collected_item = my_pkg::my_item::type_id::create("inst_collected_item", this);
			inst_collected_item.data_to_mem = inst_if_out.mem_data_in;
			inst_collected_item.mem_addr = inst_if_out.mem_addr;
			inst_collected_item.mem_we = inst_if_out.mem_we;

			// wait for PC update
			@(posedge inst_if_out.clk);
			#1;
			if (inst_if_out.reset !== 1'b0) continue;
			inst_collected_item.PC = inst_if_out.PC;
      
			inst_collected_item_port.write(inst_collected_item);
		end
	endtask
endclass

