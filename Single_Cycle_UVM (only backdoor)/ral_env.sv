`include "uvm_macros.svh"
import uvm_pkg::*;

class my_reg_env extends uvm_env;
	`uvm_component_utils(my_reg_env)

	my_pkg::my_reg_model m_reg_model;

	function new(string name, uvm_component parent);
		super.new(name, parent);
	endfunction
 
	function void build_phase(uvm_phase phase);
		super.build_phase(phase);
		m_reg_model = my_pkg::my_reg_model::type_id::create("m_reg_model", this);

    		m_reg_model.build();
		m_reg_model.lock_model();
		// save reference to reg model to uvm database
   		uvm_config_db #(my_pkg::my_reg_model)::set(null, "uvm_top", "m_reg_model", m_reg_model);
	endfunction
 
endclass