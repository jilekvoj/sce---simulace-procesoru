`timescale 1ns/10ps

`include "uvm_macros.svh"

// import knihovny UVM
import uvm_pkg::*;

class my_in_monitor extends uvm_monitor;
	`uvm_component_utils(my_in_monitor)
  
	virtual if_in inst_if_in;
	my_pkg::my_item inst_collected_item;
	// port for communication with scoreboard
	uvm_analysis_port #(my_pkg::my_item) inst_collected_item_port;
  
	function new (string name = "my_monitor", uvm_component parent = null);
		super.new(name, parent);
	endfunction

	virtual function void build_phase(uvm_phase phase);
		super.build_phase(phase);
		inst_collected_item_port = new("inst_collected_item_port", this);
	
		// get reference for a DUT input interface
		if(!uvm_config_db#(virtual if_in)::get(this, "", "inst_if_in", inst_if_in))
			`uvm_fatal("NOVIF", {"Missing inst_if_in: ", get_full_name(), ".inst_if_in"})
	endfunction
  
	virtual task run_phase(uvm_phase phase);
		forever begin
			while (inst_if_in.reset !== 1'b0) #1;
			@(negedge inst_if_in.clk);
			if (inst_if_in.reset !== 1'b0) continue;
			inst_collected_item = my_pkg::my_item::type_id::create("inst_collected_item", this);

			inst_collected_item.instruction = inst_if_in.instruction;
			inst_collected_item.data_from_mem = inst_if_in.mem_data_out;

			// odeslani analytickym portem
			inst_collected_item_port.write(inst_collected_item);
		end
	endtask
endclass
