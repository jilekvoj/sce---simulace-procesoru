`timescale 1ns/10ps

`include "uvm_macros.svh"

// import knihovny UVM
import uvm_pkg::*;

class my_item extends uvm_sequence_item;

	// deklarace jednotlivych polozek
	rand bit [31:0] instruction;
	rand bit [31:0] data_from_mem;
	bit [31:0] PC;
	bit [31:0] data_to_mem;
	bit [31:0] mem_addr;
	bit mem_we;
  
	constraint valid_instruction
	{
		instruction[31:26] dist {6'b000000 := 8, 6'b001000 := 1, 6'b100011 := 1, 6'b101011 := 1, 6'b000100 := 1, 6'b000011 := 1, 6'b000111 := 1, 6'b011111 := 2, 6'b000010 := 1};
	}

	constraint valid_ALU_OP
	{
		(instruction[31:26] == 6'b000000) -> (instruction[10:0] inside {11'b00000100000, 11'b00000100010, 11'b00000100100, 11'b00000100101, 11'b00000101010, 11'b00000000100, 11'b00000000110, 11'b00000000111});
		(instruction[31:26] == 6'b011111) -> (instruction[10:0] inside {11'b00100010000, 11'b00000010000});
	}

	constraint Beq_Balance
	{
		((instruction[31:26] == 6'b000100) && (data_from_mem[0])) -> (instruction[25:21] == instruction[20:16]);
	}

	function void post_randomize ();
		time t = $time();
		int pos;
		// init registers at the beginning of the simulation
		if (t < 34 * 10ns) begin
			instruction[31:21] = 11'b10001100000;
			pos = t / 10ns;
			pos = pos - 2;
			instruction[20:16] = pos;
		end
		// JRE instruction fix
		if (instruction[31:26] == 6'b000111)
			instruction[20:0] = 21'b000000000000000001000;
	endfunction

	`uvm_object_utils_begin(my_item)
		`uvm_field_int(instruction, UVM_DEFAULT)
		`uvm_field_int(data_from_mem, UVM_DEFAULT)
		`uvm_field_int(PC, UVM_DEFAULT)
		`uvm_field_int(data_to_mem, UVM_DEFAULT)
		`uvm_field_int(mem_addr, UVM_DEFAULT)
		`uvm_field_int(mem_we, UVM_DEFAULT)
	`uvm_object_utils_end
 
	// instructio coverage
	covergroup item_cover_instr;
		a: coverpoint instruction[31:26]
		{
			bins aa[] = {6'b000000, 6'b001000, 6'b100011, 6'b101011, 6'b000100, 6'b000011, 6'b000111, 6'b011111, 6'b000010};
		}
		b: coverpoint instruction[25:0]
		{
			option.auto_bin_max = 50;
		}
		c: cross a, b;
	endgroup

	// ALU operations coverage
	covergroup item_cover_ALU;
		a: coverpoint instruction[31:26] // only ALU op
		{
			bins aa[] = {6'b000000}; 
		}
		b: coverpoint instruction[10:0] // valid ALU ops
		{
			bins bb[] = {11'b00000100000, 11'b00000100010, 11'b00000100100, 11'b00000100101, 11'b00000101010, 11'b00000000100, 11'b00000000110, 11'b00000000111};
		}
		c: coverpoint instruction[25:16];
		d: cross a, b, c;
	endgroup
 
	function new (string name = "my_item");
		super.new(name);
		item_cover_instr = new();
		item_cover_ALU = new();
	endfunction

endclass

