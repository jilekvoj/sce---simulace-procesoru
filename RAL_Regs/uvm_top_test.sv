`include "uvm_macros.svh"
import uvm_pkg::*;
`include "Registers.sv"

`include "my_monitor.sv"
`include "my_test.sv"
`include "ral_env.sv"
`include "ral_adapter.sv"
`include "ral_regs.sv"
`include "my_driver.sv"
`include "my_agent.sv"
`include "my_item.sv"

module uvm_top_test;

	bit CLK = 0;

	my_if m_if(CLK);
	Registers DUT(m_if);

	always begin
		CLK = ~CLK;
		#5ns;
	end

	initial begin
		my_test m_test;
		m_test = new("my_test");
		uvm_config_db #(virtual my_if)::set(null, "", "m_if", m_if);
		run_test();
	end
endmodule
