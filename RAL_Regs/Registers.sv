`timescale 1ns / 1ps
`include "interface.sv"
`default_nettype none
module Registers
(
	my_if.dut interf
);

	reg [31:0] rf[31:0];

	assign interf.RD1 = rf[interf.A1];
	assign interf.RD2 = rf[interf.A2];

	always @(interf.CLK) begin
		rf[0] = 0;
	end

	always @(posedge interf.CLK) begin
		if(interf.WE3) begin
			if(interf.A3!=5 'b00000) rf[interf.A3] <= interf.WD3;
		end
	end	

endmodule