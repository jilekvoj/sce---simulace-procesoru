`ifndef RAL_ENV_168535102
`define RAL_ENV_168535102
`include "uvm_macros.svh"
`include "ral_adapter.sv"
`include "ral_regs.sv"
import uvm_pkg::*;

class my_reg_env extends uvm_env;
	`uvm_component_utils(my_reg_env)

	my_reg_model m_reg_model;
	my_adapter m_adapter;
	uvm_reg_predictor #(my_item) m_predictor;

	function new(string name, uvm_component parent);
		super.new(name, parent);
	endfunction
 
	function void build_phase(uvm_phase phase);
		super.build_phase(phase);
     
		m_reg_model = my_reg_model::type_id::create("m_reg_model", this);
		m_adapter = my_adapter::type_id::create("m_adapter");
		m_predictor = uvm_reg_predictor #(my_item)::type_id::create("m_predictor", this);

    		m_reg_model.build();
		m_reg_model.lock_model();
   		uvm_config_db #(my_reg_model)::set(null, "uvm_test_top", "m_reg_model", m_reg_model);
	endfunction
   
	function void connect_phase(uvm_phase phase);
		m_predictor.map = m_reg_model.default_map;   
		m_predictor.adapter = m_adapter;  
	endfunction
 
endclass
`endif