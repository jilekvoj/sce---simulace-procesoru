`ifndef MY_AGENT_2684513525
`define MY_AGENT_2684513525
`include "uvm_macros.svh"
`include "my_driver.sv"
`include "my_monitor.sv"
import uvm_pkg::*;

class my_agent extends uvm_agent;
	`uvm_component_utils (my_agent)

	function new (string name = "my_agent", uvm_component parent = null);
		super.new(name, parent);
	endfunction

	my_driver m_driver;
	my_monitor m_monitor;
	uvm_sequencer #(my_item) m_seq;

	virtual function void build_phase (uvm_phase phase);
		super.build_phase(phase);
		m_driver = my_driver::type_id::create("m_driver", this);
		m_monitor = my_monitor::type_id::create("m_monitor", this);
		m_seq = uvm_sequencer #(my_item)::type_id::create("m_seq", this);
	endfunction	

	virtual function void connect_phase (uvm_phase phase);
		super.connect_phase(phase);
		m_driver.seq_item_port.connect(m_seq.seq_item_export);
	endfunction
endclass
`endif