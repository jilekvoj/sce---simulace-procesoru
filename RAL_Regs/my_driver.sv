`ifndef MY_DRIVER_13518635135
`define MY_DRIVER_13518635135
`include "uvm_macros.svh"
`include "my_item.sv"
import uvm_pkg::*;

class my_driver extends uvm_driver #(my_item);
	`uvm_component_utils (my_driver)
	
	my_item m_item;
	virtual my_if m_if;

	function new (string name = "my_driver", uvm_component parent = null);
		super.new(name, parent);
	endfunction

	virtual function void build_phase (uvm_phase phase);
		super.build_phase (phase);
		if(!uvm_config_db#(virtual my_if)::get(null, "", "m_if", m_if))
			`uvm_fatal("NOVIF", {"Missing m_if: ", get_full_name(), ".inst_if_in"})
	endfunction

	virtual task run_phase (uvm_phase phase);
		m_if.A1 = 0;
		m_if.A2 = 0;
		m_if.A3 = 0;
		m_if.WD3 = 0;
		m_if.WE3 = 0;

		forever begin
			seq_item_port.get_next_item(m_item);
			if (m_item.write) begin
				m_if.WE3 = 1'b1;
				m_if.WD3 = m_item.wdata;
				m_if.A3 = m_item.addr >> 2;
				@(posedge m_if.CLK);
				m_if.WE3 = 1'b0;
			end
			else begin
				m_if.A1 = m_item.addr >> 2;
				#1;
				m_item.rdata = m_if.RD1;
			end
			seq_item_port.item_done();
		end
	endtask
endclass
`endif