`ifndef MY_MONITOR_1583518351
`define MY_MONITOR_1583518351
`include "uvm_macros.svh"
`include "my_item.sv"
import uvm_pkg::*;

class my_monitor extends uvm_monitor;
	`uvm_component_utils (my_monitor)

	uvm_analysis_port #(my_item) mon_ap;
	virtual my_if m_if;

	function new (string name = "my_monitor", uvm_component parent = null);
		super.new(name, parent);
	endfunction
	
	virtual function void build_phase (uvm_phase phase);
		super.build_phase(phase);
		mon_ap = new ("mon_ap", this);
		if(!uvm_config_db#(virtual my_if)::get(null, "", "m_if", m_if))
			`uvm_fatal("NOVIF", {"Missing m_if: ", get_full_name(), ".inst_if_in"})
	endfunction;

	virtual task run_phase (uvm_phase phase);
		fork
			forever begin
				my_item m_item = my_item::type_id::create("m_item");
				@(posedge m_if.CLK)
				m_item.write = m_if.WE3;
				if (m_if.WE3) begin
					m_item.wdata = m_if.WD3;
					m_item.addr = m_if.A3 << 2;
				end
				else begin
					m_item.addr = m_if.A1 << 2;
					m_item.rdata = m_if.RD1;
				end
				mon_ap.write(m_item);
			end
		join_none
	endtask
endclass
`endif