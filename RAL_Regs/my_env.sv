`ifndef MY_ENV_6435138
`define MY_ENV_6435138
`include "uvm_macros.svh"
`include "ral_env.sv"
`include "my_agent.sv"
import uvm_pkg::*;

class my_env extends uvm_env;
	`uvm_component_utils (my_env)

	my_agent m_agent;
	my_reg_env m_reg_env;

	function new (string name = "my_env", uvm_component parent = null);
		super.new(name, parent);
	endfunction

	virtual function void build_phase (uvm_phase phase);
		super.build_phase(phase);
		m_agent = my_agent::type_id::create("m_agent", this); 
		m_reg_env = my_reg_env::type_id::create("m_reg_env", this);
		uvm_reg::include_coverage("*", UVM_CVR_ALL);
	endfunction

	virtual function void connect_phase (uvm_phase phase);
		super.connect_phase(phase);
		m_agent.m_monitor.mon_ap.connect(m_reg_env.m_predictor.bus_in);
		m_reg_env.m_reg_model.default_map.set_sequencer(m_agent.m_seq, m_reg_env.m_adapter);
	endfunction
endclass
`endif