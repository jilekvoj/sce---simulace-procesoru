`ifndef INTERFACE_168516
`define INTERFACE_168516
interface my_if (input wire CLK);
	logic [4:0] A1, A2, A3;
        logic[31:0] WD3;
	logic WE3;
	logic [31:0] RD1, RD2;
	modport dut (input A1, A2, A3, WD3, WE3, CLK, output RD1, RD2);
endinterface
`endif