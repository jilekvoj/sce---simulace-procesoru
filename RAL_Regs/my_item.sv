`ifndef MY_ITEM_48635159
`define MY_ITEM_48635159
`include "uvm_macros.svh"
import uvm_pkg::*;

class my_item extends uvm_sequence_item;
	rand bit [31:0] addr;
	rand bit [31:0] rdata;
	rand bit [31:0] wdata;
	rand bit write;

	`uvm_object_utils_begin(my_item)
		`uvm_field_int(addr, UVM_ALL_ON)
		`uvm_field_int(rdata, UVM_ALL_ON)
		`uvm_field_int(wdata, UVM_ALL_ON)
		`uvm_field_int(write, UVM_ALL_ON)
	`uvm_object_utils_end

	function new (string name = "my_item");
		super.new(name);
	endfunction
	
	constraint c_addr 
	{
		addr inside {0, 4, 8, 16, 20, 24, 28, 32, 36, 40, 44, 48, 52, 56, 60, 64, 68, 72, 76, 80, 84, 88, 92, 96, 100, 104, 108, 112, 116, 120, 124};
	}
endclass
`endif
