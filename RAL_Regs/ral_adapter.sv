`ifndef RAL_ADAPTER_168453183
`define RAL_ADAPTER_168453183
`include "uvm_macros.svh"
`include "my_item.sv"
import uvm_pkg::*;

class my_adapter extends uvm_reg_adapter;
	`uvm_object_utils (my_adapter)
 
	function new (string name = "my_adapter");
		super.new (name);
	endfunction

   	function uvm_sequence_item reg2bus(const ref uvm_reg_bus_op rw);	
		my_item tx = my_item::type_id::create("tx");   
     
		tx.write = (rw.kind == UVM_WRITE);
		tx.addr  = rw.addr;
		if (tx.write)  tx.wdata = rw.data;
		if (!tx.write) tx.rdata = rw.data;
 
		return tx;
	endfunction
 
	function void bus2reg(uvm_sequence_item bus_item, ref uvm_reg_bus_op rw);
		my_item tx;
     
		assert( $cast(tx, bus_item) )
			else `uvm_fatal("", "A bad thing has just happened in my_adapter")
 
		rw.kind = tx.write ? UVM_WRITE : UVM_READ;
		rw.addr = tx.addr;
		rw.data = tx.rdata;
 
		rw.status = UVM_IS_OK;
	endfunction
endclass
`endif