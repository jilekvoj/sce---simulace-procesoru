`ifndef RAL_REGS_1835163
`define RAL_REGS_1835163
`include "uvm_macros.svh"
import uvm_pkg::*;

class my_reg extends uvm_reg;
	`uvm_object_utils(my_reg)
    
	rand uvm_reg_field data;

	function new (string name = "intr");
		super.new(name, 32, UVM_NO_COVERAGE);
	endfunction
 
	function void build;
		data = uvm_reg_field::type_id::create("data");  
		data.configure(this, 32, 0, "RW", 0, 0, 0, 1, 0);
    endfunction
endclass

class my_reg_model_gr extends uvm_reg_block;
	`uvm_object_utils(my_reg_model_gr)

	rand my_reg  my_reg_inst[31:0];
   
	function new (string name = "");
		super.new(name, build_coverage(UVM_NO_COVERAGE));
	endfunction
 
	function void build;
		default_map = create_map("my_map", 0, 128, UVM_LITTLE_ENDIAN);
		for (int i = 0; i < 32; i++) begin
			my_reg_inst[i] = my_reg::type_id::create($sformatf("my_reg_inst%d", i));
			my_reg_inst[i].build();
			my_reg_inst[i].configure(this);
			my_reg_inst[i].add_hdl_path_slice($sformatf("rf[%d]", i), 0, my_reg_inst[i].get_n_bits());
			default_map.add_reg(my_reg_inst[i], 4 * i, "RW");

		end
		add_hdl_path("DUT");
	endfunction
endclass

class my_reg_model extends uvm_reg_block;
	`uvm_object_utils(my_reg_model)

	rand my_reg_model_gr m_reg_model_gr;
	
	function new (string name = "");
		super.new(name);
	endfunction

	function void build;
		default_map = create_map("", 0, 128, UVM_LITTLE_ENDIAN);
	
		m_reg_model_gr = my_reg_model_gr::type_id::create("m_reg_model_gr",,get_full_name());
		m_reg_model_gr.configure(this, "DUT");	
		m_reg_model_gr.build();

		add_hdl_path("uvm_top_test");
		default_map.add_submap(m_reg_model_gr.default_map, 0);
	endfunction
endclass
`endif