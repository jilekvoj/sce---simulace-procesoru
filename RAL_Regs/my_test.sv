`ifndef MY_TEST_4635163351
`define MY_TEST_4635163351
`include "uvm_macros.svh"
import uvm_pkg::*;
`include "my_env.sv"
`include "ral_regs.sv"

`timescale 1ns / 1ps

class my_test extends uvm_test;
	`uvm_component_utils (my_test)
	
	my_env m_env;
	
	function new (string name = "my_test", uvm_component parent = null);
		super.new(name, parent);
	endfunction;

	virtual function void build_phase (uvm_phase phase);
		super.build_phase(phase);
		m_env = my_env::type_id::create ("m_env", this);
	endfunction

	virtual task main_phase(uvm_phase phase);
		
		my_reg_model m_reg_model;
		uvm_status_e status;
		int rdata;

		phase.raise_objection(this);
		
		m_env.m_reg_env.set_report_verbosity_level(UVM_HIGH);
		uvm_config_db#(my_reg_model)::get(null, "uvm_test_top", "m_reg_model", m_reg_model);

		m_reg_model.m_reg_model_gr.my_reg_inst[5].write(status, 5);
		#1ns;
		m_reg_model.m_reg_model_gr.my_reg_inst[5].read(status, rdata);

		if (rdata == 5) begin
			`uvm_info(get_type_name(), $sformatf("%d -> ok", rdata), UVM_LOW);
		end
		else
			`uvm_error(get_type_name(), $sformatf("%d -> error", rdata));

		m_reg_model.m_reg_model_gr.my_reg_inst[5].write(status, 7);
		#1ns;
		m_reg_model.m_reg_model_gr.my_reg_inst[5].read(status, rdata, UVM_BACKDOOR);

		if (rdata == 7) begin
			`uvm_info(get_type_name(), $sformatf("%d -> ok", rdata), UVM_LOW);
		end
		else
			`uvm_error(get_type_name(), $sformatf("%d -> error", rdata));

		m_reg_model.m_reg_model_gr.my_reg_inst[5].write(status, 9, UVM_BACKDOOR);
		#1ns;
		m_reg_model.m_reg_model_gr.my_reg_inst[5].read(status, rdata);

		if (rdata == 9) begin
			`uvm_info(get_type_name(), $sformatf("%d -> ok", rdata), UVM_LOW);
		end
		else
			`uvm_error(get_type_name(), $sformatf("%d -> error", rdata));

		#10ns;
		phase.drop_objection(this);
	endtask

	virtual task shutdown_phase (uvm_phase phase);
		super.shutdown_phase(phase);
		phase.raise_objection(this);
		#100;
		phase.drop_objection(this);
	endtask
endclass
`endif