`timescale 1ns / 1ps
`default_nettype none

module ALU
(
	input wire [31:0] SRC_A, SRC_B, 
	input wire [3:0] ALU_CONTROL,
        output wire [31:0] RESULT,
	output wire ZERO
);

	wire [31:0] results [15:0]; 

	assign ZERO = (RESULT == 0);

	assign results[0] = SRC_A & SRC_B;				// 0000 -> and
	assign results[1] = SRC_A | SRC_B;				// 0001 -> or
	assign results[2] = SRC_A + SRC_B;				// 0010 -> add
	assign results[3] = SRC_A ^ SRC_B;				// 0011 -> xor
	assign results[6] = SRC_A - SRC_B;				// 0110 -> sub
	assign results[7] = ($signed(SRC_A)) < ($signed(SRC_B));	// 0111 -> slt
	assign results[10] = SRC_A >> SRC_B;
	assign results[11] = $signed(SRC_A) >> SRC_B;
	assign results[12] = SRC_A << SRC_B;

	ByteAdder ByteAdd (.SRC_A(SRC_A), .SRC_B(SRC_B), .SATURATION(1'b0), .OUTPUT(results[8]));	// 1000 -> byte add
	ByteAdder ByteAddSat (.SRC_A(SRC_A), .SRC_B(SRC_B), .SATURATION(1'b1), .OUTPUT(results[9]));	// 1001 -> satureted byte add

	assign RESULT = results[ALU_CONTROL];

endmodule