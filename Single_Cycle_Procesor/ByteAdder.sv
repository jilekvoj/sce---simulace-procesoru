`timescale 1ns / 1ps
`default_nettype none

module ByteAdder #(parameter BUS_WIDTH = 32)
(
	input wire [BUS_WIDTH - 1:0] SRC_A,
	input wire [BUS_WIDTH - 1:0] SRC_B,
	input wire SATURATION,
	output wire [BUS_WIDTH - 1:0] OUTPUT
);

	generate for (genvar i = 0; i < BUS_WIDTH / 8; i++)
	begin
		assign OUTPUT[i *  8 + 7: i * 8] = SATURATION ? 
		((8'b11111111 - SRC_A[i *  8 + 7: i * 8] < SRC_B[i *  8 + 7: i * 8]) ? 8'b11111111 : SRC_A[i *  8 + 7: i * 8] + SRC_B[i *  8 + 7: i * 8]) : 
		SRC_A[i *  8 + 7: i * 8] + SRC_B[i *  8 + 7: i * 8];
	end
	endgenerate

endmodule