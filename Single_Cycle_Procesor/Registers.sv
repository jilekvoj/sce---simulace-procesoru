`timescale 1ns / 1ps
`default_nettype none
module Registers
(
	input wire [4:0] A1, A2, A3, 
        input wire [31:0] WD3,
	input wire CLK,
	input wire WE3,
	output reg [31:0] RD1, RD2);

	reg [31:0] rf[31:0];

	assign RD1 = rf[A1];
	assign RD2 = rf[A2];

	always @(CLK) begin
		rf[0] = 0;
	end

	always @(posedge CLK) begin
		if(WE3) begin
			if(A3!=5 'b00000) rf[A3] <= WD3;
		end
	end	

endmodule