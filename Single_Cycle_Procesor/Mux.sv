`timescale 1ns / 1ps
`default_nettype none

module Mux_2
(
	input wire[31:0] A, B,
	input wire SELECT, 
        output reg [31:0] RESULT
);

	assign RESULT = SELECT ? A : B;

endmodule

module Mux_2S
(
	input wire [4:0] A, B,
	input wire SELECT, 
	output reg [4:0] RESULT
);

	assign RESULT = SELECT ? A : B;

endmodule

module PC_Mux 
(
	input wire [31:0] JR, JAL, BEQ, NUL,
	input wire PC_JR, PC_JAL, PC_BEQ,
	output reg [31:0] RESULT
);

	always @(*) begin
		if(PC_JR) RESULT = JR;
		else if (PC_JAL) RESULT = JAL;
		else if (PC_BEQ) RESULT = BEQ;
		else RESULT = NUL;
	end	

endmodule
