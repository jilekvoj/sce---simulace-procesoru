interface if_out (input logic clk, input logic reset);
	logic [31:0] PC;
	logic mem_we;
	logic [31:0] mem_addr;
	logic [31:0] mem_data_in;
	modport dut (output PC, mem_we, mem_addr, mem_data_in);
endinterface

