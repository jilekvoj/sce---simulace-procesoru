`timescale 1ns / 1ps
`default_nettype none

module InstrMem
(
	input wire [31:0] addr,
	output wire [31:0] data_out
);

	reg [31:0] Mem [0:my_pkg::MEM_SIZE];

	assign data_out = Mem[addr % my_pkg::MEM_SIZE];

endmodule