`timescale 1ns / 1ps
`default_nettype none

module DataMem
(
	input wire CLK,
	input wire [31:0] addr,
	input wire [31:0] data_in,
	input wire WE,
	output wire [31:0] data_out
);

	reg [31:0] Mem [0:my_pkg::MEM_SIZE];

	assign data_out = Mem[addr % my_pkg::MEM_SIZE];

	always @(posedge CLK) begin
		if (WE == 1'b1) begin
			Mem[addr % my_pkg::MEM_SIZE] = data_in;
		end
	end

endmodule
