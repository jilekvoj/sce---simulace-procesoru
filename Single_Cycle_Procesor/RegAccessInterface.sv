interface if_reg_acc (input logic clk, input logic reset);
	logic [4:0] reg_addr;
	logic [31:0] reg_data_rd;
	logic [31:0] reg_data_wr;
	logic [31:0] PC_rd;
	logic [31:0] PC_wr;
	logic reg_rd_en;
	logic reg_wr_en;
	logic PC_rd_en;
	logic PC_wr_en;
	modport dut (output reg_data_rd, PC_rd, input reg_addr, reg_data_wr, PC_wr, reg_rd_en, reg_wr_en, PC_rd_en, PC_wr_en);
endinterface
