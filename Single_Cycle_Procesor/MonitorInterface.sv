interface if_mon (input logic clk, input logic reset);
	logic [31:0] PC;
	logic [31:0] data_to_mem;
	logic [31:0] instruction;
	logic tb_op;
	modport dut (output PC, data_to_mem, instruction);
endinterface

