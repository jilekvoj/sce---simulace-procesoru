`timescale 1ns / 1ps
`default_nettype none

module TopDUT
(
	input wire CLK, RESET,
	if_mon if_mon_inst,
	if_reg_acc if_reg_acc_inst
);

	wire [31:0] instruction, instr_addr, PC, mem_data_in, mem_data_out, mem_data_addr;
	wire mem_WE;	

	assign if_mon_inst.instruction = instruction;
	assign if_mon_inst.PC = PC;
	assign if_mon_inst.data_to_mem = mem_data_in;

	Processor Processor_inst (CLK, RESET, instruction, mem_data_out, PC, mem_WE, mem_data_addr, mem_data_in, if_reg_acc_inst);
	InstrMem InstrMem_inst (PC, instruction);
	DataMem DataMem_inst (CLK, mem_data_addr, mem_data_in, mem_WE, mem_data_out);

endmodule
