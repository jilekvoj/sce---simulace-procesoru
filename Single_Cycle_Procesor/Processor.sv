`timescale 1ns / 1ps
`default_nettype none

module Processor
(
	input wire CLK, RESET,
	input wire [31:0] instruction,
	input wire [31:0] mem_data_out,
	output wire [31:0] PC,
	output wire mem_we,
	output wire [31:0] mem_addr,
	output wire [31:0] mem_data_in,
	if_reg_acc if_reg_acc_inst
);

	wire [31:0] PC_AdrIn, SignImm, Reg_D2, Reg_D1, Reg_WD3, PC_Branch, PC_P4, Result, PC_JalAdr, ALU_SrcB, ShiftImm;
	wire [31:0] TB_Reg_WD3, TB_PC_AdrIn, TB_PC_AdrIn1;
	wire [4:0] Reg_A3, Reg_WriteAdr;
	wire [4:0] TB_Reg_A3, TB_Reg_A1;
	wire [3:0] ALU_Control;
	wire PC_beq, PC_jr, PC_jal, Reg_WE, Reg_Dst, ALU_Src, Zero, Mem_OE, Branch;
	wire TB_Reg_WE;

	assign PC_JalAdr = {PC_P4[31:28], instruction[25:0], 2 'b00};
	assign PC_beq = Zero & Branch;
	assign mem_data_in = Reg_D2;
	assign SignImm = {{17{instruction[15]}},instruction[14:0]};
	assign PC_P4 = PC + 4;
	assign ShiftImm = SignImm << 2;
	assign PC_Branch = PC_P4 + ShiftImm;

	// direct (frontdoor) access to regs from TB
	Mux_2S TB_Mux_A1(if_reg_acc_inst.reg_addr, instruction[25:21], if_reg_acc_inst.reg_rd_en, TB_Reg_A1);
	Mux_2S TB_Mux_A3(if_reg_acc_inst.reg_addr, Reg_A3, if_reg_acc_inst.reg_wr_en, TB_Reg_A3);
	Mux_2 TB_Mux_WD3(if_reg_acc_inst.reg_data_wr, Reg_WD3, if_reg_acc_inst.reg_wr_en, TB_Reg_WD3);
	Mux_2 TB_Mux_PC1(if_reg_acc_inst.PC_wr, PC_AdrIn, if_reg_acc_inst.PC_wr_en, TB_PC_AdrIn1);
	Mux_2 TB_Mux_PC(PC, TB_PC_AdrIn1, if_reg_acc_inst.reg_wr_en, TB_PC_AdrIn);
	assign if_reg_acc_inst.reg_data_rd = Reg_D1;
	assign if_reg_acc_inst.PC_rd = PC;
	assign TB_Reg_WE = if_reg_acc_inst.reg_wr_en | (Reg_WE & !if_reg_acc_inst.PC_wr_en);

	Registers GPR_Set(TB_Reg_A1, instruction[20:16], TB_Reg_A3, TB_Reg_WD3, CLK, TB_Reg_WE, Reg_D1, Reg_D2);
	Mux_2 ALU_Source(SignImm, Reg_D2, ALU_Src, ALU_SrcB);
	PC PC1(TB_PC_AdrIn, CLK, RESET, PC);
	Mux_2S Reg_Opt(instruction[15:11], instruction[20:16], Reg_Dst, Reg_WriteAdr);
	Mux_2 Result_Opt(mem_data_out, mem_addr, Mem_OE, Result);
	Mux_2S Jal_Opt1(5 'b11111, Reg_WriteAdr, PC_jal, Reg_A3);
	Mux_2 Jal_Opt2(PC_P4, Result, PC_jal, Reg_WD3);
	PC_Mux PC_Opt(Reg_D1, PC_JalAdr, PC_Branch, PC_P4, PC_jr, PC_jal, PC_beq, PC_AdrIn);
	ALU ALU1(Reg_D1, ALU_SrcB, ALU_Control, mem_addr, Zero);
	Driver CU(instruction[31:26],instruction[5:0], instruction[10:6], PC_jal, PC_jr, Reg_WE, Mem_OE, mem_we, ALU_Src, Reg_Dst, Branch, ALU_Control);

endmodule