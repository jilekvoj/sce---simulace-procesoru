interface if_in (input logic clk, input logic reset);
	logic [31:0] instruction;
	logic [31:0] mem_data_out;
	modport dut (input instruction, mem_data_out);
endinterface

