`timescale 1ns / 1ps
`default_nettype none

module PC
(
	input wire [31:0] ADDR_IN,
	input wire CLK,
	input wire RESET,
	output reg [31:0] ADDR_OUT);

	always @(*) begin
		if(RESET) ADDR_OUT <= 0;
	end	

	always @(posedge CLK) begin
		if(!RESET) ADDR_OUT <= ADDR_IN;
	end

endmodule
